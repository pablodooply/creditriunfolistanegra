<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol_user extends Model
{
    //
    protected $table      = 'rol_user';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'user_id', 'rol_id'
    ];

    protected $guarded = [
      'id'
     ];

}
