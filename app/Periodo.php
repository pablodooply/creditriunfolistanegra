<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    //
    protected $table      = 'periodo';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'nombre', 'descripcion', 'tiempo'
    ];

    protected $guarded = [
      'id'
     ];

     public function plan()
     {
       return $this->hasMany('App\Plan');
     }
}
