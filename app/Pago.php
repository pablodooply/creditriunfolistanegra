<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    //
    protected $table      = 'pago';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'descripcion', 'monto', 'fecha', 'prestamo_id', 'interes', 'mora', 'capital', 'total'
    ];

    protected $guarded = [
      'id'
     ];

     public function ficha_pago()
     {
         return $this->hasMany('App\Ficha_pago');
     }

     public function prestamo()
     {
         return $this->belongsTo('App\Prestamo');
     }
}
