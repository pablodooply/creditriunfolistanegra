<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    //
    protected $table = 'notificacion';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'descripcion', 'objeto_type', 'objeto_id', 'tipo'

    ];

    protected $guarded = [
      'id'
     ];

     public function objeto()
     {
         return $this->morphTo();
     }
}
