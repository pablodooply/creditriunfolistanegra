<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prueba extends Model
{
    //
    protected $table      = 'prueba';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'nombre'
    ];


    protected $guarded = [
      'id'
     ];

}
