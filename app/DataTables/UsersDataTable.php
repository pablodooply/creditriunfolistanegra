<?php

namespace App\DataTables;
use Yajra\DataTables\Services\DataTable;

use App\Prestamo;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables($this->query());
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
     public function query()
     {
         $query = Prestamo::with('cliente');
         return $this->applyScopes($query);
     }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
     public function html()
     {
         return $this->builder()
             ->columns([
                 'id',
                 'monto',
                 'pagado',
                 'clasificacion_id',
                 'updated_at',
             ])
             ->parameters([
                 'dom' => 'Bfrtip',
                 'buttons' => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
             ]);
     }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'email',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
