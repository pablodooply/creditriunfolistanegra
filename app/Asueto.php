<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asueto extends Model
{
    protected $table = 'asueto';

    protected $primaryKey = 'id';

    public    $timestamps = false;


    protected $guarded = [

      'id'

     ];
}
