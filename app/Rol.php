<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    //
    protected $table      = 'roles';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'nombre', 'descripcion'
    ];

    protected $guarded = [
      'id'
     ];

     public function users()
     {
         return $this
             ->belongsToMany('App\User')
             ->withTimestamps();
     }
}
