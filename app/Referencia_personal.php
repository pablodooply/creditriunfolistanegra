<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referencia_personal extends Model
{
    //
    protected $table      = 'referencia_personal';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'nombre', 'apellido', 'telefono', 'direccion', 'cliente_id'
    ];

    protected $guarded = [
      'id'
     ];

     public function cliente()
     {
         return $this->belongsTo('App\Cliente');
     }


}
