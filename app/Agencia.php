<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agencia extends Model
{
    //
    protected $table = 'agencia';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'acronimo', 'nombre', 'telefono', 'capital', 'direccion'
    ];

    protected $guarded = [
      'id'
     ];

     public function users()
     {
         return $this->hasMany('App\Users');
     }

}
