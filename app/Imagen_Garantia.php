<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Imagen_Garantia extends Model
{
  protected $table      = 'imagen_garantia';
  protected $primaryKey = 'id';
  public    $timestamps = true;

  protected $fillable = [
    'ruta', 'garantia_id', 'created_by', 'created_at', 'updated_at'
  ];

  protected $guarded = [
    'id'
   ];

   public function garantia()
   {
    return $this->belongsTo('App\Garantia');
   }
}
