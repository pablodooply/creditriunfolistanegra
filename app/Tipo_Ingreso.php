<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_Ingreso extends Model
{
  protected $table = 'tipo_ingreso';
  protected $primaryKey = 'id';
  public    $timestamps = true;
  protected $fillable = [
    'nombre', 'descripcion', 'created_by', 'created_at', 'updated_by', 'updated_at'
  ];
  protected $guarded = [
    'id'
  ];
}
