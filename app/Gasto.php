<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gasto extends Model
{
  protected $table = 'gasto';
  protected $primaryKey = 'id';
  public    $timestamps = true;
  protected $fillable = [
    'monto', 'cantidad', 'descripcion', 'foto_recibo', 'proveedor', 'tipo_gasto', 'colaborador', 'mes', 'created_by', 'created_at', 'updated_by', 'updated_at'
  ];
  protected $guarded = [
    'id'
  ];
}
