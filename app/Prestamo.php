<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Prestamo extends Model
{
    //
    protected $table      = 'prestamo';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'monto', 'capital_activo','user_id', 'cliente_id', 'plan_id', 'estado_p_id',
      'clasificacion_id', 'activo', 'pagado', 'fecha_fin', 'fecha_inicio', 'mora',
      'interes', 'capital_recuperado', 'observaciones', 'tipo', 'mora_recuperada',
      'fecha_desembolso', 'posible_primer_pago',  'foto_compromiso', 'foto_recibo', 'foto_solicitud', 'saldo', 'validacion_garantia'
    ];

    protected $guarded = [
      'id'
     ];


     public function contgarantias()
     {
       $prestamo = $this>garantia()->get();
       return count($prestamo);
     }

     public function user(){
       return $this->belongsTo('App\User');
     }

     public function cliente()
     {
         return $this->belongsTo('App\Cliente');
     }

     public function plan()
     {
         return $this->belongsTo('App\Plan');
     }

     public function ficha_pago()
     {
         return $this->hasMany('App\Ficha_pago');
     }

     public function garantia()
     {
         return $this->hasMany('App\Garantia');
     }

     public function estado_p()
     {
       return $this->belongsTo('App\Estado_p');
     }

     public function fichas_reales(){
       return $this->ficha_pago()->where('tipo', '=', 1)->orderBy('no_dia')->get();
     }

     public function no_pagados(){
       return $this->ficha_pago()->where('estado_p', '=', '2');
     }


     public function ruta()
     {
       return $this->hasMany('App\Ruta');
     }

     public function pagos()
     {
         return $this->hasMany('App\Pago');
     }

     public function ficha_actual()
     {
         return $this->ficha_pago->where('fecha', '=', Carbon::now()->toDateString());

     }

    public function notificacion()
     {
         return $this->morphMany('App\Notificacion', 'objeto');
     }

    public function activarRenovacion()
    {
        $dt = Carbon::today();
        $dtFin = Carbon::parse($this->fecha_fin);
        $diferencia = $dt->diffInDays($dtFin);
        $estado = $this->estado_p_id;

        if($diferencia <= 3 && $estado == 5){
          return true;
        }else{
          return false;
        }
    }

//***************************SCOPES****************************************
/**
 * Los scopes
 *
 * @param \Illuminate\Database\Eloquent\Builder $query
 * @return \Illuminate\Database\Eloquent\Builder
 */
      public function scopeTerminar($query, $fecha_inicio, $fecha_fin)
      {
        return $query->wherebetween('fecha_fin', [$fecha_inicio, $fecha_fin]);
      }
//*******************Metodos de para obtener html*************************
     public function obtenerClasificacion()
     {
         $clasi = "";
         switch ($this->clasificacion_id) {
           case 0:
             $clasi = "<td><a><span class='badge badge-success pull-center'>A</span></a></td>";
           break;
           case 1:
             $clasi = "<td><a><span class='badge badge-primary pull-center'>A</span></a></td>";
           break;
           case 2:
             $clasi = "<td><a><span class='badge badge-warning pull-center'>B</span></a></td>";
           break;
           case 3:
             $clasi = "<td><a><span class='badge badge-danger pull-center'>C</span></a></td>";
           break;
         }
         return $clasi;
     }

     public function obtenerClasificacion2(){
       $clasi = "";
       switch ($this->clasificacion_id) {
         case 0:
           $clasi = "<p class='text-right'><span class='badge badge-success'>A</span></p>";

         break;
         case 1:
           $clasi = "<p class='text-right'><span class='badge badge-primary'>A</span></p>";
         break;
         case 2:
           $clasi = "<p class='text-right'><span class='badge badge-warning'>B</span></p>";

         break;
         case 3:
           $clasi = "<p class='text-right'><span class='badge badge-danger'>C</span></p>";
         break;
       }
       return $clasi;

     }

     public function obtenerEstado()
     {
         $clasi = "";
         switch ($this->estado_p_id) {
           case 1:
             $clasi = "<td><a><span class='badge badge-warning pull-center'>Pendiente</span></a></td>";
           break;
           case 2:
             $clasi = "<td><a><span class='badge badge-info pull-center'>Examinada</span></a></td>";
           break;
           case 3:
             $clasi = "<td><a><span class='badge badge-info pull-center'>Aprobada</span></a></td>";
           break;
           case 5:
             $clasi = "<td><a><span class='badge badge-primary pull-center'>Activo</span></a></td>";
           break;
           case 9:
             $clasi = "<td><a><span class='badge badge-info pull-center'>Cancelado</span></a></td>";
           break;
           case 10:
             $clasi = "<td><a><span class='badge badge-danger pull-center'>Morosa</span></a></td>";
           break;
           case 11:
             $clasi = "<td><a><span class='badge badge-danger pull-center'>Rechazado</span></a></td>";
           break;
           default:
           $clasi = "<td><a><span class='badge badge-warning pull-center'>Pendiente</span></a></td>";
           break;
         }
         return $clasi;
     }

     public function obtenerEstado2()
     {
         $clasi = "";
         switch ($this->estado_p_id) {
           case 1:
             $clasi = "<a><span class='badge badge-warning pull-center'>Pendiente</span></a>";
           break;
           case 2:
             $clasi = "<a><span class='badge badge-info pull-center'>Examinada</span></a>";
           break;
           case 3:
             $clasi = "<a><span class='badge badge-info pull-center'>Aprobada</span></a>";
           break;
           case 5:
             $clasi = "<a><span class='badge badge-primary pull-center'>Activo</span></a>";
           break;
           case 9:
             $clasi = "<a><span class='badge badge-info pull-center'>Cancelado</span></a>";
           break;
           case 5:
             $clasi = "<a><span class='badge badge-danger pull-center'>Morosa</span></a>";
           break;
           case 11:
             $clasi = "<td><a><span class='badge badge-danger pull-center'>Rechazado</span></a></td>";
           break;
           default:
           $clasi = "<a><span class='badge badge-warning pull-center'>Pendiente</span></a>";
           break;
         }
         return $clasi;
     }
}
