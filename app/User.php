<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'persona_id', 'agencia_id','sueldo_base','comision_capital_activo',
        'comision_cliente_nuevo', 'combustible', 'descuento_mora','otros', 'estado'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }

    public function prestamos() //Prestamos colocados
    {
      return $this->hasMany('App\Prestamo');
    }

    public function pagos_para_mora($inicio = null, $fin = null){
      $inicio = $inicio ? $inicio : Carbon::now()->startOfMonth()->format('Y-m-d');
      $fin    = $fin ? $fin : Carbon::now()->endOfMonth()->format('Y-m-d');
      return $this->hoja_ruta()->with(['prestamos.pagos' => function($query) use($inicio,$fin){
        return $query->wherebetween('fecha', [$inicio, $fin]);
      }])->get();
    }

    public function fichas_para_mora($inicio = null, $fin = null){
      $inicio = $inicio ? $inicio : Carbon::now()->startOfMonth()->format('Y-m-d');
      $fin    = $fin ? $fin : Carbon::now()->endOfMonth()->format('Y-m-d');
      return $this->hoja_ruta()->with(['prestamos.ficha_pago' => function($query) use($inicio,$fin){
        return $query->wherebetween('fecha', [$inicio, $fin]);
      }])->get();
    }

    public function tcapital($inicio = null, $fin = null){
      $inicio = $inicio ? $inicio : Carbon::now()->startOfMonth()->format('Y-m-d');
      $fin    = $fin ? $fin : Carbon::now()->endOfMonth()->format('Y-m-d');
      $temp = new Collection();
      return  $this->hoja_ruta()->with(['prestamos.pagos' => function($query) use($inicio,$fin){
        return $query->wherebetween('fecha', [$inicio, $fin]);
      }])->get()->sum(function($query){
        return $query->prestamos->sum(function($query){
          return $query->pagos->sum('capital');
        });
      });
    }

    public function tinteres($inicio = null, $fin = null){
      $inicio = $inicio ? $inicio : Carbon::now()->startOfMonth()->format('Y-m-d');
      $fin    = $fin ? $fin : Carbon::now()->endOfMonth()->format('Y-m-d');
      $temp = new Collection();
      return  $this->hoja_ruta()->with(['prestamos.pagos' => function($query) use($inicio,$fin){
        return $query->wherebetween('fecha', [$inicio, $fin]);
      }])->get()->sum(function($query){
        return $query->prestamos->sum(function($query){
          return $query->pagos->sum('interes');
        });
      });
    }

    public function capital_total($inicio = null, $fin = null){
      return $this->tcapital($inicio, $fin) + $this->tinteres($inicio, $fin);
    }


    public function mora_recuperada($inicio = null, $fin = null){
      return $this->pagos_para_mora($inicio, $fin)->sum(function($query){
        return $query->prestamos->sum(function($query){
          return $query->pagos->sum('mora');
        });
      });
    }
    
    public function mora_pendiente($inicio = null, $fin = null){
      $mora_r = $this->mora_recuperada($inicio, $fin);
      $mora_a = $this->mora_aumentada($inicio, $fin);

      if($mora_r > $mora_a){
        $mora_pendiente = $mora_r - $mora_a;
      }else{
        $mora_pendiente = $mora_a - $mora_r;
      }
      return $mora_pendiente;
    }    

    public function mora_aumentada($inicio = null, $fin = null){
      return $this->fichas_para_mora($inicio, $fin)->sum(function($query){
        return $query->prestamos->sum(function($query){
          return $query->ficha_pago->sum('ajuste');
        });
      });
    }

    public function hoja_ruta()
    {
      return $this->hasMany('App\Hoja_ruta');
    }
    
    public function hoja_ruta_supervisor()
    {
      return $this->hasMany('App\Hoja_ruta', 'supervisor_id');
    }

    public function hoja_supervisor(){
      $hojas = collect();
      $hojas = $hojas->merge(Hoja_ruta::where('user_id', $this->id)->get());
      $hojas = $hojas->merge(Hoja_ruta::where('supervisor_id', $this->id)->get());
      return $hojas;
    }

    public function rutas_supervisor(){
        return $this->hasManyThrough('App\Ruta', 'App\Hoja_ruta', 'supervisor_id', 'hoja_ruta_id')->with('prestamo');
    }

    public function rutas_supervisor_asignadas(){
        return $this->hasManyThrough('App\Ruta', 'App\Hoja_ruta', 'user_id', 'hoja_ruta_id')->with('prestamo');
    }

    public function rutas_completas_supervisor(){
        $rutas = collect();
        $rutas = $rutas->merge($this->rutas_supervisor()->get());
        $rutas = $rutas->merge($this->rutas_supervisor_asignadas()->get());
        return $rutas;
    }
    public function timeline_gps()
    {
      return $this->hasMany('App\Timeline_gps');
    }

    public function boleta_pago()
    {
      return $this->hasMany('App\Boleta_pago','users_id');
    }

    public function obtenerEdad()
    {
        return Carbon::parse($this->persona->fecha_nacimiento)->age;
    }

    public function roles()
    {
        return $this->belongsToMany('App\Rol', 'rol_user', 'user_id', 'rol_id')->withTimestamps();
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }
    public function hasRole($role)
    {
        if ($this->roles()->where('nombre', $role)->first()) {
            return true;
        }
        return false;
    }
}
