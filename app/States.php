<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    //
    //
    protected $table = 'states';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $guarded = [
      'id'
     ];

     protected $fillable = [
       'name'
     ];

     public function cities(){
       return $this->hasMany('App\Cities', 'state_id');
     }
}
