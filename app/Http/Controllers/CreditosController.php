<?php
namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables as Datatable;
use Alert;
use App\Agencia;
use App\Cliente;
use App\Prestamo;
use App\Plan;
use App\User;
use App\Hoja_ruta;
use App\Listabn;
use App\Clasificacion;
use App\Estado_p;
use App\Ruta;
use App\Location;
use App\Referencia_personal;
use App\Persona;
use App\Periodo;
use App\Ficha_pago;
use App\Notificacion;
use App\Fechas_especiales;
use App\Rol;
use App\Pago;
use App\States;
use App\Garantia;
use App\Imagen_Garantia;
use DB;
use Image;
use DateTime;
class CreditosController extends Controller
{
    public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('revalidate', ['only' => ['index', 'create', 'show','edit','entregados','finalizar','renovar','planes','']]);
//       $this->middleware('pendientes', ['only' => ['show', function ($id) {
//
// }]]);
      // $this->middleware('subscribed', ['except' => ['fooAction', 'barAction']]);
  }
    /**
     * Muesta la informacion de los creditos, en este caso los creditos se van a dividir por
     * cada estado que tengan, [Pendiente, Activo, Mora, Vencido, Aprobados]
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prestamos = Prestamo::where('estado_p_id', 5)->get();
        foreach ($prestamos as $credito) {
          if(($credito->monto - ($credito->interes + $credito->capital_recuperado)) == 0)
          {
            $credito->update([
              'estado_p_id' => 9
            ]);
          }
        }
        $user = Auth::user();
        //Listado de todos los creditos si es administrador o secretaria.
        if($user->hasAnyRole(['Administrador', 'Secretaria'])){
          // dd('hola');
          $prestamos = Prestamo::with('cliente.persona', 'user')->orderBy('id', 'desc')->get();
// dd($prestamos);
          // dd($prestamos);
          $creditos_activos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->whereIn('estado_p_id',[3,5])
                                ->get();
          $creditos_pendientes = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->whereIn('estado_p_id',[1,2])
                                ->get();
          $creditos_vencidos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->where('prestamo.estado_p_id','=',9)
                                ->get();
          $creditos_aprobado_hoy = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->where('prestamo.estado_p_id','=',3)
                                ->where('prestamo.fecha_desembolso', Carbon::now()->format('Y-m-d'))
                                ->get();
          $creditos_vencidos_mora = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->where('prestamo.estado_p_id','=',10)
                                ->get();
          $todos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->whereIn('estado_p_id',[1,2,3,4,5,9,10,11])
                                ->get();
                                // dd($creditos_activos->first());
          $rutas=Hoja_ruta::all();
          return view('Creditos.index2', compact('prestamos', 'creditos_activos', 'creditos_vencidos', 'creditos_vencidos_mora','creditos_pendientes', 'creditos_aprobado_hoy','rutas'));
        } //Se filtra por las rutas asignadas al promotor, y se divide en 3 estados
          //Activos, vencidos y vencidos en mora.
          else if($user->hasRole('Promotor')){
            $user1 = $user;
            $user2 = $user;
            $user3 = $user;
            $user4 = $user;
            $creditos_activos = collect();
            $creditos_vencidos = collect();
            $creditos_vencidos_mora = collect();
            $todos = collect();
            $creditos_activos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                  ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                  ->select()
                                  ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                  ->whereIn('estado_p_id',[3,5])
                                  ->get();
            $creditos_vencidos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                  ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                  ->select()
                                  ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                  ->where('prestamo.estado_p_id','=',9)
                                  ->get();
            $creditos_vencidos_mora = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                  ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                  ->select()
                                  ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                  ->where('prestamo.estado_p_id','=',10)
                                  ->get();
            $todos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                  ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                  ->select()
                                  ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                  ->whereIn('estado_p_id',[1,2,3,4,5,9,10,11])
                                  ->get();
            // dd($todos);
            $rutas=Hoja_ruta::all();
            return view('Creditos.index2', compact('creditos_activos', 'creditos_vencidos', 'creditos_vencidos_mora', 'todos', 'rutas'));
          } else if($user->hasRole('Supervisor')){ //Si es supervisor se filtra por las rutas asignadas y supervisadas.
              $rutas = $user->rutas_completas_supervisor();
              $array = array();
              foreach($rutas as $ruta){
                $array[] = $ruta->prestamo;
              }
              $prestamos = collect($array);
              $creditos_activos = $prestamos->whereIn('estado_p_id',[3,4,5]);
              $creditos_pendientes = $prestamos->whereIn('estado_p_id',[1,2]);
              $creditos_vencidos = $prestamos->where('estado_p_id', '=', 9);
              $creditos_aprobado_hoy = $prestamos->where('estado_p_id','=',3)->where('fecha_desembolso', Carbon::now()->format('Y-m-d'));
              $creditos_vencidos_mora = $prestamos->where('estado_p_id', '=', 10);
              $todos=$prestamos->whereIn('estado_p_id',[0,1,2,3,5,9,10,11]);
              $rutas=Hoja_ruta::all();
              return view('Creditos.index2', compact('todos', 'creditos_activos', 'creditos_vencidos', 'creditos_vencidos_mora','creditos_pendientes', 'creditos_aprobado_hoy', 'rutas'));
          }
    }
    /**
    * Tabla que se accede por medio de Ajax, al igual que la funcion anterior devuelve todos
    * los prestamos divido por estados.
    * @param Request peticion con la informacion
    * @return DataTable devulve un datatable para poder mostrar los datos obtenidos y filtrados.
    */
    public function tableCreditos(Request $request){
      set_time_limit(0);
      $tipo = $request->get('tipo');
      $user = Auth::user();
      // $rolUser = $user->role;
      //Listado de creditos Activos
      $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
      $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
      if($user->hasAnyRole(['Administrador', 'Secretaria']))
      {
        $prestamos = Prestamo::with('cliente.persona', 'user')->get();
        $creditos_activos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                              ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                              ->select()
                              ->whereIn('estado_p_id',[3,5])
                            //   ->whereBetween('prestamo.fecha_desembolso',[$start, $end])
                              ->get();
        $creditos_pendientes = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                              ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                              ->select()
                              ->whereIn('estado_p_id',[1,2])
                            //   ->whereBetween('prestamo.fecha_desembolso',[$start, $end])
                              ->get();
        $creditos_vencidos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                              ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                              ->select()
                              ->where('prestamo.estado_p_id','=',9)
                            //   ->whereBetween('prestamo.fecha_desembolso',[$start, $end])
                              ->get();
        $creditos_aprobado_hoy = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                              ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                              ->select()
                              ->where('prestamo.estado_p_id','=',3)
                            //   ->where('prestamo.fecha_desembolso', Carbon::now()->format('Y-m-d'))
                              ->get();
        $creditos_vencidos_mora = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                              ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                              ->select()
                              ->where('prestamo.estado_p_id','=',10)
                            //   ->whereBetween('prestamo.fecha_desembolso',[$start, $end])
                              ->get();
        $todos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                              ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                              ->select()
                              ->whereIn('estado_p_id',[1,2,3,4,5,9,10,11,15])
                            //   ->whereBetween('prestamo.fecha_desembolso',[$start, $end])
                              ->get();
      }
      else if($user->hasRole('Promotor'))
      {
          $user1 = $user;
          $user2 = $user;
          $user3 = $user;
          $user4=$user;
          $creditos_activos = collect();
          $creditos_vencidos = collect();
          $creditos_vencidos_mora = collect();
          $todos=collect();
          $creditos_activos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                ->whereIn('estado_p_id',[3,5])
                                ->get();
          $creditos_vencidos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                ->where('prestamo.estado_p_id','=',9)
                                ->get();
          $creditos_vencidos_mora = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                ->where('prestamo.estado_p_id','=',10)
                                ->get();
          $todos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                ->whereIn('estado_p_id',[1,2,3,4,5,9,10,11])
                                ->get();
        }
        else if($user->hasRole('Supervisor'))
        {
            $rutas = $user->rutas_completas_supervisor();
            $array = array();
            foreach($rutas as $ruta){
              $array[] = $ruta->prestamo;
            }
            $prestamos = collect($array);
            $creditos_activos = $prestamos->whereIn('estado_p_id',[3,4,5]);
            $creditos_pendientes = $prestamos->whereIn('estado_p_id',[1,2]);
            $creditos_vencidos = $prestamos->where('estado_p_id', '=', 9);
            $creditos_aprobado_hoy = $prestamos->where('estado_p_id','=',3)->where('fecha_desembolso', Carbon::now()->format('Y-m-d'));
            $creditos_vencidos_mora = $prestamos->where('estado_p_id', '=', 10);
            $todos=$prestamos->whereIn('estado_p_id',[1,2,3,4,5,9,10,11]);
            // dd($todos);0,1,2,3,5,9,10,11
        }
      switch($tipo){
        case 1:
          $creditos = $creditos_activos;
        break;
        case 2:
          $creditos = $creditos_pendientes;
        break;
        case 3:
          $creditos = $creditos_vencidos;
        break;
        case 4:
          $creditos = $creditos_aprobado_hoy;
        break;
        case 5:
          $creditos = $creditos_vencidos_mora;
        break;
        case 6:
          $creditos = $todos;
        break;
      }
      return DataTable::of($creditos)
      ->addColumn('Codigo', function($credito){
        return '<a class="client-link" value="'.$credito->prestamo_id.'">Cre-'. $credito->prestamo_id .'</a>';
      })
      ->addColumn('Monto', function($credito){
        return  $credito->monto;
      })
      ->addColumn('Estado', function($credito){
        return $credito->obtenerEstado();
      })
      ->addColumn('Clasificacion', function($credito){
        return $credito->obtenerClasificacion();
      })
      ->addColumn('Fecha', function($credito){
        return $credito->fecha_desembolso;
      })
      ->addColumn('Tipo', function($credito){
        $cuenta=Prestamo::where('cliente_id',$credito->cliente_id)->count();
        if($credito->estado_p_id==11 || $credito->estado_p_id==9)
        {
          return '<a><span class="badge badge-danger pull-center">Innactivo</span></a>';
        }
        else {
          if($cuenta>1)
          {
            if($credito->posible_primer_pago>Carbon::now()->subDays(7))
            {
              return '<a><span class="badge badge-basic pull-center">Renovado</span></a>';
            }
            else {
              return '<a><span class="badge badge-success pull-center">Activo</span></a>';
            }
          }
          else {
            return '<a><span class="badge badge-primary pull-center">Nuevo</span></a>';
          }
        }
      })
      ->addColumn('Plan', function($credito){
        return $credito->plan->nombre;
      })
      ->addColumn('Ruta', function($credito){
        $id=$credito->id;
        $rutaActual=Hoja_ruta::whereHas('ruta.prestamo', function($query) use($credito){
          $query->where('id', $credito->prestamo_id);
        })->first();
        return $rutaActual->nombre;
      })
      ->addColumn('Cliente', function($credito){
        return $credito->cliente->persona->nombre . " " . $credito->cliente->persona->apellido;
      })
      ->addColumn('Acciones', function($credito){
        $ver = '';
        if ($credito->estado_p_id == 1 || $credito->estado_p_id == 2) {
          $ver = '<a href="#" class="btn btn-outline btn-success btn-sm" disabled><i class="fa fa-eye"></i></a>' . '<a id="" value="" class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"><strong>Q</strong></i></a>';
        }
        else {
          $ver = '<a href="' . route('creditos.show', $credito->prestamo_id) . '" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>' . '<a id="pagar-prestamo" value="'.$credito->prestamo_id.'" class="btn btn-outline btn-primary btn-sm"><i class="fa"><strong>Q</strong></i></a>';
        }
        if(Auth::user()->hasRole('Administrador')){
          if($credito->estado_p_id==3 || $credito->estado_p_id==5 || $credito->estado_p_id==10)
          {
          return $ver .
                 '<a value="'. $credito->prestamo_id .'" onclick="eliminarcredito('. $credito->prestamo_id .')" class="btn btn-outline btn-danger btn-sm eliminarcredito"><i class="fa fa-trash"></i></a>'. '<a id="cambiar-cartera" value="'.$credito->prestamo_id.'" class="btn btn-outline btn-warning btn-sm"><i class="fa fa-exchange"><strong></strong></i></a>';
               }
               else {
                 return $ver .
                        '<a value="'. $credito->prestamo_id .'" onclick="eliminarcredito('. $credito->prestamo_id .')" class="btn btn-outline btn-danger btn-sm eliminarcredito"><i class="fa fa-trash"></i></a>';
               }
             }
         else{
          return $ver;
         }
      })
      // ->orderColumn('id', '-id $1')
      ->escapeColumns([])
      ->make(true);
    }
    public function verificarEdad(Request $request)
    {
    $fecha_nacimiento = $request->get('fecha_nacimiento');
    //convertimos la fecha 1 a objeto Carbon
    $fecha_hoy = \Carbon\Carbon::now();
    //de esta manera sacamos la diferencia en minutos
    $anios=$fecha_hoy->diffInYears($fecha_nacimiento);
    //dd($anios);
  //Encontrar que exista el registro, de otra forma se procede el registro
    if($anios>=18){
      return "true";
    }
    else{
      return "false";
      }
    }
    /**
     * Muestra el formulario para la creacion de credito, y una advertencia si falta algun
     * dato que pueda poner mal su funcionamiento.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Formulario para solicitud de credito
        $usuarios = User::all();
        $planes = Plan::all();
        $hoja_ruta = Hoja_ruta::all();
        $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
        $states = States::all();
        $promotores = collect();
        foreach ($roles as $rol) {
          $promotores = $promotores->merge($rol->users);
        }
        $mensaje = "";
        if($planes->isEmpty() || $hoja_ruta->isEmpty() || $promotores->isEmpty()){
          if($planes->isEmpty() && $hoja_ruta->isEmpty() && $promotores->isEmpty()){
            $mensaje = "No existe planes, rutas, ni promotores para poder registrar un credito, se recomienda que cree por lo menos un plan, una ruta y un promotor";
          } else if($planes->isEmpty() && $hoja_ruta->isEmpty()){ //Esta registrado un promotor
            $mensaje = "No existe planes, ni rutas, para poder registrar un credito, se recomienda que se cree por lo menos un plan, una ruta";
          } else if($hoja_ruta->isEmpty() && $promotores->isEmpty()){
            $mensaje = "No existen rutas, ni promotores, para poder registrar un credito, se recomienda que se cree por lo menos una ruta y un promotor";
          } else if($hoja_ruta->isEmpty()){
            $mensaje = "No existen rutas, para poder registrar un credito, se recomienda que se cree por lo menos una ruta";
          } else if($ruta->isEmpty()){
            $mensaje = "No existen planes, para poder registrar un credito, se recomienda que se cree por lo menos un plan";
          }
          Alert::warning($mensaje,'Advertencia')->persistent("Close this");
        }
        return view('Creditos.create', compact('usuarios','planes', 'hoja_ruta','promotores','states'));
    }
    /**
     * Crea el respectivo credito con la informacion proporcionada
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request);
      if($this->verificarDPI($request) == "true"){
        $mensaje = "Ya no se puede crear el prestamo, porque el DPI ya esta en uso";
        Alert::warning($mensaje,'Advertencia')->persistent("Close this");
        return redirect()->route('creditos.create');
      }
      $idPlan = $request->input('monto');
      $plan = Plan::find($idPlan);
          // Creacion de la locacion cliente
          $locat = Location::firstOrCreate([
            'countries_id' => 1,
            'states_id' => $request->input('state'),
            'cities_id' => $request->input('city'),
          ]);
          // Creacion del perfil del cliente
          $personaNew = Persona::create([
            'nombre' => $request->input('nombre'),
            'apellido' => $request->input('apellido'),
            'dpi' => $request->input('dpi'),
            'nit' => $request->input('nit'),
            'genero' => $request->input('genero'),
            'fecha_nacimiento' => $request->input('fecha_nacimiento'),
            'telefono' => $request->input('telefono'),
            'celular1' => $request->input('celular1'),
            'celular2' => $request->input('celular2'),
            //'no_hijos'  => $request->input('no_hijos') == "" ?  0 : $request->input('no_hijos'),
            'domicilio' => $request->input('direccion'),
            'location_id' => $locat->id,
            'tipo'        => "Cliente",
          ]);
          $destinoPath = public_path('//images//clientes//documentos');
          $nombre_imagen_dpi = "";
          $nombre_imagen_recibo = "";
          $nombre_imagen_firma = "";
          if($request->file('foto_dpi') == null){
          } else{
          $imagen_dpi = $request->file('foto_dpi');
          $nombre_imagen_dpi  = $request->input('nombre'). 'DPI' . time() . '.' . $imagen_dpi->getClientOriginalExtension();
          $imagen_dpi->move($destinoPath, $nombre_imagen_dpi);
          }
          if($request->file('foto_recibo') == null){
          } else{
          $imagen_recibo = $request->file('foto_recibo');
          $nombre_imagen_recibo  = $request->input('nombre') . 'RECIBO' .time() . '.' . $imagen_recibo->getClientOriginalExtension();
          $imagen_recibo->move($destinoPath, $nombre_imagen_recibo);
          }
          if($request->file('foto_firma') == null){
          } else{
          $imagen_firma = $request->file('foto_firma');
          $nombre_imagen_firma  = $request->input('nombre') . 'RECIBO' .time() . '.' . $imagen_firma->getClientOriginalExtension();
          $imagen_firma->move($destinoPath, $nombre_imagen_firma);
          }
          // Creacion de cliente
          $clienteNew = Cliente::create([
            'fecha_ingreso' => Carbon::now(),
            'empresa_trabajo' => $request->input('empresa_trabajo'),
            'direccion_trabajo' => $request->input('direccion_trabajo'),
            'direccion_recibo' => $request->input('direccion_luz'),
            'nombre_recibo' => $request->input('recibo_luz'),
            'tipo_casa' => $request->input('tipo_casa'),
            'foto_dpi'  => $nombre_imagen_dpi,
            // 'foto_recibo' => $nombre_imagen_recibo,
            // 'foto_firma'  => $request->imagen_firma,
            'actividad'   => $request->input('actividad'),
            'observaciones' => $request->input('observaciones'),
            'telefono_empresa'  => $request->input('telefono_empresa'),
            'tiempo_trabajando' => $request->input('tiempo_trabajando'),
            'estado_civil' => $request->input('estado_civil'),
            "listabn_id"  => 1,
            'salario' => $request->input('salario') ? $request->input('salario') : 0 ,
            'no_hijos'  => $request->input('no_hijos') == "" ?  0 : $request->input('no_hijos'),
            'direccion_cobrar' => $request->input('direccion_cobrar'),
            'nacionalidad'  => $request->input('nacionalidad'),
            'persona_id'=> $personaNew -> id,
          ]);
          //Creacion de referencias
          for ($i=0; $i < 2; $i++) {
            $num = (string) (1+$i);
            $referencia = Referencia_personal::create([
              'nombre' => $request->input('nombre_r' . $num),
              'telefono' => $request->input('telefono_r'.$num),
              'direccion' => $request->input('direccion_r'.$num),
              'cliente_id' => $clienteNew->id,
            ]);
          }
          $idPlan = $request->input('monto');
          $plan = Plan::find($idPlan);
          $usuario = User::find($request->input('promotor'));
          $prestamoNew = Prestamo::create([
            'monto' => $plan->total,
            'capital_activo' =>$plan->total - $plan->interes,
            'user_id' => $usuario->id,
            'cliente_id' => $clienteNew->id,
            'plan_id' => $idPlan,
            'validacion_garantia' => ($request->has('validacion_garantia')) ? 1 : 0,
            'estado_p_id' => ($request->get('validacion_garantia') == 1) ? 15 : Estado_p::find(1)->id,
            'tipo' => 0,
            'foto_recibo' => $nombre_imagen_recibo,
            'foto_solicitud' => $nombre_imagen_firma,
            'fecha_desembolso' => $request->input('fecha_desembolso'),
            'fecha_inicio' => $request->input('fecha_desembolso'),
            'posible_primer_pago' => $request->input('fecha_inicio'),
          ]);
          $rutaNew = Ruta::create([
            'prestamo_id'=> $prestamoNew->id,
            'hoja_ruta_id' => Hoja_ruta::find($request->input('ruta'))->id,
            'hora'         => $request->input('hora'),
          ]);
          //Creacion de garantias
      if($request->get('validacion_garantia') == 1)
      {
        $nombre_garantia          = $request->nombre_garantia;
        $cantidad_garantia        = $request->cantidad_garantia;
        $valoracion_garantia      = $request->valoracion_garantia;
        $estado_producto_garantia = $request->estado_producto_garantia;
        $descripcion_garantia     = $request->descripcion_garantia;
        $categoria_garantia       = $request->categoria_garantia_id;
        // $imagen_uno_garantia      = $request->file('imagen_uno');
        // $imagen_dos_garantia      = $request->file('imagen_dos');
        // $originalPath = public_path().'/images/';
        for($count = 0; $count < count($nombre_garantia); $count++) {
          // $file=$request->{"photos".($count+1)}[$count]->save($originalPath);
          // $img = \Image::make($file);
          // ${"imagenes_garantia" . ($count+1)} =$request->{"*photos" . ($count+1)};
          // dump(${"imagenes_garantia" . ($count+1)});
          // $thumbnailImagedos = Image::make($imagen_dos_garantia[$count]);
          // $thumbnailImagedos->save($originalPath.time().$imagen_dos_garantia[$count]->getClientOriginalName());
          $garantiaNew = Garantia::create([
            'nombre' => $nombre_garantia[$count],
            'cantidad' => $cantidad_garantia[$count],
            'valoracion_cliente' => $valoracion_garantia[$count],
            'estado_producto_cliente' => $estado_producto_garantia[$count],
            'descripcion' => $descripcion_garantia[$count],
            'categoria_id' => $categoria_garantia[$count],
            'created_by' => Auth::user()->id,
            'prestamo_id' => $prestamoNew->id
            ]);
            $ind= 0;
          foreach ($request->{"photos".($count+1)} as $key)
          {
            $file = $request->{"photos".($count+1)}[$ind];
            $file->move(public_path().'/images/clientes/garantias/', $file->getClientOriginalName());
            // $request->{"photos".($count+1)}[$count]->move(base_path('//public'), $request->{"photos".($count+1)}[$count]->getClientOriginalName());
            // $request->{'photos'.($count+1)}[$count]->move(public_path().'//images//clientes//garantias//', $request->{"photos".($count+1)}[$count]->getClientOriginalName());
            $imagen_GarantiaNew= Imagen_Garantia::create([
              'garantia_id' => $garantiaNew->id,
              'ruta' =>'/images/clientes/garantias/'.$file->getClientOriginalName(),
              'created_by' => Auth::user()->id

            ]);
            $ind++;
          }
          // $insert_data[] = $data;
        }
        // if(!empty($insert_data)){
        // Garantia::insert($insert_data);
        // }
      }
          //termina creacion garantia
          $notificacion = Notificacion::create([
            'descripcion'   => 'Nuevo',
            'objeto_id'     => $prestamoNew->id,
            'objeto_type'   => 'Prestamo',
          ]);
          // $creditos = Prestamo::with('cliente.persona', 'user')->get();
          // return view('Creditos.index2', compact('creditos'));
          return redirect()->route('clientes.index');
    }
    /**
     * Muestra la informacion del cliente, pero identifica si es una peticion normal
     * o Ajax, si es peticion normal devulve la vista correspondiente que va a mostrar
     * la informacion, si no es normal va a devolver html con la informacion del cliente
     * para mostrar en la vista previa.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function refreshClasi($prestamo)
     {
// dd($prestamo);
       $montototal = $prestamo->monto;
       $pagosefectuados = Ficha_pago::where("prestamo_id",$prestamo->id)->where('estado', 1)->get();
       $interestotal = $pagosefectuados->sum('interes');
       $capitalretotal = $pagosefectuados->sum('capital');
       $saldototal = $prestamo->monto - ($prestamo->interes + $prestamo->capital_recuperado);
       // dd($saldototal);
       if($prestamo->estado_p_id == 9 && $saldototal>0)
       {
         $prestamo->update([
           'estado_p_id' => 5,
         ]);
       }
       //PLAN DIARIO
       if($prestamo->plan->nombre == "Diario")
       {
          $ficha_no_pagadas = $prestamo->ficha_pago->where('cont',1)->count();
          // dd($ficha_no_pagadas);
          $estado = 1;
          if($ficha_no_pagadas == 0){
            $estado = 1;
          }
          if($ficha_no_pagadas == 1 || $ficha_no_pagadas == 2){
            $estado = 2;
          }
          if($ficha_no_pagadas >= 3){
            $estado = 3;
          }
          $prestamo->update([
            'clasificacion_id'  => $estado,
          ]);
          // dd($prestamo);
        }
        //PLAN SEMANAL
        elseif($prestamo->plan->nombre == "Semanal")
        {
          $ficha_no_pagadas = $prestamo->ficha_pago->where('ajuste', '>', 0)->count();
          // $ficha_no_pagadas_group = $prestamo->ficha_pago->where('cont',1)->count()->groupBy('no_dia');
          $ficha_no_pagadas_group = $prestamo->ficha_pago->where('estado_p', 2)->where('cont',1)->count();
          // dd($ficha_no_pagadas_group);
          $estado = 1;
          if($ficha_no_pagadas == 0){
            $estado = 1;
          }
          elseif($ficha_no_pagadas == 1 || $ficha_no_pagadas == 2){
            $estado = 2;
          }
          elseif($ficha_no_pagadas >= 3){
            $estado = 3;
          }
          $prestamo->update([
            'clasificacion_id'  => $estado,
          ]);
        }
        //PLAN QUINCENA
        elseif($prestamo->plan->nombre == "Quincena")
        {
          if (!is_null($prestamo->ficha_pago->first())) {
            // code...
          $ficha_no_pagadas = $prestamo->ficha_pago->where('cont',1)->count();
          $ficha_siete = $prestamo->ficha_pago->where('id', $prestamo->ficha_pago->first()->id + 7)->first();
          // dd($ficha_siete);
          $estado = 1;
          if($ficha_no_pagadas == 0){
            $estado = 1;
          }
          if($ficha_no_pagadas >= 1){
            if($ficha_siete->estado_p ==2 || $ficha_siete->estado_p ==3)
            {
              $estado = 3;
            }
            else {
              $estado = 2;
            }
          }
          $prestamo->update([
            'clasificacion_id'  => $estado,
          ]);
          // dd($prestamo);
          }
        }
      }
    public function show(Request $request,$id)
    {
      //dd($id);
      $cred = Prestamo::find($id);

      if( $cred->estado_p_id == 1 || $cred->estado_p_id == 2 || $cred->estado_p_id == 15)
      {
        $prestamos = Prestamo::where('estado_p_id', 5)->get();
        foreach ($prestamos as $credito) {
          if(($credito->monto - ($credito->interes + $credito->capital_recuperado)) == 0)
          {
            $credito->update([
              'estado_p_id' => 9
            ]);
          }
        }
        $user = Auth::user();
        //Listado de todos los creditos si es administrador o secretaria.
        if($user->hasAnyRole(['Administrador', 'Secretaria'])){
          // dd('hola');
          $prestamos = Prestamo::with('cliente.persona', 'user')->orderBy('id', 'desc')->get();
// dd($prestamos);
          // dd($prestamos);
          $creditos_activos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->whereIn('estado_p_id',[3,5])
                                ->get();
          $creditos_pendientes = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->whereIn('estado_p_id',[1,2])
                                ->get();
          $creditos_vencidos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->where('prestamo.estado_p_id','=',9)
                                ->get();
          $creditos_aprobado_hoy = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->where('prestamo.estado_p_id','=',3)
                                ->where('prestamo.fecha_desembolso', Carbon::now()->format('Y-m-d'))
                                ->get();
          $creditos_vencidos_mora = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->where('prestamo.estado_p_id','=',10)
                                ->get();
          $todos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->select()
                                ->whereIn('estado_p_id',[1,2,3,4,5,9,10,11])
                                ->get();
                                // dd($creditos_activos->first());
          $rutas=Hoja_ruta::all();
          return view('Creditos.index2', compact('prestamos', 'creditos_activos', 'creditos_vencidos', 'creditos_vencidos_mora','creditos_pendientes', 'creditos_aprobado_hoy','rutas'));
        } //Se filtra por las rutas asignadas al promotor, y se divide en 3 estados
          //Activos, vencidos y vencidos en mora.
          else if($user->hasRole('Promotor')){
            $user1 = $user;
            $user2 = $user;
            $user3 = $user;
            $user4 = $user;
            $creditos_activos = collect();
            $creditos_vencidos = collect();
            $creditos_vencidos_mora = collect();
            $todos = collect();
            $creditos_activos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                  ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                  ->select()
                                  ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                  ->whereIn('estado_p_id',[3,5])
                                  ->get();
            $creditos_vencidos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                  ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                  ->select()
                                  ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                  ->where('prestamo.estado_p_id','=',9)
                                  ->get();
            $creditos_vencidos_mora = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                  ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                  ->select()
                                  ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                  ->where('prestamo.estado_p_id','=',10)
                                  ->get();
            $todos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                  ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                  ->select()
                                  ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                  ->whereIn('estado_p_id',[1,2,3,4,5,9,10,11])
                                  ->get();
            // dd($todos);
            $rutas=Hoja_ruta::all();
            return view('Creditos.index2', compact('creditos_activos', 'creditos_vencidos', 'creditos_vencidos_mora', 'todos', 'rutas'));
          } else if($user->hasRole('Supervisor')){ //Si es supervisor se filtra por las rutas asignadas y supervisadas.
              $rutas = $user->rutas_completas_supervisor();
              $array = array();
              foreach($rutas as $ruta){
                $array[] = $ruta->prestamo;
              }
              $prestamos = collect($array);
              $creditos_activos = $prestamos->whereIn('estado_p_id',[3,4,5]);
              $creditos_pendientes = $prestamos->whereIn('estado_p_id',[1,2]);
              $creditos_vencidos = $prestamos->where('estado_p_id', '=', 9);
              $creditos_aprobado_hoy = $prestamos->where('estado_p_id','=',3)->where('fecha_desembolso', Carbon::now()->format('Y-m-d'));
              $creditos_vencidos_mora = $prestamos->where('estado_p_id', '=', 10);
              $todos=$prestamos->whereIn('estado_p_id',[0,1,2,3,5,9,10,11]);
              $rutas=Hoja_ruta::all();
              return view('Creditos.index2', compact('todos', 'creditos_activos', 'creditos_vencidos', 'creditos_vencidos_mora','creditos_pendientes', 'creditos_aprobado_hoy', 'rutas'));
          }
      }
      else {
      if($request->ajax()){
        $credito = Prestamo::find($id);
        $abonado = $credito->capital_recuperado + $credito->interes;
        $mora_actual = $credito->mora - $credito->mora_recuperada;
        $html1 =
'          <div class="tab-content">
            <div id="contact-1" class="tab-pane active">
            <div class="row m-b-lg">
                <div class="col-lg-12 text-center">
                    <h2>Codigo: Cre-'.$credito->id.'</h2>
                </div>
            </div>
                <div class="client-detail">
                  <div class="full-height-scroll">
                      <strong>Datos del credito</strong>
                      <ul class="list-group clear-list">
                          <li class="list-group-item">
                          <span class="pull-right">'.$credito->cliente->persona->nombre.
                          ' '.$credito->cliente->persona->apellido.'</span>
                          Cliente
                          </li>
                          <li class="list-group-item fist-item">
                              <span class="pull-right"> '.$credito->monto.' </span>
                              Monto
                          </li>
                          <li class="list-group-item fist-item">
                              <span class="pull-right"> '.$abonado .' </span>
                              Abonado
                          </li>
                          <li class="list-group-item">
                              <span class="pull-right">'.$mora_actual.'</span>
                              Mora actual
                          </li>
                          <li class="list-group-item">
                              <span class="pull-right">'.$credito->fecha_desembolso.'</span>
                              Fecha de creacion:
                          </li>
                          <li class="list-group-item">
                              <span class="pull-right">'.$credito->fecha_inicio .'</span>
                              Fecha de inicio:
                          </li>
                          <li class="list-group-item">
                              <span class="pull-right">'. $credito->fecha_fin.'</span>
                              Fecha final:
                          </li>
                      </ul>
                      <strong>Ficha de pago</strong>
                      <table class="table table-striped table-hover">
                        <thead>
                          <tr>
                            <th>Fecha</th>
                            <th>Cuota</th>
                          </tr>
                        </thead>
                        <tbody>
                          ';
                          $html2 ='';
                          if($credito->ficha_pago){
                            foreach ($credito->ficha_pago->where('tipo',1)->take(5) as $ficha_pago) {
                              $html2 = $html2 . '<tr><td>'.Carbon::parse($ficha_pago->fecha)->format('d-m-Y') .'</td>';
                              $html2 = $html2 . $ficha_pago->estadoActual();
                            }
                          }
                          $html3 = '
                        </tbody>
                      </table>
                  </div>
                </div>
            </div>
          </div>
        ';
        $html4 = $html1 . $html2 . $html3;
                      return $html4;
      }
      //Peticion normal
      $credito = Prestamo::find($id);
// dd($id);
      $this->refreshClasi($credito);
      $contadorDiasSeguidos = 0;
      $pagos = Pago::where("prestamo_id",$id)->get();
      $fichasNoPagas = Ficha_pago::where("prestamo_id",$credito->id)
                                 // ->where('estado', 1)
                                 ->where('estado_p','!=',1)
                                 ->where('estado_p','!=',0)
                                 ->get();
      foreach ($fichasNoPagas as $fichaprobando) {
       if($fichaprobando->estado_p == 2)
       {
         $contadorDiasSeguidos +=1;
       }
       if($fichaprobando->estado_p != 2)
        {
          $contadorDiasSeguidos = 0;
        }
      }
      if ($contadorDiasSeguidos == 5) {
        // dump("Ya llego a 5 dias seguidos");
      }
      // dump($contadorDiasSeguidos);
      // dump($fichasNoPagas);
      //modificacion de datos del prestamo
      $pagosefectuados = Ficha_pago::where("prestamo_id",$credito->id)->where('estado', 1)->get();
      // $credito->monto - ($credito->interes + $credito->capital_recuperado
      $montototal = $credito->monto;
      $pagoF = Ficha_pago::whereNotNull('pago_id')->where("prestamo_id",$credito->id)->get();
      // $pagoA = $pagoF->pago_id;
      $sumpagado = 0;
      $pagadototal=0;
      $sumaInteres=0;
      $totalInteres=0;
      $sumaCapital=0;
      $totalCapital=0;
      foreach ($pagoF as $key => $f) {
        // $ar = $f;
        $arTotal[$key]=$f->pago->monto;
        $sumpagado=$arTotal[$key];
        $pagadototal=$pagadototal+$sumpagado;
        $arInteres[$key]=$f->pago->interes;
        $sumaInteres=$arInteres[$key];
        $totalInteres=$totalInteres + $sumaInteres;
        $arCapital[$key]=$f->pago->capital;
        $sumaCapital=$arCapital[$key];
        $totalCapital=$totalCapital+$sumaCapital;
      }
      // dd($totalCapital);
      // dd($sumTotal);
      // dd($pagoF->id);
      // $pagadototal = Pago::where('prestamo_id', $credito->id)->sum('monto');
      $interestotal = $pagosefectuados->sum('interes');
      $capitalretotal = $pagosefectuados->sum('capital');
      $saldototal = $montototal - ($interestotal + $capitalretotal);
      $moraTotal = Ficha_pago::where('prestamo_id', $credito->id)->sum('ajuste');
      if ($saldototal>0 && $credito->estado_p_id != 10 && $credito->estado_p_id != 1 && $credito->estado_p_id != 2 && $credito->estado_p_id != 3) {
          $credito->update([
            'estado_p_id' => 5,
          ]);
      }
      // dd($pagosefectuados);
      // dd($pagadototal);
      $credito->update([
          'pagado' => $pagadototal,
          'interes' => $totalInteres,
          'capital_recuperado' => $totalCapital,
          'saldo' => round($saldototal, 2),
          'mora'=>$moraTotal,
      ]);
      $dates= array("2017-01-08", "2017-01-09", "2017-01-10");
      // var_dump($this->SonDiasContinuos($credito->ficha_pago->where('estado_p','=','1')->pluck('fecha')->toArray()),$this->SonDiasContinuos($dates) ); // true
      // dd($credito->ficha_pago->where('estado_p','=','1')->pluck('fecha')->toArray(), $dates);
      // $dates = "2017-01-08, 2017-01-12, 2017-01-13";
      // var_dump($this->SonDiasContinuos($dates)); // false
      // dd($credito->ficha_pago);
        $id_garantias = Garantia::where('prestamo_id', $id)->pluck('id');
        $imagenes_garantia = Imagen_Garantia::whereIn('garantia_id',$id_garantias)->get();
      return view('Creditos.show',compact('credito', 'pagos','imagenes_garantia'));
        //Mostrar un prestamo en especifico
        }
    }
    /***
     * Formulario para editar un credito, pero no se usa, ya que no se sabi si se puede
     * editar la informacion de un credito.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Formulario para editar prestamo
        $credito = Credito::find($id);
        return view('Creditos.edit',compact('credito'));
    }
    public function SonDiasContinuos($fechaContinua) {
        // $fechaContinua = explode(", ", $fechaContinua);
        $diaAnterior = new DateTime($fechaContinua[0]);
        unset($fechaContinua[0]);
        foreach ($fechaContinua as $v) {
          $fechaActual = new DateTime($v);
          $diff = $fechaActual->diff($diaAnterior);
          if ($diff->days == 1) {
            $diaAnterior =  new DateTime($v);
          } else {
            return false;
          }
        }
        return true;
      }
    /**
     * Genera las fichas de pago del prestamo cuando se desembolsa
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //Se obtiene el prestamo, y se adjunta su plan y perido respectivamente
      $prestamo = Prestamo::with('plan.periodo')->where("id", "=", $id)->first();
      //Se identifica, si fue aprobada o rechazada.
      if($request->submitbutton != "Rechazar"){
        //Si fue rechazada se procede a eliminarla, pero no de la base de datos
        //solo de la aplicacion para que no se muestre
        $prestamo = Prestamo::with('plan.periodo')->where("id", "=", $id)->first();
        $estado = Estado_p::find(3);
        $plan = Plan::find($request->input('monto'));
        $prestamo->update([
        'clasificacion_id' => 1,
        'fecha_desembolso' => $request->input('fecha_desembolso'),
        'fecha_inicio' => $request->input('fecha_desembolso'),
        'estado' => 11
        ]);
        if($prestamo->plan->id != $plan->id){
            $prestamo->update([
              'plan_id' => $plan->id,
              'capital_activo' => $plan->capital,
              'monto' => $plan->total,
                ]);
        }
        $prestamo = Prestamo::where("id", "=", $id)->first();
        //Se hace los respectivos calculos para determinar que cuota, capital e interes
        //va a presentar cada ficha de pago.
        $no_pagos = $prestamo->plan->periodo->tiempo;
        $plan = $prestamo->plan->nombre;
        $observaciones = $request->input('observaciones');
        $interes = ($prestamo->plan->interes)/$no_pagos;
        $interes = round($interes,2);
        $capital = ($prestamo->capital_activo)/$no_pagos;
        $capital = round($capital,2);
        $cuota = $capital + $interes;
        $total = $capital + $interes;
        $fecha_inicio = $prestamo->fecha_inicio;
        //Obtenemos el dia actual
        $dt = Carbon::parse($fecha_inicio);
        $dt_don = Carbon::now()->addDay(40);
        //Dependiendo del Plan
        switch($plan)
          {
          case 'Diario':
            //For que creara cada ficha de pago con su respectiva fecha
            for ($i=0; $i < $no_pagos; $i++) {
              $dt->day = $dt->day + 1;
              //Se identifica si es domingo o si es una fecha especial, si lo es se salta
              if ($dt->dayOfWeek === 0 || Fechas_especiales::where('fecha', '=', $dt->format('Y-m-d'))->get()->first() != null) {
                $i--;
              }
              else{
                $ficha = $prestamo->ficha_pago()->create([
                  'no_dia'=> $i+1,
                  'fecha'=> $dt,
                  'cuota'=> $cuota,
                  'interes' => $interes,
                  'capital' => $capital,
                  'total'   => $total,
                  'mora' => 0,
                  'tipo'  =>1,
                ]);
              }
            }
          break;
          case 'Semanal':
            //For que creara cada ficha de pago con su respectiva fecha
            $dt->day = $dt->day + 7;
            $contador = 7;
            $temp = 0;
            $no_pagos_totales = ($no_pagos-1)*7+1;
            for ($i=0; $i < $no_pagos_totales; $i++) {
              //Se identifica si es domingo o una fecha especial si lo es se salta
              if ($dt->dayOfWeek === 0 || Fechas_especiales::where('fecha', '=', $dt->format('Y-m-d'))->get()->first() != null) {
                $contador==7 ? $contador=$contador : $contador++;
              }
              else{
                if($contador == 7 ){
                  $temp++;
                  $ficha = $prestamo->ficha_pago()->create([
                    'no_dia'=> $temp,
                    'fecha'=> $dt,
                    'cuota'=> $cuota,
                    'interes' => $interes,
                    'capital' => $capital,
                    'total'   => $total,
                    'mora' => 0,
                    'tipo'  => 1,
                  ]);
                  $contador = 0;
                }else{
                  $ficha = $prestamo->ficha_pago()->create([
                    'no_dia'=> $temp,
                    'fecha'=> $dt,
                    'cuota'=> 0,
                    'interes' => 0,
                    'capital' => 0,
                    'total'   => 0,
                    'mora' => 0,
                    'tipo'  => 0,
                  ]);
                }
                $contador++;
              }
              $dt->day = $dt->day + 1;
            }
          break;
          case 'Quincena':
            //For que creara cada ficha de pago con su respectiva fecha
            $dt->day = $dt->day + 14;
            $contador = 14;
            $temp = 0;
            // if($no_pagos==2)
            // {
            //   $no_pagos_totales = ($no_pagos-1)*13+2;
            // }
            // if($no_pagos==3)
            // {
            //   $no_pagos_totales = 29;
            // }
            $no_pagos_totales = ($no_pagos-1)*14+1;
            // dd($no_pagos);
            for ($i=0; $i < $no_pagos_totales; $i++) {
              //Se identifica si es domingo o si es una fecha especial si lo es se salta
              if ($dt->dayOfWeek === 0 || Fechas_especiales::where('fecha', '=', $dt->format('Y-m-d'))->get()->first() != null) {
                $contador==14 ? $contador=$contador : $contador++;
              }
              else{
                if($contador == 14 ){
                  $temp++;
                  //Creacion de fichas
                  $ficha = $prestamo->ficha_pago()->create([
                    'no_dia'=> $temp,
                    'fecha'=> $dt,
                    'cuota'=> $cuota,
                    'interes' => $interes,
                    'capital' => $capital,
                    'total'   => $total,
                    'mora' => 0,
                    'tipo'  => 1,
                  ]);
                  $contador = 0;
                }else{
                  //Creacion de subfichas
                  $ficha = $prestamo->ficha_pago()->create([
                    'no_dia'=> $temp,
                    'fecha'=> $dt,
                    'cuota'=> 0,
                    'interes' => 0,
                    'capital' => 0,
                    'total'   => 0,
                    'mora' => 0,
                    'tipo'  => 0,
                  ]);
                }
                $contador++;
              }
              $dt->day = $dt->day + 1;
            }
          break;
        }
        //Se establece las fecha en que fue generado todo
        $prestamo->update([
          'fecha_fin' => $prestamo->ficha_pago->last()->fecha,
          'fecha_inicio'  => $prestamo->ficha_pago->first()->fecha,
          'fecha_desembolso'  => Carbon::now()->format('Y-m-d'),
          'estado_p_id' => 5,
          'observaciones' => $observaciones,
        ]);
        $prestamo->ruta->first()->hoja_ruta->update([
          'total_capital' => $prestamo->ruta->first()->hoja_ruta->total_capital + $prestamo->monto,
          'capital_activo'  => $prestamo->ruta->first()->hoja_ruta->total_capital + $prestamo->monto,
          'hora'=>$request->get('hora'),
        ]);
        $prestamo->ruta->first()->update([
          'hora'=>$request->get('hora')
        ]);
        //Se crea la notificacion para mostrar que existe algo pendiente por revisar.
        $notificacion = Notificacion::where('objeto_type', 'Prestamo')->where('objeto_id', $prestamo->id);
        if($notificacion->first() == null){
        }else{
          $notificacion->first()->delete();
        }
        //Obtenemos el dia actual
        $dt = Carbon::now();
        if ($request->input('aprobado')=='true') {
          $prestamo->estado_p()->associate($estado);
          if($prestamo->tipo==0){
          $prestamo->user->update([
            'comision_cliente_nuevo' => $prestamo->user->comision_cliente_nuevo + 50,
            'fecha_inicio'           => Carbon::now(),
          ]);
        }
          $prestamo->save();
          //Restamos capital
          $agencia = Agencia::find(1);
          $capital_actual = $agencia->capital - $prestamo->capital_activo;
          $agencia->update([
            'capital' => $capital_actual,
          ]);
          $agencia->save();
          // $notificacion = Notificacion::where("object_type", "Prestamo")->where("object_id",$prestamo->id)->first();
        }
      }else{
        $prestamo->update([
          'activo' => 0,
          'estado_p_id' => 11,
        ]);
        $notificacion = Notificacion::where('objeto_type', 'Prestamo')->where('objeto_id', $prestamo->id);
        if($notificacion->first() == null){
        }else{
          $notificacion->first()->delete();
        }
      }
      return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
      {
        //Eliminar credito
      }
      /**
      * Funcion de prestamos a entregar, muestra los prestamo que estan pendientes
      * por entregar, dependiendo del tipo de usuario, es como va a mostrar los prestamos
      * si es administrador son todos los prestamos y si es promotor se filtra por las
      * rutas asignadas.
      * @param Request recibe la peticion, laravel siempre manda la peticion
      * @return view retorna la vista correspondiente a los prestamos a entregar.
      */
    public function entregados(Request $request)
      {
      $promotor = Auth::user();
      $paso = array('Administrador', 'Secretaria');
      if($promotor->hasAnyRole($paso)){
        $prestamos = Prestamo::where('estado_p_id', '=', '3')->get();
      } else if($promotor->hasRole('Promotor')){
        $prestamos = collect();
        $temp1 = $promotor->hoja_ruta()->with(['prestamos' => function($query){
          return $query->whereIn('estado_p_id',[3]);
        }])->get();
        foreach ($temp1 as $hoja) {
          $prestamos = $prestamos->merge($hoja->prestamos);
        }
        // $prestamos = $promotor->prestamos->whereIn('estado_p_id',[3]);
      } else if($promotor->hasRole('Supervisor')){
          $rutas = $promotor->rutas_completas_supervisor();
          $array = array();
          foreach($rutas as $ruta){
            $array[] = $ruta->prestamo;
          }
          $TempPrestamos = collect($array);
          $prestamos = $TempPrestamos->where('estado_p_id',3);
        }
        return view('Creditos.entregados', compact('prestamos'));
      }
      /**
      * Esta funcion es para determinar que prestamos estan prontos a finalizar,
      * por defecto se hace desde el dia de hoy hasta siete dias despues,
      * y despues los muestra en su respectiva vista.
      *
      */
    public function finalizar(Request $request){
      $inicio = Carbon::now();
      $fin = Carbon::now()->addDay(7);
      $user = Auth::user();
      //Si es administrador o secretaria se muestra todos los creditos de todas las rutas
      //que estan proximas a finalizar.
      if($user->hasAnyRole(['Administrador', 'Secretaria'])){
      $prestamos= Prestamo::Terminar($inicio, $fin)->orderBy('fecha_fin','ASC')->get();}
      else if($user->hasRole('Promotor')){
        $prestamos = collect();
        $temp1 = $user->hoja_ruta()->with(['prestamos' => function($query)use ($inicio,$fin){
          return $query->wherebetween('fecha_fin', [$inicio, $fin]);
        }])->get();
        foreach ($temp1 as $hoja) {
          // code...
          $prestamos = $prestamos->merge($hoja->prestamos);
        }
        //Si es supervisor, se filtra por las rutas asignadas y por rutas supervisadas.
      }else if($user->hasRole('Supervisor')){
        $prestamos = collect();
        $hoja_1 = Hoja_ruta::where('supervisor_id', $user->id)->with(['ruta.prestamo' => function($query)use ($inicio,$fin){
          return $query->wherebetween('fecha_fin', [$inicio, $fin]);
        }])->get();
        $hoja_2 =  Hoja_ruta::where('user_id', $user->id)->with(['ruta.prestamo' => function($query)use ($inicio,$fin){
          return $query->wherebetween('fecha_fin', [$inicio, $fin]);
        }])->get();
        foreach ($hoja_1 as $hoja) {
          $prestamos = $prestamos->merge($hoja->prestamos);
        }
        foreach ($hoja_2 as $hoja) {
          $prestamos = $prestamos->merge($hoja->prestamos);
        }
      }
      return view('Creditos.proxFinalizar', compact('prestamos'));
    }
    /**
    * Muestra el formulario para la renovacion de creditos, con la informacion
    * necesaria para realizar la renovacion y la informacion del cliente y sus
    * prestamos anteriores.
    *
    * @param int $id del cliente a quien se le quiere renovar el prestamo
    */
    public function renovar($id){
      // $promotores = User::where('estado', '=', 1)->whereIn('rol_id',[2,4])->get();
      $rutas = Hoja_ruta::all();
      $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
      $promotores = collect();
      foreach ($roles as $rol) {
        $promotores = $promotores->merge($rol->users);
      }
      $hoja_ruta = Hoja_ruta::where('activa', '=', 1)->get();
      $cliente = Cliente::with('prestamos.plan.periodo', 'persona')->find($id);
      $prestamos_ant = Prestamo::where("cliente_id", '=', $id)->whereIn('estado_p_id', [3,5,10])->get();
      // dd($prestamos_ant);
      $saldototal = 0;
      foreach($prestamos_ant as $credito)
      {
        // $saldo = $credito->monto - $credito->pagado;
        //
        // $saldototal = $saldototal + $saldo;
        $pagosSaldo = \App\Pago::where('prestamo_id',$credito->id)->sum('monto');
        $pagosMora = \App\Pago::where('prestamo_id',$credito->id)->sum('mora');
        $totalPagosFicha=\App\Ficha_pago::where('prestamo_id',$credito->id)->whereNotNull('pago_id')->get();
        $sumaTotalPagos=0;
        foreach ($totalPagosFicha as $pagosList) {
          $listadoPagos[]=\App\Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
          // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
        }
        if(isset($listadoPagos))
        {
          foreach ($listadoPagos as $sumaPagos) {
            $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
          }
        }
        else {
          $sumaTotalPagos=0;
        }
        // $totalRealPago=\App\Ficha_pago::where('')
         $saldototalPre=$sumaTotalPagos-$pagosMora;
        $moraPendiente=$credito->mora - $credito->mora_recuperada;
        $saldototal2=($credito->monto - $saldototalPre)+$moraPendiente;
        if($saldototal2<0)
          {
            $saldototal=0;
          }
          else {
            $saldototal=$saldototal2;
          }
      }
      // dd($saldoFinal);
      // dd($saldototal);
      return view('Creditos.renovacion', compact('promotores', 'hoja_ruta', 'cliente', 'saldototal'));
    }
    /**
    * Funcion que crea la renovacion,
    *
    * @param Request recibe la peticion que contiene toda la informacion de la
    * renovacion
    */
    public function renovacionCreate(Request $request){
      $usuario = User::find($request->input('promotor'));
      $idPlan = $request->input('monto');
      $plan = Plan::find($idPlan);
        if (is_null($plan))
        {
        echo 'El plan de pago es nulo';
        return redirect()->route('creditos.renovacion', [$cliente]);
        }
        else
        {
         $prestamoNew = Prestamo::create([
            'monto' => $plan->total,
            'capital_activo' =>$plan->total - $plan->interes,
            'user_id' => $usuario->id,
            'validacion_garantia' => ($request->has('validacion_garantia')) ? 1 : 0,
            'cliente_id' => $request->input('cliente'),
            'plan_id' => $idPlan,
            'estado_p_id' => ($request->get('validacion_garantia') == 1) ? 15 : Estado_p::find(1)->id,
            'tipo' => 0,
            'fecha_desembolso' => $request->input('fecha_desembolso'),
            'fecha_inicio' => $request->input('fecha_desembolso'),
            'posible_primer_pago' => $request->input('fecha_inicio'),
            'clasificacion_id' => 1
            ]);
        $rutaNew = Ruta::create([
        'prestamo_id'=> $prestamoNew->id,
        'hoja_ruta_id' => Hoja_ruta::find($request->input('ruta'))->id,
        'hora'         => $request->input('hora'),
      ]);
            $notificacion = Notificacion::create([
            'descripcion'   => 'Nuevo',
            'objeto_id'     => $prestamoNew->id,
            'objeto_type'   => 'Prestamo',
            'tipo'          => 1,
            ]);
            $cliente = Cliente::find($request->input('cliente'));
      //Creacion de garantias
      if($request->get('validacion_garantia') == 1)
      {
        $nombre_garantia          = $request->nombre_garantia;
        $cantidad_garantia        = $request->cantidad_garantia;
        $valoracion_garantia      = $request->valoracion_garantia;
        $estado_producto_garantia = $request->estado_producto_garantia;
        $descripcion_garantia     = $request->descripcion_garantia;

        $categoria_garantia       = $request->categoria_garantia_id;
        // $imagen_uno_garantia      = $request->file('imagen_uno');
        // $imagen_dos_garantia      = $request->file('imagen_dos');
        // $originalPath = public_path().'/images/';
        for($count = 0; $count < count($nombre_garantia); $count++) {
          // $file=$request->{"photos".($count+1)}[$count]->save($originalPath);
          // $img = \Image::make($file);
          // ${"imagenes_garantia" . ($count+1)} =$request->{"*photos" . ($count+1)};
          // dump(${"imagenes_garantia" . ($count+1)});
          // $thumbnailImagedos = Image::make($imagen_dos_garantia[$count]);
          // $thumbnailImagedos->save($originalPath.time().$imagen_dos_garantia[$count]->getClientOriginalName());
          $garantiaNew = Garantia::create([
            'nombre' => $nombre_garantia[$count],
            'cantidad' => $cantidad_garantia[$count],
            'valoracion_cliente' => $valoracion_garantia[$count],
            'estado_producto_cliente' => $estado_producto_garantia[$count],
            'descripcion' => $descripcion_garantia[$count],
            'observacion' => $observacion_garantia[$count],
            'categoria_id' => $categoria_garantia[$count],
            'created_by' => Auth::user()->id,
            'prestamo_id' => $prestamoNew->id
            ]);
            $ind= 0;
          foreach ($request->{"photos".($count+1)} as $key)
          {
            $file = $request->{"photos".($count+1)}[$ind];
            $file->move(public_path().'/images/clientes/garantias/', $file->getClientOriginalName());
            // $request->{"photos".($count+1)}[$count]->move(base_path('//public'), $request->{"photos".($count+1)}[$count]->getClientOriginalName());
            // $request->{'photos'.($count+1)}[$count]->move(public_path().'//images//clientes//garantias//', $request->{"photos".($count+1)}[$count]->getClientOriginalName());
            $imagen_GarantiaNew= Imagen_Garantia::create([
              'garantia_id' => $garantiaNew->id,
              'ruta' =>'/images/clientes/garantias/'.$file->getClientOriginalName(),
              'created_by' => Auth::user()->id

            ]);
            $ind++;
          }
          // $insert_data[] = $data;
        }
        // if(!empty($insert_data)){
        // Garantia::insert($insert_data);
        // }
      }
      return redirect()->route('clientes.show', [$cliente]);
        }
    }
    //funcion que muestra las fechas especiales que han sido ingresadas.
    public function fecha_esp(){
      $fechas = Fechas_especiales::all();
      return view('Creditos.fecha_descanso', compact('fechas'));
    }
    //Funcion que crea una nueva fecha especial
    public function fecha_create(Request $request){
      $fecha = Fechas_especiales::create([
        'fecha'       =>    $request->input('fecha'),
        'descripcion' =>    $request->input('descripcion'),
      ]);
      return redirect()->route('creditos.fecha');
    }
    /**
    * Funcion que muestra la informacion del credito cuando es una peticion ajax
    * crea un array que contiene toda la informacion del credito que se esta solicitando
    * @param int $id del credito que se quiere obtener la informacion.
    * @return array retorna un array con la informacion.
    */
    public function credinfo($id){
      $prestamo = Prestamo::find($id);
      // $pagosSaldo = \App\Pago::where('prestamo_id',$credito->id)->sum('monto');
      // $pagosMora = \App\Pago::where('prestamo_id',$credito->id)->sum('mora');
      // $totalPagosFicha=\App\Ficha_pago::where('prestamo_id',$credito->id)->whereNotNull('pago_id')->get();
      // $sumaTotalPagos=0;
      // foreach ($totalPagosFicha as $pagosList) {
      //   $listadoPagos[]=\App\Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
      //   // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
      // }
      // if(isset($listadoPagos))
      // {
      //   foreach ($listadoPagos as $sumaPagos) {
      //     $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
      //   }
      // }
      // else {
      //   $sumaTotalPagos=0;
      // }
      // // $totalRealPago=\App\Ficha_pago::where('')
      //  $saldoTotal=$sumaTotalPagos-$pagosMora;
      if($prestamo->estado_p_id==5 || $prestamo->estado_p_id==4){
        $ficha_actual = $prestamo->ficha_pago->where('fecha','=', Carbon::now()->format(Y-m-d));
        $datos = response()->json([
          'cliente' => $ficha_actual->prestamo->cliente->nombre . $ficha_actual->prestamo->cliente->nombre,
          'no_cuota' => $ficha_actual->no_dia,
          'cuota'  => $ficha_actual->cuota,
          'prestamo_id' => $credito->id,
          'prestamo' => $credito->id,
          'no_prestamo' => $credito->cliente->prestamos->count(),
          'mora'  => $ficha_pago->mora,
          'total' => $ficha_pago->cuota,
          'dia_perdon' => $ficha_pago->cont,
        ]);
      }else{
        $ficha_actual = $prestamo->ficha_pago->last();
        $pagosSaldo = \App\Pago::where('prestamo_id',$prestamo->id)->sum('monto');
        $pagosMora = \App\Pago::where('prestamo_id',$prestamo->id)->sum('mora');
        $totalPagosFicha=\App\Ficha_pago::where('prestamo_id',$prestamo->id)->whereNotNull('pago_id')->get();
        $sumaTotalPagos=0;
        foreach ($totalPagosFicha as $pagosList) {
          $listadoPagos[]=\App\Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
          // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
        }
        if(isset($listadoPagos))
        {
          foreach ($listadoPagos as $sumaPagos) {
            $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
          }
        }
        else {
          $sumaTotalPagos=0;
        }
        // $totalRealPago=\App\Ficha_pago::where('')
         $saldoTotal=$sumaTotalPagos-$pagosMora;
         $capitalVencido=round(($prestamo->monto - $saldoTotal)+($prestamo->mora - $prestamo->mora_recuperada), 2);
        $datos = response()->json([
          'cliente' => $ficha_actual->prestamo->cliente->persona->nombre . $ficha_actual->prestamo->cliente->persona->apellido,
          'no_cuota' => $ficha_actual->no_dia,
          'cuota'  => $capitalVencido,
          'capital_vencido' => $capitalVencido,
          'interes' => $ficha_actual->interes,
          'prestamo_id' => $prestamo->id,
          'prestamo' => $prestamo->id,
          'no_prestamo' => $prestamo->id,
          'mora'  => $ficha_actual->mora,
          'total' => $capitalVencido,
        ]);
      }
      return $datos;
    }
    //Funcion que retorna todos los municipios filtrados por departamento.
    public function infomuni($id){
      $departamento = States::find($id);
      $municipios = $departamento->cities->pluck('id','name');
      return $municipios->toArray();
    }
    /**
    * Funcion que retorna los datos de los proximos prestamos a finalizar en una
    * fecha determinada, por medio de una peticion ajax, se comienza filtrando
    * dependiendo el tipo de usuario que es.
    * @param Request peticion que contien la informacion de las fechas
    * @return array retorna un array con la informacion de los prestamos a finalizar
    */
    public function apiProxFin(Request $request){
      $inicio = $request->get('inicio');
      $fin = $request->get('fin');
      $inicio_for = Carbon::createFromFormat('d/m/Y', $inicio)->format('Y-m-d');
      $fin_for = Carbon::createFromFormat('d/m/Y', $fin)->format('Y-m-d');
      $user = Auth::user();
      if($user->hasAnyRole(['Administrador','Secretaria'])){
        $prestamos = Prestamo::Terminar($inicio_for, $fin_for)->with('cliente' , 'user')->get();
    }else if($user->hasRole('Promotor')){
        $user->hoja_ruta()->with(['prestamos' => function($query){
          return $query->Terminar();
        }]);
    } else if($user->hasRole('Supervisor')){
        $prestamos = collect();
        $hoja_1 = Hoja_ruta::where('supervisor_id', $user->id)->with(['ruta.prestamo' => function($query)use ($inicio,$fin){
          return $query->wherebetween('fecha_fin', [$inicio, $fin]);
        }])->get();
        $hoja_2 =  Hoja_ruta::where('user_id', $user->id)->with(['ruta.prestamo' => function($query)use ($inicio,$fin){
          return $query->wherebetween('fecha_fin', [$inicio, $fin]);
        }])->get();
        foreach ($hoja_1 as $hoja) {
          $prestamos = $prestamos->merge($hoja->prestamos);
        }
        foreach ($hoja_2 as $hoja) {
          $prestamos = $prestamos->merge($hoja->prestamos);
        }
    }
      $data = array();
      foreach ($prestamos as $prestamo) {
        $data_p = array([
            'id'  => $prestamo->id,
            'monto' => $prestamo->monto + $prestamo->plan->interes,
            'clasificacion' => $prestamo->obtenerClasificacion(),
            'fecha_final'   => $prestamo->fecha_fin,
            'cliente'       => $prestamo->cliente->persona->nombre . " " . $prestamo->cliente->persona->apellido,
            'id_cliente'    => $prestamo->cliente->id,
            'promotor'      => $prestamo->user->persona->nombre . " " .  $prestamo->user->persona->apellido,
          ]);
          $data[] = $data_p;
      }
      return $data;
    }
    /**
    * Muestra los planes que han sido ingresados, y la opcion  de poder crear un nuevo plan
    */
    public function planes()
    {
      $planes = Plan::all();
      return view('Creditos.planes', compact('planes'));
    }
    /**
    * Funcion que crea un nuevo plan
    * @param Request recibe la peticion con la informacion.
    * @return Redirect retorna una vista donde muestra los planes creados.
    */
    public function planesStore(Request $request)
    {
      //Creacion de periodo
      $periodo = Periodo::firstOrCreate([
        'nombre' => $request->input('plan'),
        'tiempo' => $request->input('no_pagos'),
      ]);
      $monto_total = $request->input('monto') + $request->input('interes');
      $cuota_plan = $monto_total/$periodo->tiempo;
      //Creacion de Planes
      $plan = Plan::firstOrCreate([
        'total' => $monto_total,
        'capital' => $request->input('monto'),
        'nombre' => $request->input('plan'),
        'interes' => $request->input('interes'),
        'mora' => $request->input('mora'),
        'cuota' => $cuota_plan,
        'periodo_id' => $periodo->id,
      ]);
      return redirect()->route('creditos.planes');
    }
    /**
    * Funcion que obtiene los planes, en un periodo determinado, esta funcion se usa
    * cuando se crea un nueva solicitud, y se va filtrando segun se va cambiando de
    * periodo.
    * @param Request peticion que contiene toda la informacion para filtrar.
    * @return array retorna un array con la informacion de los planes.
    */
    public function infoMonto(Request $request){
      $tipo = $request->get('tipo');
      $planes = Plan::where('periodo_id','=',$tipo)->select('id','capital')->get();
      // $planes = Plan::select('id','capital')->where('nombre',$tipo)->get();
      return $planes->toArray();
    }
    /**
    * Ya selecionado todos los datos para el prestamo, solo faltaria retornas los ultimos
    * datos del plan, por esta funcion lo hace, recibe el id del plan y retorna la informacion
    * en un array
    * @param Request recibe una peticion con la informacion.
    * @return array retorna un array con la informacion necesaria.
    */
    public function infoPlan(Request $request)
    {
        $idPlan = $request->get('tipo');
        $plan = Plan::find($idPlan);
        $datos = array(
          'interes' => $plan->interes,
          'mora'  => $plan->mora,
          'no_pagos' => $plan->periodo->tiempo,
          'total' => $plan->total,
        );
// dd($datos);
        return $datos;
    }
    /**
    * Funcion que obtiene todos los No. de pagos de un determinado plan
    * @param Request recibe una peticion con la informacion necesaria
    * @return array un array con todos los no. de pago que corresponden a ese plan.
    */
    public function infoNoPagos(Request $request)
    {
      $tipo = $request->get('tipo');
      // $plan = Plan::where('nombre',$tipo)->with('periodo')->groupBy('periodo.id')->get();
      $periodo = Periodo::where('nombre', $tipo)->select('id','tiempo')->get();
      return $periodo->toArray();
    }
    /**
    * Funcion que verifica si el DPI que se esta ingresando ya existe
    * @param Request peticion con la informacion del DPI
    * @return boolean si existe retorna verdadero de lo contrario false.
    */
    public function verificarDPI(Request $request){
      $dpi = $request->get('dpi');
      $cliente = Persona::where('dpi', $dpi)->count();
      if($cliente > 0){
        return "true";
      }else{
        return "false";
      }
    }
    /**
    * Funcion que agrega una foto de compromiso a un credito.
    * @param Request recibe la informacion necesaria para actualizar la foto de compromiso
    * @param int $id del credito del que se quiere actualizar
    */
    public function agregarFotoCompromiso(Request $request, $id){
      $credito = Prestamo::find($id);
      $destinoPath = public_path('//images//clientes//documentos');
      if($request->file('img_compromiso') !=null){
        $nombre_imagen_compromiso = "";
        $imagen_compromiso = $request->file('img_compromiso');
        $nombre_imagen_compromiso  = $request->input('nombre') . 'Compromiso' .time() . '.' . $imagen_compromiso->getClientOriginalExtension();
        $imagen_compromiso->move($destinoPath , $nombre_imagen_compromiso);
        $credito->update([
          'foto_compromiso' => $nombre_imagen_compromiso,
        ]);
      }
      return redirect()->back();
    }
    /**
    * Agregar imagenes a credito, identifica si se agrego una imagen en caso que
    * sea verdadero se actualiza el prestamo con la imagenes respectivas.
    * @param Request peticion que contiene la informacion
    * @return Redirect vuelve a cargar la pagina en la que se encuentra.
    */
    public function agregarImagen(Request $request, $id = null){
      $prestamo = Prestamo::find($id);
      $destinoPath = public_path('images/clientes/documentos');
      if($request->file('img_recibo') !=null){
        $nombre_imagen_recibo = "";
        $imagen_recibo = $request->file('img_recibo');
        $nombre_imagen_recibo  = $request->input('nombre') . 'RECIBO' .time() . '.' . $imagen_recibo->getClientOriginalExtension();
        $imagen_recibo->move($destinoPath, $nombre_imagen_recibo);
        $prestamo->update([
          'foto_recibo' => $nombre_imagen_recibo,
        ]);
      }
      if($request->file('img_solicitud') !=null){
        $nombre_imagen_firma = "";
        $imagen_firma = $request->file('img_solicitud');
        $nombre_imagen_firma  = $request->input('nombre') . 'RECIBO' .time() . '.' . $imagen_firma->getClientOriginalExtension();
        $imagen_firma->move($destinoPath, $nombre_imagen_firma);
        $prestamo->update([
          'foto_solicitud' => $nombre_imagen_firma,
        ]);
      }
      return redirect()->back();
    }
    /**
    *
    *
    */
    public function cambiandoImagenes(){
      $clientes = Cliente::all();
      foreach ($clientes as $cliente) {
        $prestamo = $cliente->prestamos->last();
        $prestamo->update([
          'foto_recibo' =>  $cliente->foto_recibo,
          'foto_solicitud'  =>  $cliente->foto_firma,
        ]);
      }
      return 'Datos actualizados';
    }
    /**
    * Funciono que obtiene la informacion y que puede editar un plan, dependiendo
    * el tipo de peticion en el ajax, es lo que hace, si es GET devulve la informacion
    * de un respectivo plan, y si es POST se actualiza la informacion con los datos ingresados.
    *
    * @param Request peticion con la informacion del AJAX
    * @return Redirect recarga la pagina anterior.
    */
    public function editar_plan(Request $request){
      if($request->isMethod('GET')){
        $id = $request->get('id');
        $plan = Plan::find($id);
        $datos = array(
          'mora'      => $plan->mora,
          'plan'      => $plan->nombre,
          'capital'   => $plan->capital,
          'interes'   => $plan->interes,
          'nopagos'  => $plan->periodo->tiempo,
          'id'  => $plan->id,
        );
        return $datos;
      } else{
        $id = $request->get('plan_edit');
        $plan = Plan::find($id);
        $periodo = Periodo::firstOrCreate([
          'nombre' => $request->get('plan_plan'),
          'tiempo'  => $request->get('nopagos_e'),
        ]);
        $plan->update([
          'nombre' => $request->get('plan_plan'),
          'capital' => $request->get('capital_e'),
          'mora'  => $request->get('mora_e'),
          'interes' => $request->get('interes_e'),
          'nopagos' => $request->get('nopagos_e'),
          'total' => $request->get('capital_e') + $request->get('interes_e'),
          'cuota' => $request->get('cuota_e'),
          'periodo_id' => $periodo->id,
        ]);
      }
      return redirect('/planes/create');
    }
    public function eliminarcredito(Request $request)
    {
        $prestamo_id= $request->get('valor');
        $prestamo   = \App\Prestamo::find($prestamo_id);
        $cliente    = \App\Cliente::where('id', $prestamo->cliente_id)->first();
        $persona    = \App\Persona::where('id', $cliente->persona_id)->first();
        $ruta       = \App\Ruta::where('prestamo_id', $prestamo_id)->first();
        $ficha_pago = \App\Ficha_pago::where('prestamo_id', $prestamo_id)->get();
        $pago       = \App\Pago::where('prestamo_id', $prestamo_id)->get();
        // if(\App\Prestamo::where('cliente_id',$cliente->id)->count()<=1)
        //
        // {
        //
        //   $deletedCliente  = $cliente->delete();
        //
        //   $deletePersona   = $persona->delete();
        //
        // }
        $deletedPrestamos  = $prestamo->delete();
        $deletedRuta = $ruta->delete();
        $deletedPago  = \App\Pago::where('prestamo_id', $prestamo_id)->delete();
        $deletedFicha_Pago = \App\Ficha_pago::where('prestamo_id', $prestamo_id)->delete();
        // $colleccion = collect([$prestamo_id, $prestamo, $cliente, $persona, $ruta, $ficha_pago, $pago]);
    }
// FUNCIONES PARA CREAR SOLO EL CLIENTE SIN TENER QUE HACER PRESTAMO
public function createcliente()
{
    //Formulario para solicitud de credito
    $usuarios = User::all();
    $planes = Plan::all();
    $hoja_ruta = Hoja_ruta::all();
    $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
    $states = States::all();
    $promotores = collect();
    foreach ($roles as $rol) {
      $promotores = $promotores->merge($rol->users);
    }
    $mensaje = "";
    if($planes->isEmpty() || $hoja_ruta->isEmpty() || $promotores->isEmpty()){
      if($planes->isEmpty() && $hoja_ruta->isEmpty() && $promotores->isEmpty()){
        $mensaje = "No existe planes, rutas, ni promotores para poder registrar un credito, se recomienda que cree por lo menos un plan, una ruta y un promotor";
      } else if($planes->isEmpty() && $hoja_ruta->isEmpty()){ //Esta registrado un promotor
        $mensaje = "No existe planes, ni rutas, para poder registrar un credito, se recomienda que se cree por lo menos un plan, una ruta";
      } else if($hoja_ruta->isEmpty() && $promotores->isEmpty()){
        $mensaje = "No existen rutas, ni promotores, para poder registrar un credito, se recomienda que se cree por lo menos una ruta y un promotor";
      } else if($hoja_ruta->isEmpty()){
        $mensaje = "No existen rutas, para poder registrar un credito, se recomienda que se cree por lo menos una ruta";
      } else if($ruta->isEmpty()){
        $mensaje = "No existen planes, para poder registrar un credito, se recomienda que se cree por lo menos un plan";
      }
      Alert::warning($mensaje,'Advertencia')->persistent("Close this");
    }
    return view('Creditos.createunique', compact('usuarios','planes', 'hoja_ruta','promotores','states'));
}
/**
 * Crea el respectivo credito con la informacion proporcionada
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function storecliente(Request $request)
{
  // dd($request);
  if($this->verificarDPI($request) == "true"){
    $mensaje = "Ya no se puede crear el prestamo, porque el DPI ya esta en uso";
    Alert::warning($mensaje,'Advertencia')->persistent("Close this");
    return redirect()->route('Creditos.createunique');
  }
  $idPlan = $request->input('monto');
  $plan = Plan::find($idPlan);
      // Creacion de la locacion cliente
      $locat = Location::firstOrCreate([
        'countries_id' => 1,
        'states_id' => $request->input('state'),
        'cities_id' => $request->input('city'),
      ]);
      // Creacion del perfil del cliente
      $personaNew = Persona::create([
        'nombre' => $request->input('nombre'),
        'apellido' => $request->input('apellido'),
        'dpi' => $request->input('dpi'),
        'nit' => $request->input('nit'),
        'genero' => $request->input('genero'),
        'fecha_nacimiento' => $request->input('fecha_nacimiento'),
        'telefono' => $request->input('telefono'),
        'celular1' => $request->input('celular1'),
        'celular2' => $request->input('celular2'),
        //'no_hijos'  => $request->input('no_hijos') == "" ?  0 : $request->input('no_hijos'),
        'domicilio' => $request->input('direccion'),
        'location_id' => $locat->id,
        'tipo'        => "Cliente",
      ]);
      $destinoPath = public_path('//images//clientes//documentos');
      $nombre_imagen_dpi = "";
      $nombre_imagen_recibo = "";
      $nombre_imagen_firma = "";
      if($request->file('foto_dpi') == null){
      } else{
      $imagen_dpi = $request->file('foto_dpi');
      $nombre_imagen_dpi  = $request->input('nombre'). 'DPI' . time() . '.' . $imagen_dpi->getClientOriginalExtension();
      $imagen_dpi->move($destinoPath, $nombre_imagen_dpi);
      }
      if($request->file('foto_recibo') == null){
      } else{
      $imagen_recibo = $request->file('foto_recibo');
      $nombre_imagen_recibo  = $request->input('nombre') . 'RECIBO' .time() . '.' . $imagen_recibo->getClientOriginalExtension();
      $imagen_recibo->move($destinoPath, $nombre_imagen_recibo);
      }
      if($request->file('foto_firma') == null){
      } else{
      $imagen_firma = $request->file('foto_firma');
      $nombre_imagen_firma  = $request->input('nombre') . 'RECIBO' .time() . '.' . $imagen_firma->getClientOriginalExtension();
      $imagen_firma->move($destinoPath, $nombre_imagen_firma);
      }
      // Creacion de cliente
      $clienteNew = Cliente::create([
        'fecha_ingreso' => Carbon::now(),
        'empresa_trabajo' => $request->input('empresa_trabajo'),
        'direccion_trabajo' => $request->input('direccion_trabajo'),
        'direccion_recibo' => $request->input('direccion_luz'),
        'nombre_recibo' => $request->input('recibo_luz'),
        'tipo_casa' => $request->input('tipo_casa'),
        'foto_dpi'  => $nombre_imagen_dpi,
        // 'foto_recibo' => $nombre_imagen_recibo,
        // 'foto_firma'  => $request->imagen_firma,
        'actividad'   => $request->input('actividad'),
        'observaciones' => $request->input('observaciones'),
        'telefono_empresa'  => $request->input('telefono_empresa'),
        'tiempo_trabajando' => $request->input('tiempo_trabajando'),
        'estado_civil' => $request->input('estado_civil'),
        "listabn_id"  => 1,
        'salario' => $request->input('salario') ? $request->input('salario') : 0 ,
        'no_hijos'  => $request->input('no_hijos') == "" ?  0 : $request->input('no_hijos'),
        'direccion_cobrar' => $request->input('direccion_cobrar'),
        'nacionalidad'  => $request->input('nacionalidad'),
        'persona_id'=> $personaNew -> id,
      ]);
      //Creacion de referencias
      for ($i=0; $i < 2; $i++) {
        $num = (string) (1+$i);
        $referencia = Referencia_personal::create([
          'nombre' => $request->input('nombre_r' . $num),
          'telefono' => $request->input('telefono_r'.$num),
          'direccion' => $request->input('direccion_r'.$num),
          'cliente_id' => $clienteNew->id,
        ]);
      }
      return redirect()->route('clientes.index');
}
}
