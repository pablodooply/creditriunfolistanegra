<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Garantia;
use App\Imagen_Garantia;
use App\Categoria;
use App\Prestamo;
use Image;
use Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;

class GarantiaController extends Controller
{

    public function index()
    {
      return view('Garantias.index');
    }

    public function api()
    {
      $prestamos = Prestamo::has('garantia', '>', 0)->where('activo', 1)->with('cliente')->with('cliente.persona');
      // $categorias = Categoria::select()->where('estado', 1);
      return Datatables::of($prestamos)
      ->addColumn('id', function ($prestamo) {
          return '<a href="/creditos/'.$prestamo->id .'">CRE-' . str_pad($prestamo->id,6,"0",STR_PAD_LEFT) . '</a>';
      })
      ->editColumn('cliente.persona.nombre', function ($prestamo) {
          return $prestamo->cliente->persona->nombre ;
      })
      ->editColumn('cliente.persona.apellido', function ($prestamo) {
          return $prestamo->cliente->persona->apellido;
      })
      ->addColumn('cantgarantias', function ($prestamo) {
          return count($prestamo->garantia()->get());
      })
      ->addColumn('accion', function ($prestamo) {
          $id = $prestamo->id;
          return  view('Garantias.partials.accion', compact('id'));
      })
      ->addColumn('estado', function ($prestamo) {
        if ($prestamo->estado_p_id == 15) {
          return 'Pendiente';
        }
        else {
          return 'Aprobado';
        }
      })
      ->rawColumns(['accion', 'id'])
      ->setRowClass('text-center')
      ->make(true);
    }

    public function div_create(Request $request)
    {
      if ($request->isMethod('get'))
      {
        // $garantia = Garantia::create(['nombre' => null]);
        $id_div = $request->get('id_div');
        // $categorias = Categoria::where('estado', 1)->pluck('nombre','id')->ToArray();
        $categorias = Categoria::where('estado', 1)->pluck("nombre","id")->ToArray();
        return view('Creditos.partials.div-create', compact('categorias', 'id_div'));
      }
      else
      {
      }
    }

    public function modal_pendiente_valoracion(Request $request, $id)
    {
      if ($request->isMethod('get'))
      {
        $prestamo = Prestamo::find($id);
        $garantias = Garantia::where('prestamo_id', $id)->get();
        $id_garantias = Garantia::where('prestamo_id', $id)->pluck('id');
        $imagenes_garantia = Imagen_Garantia::whereIn('garantia_id',$id_garantias)->get();
        return view('Notificacion.partials.cont-modal-pendiente-valoracion', compact('prestamo', 'garantias','imagenes_garantia'));
      }
      else {
        $valoracion = $request->get('valoracion_empresa');
        // dd($valoracion);
        $estado_producto = $request->get('estado_producto_empresa');
        $observacion = $request->get('observacion');

        $garantia = Garantia::find($id);
        $garantia->update([
          'valoracion_empresa' => $valoracion,
          'estado_producto_empresa' => $estado_producto,
          'observacion' => $observacion
        ]);
      }
    }


    public function aprobargarantia($id)
    {
      $prestamo = Prestamo::find($id);
      $prestamo->update([
        'estado_p_id' => 1
      ]);
    }

    public function modal_garantias_aprobadas(Request $request, $id)
    {
        $prestamo = Prestamo::find($id);
        $garantias = Garantia::where('prestamo_id', $id)->get();
        $id_garantias = Garantia::where('prestamo_id', $id)->pluck('id');
        $imagenes_garantia = Imagen_Garantia::whereIn('garantia_id',$id_garantias)->get();
        return view('Notificacion.partials.cont-modal-valoracion-aprobada', compact('prestamo', 'garantias','imagenes_garantia'));
    }
}