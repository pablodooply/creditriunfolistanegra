<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use App\Hoja_ruta;
use App\Prospecto;

class ProspectoController extends Controller
{

    public function index(){

      if(Auth::user()->hasAnyRole(['Administrador', 'Supervisor'])){
        $rutas = Hoja_ruta::where('activa', '=', 1)->get();
        $prospectos=Prospecto::all();
      } else{
        $user = Auth::user();
        $rutas =  $user->hoja_ruta->where('activa', '=',1);
        $prospectos = collect();
        foreach ($rutas as $ruta) {
          // code...
          $prospectos = $prospectos->merge($ruta->prospectos);
        }
      }


      return view('Prospectos.prospecto', compact('rutas', 'prospectos'));
    }

    public function create(Request $request){
      $prospecto = Prospecto::create([
        'nombre'  => $request->input('nombre'),
        'apellido' => $request->input('apellido'),
        'telefono' => $request->input('telefono'),
        'domicilio' => $request->input('domicilio'),
        'hoja_ruta_id' => $request->input('ruta'),
        'observacion' => $request->input('observacion'),
        'fecha_proxima' => $request->input('fecha'),
      ]);

      return redirect()->back();
    }

    public function delete($id){
      $prospecto = Prospecto::find($id);
      $prospecto->delete();

      return redirect()->back();
    }

    public function infoProspectos(Request $request){
      $inicio = Carbon::createFromFormat('d/m/Y',$request->get('inicio'))->format('Y-m-d');
      $fin = Carbon::createFromFormat('d/m/Y',$request->get('fin'))->format('Y-m-d');

      if(Auth::user()->hasAnyRole(['Administrador', 'Supervisor'])){
        // $rutas = Hoja_ruta::where('activa', '=', 1)->get();
        // $prospectos = Prospecto::where(function($query) use($inicio, $fin){
        //   return $query->whereBetween('fecha_proxima', [$inicio, $fin]);
        // })->with('hoja_ruta:id,nombre')->get();
        $prospectos = DB::table('prospecto')
                        ->join('hoja_ruta', 'hoja_ruta.id', '=', 'prospecto.hoja_ruta_id')
                        ->whereBetween('fecha_proxima', [$inicio, $fin])
                        ->select('hoja_ruta.nombre as hoja', 'prospecto.*' )
                        ->get();

      } else{
        $user = Auth::user();
        $prospectos = DB::table('prospecto')
                        ->join('hoja_ruta', 'hoja_ruta.id', '=', 'prospecto.hoja_ruta_id')
                        ->where('hoja_ruta.user_id','=',$user->id)
                        ->whereBetween('fecha_proxima', [$inicio, $fin])
                        ->select('prospecto.*', 'hoja_ruta.nombre as hoja')
                        ->get();

      }

      return $prospectos;
    }
}
