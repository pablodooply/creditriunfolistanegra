<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use App\Ficha_pago;
use App\Agencia;
use App\Pago;
use App\Rol;
use App\User;
use App\Ruta;
use App\Prestamo;
use App\Hoja_ruta;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
      // if(Auth::user()->hasRole('Administrador')){
        $agencia = Agencia::find(1);
        $pagos = Pago::all();
        $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
        $promotores = collect();
        $capital_recuperado = 0;
        $promotor_mas_capital = null;
        $posiciones = array();
        $key = 0;

        if(Auth::user()->hasRole('Promotor')){
          set_time_limit(0);
          $fichas_pago_todos = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                      ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                      ->join('ficha_pago', 'prestamo.id', '=', 'ficha_pago.prestamo_id')
                      // ->join('cliente','cliente.id','=','prestamo.cliente_id')
                      // ->join('persona', 'persona.id', '=','cliente.persona_id')
                      ->where('hoja_ruta.user_id', '=', Auth::user()->id)
                      // ->where('ficha_pago.estado_p', '=',0)
                      ->whereIn('prestamo.estado_p_id',[0,1,2,3,5,10])
                      ->groupBy('ficha_pago.prestamo_id')
                      // ->orderBy('ficha_pago.estado_p', 'ASC')
                      // ->orderBy('ruta.hora', 'ASC')
                      ->get();
// dd($fichas_pago_todos);
          $fichas_pago_hoy = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                      ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                      ->join('ficha_pago', 'prestamo.id', '=', 'ficha_pago.prestamo_id')
                      ->join('cliente','cliente.id','=','prestamo.cliente_id')
                      ->join('persona', 'persona.id', '=','cliente.persona_id')
                      ->where('hoja_ruta.user_id', '=', Auth::user()->id)
                      ->where('ficha_pago.fecha', '=', Carbon::now()->format("Y-m-d"))
                      ->where('ficha_pago.tipo', '=',1)
                      ->where('prestamo.estado_p_id', '=', 5)
                      ->orderBy('ficha_pago.estado_p', 'ASC')
                      ->orderBy('ruta.hora', 'ASC')
                      ->get();

          $ficha_pagos_pendientes = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                      ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                      ->join('ficha_pago', 'prestamo.id', '=', 'ficha_pago.prestamo_id')
                      ->join('cliente','cliente.id','=','prestamo.cliente_id')
                      ->join('persona', 'persona.id', '=','cliente.persona_id')
                      ->where('hoja_ruta.user_id', '=', Auth::user()->id)
                      ->where('ficha_pago.fecha', '<', Carbon::now()->format("Y-m-d"))
                      ->where('ficha_pago.tipo', '=',1)
                      ->where('ficha_pago.estado_p', '=', 0)
                      ->orderBy('ficha_pago.estado_p', 'ASC')
                      ->orderBy('ruta.hora', 'ASC')
                      ->get();

          $prestamos_vencidos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                ->where('prestamo.estado_p_id','=',10)
                                ->get();
// dd($prestamos_vencidos);
          $prestamos_hoy = $this->indexPromotor();
          $hojas_rutas =  Auth::user()->hoja_ruta;


          return view('home/indexPromotor', compact('fichas_pago_todos', 'hojas_rutas','prestamos_hoy', 'fichas_pago_hoy', 'ficha_pagos_pendientes', 'prestamos_vencidos'));
        }else {

          return view('home/index', compact('agencia', 'pagos', 'promotor_mas_capital'));
        }
    }

    public function apiCreditos()
    {
      $creditos = Prestamo::where('activo',1)->orderBy('id','desc');
      return \Yajra\Datatables\Datatables::of($creditos)
      ->addColumn('Codigo', function($credito){
        return '<a class="client-link" value="'.$credito->id.'">Cre-'. $credito->id .'</a>';
      })
      ->addColumn('Monto', function($credito){
        return  $credito->monto;
      })
      ->addColumn('Estado', function($credito){
        return $credito->obtenerEstado();
      })
      ->addColumn('Clasificacion', function($credito){
        return $credito->obtenerClasificacion();
      })
      ->addColumn('Tipo', function($credito){
        $cuenta=Prestamo::where('cliente_id',$credito->cliente_id)->count();
        if($credito->estado_p_id==11 || $credito->estado_p_id==9)
        {
          return '<a><span class="badge badge-danger pull-center">Innactivo</span></a>';
        }
        else {
          if($cuenta>1)
          {
            if($credito->posible_primer_pago>Carbon::now()->subDays(7))
            {
              return '<a><span class="badge badge-basic pull-center">Renovado</span></a>';
            }
            else {
              return '<a><span class="badge badge-success pull-center">Activo</span></a>';
            }
          }
          else {
            return '<a><span class="badge badge-primary pull-center">Nuevo</span></a>';
          }
        }
      })

      ->addColumn('Plan', function($credito){
        return $credito->plan->nombre;
      })
      ->addColumn('Cliente', function($credito){
        return $credito->cliente->persona->nombre . " " . $credito->cliente->persona->apellido;
      })
      ->addColumn('Acciones', function($credito){
        $ver = '';
        // $ver = '<a href="' . route('home.detallesRapidos',['id' => $credito->id]) . '" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>';
        $ver='<a class="btn btn-sm btn-white btn-bitbucket btn-show-categoria" onclick="detallesRapidos('.$credito->id.')" data-toggle="modal" data-target="#modal-detalles"><i class="fa fa-eye text-success"></i>';
        return $ver;
      })
      ->escapeColumns([])
      ->make(true);
    }

    public function apiAsesor(Request $request)
    {
      $usuarios = User::whereHas('hoja_ruta' , function($query) {
        $query->whereNotNull('user_id');
      });

      return \Yajra\Datatables\Datatables::of($usuarios)
      ->addColumn('Codigo', function($usuario){
        return '<a class="client-link" value="'.$usuario->id.'">Emp-'. $usuario->id .'</a>';
      })
      ->addColumn('Nombre', function($usuario){
        return  $usuario->persona->nombre . '' . $usuario->persona->apellido;
      })
      ->addColumn('Fichas', function($usuario) use ($request){
        $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
        $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
        $sum=0;
        $pagos=Ficha_pago::whereHas('prestamo', function($query) use($usuario){
          $query->whereHas('ruta.hoja_ruta', function($query) use($usuario){
            $query->where('user_id',$usuario->id);
          });
        })->whereBetween('fecha',[$start, $end])->sum('cuota');
        $sum=number_format($pagos,2);
        return 'Q '. $sum;
      })
      ->addColumn('Capital', function($usuario) use ($request){
        $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
        $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
        $sum=0;
        $pagos=Pago::whereHas('prestamo', function($query) use($usuario){
          $query->whereHas('ruta.hoja_ruta', function($query) use($usuario){
            $query->where('user_id',$usuario->id);
          });
        })->whereBetween('fecha',[$start, $end])->sum('monto');
        $sum=number_format($pagos,2);
        return 'Q '. $sum;
      })
      ->addColumn('Recuperado', function($usuario) use ($request){
        $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
        $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
        $sum=0;
        $pagos=Pago::whereHas('prestamo', function($query) use($usuario){
          $query->whereHas('ruta.hoja_ruta', function($query) use($usuario){
            $query->where('user_id',$usuario->id);
          });
        })->whereBetween('fecha',[$start, $end])->sum('capital');
        $sum=number_format($pagos,2);
        return  'Q '.$sum;
      })
      ->addColumn('Interes', function($usuario) use ($request){
        $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
        $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
        $sum=0;
        $pagos=Pago::whereHas('prestamo', function($query) use($usuario){
          $query->whereHas('ruta.hoja_ruta', function($query) use($usuario){
            $query->where('user_id',$usuario->id);
          });
        })->whereBetween('fecha',[$start, $end])->sum('interes');
        $sum=number_format($pagos,2);
        return 'Q '. $sum;
      })
      ->addColumn('Mora', function($usuario) use ($request){
        $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
        $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
        $sum=0;
        $recuperada=Prestamo::whereHas('user',function($query) use($usuario){
          $query->where('user_id',$usuario->id);
        })->sum('mora_recuperada');
        $sum=number_format($recuperada,2);
        return 'Q '. $sum;
      })
      ->addColumn('Pendiente', function($usuario){
        $mora=Prestamo::whereHas('user',function($query) use($usuario){
          $query->where('user_id',$usuario->id);
        })->sum('mora');
        $recuperada=Prestamo::whereHas('user',function($query) use($usuario){
          $query->where('user_id',$usuario->id);
        })->sum('mora_recuperada');
        $pendiente=number_format($mora-$recuperada,2);
        return 'Q '. $pendiente;
      })
      ->addColumn('Acciones', function($usuario){
        $ver = '';
        // $ver = '<a href="' . route('home.detallesRapidos',['id' => $credito->id]) . '" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>';
        $ver='<a class="btn btn-sm btn-white btn-bitbucket btn-show-categoria" onclick="detallesAsesor('.$usuario->id.')" data-toggle="modal" data-target="#modal-asesor"><i class="fa fa-eye text-success"></i>';
        return $ver;
      })
      ->escapeColumns([])
      ->make(true);
    }


    public function detallesRapidos($id)
    {
      $credito = Prestamo::find($id);
      return view('home.partials.modal_detalles', compact('credito'));
    }

    public function detallesAsesor($id)
    {
      $credito = User::find($id);
      return view('home.partials.modal_asesor', compact('credito'));
    }

    public function apiAsesorCredito($id, Request $request)
    {
      $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
      $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
      $creditos = Prestamo::where('activo',1)->whereHas('ruta.hoja_ruta' , function($query) use ($id) {
        $query->where('user_id', $id);
      })->whereHas('ficha_pago', function($query) use($id, $start, $end) {
        $query->whereBetween('fecha', [$start, $end]);
      });

      // dd($creditos);
      return \Yajra\Datatables\Datatables::of($creditos)
      ->addColumn('Codigo', function($credito){
        return '<a class="client-link" value="'.$credito->id.'">Cre-'. $credito->id .'</a>';
      })
      ->addColumn('Monto', function($credito) use ($request, $start, $end){
        // $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
        // $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
        $pagos=Ficha_pago::where('prestamo_id',$credito->id)->whereBetween('fecha',[$start, $end])->sum('cuota');
        // $pagos=$credito->monto;
        return  $pagos;
      })
      ->addColumn('Suma', function($credito) use ($request, $start, $end){
        // $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
        // $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
        $pagos=Ficha_pago::where('prestamo_id',$credito->id)->whereBetween('fecha',[$start, $end])->count();
        // $pagos=$credito->monto;
        return  $pagos;
      })
      ->addColumn('Cuota', function($credito) use ($request, $start, $end){
        // $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
        // $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
        $pagos=Ficha_pago::where('prestamo_id',$credito->id)->first()->cuota;
        // $pagos=$credito->monto;
        return  $pagos;
      })
      ->addColumn('Estado', function($credito){
        return $credito->obtenerEstado();
      })
      ->addColumn('Clasificacion', function($credito){
        return $credito->obtenerClasificacion();
      })
      ->addColumn('Tipo', function($credito){
        $cuenta=Prestamo::where('cliente_id',$credito->cliente_id)->count();
        if($credito->estado_p_id==11 || $credito->estado_p_id==9)
        {
          return '<a><span class="badge badge-danger pull-center">Innactivo</span></a>';
        }
        else {
          if($cuenta>1)
          {
            if($credito->posible_primer_pago>Carbon::now()->subDays(7))
            {
              return '<a><span class="badge badge-basic pull-center">Renovado</span></a>';
            }
            else {
              return '<a><span class="badge badge-success pull-center">Activo</span></a>';
            }
          }
          else {
            return '<a><span class="badge badge-primary pull-center">Nuevo</span></a>';
          }
        }
      })

      ->addColumn('Plan', function($credito){
        return $credito->plan->nombre;
      })
      ->addColumn('Cliente', function($credito){
        return $credito->cliente->persona->nombre . " " . $credito->cliente->persona->apellido;
      })
      ->addColumn('Acciones', function($credito){
        $ver = '';
        // $ver = '<a href="' . route('home.detallesRapidos',['id' => $credito->id]) . '" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>';
        $ver='<a href="'.route('creditos.show', $credito->id).'" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-eye"> Ver</i></a>';
        return $ver;
      })

      ->escapeColumns([])
      ->make(true);
    }

    public function apiCuadre(Request $request)
    {

        $start = Carbon::parse($request->get('fechas1'))->format('Y-m-d');
        $end = Carbon::parse($request->get('fechas2'))->format('Y-m-d');

        $pagosmonto=Pago::whereBetween('fecha',[$start, $end])->sum('monto');
        $summonto=number_format($pagosmonto,2);


        $pagoscapital=Pago::whereBetween('fecha',[$start, $end])->sum('capital');
        $sumcapital=number_format($pagoscapital,2);

        $pagosinteres=Pago::whereBetween('fecha',[$start, $end])->sum('interes');
        $suminteres=number_format($pagosinteres,2);

        $recuperada=Prestamo::sum('mora_recuperada');
        $sumrecuperada=number_format($recuperada,2);

        $mora=Prestamo::sum('mora');
        $pendiente=number_format($mora-$sumrecuperada,2);

        $collect=array(['Capital' => 'Q '.$summonto, 'Recuperado' => 'Q '.$sumcapital, 'Interes' => 'Q '.$suminteres, 'Pendiente' => 'Q '.$sumrecuperada, 'Mora' => 'Q '.$pendiente]);
        // return response()
        //     ->json(['Capital' => $summonto, 'Recuperado' => $sumcapital, 'Interes' => $suminteres, 'Pendiente' => $sumrecuperada, 'Mora' => $pendiente]);
            return DataTables::of($collect)->make(true);
    }


    public function tablaCreditos()
    {
      return view('home.partials.tablas.tablacredito');
    }

    public function tablaAsesor()
    {
      return view('home.partials.tablas.tablaasesor');
    }

    public function tablaGeneral()
    {
      return view('home.partials.tablas.tablageneral');
    }

    public function indexPromotor(){
      $hojas_rutas =  Auth::user()->hoja_ruta;
      $prestamos_hoy = collect();
      foreach($hojas_rutas as $hoja_ruta){
        foreach ($hoja_ruta->prestamos as $prestamo) {
          // code
          $ficha_hoy = $prestamo->ficha_pago->where('fecha', Carbon::now()->format('Y-m-d'))->where('tipo','=',1);
          if($ficha_hoy == null){}
            else{
              $prestamos_hoy = $prestamos_hoy->merge($ficha_hoy);
            }
        }
      }
      return $prestamos_hoy;
    }

    public function prueba(Request $request)
    {
      $valor1 = $request->get('valor1');
      $valor2 = $request->get('valor2');


    }
}
