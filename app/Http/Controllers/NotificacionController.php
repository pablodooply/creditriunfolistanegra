<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notificacion;
use App\Prestamo;
use App\Periodo;
use App\Plan;
use \Carbon\Carbon;
class NotificacionController extends Controller
{
    /**
    * Busca todos los prestamos pendientes, para su enlistado en la parte de
    * notificacion, solo el administrador y supervisor tiene acceso a ella.
    */
    public function pendiente(Request $request){
      $usuario = Auth::user();
      if($usuario->hasRole('Administrador')){
        $prestamos = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        $prestamos_examinados = Prestamo::whereIn('estado_p_id',[15])->orderBy('created_at','DES')->get();
        $proximos = Prestamo::whereIn('estado_p_id',[5,3])->whereBetween('fecha_fin',[Carbon::now(), Carbon::now()->addDays(8)])->get();
        $mora = Prestamo::whereIn('estado_p_id',[5,3, 10])->whereHas('ficha_pago' , function($query) {
          $query->where('ajuste', '>', 0);
        })->whereRaw('mora_recuperada != mora')->get();
      } else if($usuario->hasRole('Supervisor')){
        $prestamos1 = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        if(count($prestamos1)>0)
        {$prestamos = Prestamo::whereIn('estado_p_id',[1])
        ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
          $query->where('supervisor_id', Auth::user()->id);
         })
        ->orderBy('created_at','DES')->get();}
      }
      else {
        $prestamos = Prestamo::whereIn('estado_p_id',[1])
        ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
          $query->where('id',  0);
        });
      }
      if($request->ajax()){
        return view('Notificacion.index', compact('prestamos'));}
      return view('Notificacion.pendientes', compact('prestamos','prestamos_examinados', 'proximos', 'mora'));
    }
    public function proximos(Request $request){
      $usuario = Auth::user();
        $prestamos = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        $prestamos_examinados = Prestamo::whereIn('estado_p_id',[15])->orderBy('created_at','DES')->get();
        $proximos = Prestamo::whereIn('estado_p_id',[5,3])->whereBetween('fecha_fin',[Carbon::now(), Carbon::now()->addDays(8)])->get();
        $mora = Prestamo::whereIn('estado_p_id',[5,3, 10])->whereHas('ficha_pago' , function($query) {
          $query->where('ajuste', '>', 0);
        })->whereRaw('mora_recuperada != mora')->get();
      return view('Notificacion.proximos', compact('prestamos','prestamos_examinados', 'proximos', 'mora'));
    }
    public function pendiente_valoracion(Request $request){
      $usuario = Auth::user();
      if($usuario->hasRole('Administrador')){
        $prestamos = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        $prestamos_examinados = Prestamo::whereIn('estado_p_id',[15])->orderBy('created_at','DES')->get();
        $proximos = Prestamo::whereIn('estado_p_id',[5,3])->whereBetween('fecha_fin',[Carbon::now(), Carbon::now()->addDays(8)])->get();
        $mora = Prestamo::whereIn('estado_p_id',[5,3, 10])->whereHas('ficha_pago' , function($query) {
          $query->where('ajuste', '>', 0);
        })->whereRaw('mora_recuperada != mora')->get();
      } else if($usuario->hasRole('Supervisor')){
        $prestamos1 = Prestamo::whereIn('estado_p_id',[15])->orderBy('created_at','DES')->get();
        if(count($prestamos1)>0)
        {$prestamos = Prestamo::whereIn('estado_p_id',[15])
        ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
          $query->where('supervisor_id', Auth::user()->id);
         })
        ->orderBy('created_at','DES')->get();}
      }
      else {
        $prestamos = Prestamo::whereIn('estado_p_id',[15])
        ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
          $query->where('id',  0);
        });
      }
      // dd($prestamos_examinados);
      if($request->ajax()){
        return view('Notificacion.index_valoracion', compact('prestamos'));}
      return view('Notificacion.pendientes_valoracion', compact('prestamos','prestamos_examinados', 'proximos', 'mora'));
    }
    public function examinados(Request $request){
      $usuario = Auth::user();
        $prestamos = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        $prestamos_examinados = Prestamo::whereIn('estado_p_id',[2])->orderBy('created_at','DES')->get();
        $proximos = Prestamo::whereIn('estado_p_id',[5,3])->whereBetween('fecha_fin',[Carbon::now(), Carbon::now()->addDays(8)])->get();
        $mora = Prestamo::whereIn('estado_p_id',[5,3, 10])->whereHas('ficha_pago' , function($query) {
          $query->where('ajuste', '>', 0);
        })->whereRaw('mora_recuperada != mora')->get();
      return view('Notificacion.examinados', compact('prestamos','prestamos_examinados', 'proximos', 'mora'));
    }
    public function morosos(Request $request){
      $usuario = Auth::user();
        $prestamos = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        $prestamos_examinados = Prestamo::whereIn('estado_p_id',[2])->orderBy('created_at','DES')->get();
        $proximos = Prestamo::whereIn('estado_p_id',[5,3])->whereBetween('fecha_fin',[Carbon::now(), Carbon::now()->addDays(8)])->get();
        $mora = Prestamo::whereIn('estado_p_id',[5,3, 10])->whereHas('ficha_pago' , function($query) {
          $query->where('ajuste', '>', 0);
        })->whereRaw('mora_recuperada != mora')->get();
      return view('Notificacion.mora', compact('prestamos','prestamos_examinados', 'proximos', 'mora'));
    }
    /**
    * muesta la informacion del prestamo para su aprobacion,
    * @param int $id del prestamo que va a mostrar la informacion.
    * @return view que muestra la informacion del credito.
    */
    public function show($id){
      $prestamo = Prestamo::find($id);
      $prestamos_ant = Prestamo::where("cliente_id", '=', $prestamo->cliente_id)->where('id', "!=", $id)->where('estado_p_id', '!=', 11)->get();
      $periodos = Periodo::where('nombre',$prestamo->plan->nombre)->get();
      // $planes = Plan::where('periodo_id',$prestamo->plan->periodo->id)->get();
      $planes = Plan::where('periodo_id',$prestamo->plan->periodo->id)->pluck("capital", "id")->toArray();
      $saldototal = 0;
      foreach($prestamos_ant as $credito)
      {
        $saldo = $credito->monto - $credito->pagado;
        $saldototal = $saldototal + $saldo;
      }
      // dd($saldototal);
      return view('Notificacion.show',compact('prestamo', 'prestamos_ant', 'periodos', 'planes', 'saldototal'));
    }
    public function create(){
      return view('Notificacion.create');
    }
    /**
    * Indica el numero de prestamos pendientes en la barra superior.
    *
    *
    */
    public function num(){
      $usuario = Auth::user();
      if($usuario->hasRole('Administrador')){
        $prestamos = Prestamo::whereIn('estado_p_id',[1]);
        // $prestamos = Notificacion::whereIn('descripcion',['Nuevo'])->where('objeto_type', '=', 'Prestamo')->where('estado', '=', 1);
      } else if($usuario->hasRole('Supervisor')){
        // $prestamos = Notificacion::where('descripcion','=','Nuevo')
        // ->where('objeto_type', '=', 'Prestamo')->where('estado', '=', 1);
        $prestamos1 = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        if(count($prestamos1)>0)
        {$prestamos = Prestamo::whereIn('estado_p_id',[1])
        ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
          $query->where('supervisor_id', Auth::user()->id);
         })
        ->orderBy('created_at','DES')->get();}
        else {
          $prestamos = Prestamo::whereIn('estado_p_id',[1])
          ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
            $query->where('id',  0);
          });
        }
      }
      return $prestamos->count();
    }
    public function num_valoracion(){
      $usuario = Auth::user();
      if($usuario->hasRole('Administrador')){
        $prestamos = Prestamo::whereIn('estado_p_id',[15]);
        // $prestamos = Notificacion::whereIn('descripcion',['Nuevo'])->where('objeto_type', '=', 'Prestamo')->where('estado', '=', 1);
      }
      else if($usuario->hasRole('Supervisor')){
        // $prestamos = Notificacion::where('descripcion','=','Nuevo')
        // ->where('objeto_type', '=', 'Prestamo')->where('estado', '=', 1);
        $prestamos1 = Prestamo::whereIn('estado_p_id',[15])->orderBy('created_at','DES')->get();
        if(count($prestamos1)>0)
        {
          $prestamos = Prestamo::whereIn('estado_p_id',[15])
        ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
          $query->where('supervisor_id', Auth::user()->id);
         })
        ->orderBy('created_at','DES')->get();}
        else {
          $prestamos = Prestamo::whereIn('estado_p_id',[15])
          ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
            $query->where('id',  0);
          });
        }
      }
      return $prestamos->count();
    }
    public function num_proximo(){
      $usuario = Auth::user();
        $prestamos = Prestamo::whereIn('estado_p_id',[5,3])->whereBetween('fecha_fin',[Carbon::now(), Carbon::now()->addDays(8)]);
        // dd($prestamos);
        // $prestamos = Notificacion::whereIn('descripcion',['Nuevo'])->where('objeto_type', '=', 'Prestamo')->where('estado', '=', 1);
      return $prestamos->count();
    }
    public function num_mora(){
      $usuario = Auth::user();
        $prestamos = Prestamo::whereIn('estado_p_id',[5,3, 10])->whereHas('ficha_pago' , function($query) {
          $query->where('ajuste', '>', 0);
        })->whereRaw('mora_recuperada != mora')->get();
        // dd($prestamos);
        // $prestamos = Notificacion::whereIn('descripcion',['Nuevo'])->where('objeto_type', '=', 'Prestamo')->where('estado', '=', 1);
      return $prestamos->count();
    }
}
