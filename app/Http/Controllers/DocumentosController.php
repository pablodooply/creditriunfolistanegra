<?php

namespace App\Http\Controllers;


use App\DataTables\UsersDataTable;
use Illuminate\Http\Request;

use PDF;
use App\Cliente;
use App\Prestamo;
use App\Boleta_pago;
use App\User;

class DocumentosController extends Controller
{
    //
    public function compromiso($id) {
      $credito = Prestamo::find($id);
      $view = \View::make('Documentos/compromiso2', compact('credito'));
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      return $pdf->stream('compromiso.pdf');
    }

    public function imprimir_cheque($id) {
      $credito = Prestamo::find($id);
      return view('Documentos/cheque_impresion', compact('credito'));
    }

    public function ficha($id){
      $credito = Prestamo::find($id);
      $total = $credito->ficha_pago->where('tipo',1)->count();
      if($total%2 == 1){
        $cant1 = ceil($total);
        $cant2 = $total - $cant1;
      } else{
        $cant1 = $total;
        $cant2 = $total;
      }
        $tabla = $credito->fichas_reales();
      // $view = \View::make('Documentos/ficha_pago', compact('credito'))->render();

      return view('Documentos.ficha_pago_modificada', compact('credito', 'tabla','cant1' , 'cant2'));
    }

    public function fichaPDF($id){
      $credito = Prestamo::find(4);

      $view = \View::make('Documentos/ficha_pago_pdf', compact('credito'))->render();
      $dompdf = \App::make('dompdf.wrapper');
      $dompdf->loadHTML($view);
      // $dompdf->setPaper('A4', 'landscape');
      return view('Documentos/ficha_pago_pdf', compact('credito'));
      // return $dompdf->stream('nuevo.pdf');
    }

    public function printCompromiso($id){
      $credito = Prestamo::find($id);
      return view('Documentos/compromiso', compact('credito'));
    }

    public function printPagoColaborador($id){
      $boleta = Boleta_pago::find($id);
      $colaborador = User::find($boleta->users_id);
      return view('Documentos/pagocolaborador', compact('boleta','colaborador'));
    }

    public function printSolicitud($id){
      $credito = Prestamo::find($id);

      return view('Documentos/solicitud', compact('credito'));
    }

    public function tipoCredito($id){
      switch ($id) {
        case 1:
          return $this->printTableCreditos();
          break;
        case 2:
          return $this->pdfTableCreditos();
          break;
      }
    }

    public function printTableCreditos(){
      $tipo = 1;
      $prestamos = Prestamo::with('cliente.persona', 'user')->get();
      $creditos = $prestamos->whereIn('estado_p_id',[3,4,5]);

      return view('Documentos.tablaCreditos', compact('creditos','tipo'));
    }

    public function pdfTableCreditos(){
      $prestamos = Prestamo::with('cliente.persona', 'user')->get();
      $creditos = $prestamos->whereIn('estado_p_id',[3,4,5]);
      $tipo = 2;
      $view = \View::make('Documentos/tablaCreditos', compact('creditos','tipo'))->render();
      $dompdf = \App::make('dompdf.wrapper');
      $dompdf->loadHTML($view);
      $name = 'Documento' . date('Y-m-d H:i:s') . '.pdf';
      return $dompdf->stream($name);
    }


    public function tipoReporte($id){
      switch ($id) {
        case 1:
          return $this->exportReporteClientes();
          break;

        case 2:
          return $this->exportReportePromotores();
          break;

        case 3:
          return $this->exportReportesMoras();
          break;

        case 4:
          return $this->exportReportesRuta();
          break;
      }
    }

    public function exportReporteClientes(){

    }

    public function exportReportePromotores(){}

    public function exportReportesMoras(){}

    public function exportReportesRuta(){}
}
