<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Categoria;
use Illuminate\Support\Facades\Auth;
class CategoriaController extends Controller
{
    public function index()
    {
      return view('categorias.index');
    }

    public function api()
    {
      $categorias = Categoria::select()->where('estado', 1);
      return Datatables::of($categorias)
      ->editColumn('id', function ($categoria) {
          return 'CAT-' . str_pad($categoria->id,6,"0",STR_PAD_LEFT);
      })
      ->addColumn('nombre', function ($categoria) {
          return $categoria->nombre;
      })
      ->addColumn('accion', function ($categoria) {
          $id = $categoria->id;
          return  view('categorias.partials.accion', compact('id'));
      })
      ->rawColumns(['accion'])
      ->setRowClass('text-center')
      ->make(true);
    }

    public function modal_show($id)
    {
      $categoria = Categoria::find($id);
      return view('categorias.partials.cont-modal-show-categoria', compact('categoria'));
    }

    public function modal_edit(Request $request, $id)
    {
      if ($request->isMethod('get'))
      {
        $categoria = Categoria::find($id);
        return view('categorias.partials.cont-modal-edit-categoria', compact('categoria'));
      }
      else
      {
        $categoria = Categoria::find($id);
        $categoria->update([
          'nombre' => $request->get('nombre'),
          'created_by' => Auth::user()->id
        ]);
      }
    }

    public function modal_create(Request $request)
    {
      if ($request->isMethod('get'))
      {
        return view('categorias.partials.cont-modal-create-categoria');
      }
      else
      {
        $categoria = Categoria::create([
          'nombre' => $request->get('nombre'),
          'created_by' => Auth::user()->id
        ]);
      }
    }

    public function delete($id)
    {
      $categoria = Categoria::find($id);
      // dd($categoria->nombre);
      $categoria->update([
        'estado' => 0
      ]);
    }
}
