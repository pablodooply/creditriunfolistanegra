<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use Carbon\Carbon;

use App\Hoja_ruta;
use App\User;
use App\Pago;
use App\Ficha_pago;
use App\Rol;

class RutaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::user()->hasAnyRole('Administrador','Secretaria')){
        $rutas = Hoja_ruta::with('user', 'ruta.prestamo.pagos')->get();
      }else{
        $rutas = Auth::user()->hoja_supervisor();
      }
      return view('Ruta.index', compact('rutas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $rutas = Hoja_ruta::all();
        $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
        $promotores = collect();
        foreach ($roles as $rol) {
          $promotores = $promotores->merge($rol->users);
        }
        return view('Ruta.create', compact('rutas','promotores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rutaNew = Hoja_ruta::create([
          'nombre' => $request->input('nombre'),
          'user_id' => $request->input('promotor'),
        ]);

        return redirect()->route('rutas.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $hoja_ruta = Hoja_ruta::where('id', '=', $id)->with(['prestamos' => function($query){
          return $query->whereIn('estado_p_id',[3,4,5,9,10]);
        }])->get()->first();

        // $ficha_pago = Ficha_pago::where('fecha', '=', Carbon::now()->format('Y-m-d'))->get();
        // $pagos = Pago::where('fecha', '=', Carbon::now()->format('Y-m-d'));
        return view('Ruta.show', compact('hoja_ruta'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cierre($id)
    {
      $hoja_ruta = Hoja_ruta::findorfail($id);
      $collecPagos = collect();
      foreach ($hoja_ruta->prestamos as $prestamo) {
        # code...
        $pagos_filtrados = $prestamo->pagos->where('fecha', '=',Carbon::now()->format('Y-m-d'))->get();
        $collecPagos = $collecPagos->merge($pagos_filtrados);
      }
      // $ficha_pago = Ficha_pago::where('fecha', '=', Carbon::now()->format('Y-m-d'))->get();
      // $pagos = Pago::where('fecha', '=', Carbon::now()->format('Y-m-d'));
      return view('Ruta.cierre', compact('hoja_ruta'));
    }

    public function infoPromotor(Request $request,$id){
      $hoja_ruta = Hoja_ruta::find($id);
        $promotor = $hoja_ruta->user;
        $nombre = $promotor->persona->nombre . " " . $promotor->persona->apellido;

      return $nombre;

    }

    public function transferir(Request $request){
      $idRuta = $request->input('idRuta');
      $idPromotor = $request->input('idPromotor');

      $hoja_ruta = Hoja_ruta::find($idRuta);

      $hoja_ruta->update([
        'user_id' => $idPromotor,
      ]);

      $rutas = Hoja_ruta::with('user', 'ruta.prestamo.pagos')->get();

      // return view('Ruta.tables.tableIndex' , compact('rutas'));
      return redirect()->route('rutas.index');
    }

    public function transferenciaRuta($id){
      $hoja_ruta = Hoja_ruta::find($id);
      $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
      $promotores = collect();
      foreach ($roles as $rol) {
        $promotores = $promotores->merge($rol->users);
      }
      return view('Ruta.transferir', compact('hoja_ruta', 'hojas_rutas', 'promotores'));
    }


    public function modalSupervisor(){
      $roles = Rol::whereIn('nombre',['Supervisor'])->get();
      $supervisores = collect();
      foreach ($roles as $rol) {
        $supervisores = $supervisores->merge($rol->users);
      }
      $rutas = Hoja_ruta::where('activa',1)->get();

      return view('Ruta.modalSupervisor', compact('supervisores', 'rutas'));
    }

    public function asignacion(Request $request){
      $supervisor = User::find($request->input('super'));
      $arrayRutas = $request->input('input');

      if($arrayRutas != null){
        foreach ($arrayRutas as $idRuta) {
          $ruta = Hoja_ruta::find($idRuta);
          $ruta->supervisor_id = $supervisor->id;
          $ruta->push();
        }
      }
      return redirect()->back();
    }
    
    public function editar_rutas(Request $request){
      $idRuta = $request->get('id');
      $ruta = Hoja_ruta::find($idRuta);

      $ruta->update([
        'nombre'  => $request->get('observacion'),
      ]);
    }


}
