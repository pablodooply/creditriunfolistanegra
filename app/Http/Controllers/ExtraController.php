<?php

namespace App\Http\Controllers;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Gasto;
use App\User;
use App\Tipo_Ingreso;
use App\Ingreso;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ExtraController extends Controller
{
    public function indexGastos()
    {
      $usuarios=User::all();
      return view('extras.gasto.index', compact('usuarios'));
    }

    public function storeGastos(Request $request)
    {
      // dd($request->tipo_gasto);
      if($request->tipo_gasto=='Planilla')
      {
        if($request->file('recibo') == null){
          $gasto=Gasto::create([
            'tipo_gasto'=>$request->tipo_gasto,
            'monto'=>$request->monto,
            'cantidad'=>$request->cantidad,
            'descripcion'=>$request->descripcion,
            'proveedor'=>$request->proveedor,
            'mes'=>$request->mes,
            'colaborador'=>$request->colaborador,
            'created_by'=>Auth::user()->id,
          ]);
        } else{
          $destinoPath = public_path('//images//extras//gastos');
          $recibo = $request->file('recibo');
          $nombre_recibo  = 'Recibo' . time() . '.' . $recibo->getClientOriginalExtension();
          $recibo->move($destinoPath, $nombre_recibo);
          $gasto=Gasto::create([
            'tipo_gasto'=>$request->tipo_gasto,
            'monto'=>$request->monto,
            'cantidad'=>$request->cantidad,
            'descripcion'=>$request->descripcion,
            'foto_recibo'=>$nombre_recibo,
            'proveedor'=>$request->proveedor,
            'mes'=>$request->mes,
            'colaborador'=>$request->colaborador,
            'created_by'=>Auth::user()->id,
          ]);
        }
        return redirect()->back()->with('message', 'Gasto ingresado correctamente');
      }
      else {
        if($request->file('recibo') == null){
          $gasto=Gasto::create([
            'tipo_gasto'=>$request->tipo_gasto,
            'monto'=>$request->monto,
            'cantidad'=>$request->cantidad,
            'descripcion'=>$request->descripcion,
            'proveedor'=>$request->proveedor,
            'created_by'=>Auth::user()->id,
          ]);
        } else{
          $destinoPath = public_path('//images//extras//gastos');
          $recibo = $request->file('recibo');
          $nombre_recibo  = 'Recibo' . time() . '.' . $recibo->getClientOriginalExtension();
          $recibo->move($destinoPath, $nombre_recibo);
          $gasto=Gasto::create([
            'tipo_gasto'=>$request->tipo_gasto,
            'monto'=>$request->monto,
            'cantidad'=>$request->cantidad,
            'descripcion'=>$request->descripcion,
            'foto_recibo'=>$nombre_recibo,
            'proveedor'=>$request->proveedor,
            'created_by'=>Auth::user()->id,
          ]);
        }
        return redirect()->back()->with('message', 'Gasto ingresado correctamente');
      }

    }

    public function apiGasto()
    {
      $gastos = Gasto::orderBy('id','desc');
      // dd($gastos);
      return \Yajra\Datatables\Datatables::of($gastos)
      ->editColumn('id', function ($gasto) {
          return '<strong>GTO-' . str_pad($gasto->id,6,"0",STR_PAD_LEFT).'</strong>';
      })
      ->editColumn('proveedor', function ($gasto) {
        $prov=$gasto->proveedor;
        if($gasto->tipo_gasto=='Planilla')
        {
          $colaborador=User::find($gasto->colaborador);
          return '<a><span class="badge badge-info pull-center">'.$colaborador->name.'</span></a>';
        }
        else {
          if($prov=="")
          {
            return '<a><span class="badge badge-danger pull-center">No ingresado</span></a>';
          }
          else {
            return $gasto->proveedor;
          }
        }

      })
      ->editColumn('cantidad', function ($gasto) {
        if($gasto->tipo_gasto=='Planilla')
        {
          return '<a><span class="badge badge-info pull-center">'.$gasto->mes.'</span></a>';
        }
        else {
          return $gasto->cantidad;
        }
      })
      ->editColumn('tipo_gasto', function ($gasto) {
        if($gasto->tipo_gasto=='Oficina')
        {
          return '<a><span class="badge badge-warning pull-center">Oficina</span></a>';
        }
        if($gasto->tipo_gasto=='Comun')
        {
          return '<a><span class="badge badge-basic pull-center">Comun</span></a>';
        }
        if($gasto->tipo_gasto=='Planilla')
        {
          return '<a><span class="badge badge-primary pull-center">Planilla</span></a>';
        }
      })
      ->editColumn('created_by', function ($gasto) {
        $user=User::where('id',$gasto->created_by)->first();
          return $user->name;
      })
      ->editColumn('acciones', function ($gasto) {
        if(is_null($gasto->foto_recibo))
        {
          return '<a id="edit_gasto" value="'.$gasto->id.'" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>'.'<a value="'. $gasto->id .'" onclick="eliminargasto('. $gasto->id .')" class="btn btn-outline btn-danger btn-sm eliminargasto"><i class="fa fa-trash"></i></a>';
        }
        else {
          return '<a id="edit_gasto" value="'.$gasto->id.'" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>'.'<a value="'. $gasto->id .'" onclick="eliminargasto('. $gasto->id .')" class="btn btn-outline btn-danger btn-sm eliminargasto"><i class="fa fa-trash"></i></a>'.'<a class="btn btn-outline btn-warning btn-sm" id="foto_gasto" value="'.$gasto->id.'"><i class="fa fa-camera"></i></a>';
        }

      })
      ->escapeColumns([])
      ->make(true);

    }

    public function gastosEdit($id)
    {
      $gasto=Gasto::find($id);
      $usuario=User::find($gasto->colaborador);
      if($gasto->tipo_gasto=='Planilla')
      {
        return response(['gasto_id'=>$gasto->id, 'tipo'=>$gasto->tipo_gasto, 'total'=>$gasto->monto, 'descripcion'=>$gasto->descripcion, 'mes'=>$gasto->mes, 'usuario'=>$usuario->id]);
      }
      else {
        if($gasto->proveedor=="")
        {
          return response(['gasto_id'=>$gasto->id, 'tipo'=>$gasto->tipo_gasto, 'total'=>$gasto->monto, 'descripcion'=>$gasto->descripcion, 'mes'=>$gasto->mes, 'cantidad'=>$gasto->cantidad]);
        }
        else {
          return response(['gasto_id'=>$gasto->id, 'tipo'=>$gasto->tipo_gasto, 'total'=>$gasto->monto, 'descripcion'=>$gasto->descripcion, 'mes'=>$gasto->mes, 'cantidad'=>$gasto->cantidad, 'proveedor'=>$gasto->proveedor]);
        }
      }
    }

    public function gastosFoto($id)
    {
      $gasto=Gasto::find($id);
      // dd($gasto->foto_recibo);
      $foto=asset('images/extras/gastos/'.$gasto->foto_recibo);
      return response(['foto'=>$foto]);
    }

    public function gastosUpdate(Request $request)
    {
      // dd($request);
      $gasto=Gasto::find($request->gasto_id);
      if($request->tipo_gasto_edit=='Planilla')
      {
        if($request->file('recibo_edit') == null){
          $gasto->update([
            'tipo_gasto'=>$request->tipo_gasto_edit,
            'monto'=>$request->monto_edit,
            'cantidad'=>$request->cantidad_edit,
            'descripcion'=>$request->descripcion_edit,
            'proveedor'=>$request->proveedor_edit,
            'mes'=>$request->mes_edit,
            'colaborador'=>$request->colaborador_edit,
            'updated_by'=>Auth::user()->id,
            'updated_at'=>Carbon::now()->toDateString(),
          ]);
        } else{
          $destinoPath = public_path('//images//extras//gastos');
          $recibo = $request->file('recibo_edit');
          $nombre_recibo  = 'Recibo_update' . time() . '.' . $recibo->getClientOriginalExtension();
          $recibo->move($destinoPath, $nombre_recibo);
          $gasto->update([
            'tipo_gasto'=>$request->tipo_gasto_edit,
            'monto'=>$request->monto_edit,
            'cantidad'=>$request->cantidad_edit,
            'descripcion'=>$request->descripcion_edit,
            'foto_recibo'=>$nombre_recibo,
            'proveedor'=>$request->proveedor_edit,
            'mes'=>$request->mes_edit,
            'colaborador'=>$request->colaborador_edit,
            'updated_by'=>Auth::user()->id,
            'updated_at'=>Carbon::now()->toDateString(),
          ]);
        }
        return redirect()->back()->with('message', 'Gasto editado correctamente');
      }
      else {
        if($request->file('recibo_edit') == null){
          $gasto->update([
            'tipo_gasto'=>$request->tipo_gasto_edit,
            'monto'=>$request->monto_edit,
            'cantidad'=>$request->cantidad_edit,
            'descripcion'=>$request->descripcion_edit,
            'proveedor'=>$request->proveedor_edit,
            'updated_by'=>Auth::user()->id,
            'updated_at'=>Carbon::now()->toDateString(),
          ]);
        } else{
          $destinoPath = public_path('//images//extras//gastos');
          $recibo = $request->file('recibo_edit');
          $nombre_recibo  = 'Recibo_update' . time() . '.' . $recibo->getClientOriginalExtension();
          $recibo->move($destinoPath, $nombre_recibo);
          $gasto->update([
            'tipo_gasto'=>$request->tipo_gasto_edit,
            'monto'=>$request->monto_edit,
            'cantidad'=>$request->cantidad_edit,
            'descripcion'=>$request->descripcion_edit,
            'foto_recibo'=>$nombre_recibo,
            'proveedor'=>$request->proveedor_edit,
            'updated_by'=>Auth::user()->id,
            'updated_at'=>Carbon::now()->toDateString(),
          ]);
        }
        return redirect()->back()->with('message', 'Gasto editado correctamente');
    }
  }
  public function eliminargasto(Request $request)
  {
      $gasto_id= $request->get('valor');
      $gasto   = Gasto::find($gasto_id);
      $deletegasto  = $gasto->delete();
  }

  //Capital secundario

  public function indexCapital()
  {
    return view('extras.capital.index');
  }

  public function apiCapital(Request $request)
  {

      $start = Carbon::parse($request->get('fecha1'))->toDateTimeString();
      $end = Carbon::parse($request->get('fecha2'))->toDateTimeString();

      $Gasto=Gasto::whereBetween('created_at',[$start, $end])->sum('monto');
      $totalGasto=round($Gasto,2);

      $Ingreso=Ingreso::whereBetween('created_at',[$start, $end])->sum('monto');
      $totalIngreso=round($Ingreso,2);

      $capitalSecundario=$totalIngreso-$totalGasto;

      $collect=array(['Capital' => 'Q '.$capitalSecundario, 'Gasto' => 'Q '.$totalGasto, 'Ingreso' => 'Q '.$totalIngreso]);
      // return response()
      //     ->json(['Capital' => $summonto, 'Recuperado' => $sumcapital, 'Interes' => $suminteres, 'Pendiente' => $sumrecuperada, 'Mora' => $pendiente]);
          return DataTables::of($collect)->make(true);
  }

  //Ingresos

  public function indexIngreso()
  {
    $tipo_ingreso=Tipo_Ingreso::all();
    $usuarios=User::all();
    return view('extras.ingreso.index', compact('tipo_ingreso', 'usuarios'));
  }

  public function storeIngreso(Request $request)
  {
    // dd($request);
    if($request->file('recibo') == null){
      $ingreso=Ingreso::create([
        'monto'=>$request->monto,
        'descripcion'=>$request->descripcion,
        'tipo_ingreso_id'=>$request->tipo_ingreso,
        'proveedor'=>$request->proveedor,
        'cantidad'=>$request->cantidad,
        'created_by'=>Auth::user()->id,
      ]);
    } else{
      $destinoPath = public_path('//images//extras//ingresos');
      $recibo = $request->file('recibo');
      $nombre_recibo  = 'Recibo' . time() . '.' . $recibo->getClientOriginalExtension();
      $recibo->move($destinoPath, $nombre_recibo);
      $ingreso=Ingreso::create([
        'monto'=>$request->monto,
        'descripcion'=>$request->descripcion,
        'tipo_ingreso_id'=>$request->tipo_ingreso,
        'proveedor'=>$request->proveedor,
        'cantidad'=>$request->cantidad,
        'foto_recibo'=>$nombre_recibo,
        'created_by'=>Auth::user()->id,
      ]);
    }
    return redirect()->back()->with('message', 'Ingreso almacenado correctamente');
  }

  public function tipo_gasto_modal(Request $request)
  {
    if ($request->isMethod('get'))
    {
      return view('extras.ingreso.partials.tipo_gasto_create');
    }
    else
    {
      // dd($request);
      $tipo_ingreso = Tipo_Ingreso::create([
        'nombre' => $request->nombre,
        'descripcion' => $request->descripcion,
        'created_by' => Auth::user()->id
      ]);
      return redirect()->back()->with('message', 'Tipo de ingreso almacenado correctamente');
    }
  }

  public function tablaIngreso()
  {
    return view('extras.ingreso.partials.table.ingreso');
  }


  public function apiIngreso()
  {
    $ingresos = Ingreso::orderBy('id','desc');
    // dd($gastos);
    return \Yajra\Datatables\Datatables::of($ingresos)
    ->editColumn('id', function ($ingreso) {
        return '<strong>ING-' . str_pad($ingreso->id,6,"0",STR_PAD_LEFT).'</strong>';
    })
    ->editColumn('proveedor', function ($ingreso) {
      $prov=$ingreso->proveedor;

      if($prov=="")
      {
        return '<a><span class="badge badge-danger pull-center">No ingresado</span></a>';
      }
      else {
        return $ingreso->proveedor;
      }

    })
    ->editColumn('cantidad', function ($ingreso) {
      return $ingreso->cantidad;
    })
    ->editColumn('tipo_ingreso_id', function ($ingreso) {
      $tipo=Tipo_Ingreso::find($ingreso->tipo_ingreso_id);
      return $tipo->nombre;
    })
    ->editColumn('created_by', function ($ingreso) {
      $user=User::where('id',$ingreso->created_by)->first();
        return $user->name;
    })
    ->editColumn('acciones', function ($ingreso) {
      if(is_null($ingreso->foto_recibo))
      {
        return '<a id="edit_ingreso" value="'.$ingreso->id.'" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>'.'<a value="'. $ingreso->id .'" onclick="eliminaringreso('. $ingreso->id .')" class="btn btn-outline btn-danger btn-sm eliminaringreso"><i class="fa fa-trash"></i></a>';
      }
      else {
        return '<a id="edit_ingreso" value="'.$ingreso->id.'" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>'.'<a value="'. $ingreso->id .'" onclick="eliminaringreso('. $ingreso->id .')" class="btn btn-outline btn-danger btn-sm eliminaringreso"><i class="fa fa-trash"></i></a>'.'<a class="btn btn-outline btn-warning btn-sm" id="foto_ingreso" value="'.$ingreso->id.'"><i class="fa fa-camera"></i></a>';
      }

    })
    ->escapeColumns([])
    ->make(true);

  }

  public function tablaTipo()
  {
    return view('extras.ingreso.partials.table.tipo');
  }

  public function apiTipoIngreso()
  {
    $tipos = Tipo_Ingreso::orderBy('id','desc');
    // dd($gastos);
    return \Yajra\Datatables\Datatables::of($tipos)
    ->editColumn('id', function ($tipo) {
        return '<strong>TIP-' . str_pad($tipo->id,6,"0",STR_PAD_LEFT).'</strong>';
    })
    ->editColumn('created_by', function ($tipo) {
      $user=User::where('id',$tipo->created_by)->first();
        return $user->name;
    })
    ->editColumn('acciones', function ($tipo) {
      return '<a id="edit_tipo" value="'.$tipo->id.'" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>'.'<a value="'. $tipo->id .'" onclick="eliminartipoingreso('. $tipo->id .')" class="btn btn-outline btn-danger btn-sm eliminartipoingreso"><i class="fa fa-trash"></i></a>';

    })
    ->escapeColumns([])
    ->make(true);

  }

  public function ingresoEdit($id)
  {
    $ingreso=Ingreso::find($id);
    return response(['ingreso_id'=>$ingreso->id, 'tipo'=>$ingreso->tipo_ingreso_id, 'total'=>$ingreso->monto, 'descripcion'=>$ingreso->descripcion, 'proveedor'=>$ingreso->proveedor, 'cantidad'=>$ingreso->cantidad]);

  }

  public function ingresoUpdate(Request $request)
  {
    // dd($request);
    $ingreso=Ingreso::find($request->ingreso_id);

      if($request->file('recibo_ingreso_edit') == null){
        $ingreso->update([
          'monto'=>$request->monto_ingreso,
          'descripcion'=>$request->descripcion_ingreso,
          'tipo_ingreso_id'=>$request->tipo_ingreso_edit,
          'proveedor'=>$request->proveedor_edit_ingreso,
          'cantidad'=>$request->cantidad_ingreso,
          'updated_by'=>Auth::user()->id,
          'updated_at'=>Carbon::now()->toDateString(),
        ]);
      } else{
        $destinoPath = public_path('//images//extras//ingresos');
        $recibo = $request->file('recibo_ingreso_edit');
        $nombre_recibo  = 'Recibo_update' . time() . '.' . $recibo->getClientOriginalExtension();
        $recibo->move($destinoPath, $nombre_recibo);
        $ingreso->update([
          'monto'=>$request->monto_ingreso,
          'descripcion'=>$request->descripcion_ingreso,
          'tipo_ingreso_id'=>$request->tipo_ingreso_edit,
          'proveedor'=>$request->proveedor_edit_ingreso,
          'cantidad'=>$request->cantidad_ingreso,
          'foto_recibo'=>$nombre_recibo,
          'updated_by'=>Auth::user()->id,
          'updated_at'=>Carbon::now()->toDateString(),
        ]);
      }
      return redirect()->back()->with('message', 'Ingreso actualizado correctamente');
    }

    public function eliminarIngreso(Request $request)
    {
        $ingreso_id= $request->get('valor');
        $ingreso   = Ingreso::find($ingreso_id);
        $deletegasto  = $ingreso->delete();
    }

    public function ingresoFoto($id)
    {
      $ingreso=Ingreso::find($id);
      // dd($gasto->foto_recibo);
      $foto=asset('images/extras/ingresos/'.$ingreso->foto_recibo);
      return response(['foto'=>$foto]);
    }

    public function tipoIngresoEdit($id)
    {
      $tipo_ingreso=Tipo_Ingreso::find($id);
      return response(['tipo_id'=>$tipo_ingreso->id, 'nombre'=>$tipo_ingreso->nombre, 'descripcion'=>$tipo_ingreso->descripcion]);

    }

    public function tipoIngresoUpdate(Request $request)
    {
      // dd($request);
      $tipo=Tipo_Ingreso::find($request->tipo_ingreso_id);
      $tipo->update([
        'nombre'=>$request->nombretipo,
        'descripcion'=>$request->descripcion_tipo,
        'updated_by'=>Auth::user()->id,
        'updated_at'=>Carbon::now()->toDateString(),
      ]);

        return redirect()->back()->with('message', 'Tipo ingreso actualizado correctamente');
      }

      public function eliminarTipoIngreso(Request $request)
      {
          $tipo_id= $request->get('valor');
          $tipo   = Tipo_Ingreso::find($tipo_id);
          $ingresos=Ingreso::where('tipo_ingreso_id',$tipo->id)->delete();
          $deleteTipo  = $tipo->delete();
      }
}
