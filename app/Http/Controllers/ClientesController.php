<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use Yajra\Datatables\Datatables as Datatable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Cliente;
use App\Persona;
use App\Rol;
use App\Hoja_ruta;
use App\States;
use App\Location;
use DB;
use App\Prestamo;
class ClientesController extends Controller
{
    /**
     * Muestra el listado de clientes, se filtra por los tipos de usuario
     * si es administrador y secretaria, se muetra todos los creditos y
     * si es otro usuario se filtra por los creditos que tiene asignados o
     * si es supervisor.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if($user->hasAnyRole(['Administrador', 'Secretaria'])){
          //Muestra el listado de clientes, y se tiene una lista de si estan bloqueados
          //o no
          $clientes = Cliente::where('listabn_id','=',1)->get();
          $clientes_listaNegra = Cliente::where('listabn_id', '=', '2')->get();
          return view('Clientes.index',compact('clientes', 'clientes_listaNegra'));
        } else if($user->hasRole('Promotor')){
          $clientes = Cliente::join('persona', 'persona.id', '=', 'cliente.persona_id')
                             ->join('prestamo', 'cliente.id', '=', 'prestamo.cliente_id')
                             ->join('ruta', 'prestamo.id', '=', 'ruta.prestamo_id')
                             ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                             ->where('hoja_ruta.user_id', '=',$user->id)
                             ->where('cliente.listabn_id', '=', 1)
                             ->select('cliente.*')
                             ->get();
          $clientes_listaNegra = Cliente::join('persona', 'persona.id', '=', 'cliente.persona_id')
                             ->join('prestamo', 'cliente.id', '=', 'prestamo.cliente_id')
                             ->join('ruta', 'prestamo.id', '=', 'ruta.prestamo_id')
                             ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                             ->where('hoja_ruta.user_id', '=',$user->id)
                             ->where('cliente.listabn_id', '=', 2)
                             ->select('cliente.*')
                             ->get();
          return view('Clientes.index',compact('clientes', 'clientes_listaNegra'));
        } else if($user->hasRole('Supervisor')){
          /**
          * En el usuario supervisor, se trae todas las rutas que tiene asignados
          * para cobrar o para supervisar, depues se unen todas las ruta y por
          * ultimo se dividen en cuales estan bloqueados y cuales estan activos.
          */
          $rutas = $user->rutas_supervisor()->get();
          $array = array();
          foreach($rutas as $ruta){
            if(isset($ruta->prestamo->cliente))
            {
              $array[] = $ruta->prestamo->cliente;
            }
          }
          $tempClientes = collect($array);
          // $tempClientes = $tempClientes->groupBy('Cliente.id');
          $clientes = $tempClientes->where('listabn_id', 1);
          $clientes_listaNegra = $tempClientes->where('listabn_id', 2);
          return view('Clientes.index',compact('clientes', 'clientes_listaNegra'));
        }
  }
    /**
    * Tabla de clientes para DataTables, en este metodo se declara los filtros
    * para la tabla y la obtencion de datos.
    */
    public function tableCliente(Request $request){
      $user = Auth::user();
      if($user->hasAnyRole(['Administrador', 'Secretaria'])){
      $start = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
      $end = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
      $clientes = Cliente::where('listabn_id','=',1)->with('persona:id,nombre,apellido,dpi')->with('prestamos')->whereBetween('created_at',[$start, $end])->get();
      // $clientes_listaNegra = Cliente::where('listabn_id', '=', '2')->get();
      }
      else if($user->hasRole('Promotor')){
        $clientes = Cliente::join('persona', 'persona.id', '=', 'cliente.persona_id')
                           ->join('prestamo', 'cliente.id', '=', 'prestamo.cliente_id')
                           ->join('ruta', 'prestamo.id', '=', 'ruta.prestamo_id')
                           // ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                           ->where('hoja_ruta.user_id', '=',$user->id)
                           ->where('cliente.listabn_id', '=', 1)
                           ->select('cliente.*')
                           ->groupBy('cliente.id')
                           ->get();
                           // dd($clientes);
        $clientes_listaNegra = collect();
        } else if($user->hasRole('Supervisor')){
        $rutas = Hoja_ruta::where('supervisor_id', $user->id)->get();
        // dd($rutas);
        $arregloRutas = [];
        foreach ($rutas as $ruta) {
          $arregloRutas = array_merge($arregloRutas, [$ruta->id]);
        }
        // dd($arregloRutas);
        $clientes = Cliente::where('listabn_id','=',1)
                           ->with('persona:id,nombre,apellido,dpi')
                           ->with('prestamos')
                           ->whereHas('prestamos', function($q) use($user, $arregloRutas)
                           {
                              // $q->where('prestamo.user_id', $user->id) ;
                              $q->whereHas('ruta', function($u) use($user, $arregloRutas)
                              {
                                $u->whereIn('ruta.hoja_ruta_id', $arregloRutas);
                              });
                           })->get();
      }
      $prestamos=[];
      $cuentas=[];
      foreach ($clientes as $cliente) {
        $cliente['cuenta']=Prestamo::where('cliente_id',$cliente->id)->count();
        if($cliente['cuenta']>0)
        {
          $cliente['fecha_inicio']=Prestamo::where('cliente_id',$cliente->id)->orderBy('id', 'DESC')->first()->fecha_inicio;
        }
        else {
          $cliente['fecha_inicio']=Carbon::now()->subDays(8);
        }
      }
      return DataTable::of($clientes)
      ->addColumn('Codigo', function($cliente){
        return '<a class="client-link" value="'.$cliente->id.'">Cli-'. $cliente->id .'</a>';
      })
      ->addColumn('clasificacion', function($cliente){
        if (!is_null($cliente->prestamos->last())) {
          return $cliente->prestamos->last()->obtenerClasificacion2();
        }
        else {
          return '<p class="text-right"><span class="badge badge-success">A</span></p>';
        }
      })
      ->addColumn('Nombre', function($cliente){
        return isset($cliente->persona->nombre) ? $cliente->persona->nombre . " " . $cliente->persona->apellido : 'No se ingresó';
      })
      ->addColumn('Fecha', function($cliente){
        return Carbon::parse($cliente->created_at)->format('d-m-Y');
      })
      ->addColumn('Tipo', function($cliente){
        if($cliente->cuenta <= 1 && $cliente->fecha_inicio > Carbon::now()->subDays(7))
        {
          return '<a><span class="badge badge-primary pull-center">Nuevo</span></a>';
        }
        else {
          return '<a><span class="badge badge-success pull-center">Activo</span></a>';
        }
      })
      ->addColumn('DPI', function($cliente){
        return isset($cliente->persona->dpi) ? $cliente->persona->dpi : 'No se ingresó';
      })
      ->addColumn('Prestamo', function($cliente){
        if(isset($cliente->prestamos->last()->id))
        {
          return 'Cre-'. $cliente->prestamos->last()->id;
        }
        else
        {
          return 'No hay';
        }
      })
      ->addColumn('Acciones', function($cliente) use($user){
          $cadena ="";
         if($user->hasAnyRole(['Administrador','Supervisor'])){
            $cadena = '<a href="'. route('clientes.edit', $cliente->id).'" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>';
          }
        return '<a href="' . route('clientes.show', $cliente->id).'" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>' . ($cadena ? $cadena : "");
      })
      ->escapeColumns([])
      ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Formulario para crear cliente
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Creacion del cliente
    }
    /**
     * Metodo que muestra la informacion de un cliente en especifico, si es
     * una peticion ajax, devuelve un html, con la "vista previa" del cliente,
     * y si no lo es devulve la vista correspondiente a al cliente.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request ,$id)
    {
        //Detalle de un cliente, perfil y hoja de pago
        if($request->ajax()){
          $cliente = Cliente::find($id);
          $clasificacion = $cliente->clasificacion_id ? $cliente->clasificacion_id : 0;
          $html1 =
'            <div class="tab-content">
              <div id="contact-1" class="tab-pane active">
                  <div class="row m-b-lg">
                      <div class="col-lg-12 text-center">';
                      $html12 = "";
                      switch  ($clasificacion) {
                        case 0:
                          $html12 = '<p class="text-right"><span class="badge badge-success">P</span></p>';
                        case 1:
                          # code...
                         $html12 ='  <p class="text-right"><span class="badge badge-primary">A</span></p>';
                          break;
                        case 2:
                          # code...
                          $html12 ="<p class='text-right'><span class='badge badge-warning'>B</span></p>";
                          break;
                        case 3:
                          # code...
                         $html12 ="<p class='text-right'><span class='badge badge-danger'>C</span></p>";
                          break;
                      };
                      $html13='
                          <h2>'. $cliente->persona->nombre.' ' .$cliente->persona->apellido. '</h2>
                      </div>
                  </div>
                  <div class="client-detail">
                    <div class="full-height-scroll">
                        <strong>Datos Personales</strong>
                        <ul class="list-group clear-list">
                            <li class="list-group-item fist-item">
                                <span class="pull-right"> 24 </span>
                                Edad
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right"> '.$cliente->persona->dpi.' </span>
                                Dpi
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">'.$cliente->persona->nit.'</span>
                                Nit
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">'.$cliente->persona->telefono.'</span>
                                Telefono
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">'.$cliente->persona->celular1.'</span>
                                Celular 1
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">'.$cliente->persona->celular2.'</span>
                                Celular 2
                            </li>
                        </ul>
                        <strong>Prestamos</strong>
                        <table class="table table-striped table-hover">
                          <thead>
                            <tr>
                              <th>Codigo</th>
                              <th>Estado</th>
                              <th>Clasificacion</th>
                            </tr>
                          </thead>
                          <tbody>;
                            ';
                            $html2 ='';
                            foreach ($cliente->prestamos as $prestamo) {
                              $html2 = $html2 . '<tr><td>Cre-'.$prestamo->id .'</td>';
                                $html2 = $html2 . $prestamo->obtenerEstado();
                                $html2 = $html2 . $prestamo->obtenerClasificacion();
                            };
                            $html3 ='
                          </tbody>
                        </table>
                    </div>
                  </div>
              </div>
            </div>
          ';
          $html4 = $html1 . $html12 . $html13 . $html2 . $html3;
                        return $html4;
        }
        $cliente = Cliente::find($id);
        $hoja_rutas = Hoja_ruta::with('user')->where('activa', '=', 1)->get();
        return view('Clientes.show',compact('cliente', 'hoja_rutas'));
    }
    /**
     * Muesta la informacion para editar un cliente.
     *
     * @param  int  recibe un id del cliente en especifico.
     * @return \Illuminate\Http\view una vista que contiene la informacion del cliente y los estados.
     */
    public function edit($id)
    {
        //Editar informacion de un cliente en especifico
        $cliente = Cliente::find($id);
        $states = States::all();
        return view('Clientes.edit',compact('cliente', 'states'));
    }
    /**
     * Actualiza un cliente en especifico, despues de la edicion, se realiza la
     * actualizacion de datos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Actualizacion de datos cliente
        // Creacion de cliente
        $clienteUp = Cliente::find($id);
        $clienteUp->update([
          'empresa_trabajo' => $request->input('Empresa'),
          'direccion_trabajo' => $request->input('direccion_trabajo'),
          'direccion_recibo' => $request->input('direccion_luz'),
          'direccion_cobrar' => $request->input('direccion_cobrar'),
          'nombre_recibo' => $request->input('recibo_luz'),
          'tiempo_trabajando' => $request->input('tiempo_trabajando'),
          'estado_civil' => $request->input('estado_civil'),
          'salario' => $request->input('salario'),
          'no_hijos' => $request->input('no_hijos') ? $request->input('no_hijos') : 0,
          'tipo_casa' => $request->input('tipo_casa'),
          'actividad' => $request->input('actividad'),
        ]);
        // Creacion del perfil del cliente
        $clienteUp->persona->update([
          'nombre' => $request->input('nombre'),
          'apellido' => $request->input('apellido'),
          'dpi' => $request->input('dpi'),
          'nit' => $request->input('nit'),
          'genero' => $request->get('genero'),
          'fecha_nacimiento' => $request->input('fecha_nacimiento'),
          'telefono' => $request->input('telefono'),
          'celular1' => $request->input('celular1'),
          'celular2' => $request->input('celular2'),
          'domicilio' => $request->input('direccion'),
          'foto_perfil' => $request->input('foto'),
        ]);
        //Se obtiene la localizacion del cliente
        $idLocation = $clienteUp->persona->location_id;
        $location = Location::find($idLocation);
        $location->update([
          'states_id' => $request->get('state'),
          'cities_id' => $request->get('city'),
        ]);
        /**
        * Se obtiene la ruta de las imagenes, y despues se verifica si fue actualizada
        * si fue actualizada se procede a subirla y cambiarla, en caso contrario se queda
        * con las imagenes de antes.
        */
        $destinoPath = public_path('//images//clientes//documentos');
        $nombre_imagen_dpi = "";
        $nombre_imagen_recibo = "";
        $nombre_imgen_firma = "";
        if($request->file('foto_dpi') == null){
        } else{
        $imagen_dpi = $request->file('foto_dpi');
        $nombre_imagen_dpi  = $request->input('nombre'). 'DPI' . time() . '.' . $imagen_dpi->getClientOriginalExtension();
        $imagen_dpi->move($destinoPath, $nombre_imagen_dpi);
        }
        if($request->file('foto_recibo') == null){
        } else{
        $imagen_recibo = $request->file('foto_recibo');
        $nombre_imagen_recibo  = $request->input('nombre') . 'RECIBO' .time() . '.' . $imagen_recibo->getClientOriginalExtension();
        $imagen_recibo->move($destinoPath, $nombre_imagen_recibo);
        }
        if($request->file('foto_firma') == null){
        } else{
        $imagen_firma = $request->file('foto_firma');
        $nombre_imagen_firma  = $request->input('nombre') . 'RECIBO' .time() . '.' . $imagen_firma->getClientOriginalExtension();
        $imagen_firma->move($destinoPath, $nombre_imagen_firma);
        }
        return back()->withInput();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Eliminar cliente
    }
    /**
     * funcion para obtener a los cumpleañeros en una determinada fecha, por defecto
     * se muestra los cumpleañeros de 15 dias antes y despues. Solo la pantalla de inicio
     * de cumpleañeros.
     */
    public function cumpleanios(Request $request){
      $inicio = Carbon::now()->subDay(15)->format("m-d");
      $fin = Carbon::now()->addDay(15)->format("m-d");
      $day = date('d', strtotime($inicio));
      $month = date('m', strtotime($inicio));
// dd($inicio);
      if(Auth::user()->hasAnyRole(['Administrador','Secretaria','Supervisor'])){
        $personas = Persona::where(function($query) use ($inicio, $fin){
          $query->whereBetween(DB::raw('DATE_FORMAT(persona.fecha_nacimiento, "%m-%d")'), array($inicio,$fin));})
          ->where('tipo', '=', 'Cliente')
          ->where('cumple', '=', 1)
          ->with('cliente.persona','cliente.prestamos.ruta.hoja_ruta.user')->get();
          return view('Clientes.cumple', compact('personas'));
      } else{
        //El promotor va a tener filtrado los cumpleañeros por las rutas que tiene asignado
        //En este consulta se obtiene las hojas de ruta y se adjunta los cumpleañeros
        $hojas_ruta = Auth::user()->hoja_ruta()->with(['prestamos.cliente.persona' => function($query) use($inicio, $fin){
          return $query->whereBetween(DB::raw('DATE_FORMAT(persona.fecha_nacimiento, "%Y-%m-%d")'), array($inicio,$fin))
                 ->where('cumple', '=', 1);
        }])->get();
        $personas = array();
        //Siguiente se recorre la hoja de ruta y se obtiene solo las personas para mostrarlo en la tabla.
        foreach ($hojas_ruta as $hoja) {
          foreach ($hoja->prestamos as $prestamo) {
            $personaTemp = $prestamo->cliente->persona;
            if($personaTemp != null){
              $personas[] = $prestamo->cliente->persona;
            }
          }
        }
        return view('Clientes.cumple', compact('personas'));
      }
    }
    /**
    * La siguiente funcion, es para obtener los datos en forma de una tabla de DataTable,
    * obtiene las fechas que fueron introducidas por el usuario, y se identifica que
    * rol tiene el usuario para determinar que informacion va a mostrar.
    */
    public function infoCumpleanios(Request $request){
        $inicio = Carbon::parse($request->get('inicio'))->format('m');
        $iniciodia = Carbon::parse($request->get('inicio'))->format('d');
        $fin = Carbon::parse($request->get('fin'))->format('m');
        $findia = Carbon::parse($request->get('fin'))->format('d');
        // $inicio = Carbon::createFromFormat('Y-m-d',$request->get('inicio'))->format('Y-m-d');
        // $fin = Carbon::createFromFormat('Y-m-d',$request->get('fin'))->format('Y-m-d');
        $tipo = $request->get('tipo');
        $datos = array();
        $permiso = 2;
        if(Auth::user()->hasAnyRole(['Administrador','Secretaria','Supervisor'])){
          if(Auth::user()->hasRole('Administrador')){ $permiso=1;}
        $personas = Persona::where(function($query) use ($inicio, $fin, $iniciodia, $findia){
                    $query->whereMonth('fecha_nacimiento', '>=' , $inicio)
                          ->whereDay('fecha_nacimiento',   '>=' , $iniciodia)
                          ->whereMonth('fecha_nacimiento', '<=' , $fin)
                          ->whereDay('fecha_nacimiento',   '<=' , $findia)
                          ;})
                    ->where('tipo', '=', 'Cliente')
                    ->where('cumple', '=', 1)
                    ->where('estado', '=', $tipo)
                    ->get();
        //Se crea un array con la informacion de cada persona.
        foreach ($personas->sortBy('fecha_nacimiento') as $persona) {
          $ClienteClas = \App\Cliente::where('persona_id', $persona->id)->first();
          $ClasPrestamo = \App\Prestamo::where('cliente_id',$ClienteClas->id)->orderBy('id','DESC')->first();
          // dd($ClasPrestamo->cliente->persona);
          $clasi="<td><span class='badge badge-primary'>A</span></td>";
          // switch ($ClasCliente->clasificacion_id) {
          //   case 0:
          //     $clasi = "<td><span class='badge badge-success'>NULL</span></td>";
          //   break;
          //   case 1:
          //     $clasi = "<td class='text-right'><span class='badge badge-primary'>A</span></td>";
          //   break;
          //   case 2:
          //     $clasi = "<td class='text-right'><span class='badge badge-warning'>B</span></td>";
          //   break;
          //   case 3:
          //     $clasi = "<td class='text-right'><span class='badge badge-danger'>C</span></td>";
          //   break;
          // }
          if($persona->cliente->first() != null){
            $temp = array(
              'codigo'  => $persona->cliente->first()->id,
              'cliente' => $persona->nombre . ' ' . $persona->apellido,
              // 'clasificacion' => $persona->cliente->first()->obtenerClasificacion2(),
              'clasificacion'=>$clasi,
              'fecha'  => Carbon::parse($persona->fecha_nacimiento)->format('Y-m-d'),
              // 'prestamo'  => $persona->cliente->first()->prestamos->last()->id,
              'prestamo' => $ClasPrestamo->id,
              'monto'  => $persona->cliente->first()->prestamos->last()->monto,
              'hoja_ruta' => $persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->nombre,
              'promotor'  => $persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->user->persona->nombre . ' '.$persona->cliente->first()->prestamos->first()->ruta->first()->hoja_ruta->user->persona->apellido,
              'tipo'  => $permiso,
            );
            $datos[] = $temp;
          }
        }
        $datosApi = datatables()->collection($datos)->toJson();
        return $datos;
      }else{
        //Se filtra por las rutas que tiene asignado en caso que sea promotor.
        $hojas_ruta = Auth::user()->hoja_ruta()->with(['prestamos.cliente.persona' => function($query) use($inicio, $fin){
          return $query->whereBetween(DB::raw('DATE_FORMAT(persona.fecha_nacimiento, "%Y-%m-%d")'), array($inicio,$fin))
                 ->where('cumple', '=', 1);
        }])->get();
        $personas = array();
        //Se recorre cada hoja y cada prestamo dentro de esa hoja para crear el array con la informacion.
        foreach ($hojas_ruta as $hoja) {
          foreach ($hoja->prestamos as $prestamo) {
            $persona = $prestamo->cliente->persona;
            if($persona != null){
              $temp = array(
                'codigo'  => $persona->cliente->first()->id,
                'cliente' => $persona->nombre . ' ' . $persona->apellido,
                'clasificacion' => $persona->cliente->first()->obtenerClasificacion2(),
                'fecha'  => Carbon::parse($persona->fecha_nacimiento)->format('Y-m-d'),
                'prestamo'  => $persona->cliente->first()->prestamos->last()->id,
                'monto'  => $persona->cliente->first()->prestamos->last()->monto,
                'hoja_ruta' => $persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->nombre,
                'promotor'  => $persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->user->persona->nombre . ' '.$persona->cliente->first()->prestamos->first()->ruta->first()->hoja_ruta->user->persona->apellido,
                'tipo' => $permiso,
              );
              $datos[] = $temp;
            }
          }
        }
        return $datos;
      }
    }
    /**
    * Funcion que solo devulve si un cliente esta en lista blanca o negra.
    *
    * @param id El id del cliente.
    * @return int 2 Si no esta lista negra y 1 si esta lista negra.
    */
    public function listaNB($id){
      $cliente = Cliente::find($id);
      if($cliente->listabn_id == 1){
        $cliente->update([
          "listabn_id"    =>  2,
        ]);
        return 2;
      }else{
        $cliente->update([
          "listabn_id"    =>  1,
        ]);
        return 1;
      }
    }
    /**
    * Funcion usada para hacer la actulizacion de transferencia, esto cuando un cliente
    * se transfiere a otra ruta.
    * @param id Recibe el id del cliente a transferir
    * @param request Recibe la peticion con la informacion necesaria de la transferencia
    * @return redirect retorna una vista cuando ya se completo.
    */
    public function transferir(Request $request, $id){
      $cliente = Cliente::find($id);
      $idHojaNueva =  $request->input('hoja_ruta_nueva');
      $prestamo = $cliente->prestamos->last();
      $hoja_ruta_actual = $prestamo->ruta->first()->hoja_ruta;
      $hoja_ruta_nueva = Hoja_ruta::findorfail($idHojaNueva);
      $hoja_ruta_actual->update([
        'total_activo' => $hoja_ruta_actual->total_activo - $prestamo->monto,
        'capital_activo' => $hoja_ruta_actual->capital_activo - $prestamo->capital_activo,
        'mora'           => $hoja_ruta_actual->mora - $prestamo->mora,
      ]);
      $hoja_ruta_nueva->update([
        'total_activo'  => $hoja_ruta_nueva->total_activo + $prestamo->monto,
        'capital_activo' => $hoja_ruta_nueva->capital_activo + $prestamo->capital_activo,
        'mora'           => $hoja_ruta_nueva->mora + $prestamo->mora,
      ]);
      $cliente->prestamos->last()->ruta->first()->update([
        'hoja_ruta_id'  =>  $request->input('hoja_ruta_nueva'),
      ]);
      return redirect()->route('clientes.show', ['id' => $id]);
    }
    /**
    * funcion que obtiene la informacion del cliente, esto para usarse en un metodo ajax
    * @param id Recibe el id del cliente
    * @return array retorna un array con la informacion respectiva.
    */
    public function infoCliente($id){
      $cliente = Cliente::find($id);
      $info = array(
        'id'  => $cliente->id,
        'nombre'  => $cliente->persona->nombre . " " . $cliente->persona->apellido,
        'prestamo' => 'Cre-'. $cliente->prestamos->last()->id,
        'monto'    => $cliente->prestamos->last()->monto,
      );
      return $info;
    }
    /**
    * Funcion para establecer que el cumpleañero ya fue tomado en cuenta.
    *
    * @param int recibe el id del cliente
    */
    public function quitarCumple($id){
      $cliente = Cliente::find($id);
      $cliente->persona->update([
        'cumple'  =>  '0',
      ]);
      $cliente->persona->save();
    }
    /**
    * Funcion para agregar imagen de un cliente.
    *
    * @param Request recibe la peticion con la informacion
    * @param int id del cliente
    * @return redirect retorna la vista anterior desde que se accedio a este metodo, en esta clasificacion
    * recarga la pagina.
    */
    public function agregarImagen(Request $request, $id = null){
      //Se obtiene al cliente y la ruta, y se determina si fue cambiada la imagen
      //Si fue cambiada entonces se actualiza si no hay nada, no se hace nada.
      $cliente = Cliente::find($id);
      $destinoPath = public_path('//images//clientes//documentos');
      if($request->file('img_dpi') !=null){
        $nombre_imagen_dpi = "";
        $imagen_dpi = $request->file('img_dpi');
        $nombre_imagen_dpi  = $request->input('nombre'). 'DPI' . time() . '.' . $imagen_dpi->getClientOriginalExtension();
        $imagen_dpi->move($destinoPath, $nombre_imagen_dpi);
        $cliente->update([
          'foto_dpi' => $nombre_imagen_dpi,
        ]);
      }
      if($request->file('img_luz') !=null){
        $nombre_imagen_recibo = "";
        $imagen_recibo = $request->file('img_luz');
        $nombre_imagen_recibo  = $request->input('nombre') . 'RECIBO' .time() . '.' . $imagen_recibo->getClientOriginalExtension();
        $imagen_recibo->move($destinoPath, $nombre_imagen_recibo);
        $cliente->update([
          'foto_recibo' => $nombre_imagen_recibo,
        ]);
      }
      if($request->file('img_solicitud') !=null){
        $nombre_imagen_firma = "";
        $imagen_firma = $request->file('img_solicitud');
        $nombre_imagen_firma  = $request->input('nombre') . 'RECIBO' .time() . '.' . $imagen_firma->getClientOriginalExtension();
        $imagen_firma->move($destinoPath, $nombre_imagen_firma);
        $cliente->update([
          'foto_firma' => $nombre_imagen_firma,
        ]);
      }
      return redirect()->back();
    }
}
