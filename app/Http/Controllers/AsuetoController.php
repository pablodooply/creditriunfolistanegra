<?php

namespace App\Http\Controllers;
use App\Asueto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class AsuetoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('extras.asueto.index');
    }

    public function apiAsueto()
    {

      $asuetos = Asueto::orderBy('fecha_inicio','desc');
      return \Yajra\Datatables\Datatables::of($asuetos)
      ->addColumn('codigo', function($asueto){
        return '<a class="client-link" value="'.$asueto->id.'">'. $asueto->id .'</a>';
      })
      ->addColumn('titulo', function($asueto){
        return  $asueto->titulo;
      })
      ->addColumn('fecha_inicio', function($asueto){
        return date('d/m/Y', strtotime($asueto->fecha_inicio));
      })
      ->addColumn('fecha_fin', function($asueto){
        return date('d/m/Y', strtotime($asueto->fecha_fin));
      })
      ->addColumn('descripcion', function($asueto){
        return $asueto->descripcion;
      })

      ->addColumn('acciones', function($asueto){
        $ver = '';
        $ver='<a class="btn btn-sm btn-white btn-bitbucket btn-show-categoria" onclick="editar('.$asueto->id.')" data-toggle="modal" data-target="#asueto-modal-creacion"><i class="fa fa-edit text-warning"></i></a><a class="btn btn-sm btn-white btn-bitbucket btn-show-categoria" onclick="eliminar('.$asueto->id.')" ><i class="fa fa-trash text-danger"></i></a>';
        return $ver;
      })
      ->escapeColumns([])
      ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('extras.asueto.modalCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), [
            'titulo'    => 'required',
            'fecha_inicio'    => 'required',
            'fecha_fin'    => 'required',
  
          ]);
          if ($validator->fails()) {
              return array(
                  'fail' => true,
                  'errors' => $validator->getMessageBag()->toArray()
              );
          }
          $hoy = Carbon::now()->format('Y-m-d');
          $fecha_inicio = Carbon::parse($request->get('fecha_inicio'))->format('Y-m-d');
          $fecha_fin = Carbon::parse($request->get('fecha_fin'))->format('Y-m-d');
          
          if($fecha_inicio < $hoy){
            return array('fail' => true, 'errors' => ['usd' => 'Ingrese una fecha mayor o igual a la actual']);

          }
          if($fecha_fin < $fecha_inicio){
            return array('fail' => true, 'errors' => ['usd' => 'La fecha final debe ser mayor o igual a la fecha inicial']);

          }

          $asueto = New Asueto();
          $asueto->titulo = $request->get('titulo');
          $asueto->fecha_inicio = $request->get('fecha_inicio');
          $asueto->fecha_fin = $request->get('fecha_fin');
          $asueto->descripcion = $request->get('descripcion');
          $asueto->save();
          
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar(Request $request, $id)
    {
      if ($request->isMethod('get')){
        $asueto= Asueto::find($id);
        return view('extras.asueto.modalEdit', compact('asueto'));
      }else{

        $validator = Validator::make(Input::all(), [
          'titulo'    => 'required',
          'fecha_inicio'    => 'required',
          'fecha_fin'    => 'required',

        ]);
    if ($validator->fails()) {
      return array(
          'fail' => true,
          'errors' => $validator->getMessageBag()->toArray()
      );
   }
          $asueto = Asueto::find($id);
          $asueto->titulo = $request->get('titulo');
          $asueto->fecha_inicio = $request->get('fecha_inicio');
          $asueto->fecha_fin = $request->get('fecha_fin');
          $asueto->descripcion = $request->get('descripcion');
          $asueto->push();

  }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
      $asueto = Asueto::find($id);
      $asueto->delete();
    }
}
