<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Cliente;
use App\User;
use App\Ruta;
use App\Prestamo;
use App\Pago;
use App\Ficha_pago;
use App\Agencia;
use App\Boleta_pago;
use App\Comentario;
use App\Plan;
use App\Hoja_ruta;

class PagoController extends Controller
{

  private $no_mora = 2;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function noPagoCuota(Request $request){
       //Se obtiene ficha de pago que corresponde al que no se pago
       $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
                               where('no_dia', $request->input('no_cuota'))->first();

       //Se obtiene el interes y las cuotas actuales de las fichas
       $interes = $ficha_pago->interes;
       $capital = $ficha_pago->capital;
       $cuota = $ficha_pago->cuota;

       //Se obtiene ficha siguiente del prestamo
       $ficha_pago_siguiente = $ficha_pago->siguiente();

       //Se obtiene el numero de fichas no pagadas
       $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->count();
       $ficha_no_pagada = $ficha_pago->subficha_anterior() ? $ficha_pago->subficha_anterior()->estado_p : 0;

       if($ficha_no_pagada == '2' || $ficha_no_pagada == '3' ){

         $this->refreshClasi($ficha_pago->prestamo);


         $ficha_pago->update([
           'estado_p' => 2,
           'estado' => 1,
           'updated_at' => Carbon::now(),
         ]);

         $ficha_pago->save();


         // $mora_a_recargar = $ficha_pago->prestamo->plan->mora;
         $mora_a_recargar = $this->establecerMoraDiario($ficha_pago->subficha_anterior(), $ficha_pago, $ficha_pago_siguiente);

         $ficha_pago_siguiente->update([
           'interes'     => $ficha_pago_siguiente->interes + $interes,
           // 'mora'        => $mora + $mora_a_recargar,
           'capital'     => $ficha_pago_siguiente->capital + $capital,
           // 'ajuste'      => $mora_a_recargar,
           'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora_a_recargar,
         ]);
         // $recargo_mora = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $ficha_pago->prestamo->plan->mora;
         // $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
         //   'mora'  => $recargo_mora,
         // ]);


         $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora_a_recargar,0,0);

       } else {
        $this->refreshClasi($ficha_pago->prestamo);
         // $ficha_pago->prestamo->update([
         //   "clasificacion_id"  => 1,
         // ]);
         $ficha_pago->update([
           'estado_p' => '2',
           'estado' => 1,
           'updated_at' => Carbon::now(),
         ]);
         $ficha_pago->save();

         $ficha_pago_siguiente->update([
           'interes'     => $ficha_pago_siguiente->interes + $interes,
           'mora'        => 0,
           'capital'     => $ficha_pago_siguiente->capital + $capital,
           'ajuste'      => 0,
           'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
         ]);

       }

     }


    public function index()
      {
        //Historial de pagos
        $usuario = Auth::user();
        //Historial de pagos
        if($usuario->hasAnyRole(['Administrador', 'Secretaria'])){
          $pagos = Pago::orderBy('created_at', 'DES')->get();
        }else if($usuario->hasRole('Supervisor')){
          $rutas = $usuario->rutas_supervisor()->with('prestamo.pagos')->get();
          $temp = array();
          foreach($rutas as $ruta){
            foreach ($ruta->prestamo->pagos as $pago) {
              // code...
              $temp[] = $pago;
            }
          }
          // dd($pagos);
          $pagos = collect($temp);
        }
        return view('Pagos.index', compact('pagos'));
    }



    public function setComentario(Request $request){
      if($request->isMethod('POST')){
        $texto = $request->get('observacion');
        $idObs = $request->get('id');
        $observacion = Comentario::find($idObs);
        $observacion->update([
          'descripcion' => $texto,
        ]);
      } else{
        $id = $request->get('id');
        $comentario = Comentario::find($id);
        return $comentario->descripcion;
      }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
      {

        //form de pago
        $promotor = Auth::user();
        $todoscreditos = collect();
        if($promotor->hasAnyRole('Administrador', 'Secretaria')){
          $fichas_pago_hoy = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                      ->join('ficha_pago', 'prestamo.id', '=', 'ficha_pago.prestamo_id')
                      ->join('cliente','cliente.id','=','prestamo.cliente_id')
                      ->join('persona', 'persona.id', '=','cliente.persona_id')
                      // ->where('ficha_pago.estado_p', '=', 0)
                      ->where('ficha_pago.fecha', '<=', Carbon::now()->format("Y-m-d"))
                      ->where('ficha_pago.tipo', '=',1)
                      ->orderBy('ficha_pago.estado_p', 'ASC')
                      ->orderBy('ficha_pago.destacado', 'DESC')
                      ->orderBy('ruta.hora', 'ASC')
                      ->groupBy('prestamo.id')
                      ->get();



          $prestamos_mora = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                      ->join('cliente','cliente.id','=','prestamo.cliente_id')
                      ->join('persona', 'persona.id', '=','cliente.persona_id')
                      ->where('prestamo.estado_p_id', '=', 10)
                      ->get();
        }else if($promotor->hasRole('Supervisor')){

          $fichas_pago_hoy = Ruta::join('hoja_ruta', 'hoja_ruta.id','=','ruta.hoja_ruta_id')
                      ->join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                      ->join('ficha_pago', 'prestamo.id', '=', 'ficha_pago.prestamo_id')
                      ->join('cliente','cliente.id','=','prestamo.cliente_id')
                      ->join('persona', 'persona.id', '=','cliente.persona_id')
                      ->where('ficha_pago.fecha', '=', Carbon::now()->format("Y-m-d"))
                      ->where('ficha_pago.tipo', '=',1)
                      ->where(function ($query) use($promotor){ return $query->where('hoja_ruta.user_id','=', $promotor->id)
                                            ->orWhere('hoja_ruta.supervisor_id','=',$promotor->id);})
                      ->orderBy('ficha_pago.estado_p', 'ASC')
                      ->orderBy('ruta.hora', 'ASC')
                      ->get();



          $prestamos_mora = Ruta::join('hoja_ruta', 'hoja_ruta.id','=','ruta.hoja_ruta_id')
                      ->join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                      ->join('cliente','cliente.id','=','prestamo.cliente_id')
                      ->join('persona', 'persona.id', '=','cliente.persona_id')
                      ->where(function ($query) use($promotor){ return $query->where('hoja_ruta.user_id','=', $promotor->id)
                              ->orWhere('hoja_ruta.supervisor_id','=',$promotor->id);})
                      ->where('prestamo.estado_p_id', '=', 10)
                      ->get();
        }

        $rutas=Hoja_ruta::all();
        return view('Pagos.create', compact("promotor", "fichas_pago_hoy", 'prestamos_mora', 'rutas'));
    }



    public function refreshPendientes()
    {
      //form de pago
      $promotor = Auth::user();
      $todoscreditos = collect();
      if($promotor->hasAnyRole('Administrador', 'Secretaria')){
        $fichas_pago_hoy = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                    ->join('ficha_pago', 'prestamo.id', '=', 'ficha_pago.prestamo_id')
                    ->join('cliente','cliente.id','=','prestamo.cliente_id')
                    ->join('persona', 'persona.id', '=','cliente.persona_id')
                    ->where('ficha_pago.fecha', '=', Carbon::now()->format("Y-m-d"))
                    ->where('ficha_pago.tipo', '=',1)
                    ->orderBy('ficha_pago.estado_p', 'ASC')
                    ->orderBy('ficha_pago.destacado', 'DESC')
                    ->orderBy('ruta.hora', 'ASC')
                    ->get();



        $prestamos_mora = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                    ->join('cliente','cliente.id','=','prestamo.cliente_id')
                    ->join('persona', 'persona.id', '=','cliente.persona_id')
                    ->where('prestamo.estado_p_id', '=', 10)
                    ->get();
      }else if($promotor->hasRole('Supervisor')){

        $fichas_pago_hoy = Ruta::join('hoja_ruta', 'hoja_ruta.id','=','ruta.hoja_ruta_id')
                    ->join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                    ->join('ficha_pago', 'prestamo.id', '=', 'ficha_pago.prestamo_id')
                    ->join('cliente','cliente.id','=','prestamo.cliente_id')
                    ->join('persona', 'persona.id', '=','cliente.persona_id')
                    ->where('ficha_pago.fecha', '=', Carbon::now()->format("Y-m-d"))
                    ->where('ficha_pago.tipo', '=',1)
                    ->where(function ($query) use($promotor){ return $query->where('hoja_ruta.user_id','=', $promotor->id)
                                          ->orWhere('hoja_ruta.supervisor_id','=',$promotor->id);})
                    ->orderBy('ficha_pago.estado_p', 'ASC')
                    ->orderBy('ruta.hora', 'ASC')
                    ->get();



        $prestamos_mora = Ruta::join('hoja_ruta', 'hoja_ruta.id','=','ruta.hoja_ruta_id')
                    ->join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                    ->join('cliente','cliente.id','=','prestamo.cliente_id')
                    ->join('persona', 'persona.id', '=','cliente.persona_id')
                    ->where(function ($query) use($promotor){ return $query->where('hoja_ruta.user_id','=', $promotor->id)
                            ->orWhere('hoja_ruta.supervisor_id','=',$promotor->id);})
                    ->where('prestamo.estado_p_id', '=', 10)
                    ->get();
      }
      return view('Pagos.partials.listapendientes', compact("promotor", "fichas_pago_hoy", 'prestamos_mora'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function SIpago(Request $request)
   {
       // dd($request->submitbutton);
       $ultimo = $this->esUltimaCuota($request);
       if($ultimo==0)
       {
         $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
                                 where('no_dia', $request->input('no_cuota'))->
                                 where('estado', 0)->first();

         $credito=Prestamo::find($ficha_pago->prestamo->id);
         $this->refreshClasi($credito);
         $totalDeuda=$credito->monto - ($credito->interes + $credito->capital_recuperado) + $credito->mora;
         //Enlazando con ficha de pagos
         if($request->cuota <= $totalDeuda)
         {

          $this->verificarPago($request);// $this->pagoCuota($request);
          $this->addComentario($request->get('observaciones'),$ficha_pago);
         }
       }
   }

   public function NOpago(Request $request)
   {
     // dd($request);
       // dd($request->submitbutton);
       $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
       where('no_dia', $request->input('no_cuota'))->
       where('estado', 0)->first();
       $credito=Prestamo::find($ficha_pago->prestamo->id);
       $ultimo = $this->esUltimaCuota($request);
       $this->refreshClasi($credito);
       if($ultimo==0)
       {
         // $credito->update([
         //   'clasificacion_id'  => ,
         // ]);
         // $this->refreshClasi($credito);
         if($ficha_pago->prestamo->plan->nombre == "Semanal" || $ficha_pago->prestamo->plan->nombre == "Quincena")
           $this->noPagoOtro($request, $ficha_pago);
         else
         $this->noPagoCuota($request);

         $this->addComentario($request->get('observaciones'),$ficha_pago);
       }
   }

   public function refreshClasi($prestamo)
   {

     $montototal = $prestamo->monto;
     $pagosefectuados = Ficha_pago::where("prestamo_id",$prestamo->id)->where('estado', 1)->get();
     $interestotal = $pagosefectuados->sum('interes');
     $capitalretotal = $pagosefectuados->sum('capital');
     $saldototal = $montototal - ($interestotal + $capitalretotal);
     if($prestamo->estado_p_id == 9 && $saldototal>0)
     {
       $prestamo->update([
         'estado_p_id' => 5,
       ]);
     }
     //PLAN DIARIO
     if($prestamo->plan->nombre == "Diario")
     {
        $ficha_no_pagadas = $prestamo->ficha_pago->where('cont',1)->count();
        // dd($ficha_no_pagadas);
        // $estado = 1;
        if($ficha_no_pagadas == 0){
          $estado = 1;
        }
        if($ficha_no_pagadas == 1 || $ficha_no_pagadas == 2){
          $estado = 2;
        }
        if($ficha_no_pagadas >= 3){
          $estado = 3;
        }

        $prestamo->update([
          'clasificacion_id'  => $estado,
        ]);
        // dd($prestamo);
      }
      //PLAN SEMANAL
      if($prestamo->plan->nombre == "Semanal")
      {
        $ficha_no_pagadas = $prestamo->ficha_pago->where('ajuste', '>', 0)->count();
        // $ficha_no_pagadas_group = $prestamo->ficha_pago->where('cont',1)->count()->groupBy('no_dia');
        $ficha_no_pagadas_group = $prestamo->ficha_pago->where('estado_p', 2)->where('cont',1)->count();
        // dd($ficha_no_pagadas_group);
        // $estado = 1;
        if($ficha_no_pagadas == 0){
          $estado = 1;
        }
        if($ficha_no_pagadas == 1 || $ficha_no_pagadas == 2){
          $estado = 2;
        }
        if($ficha_no_pagadas >= 3){
          $estado = 3;
        }
        $prestamo->update([
          'clasificacion_id'  => $estado,
        ]);
      }
      //PLAN QUINCENA
      if($prestamo->plan->nombre == "Quincena")
      {
        if (!is_null($prestamo->ficha_pago->first())) {
          // code...

        $ficha_no_pagadas = $prestamo->ficha_pago->where('cont',1)->count();
        $ficha_siete = $prestamo->ficha_pago->where('id', $prestamo->ficha_pago->first()->id + 7)->first();

        // dd($ficha_siete);
        $estado = 1;
        if($ficha_no_pagadas == 0){
          $estado = 1;
        }
        if($ficha_no_pagadas >= 1){
          if($ficha_siete->estado_p ==2 || $ficha_siete->estado_p ==3)
          {
            $estado = 3;
          }
          else {

            $estado = 2;
          }
        }


        $prestamo->update([
          'clasificacion_id'  => $estado,
        ]);
        // dd($prestamo);
        }
      }
    }


    public function store(Request $request){

      $estado_p=Prestamo::where('id',$request->input('cliente'))->first();
      // dd($request);
      if($estado_p->estado_p_id==10)
      {
        switch($request->submitbutton) {
            case 'Pago':
            $ficha_ultima=Ficha_pago::where('prestamo_id',$estado_p->id)->orderBy('id', 'DESC')->first();

            $ficha_ejemplo=Ficha_pago::where('prestamo_id',$estado_p->id)->first();
            // if($ficha_pago->estado_p == 5)
            // {
            //   $pago = $ficha_pago->pago;
            //   $pago->delete();
            // }
            $interes_actual = $ficha_ultima->prestamo->interes;
            $capital_actual = $ficha_ultima->prestamo->capital_actual;
            $mora_actual = $ficha_ultima->prestamo->mora;
            $mora_recuperada = $ficha_ultima->prestamo->mora_recuperada;
            $credito=Prestamo::find($request->input('cliente'));

            $pagosSaldo = Pago::where('prestamo_id',$credito->id)->sum('monto');
            $pagosMora = Pago::where('prestamo_id',$credito->id)->sum('mora');
            $totalPagosFicha=Ficha_pago::where('prestamo_id',$credito->id)->whereNotNull('pago_id')->get();
            $sumaTotalPagos=0;
            foreach ($totalPagosFicha as $pagosList) {
              $listadoPagos[]=Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
              // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
            }
            if(isset($listadoPagos))
            {
              foreach ($listadoPagos as $sumaPagos) {
                $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
              }
            }
            else {
              $sumaTotalPagos=0;
            }
            // $totalRealPago=\App\Ficha_pago::where('')
             $saldoTotal=$sumaTotalPagos-$pagosMora;
             $final=round(($credito->monto - $saldoTotal)+($credito->mora - $credito->mora_recuperada), 2);

             $mora_pendiente=$credito->mora - $credito->mora_recuperada;
             $calculos=$this->calcularMoraInteresCapital($ficha_ejemplo->cuota,$ficha_ejemplo);
             $mora = $calculos['mora'];
             $interes = $calculos['interes'];
             $capital = $calculos['capital'];
             $primerPaso=100*$credito->plan->interes;
             $interesPorcentaje=($primerPaso/$credito->plan->total)/100;
             $capitalPorcentaje=1-$interesPorcentaje;

            $pagoViejo=Pago::where('id',$ficha_ultima->pago_id)->first();
            // $pagoViejo->monto;
            // dd($capitalPorcentaje*$final);
            $monto2=$request->input('cuota');
            $ajuste = $final;
            $esCero = $ajuste - $monto2;
            if($esCero == 0){
              $estadoP=1;
            }else if($esCero > 0){
              $estadoP=3;
            }
            else{
              return redirect()->back();
            }
            if(is_null($ficha_ultima->pago_id))
            {
              $pagoFinal=Pago::create([
                'descripcion'=>$request->input('observaciones'),
                'monto'=>$request->input('cuota'),
                'mora'=>$mora_pendiente,
                'interes'=>$interes,
                'capital'=>$capital,
                'fecha'=>Carbon::now()->toDateString(),
                'prestamo_id'=> $credito->id,
              ]);
              $ficha_extra=Ficha_pago::create([
                'no_dia'=>$ficha_ultima->no_dia,
                'fecha'=>$ficha_ultima->fecha,
                'cuota'=>$final-$mora_pendiente,
                'mora'=>$mora_pendiente,
                'total'=>$ficha_ejemplo->cuota,
                'interes'=>$interesPorcentaje*$final,
                'capital'=>$capitalPorcentaje*$final,
                'ajuste'=>0,
                'total_pago'=>$final,
                'prestamo_id'=>$credito->id,
                'pago_id'=>$pagoFinal->id,
                'estado_p'=>$estadoP,
                'estado'=>1,
                'tipo'=>1,
                'cont'=>1,
              ]);
              $interes_actual = $ficha_ultima->prestamo->interes;
              $capital_actual = $ficha_ultima->prestamo->capital_recuperado;
              $mora_actual = $credito->mora;
              $credito->update([
                // 'estado_p_id' => 9,
                // 'pagado'      => $credito->pagado + $pagoFinal->monto,
                // 'interes'     => $interes_actual + $interes,
                // 'capital'     => $capital_actual + $capital,
                // 'capital_recuperado' => $ficha_extra->prestamo->capital_recuperado + $pagoFinal->capital,
                // 'saldo' => $ficha_extra->cuota - $ficha_extra->monto,

                'pagado'  => $ficha_ultima->prestamo->pagado + $pagoFinal->monto,
                'interes' => $interes_actual + $pagoFinal->interes,
                'capital_recuperado' => $capital_actual + $pagoFinal->capital,
                'mora_recuperada'  => $mora_actual,
                'estado_p_id' => '9',
              ]);

              $agencia = Agencia::find(1);
              $agencia->update([
                  'capital'   => $agencia->capital + $pagoFinal->capital,
              ]);
            }

            else {
              $monto=$request->input('cuota');
              $pago=$ficha_ultima->pago;
              $montoU=$pago->monto + $monto;
              $interesU=$pago->interes+$ficha_ultima->interes;
              $capitalU=$pago->capital+$ficha_ultima->capital;
              $moraU=$pago->mora+$ficha_ultima->mora;
              $mora_pendiente=$credito->mora - $credito->mora_recuperada;
              $primerPaso=100*$credito->plan->interes;
              $interesPorcentaje=($primerPaso/$credito->plan->total)/100;
              $capitalPorcentaje=1-$interesPorcentaje;
              $pago->update([
                'descripcion'     =>    $request->input('observaciones'),
                'monto'           =>    $montoU,
                'interes'         =>    $interesU,
                'capital'         =>    $capitalU,
                'mora'            =>    $moraU,
                'fecha'           =>    Carbon::now()->toDateString(),
                'prestamo_id'        => $credito->id,
              ]);
              $ficha_extra=Ficha_pago::create([
                'no_dia'=>$ficha_ultima->no_dia,
                'fecha'=>Carbon::now()->toDateString(),
                'cuota'=>$ficha_ultima->cuota,
                'mora'=>0,
                'total'=>$ficha_ultima->cuota,
                'interes'=>$interesPorcentaje*$monto,
                'capital'=>$capitalPorcentaje*$monto,
                'ajuste'=>0,
                'total_pago'=>$monto,
                'prestamo_id'=>$credito->id,
                'pago_id'=>$pago->id,
                'estado_p'=>1,
                'estado'=>1,
                'tipo'=>1,
                'cont'=>1,
              ]);

              //Se actualiza el prestamo
              $interes_actual = $ficha_ultima->prestamo->interes;
              $capital_actual = $ficha_ultima->prestamo->capital_recuperado;
              $mora_actual = $credito->mora;


              // Se actualiza el prestamo con lo que se a pagado
              $credito->update([
                          'pagado'  => $ficha_ultima->prestamo->pagado + $pago->monto,
                          'interes' => $interes_actual + $pago->interes,
                          'capital_recuperado' => $capital_actual + $pago->capital,
                          'mora_recuperada'  => $mora_actual,
                          'estado_p_id' => '9',
              ]);

              $agencia = Agencia::find(1);
              $agencia->update([
                  'capital'   => $agencia->capital + $pago->capital,
              ]);
            }
            return redirect()->back();
            break;
            case 'No pago':
              $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
                                      where('no_dia', $request->input('no_cuota'))->
                                      where('estado', 0)->first();

              if($ficha_pago->prestamo->plan->nombre == "Semanal" || $ficha_pago->prestamo->plan->nombre == "Quincena")
                $this->noPagoOtro($request, $ficha_pago);
              else
              $this->noPagoCuota($request);

              $this->addComentario($request->get('observaciones'),$ficha_pago);
                return redirect()->back();
            break;
            case 'Dia de perdon':
            $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
                                    where('no_dia', $request->input('no_cuota'))->
                                    where('estado', 0)->first();

            if($ficha_pago->prestamo->plan->nombre == "Semanal" || $ficha_pago->prestamo->plan->nombre == "Quincena")
              $this->noPagoOtro($request, $ficha_pago);
            else
            $this->noPagoCuota($request);

            $this->addComentario($request->get('observaciones'),$ficha_pago);
            return redirect()->back();
            break;
        }
      }
      else {
        $ultimo = $this->esUltimaCuota($request);
        if($ultimo==0){
          switch($request->submitbutton) {
              case 'Pago':
              $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
                                      where('no_dia', $request->input('no_cuota'))->
                                      where('estado', 0)->first();


              // if($ficha_pago->estado_p == 5)
              // {
              //   $pago = $ficha_pago->pago;
              //   $pago->delete();
              // }
              $credito=Prestamo::find($request->input('cliente'));
              $totalDeuda=$credito->monto - ($credito->interes + $credito->capital_recuperado) + $credito->mora;
                  //Enlazando con ficha de pagos

              if($request->cuota <= $totalDeuda)
              {

               $this->verificarPago($request);// $this->pagoCuota($request);
               $this->addComentario($request->get('observaciones'),$ficha_pago);
               return redirect()->back();
              }
              else
              {
               return redirect()->back();
              }
              break;
              case 'No pago':
                $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
                                        where('no_dia', $request->input('no_cuota'))->
                                        where('estado', 0)->first();

                if($ficha_pago->prestamo->plan->nombre == "Semanal" || $ficha_pago->prestamo->plan->nombre == "Quincena")
                  $this->noPagoOtro($request, $ficha_pago);
                else
                $this->noPagoCuota($request);

                $this->addComentario($request->get('observaciones'),$ficha_pago);
                  return redirect()->back();
              break;
              case 'Dia de perdon':
              $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
                                      where('no_dia', $request->input('no_cuota'))->
                                      where('estado', 0)->first();

              if($ficha_pago->prestamo->plan->nombre == "Semanal" || $ficha_pago->prestamo->plan->nombre == "Quincena")
                $this->noPagoOtro($request, $ficha_pago);
              else
              $this->noPagoCuota($request);

              $this->addComentario($request->get('observaciones'),$ficha_pago);
              return redirect()->back();
              break;
          }
        }
        else
        {
          return redirect()->back();
        }
      }
    }


    public function verificarTOTAL(Request $request){
     $total = $request->get('total');
     $saldo = $request->get('saldo');

     if($total > $saldo){
       return "true";
     }else{
       return "false";
     }
    }

    public function addComentario($observaciones, $ficha_pago){
      if($observaciones != null){
        Comentario::updateOrCreate(['commentable_type' => '\App\Ficha_pago', 'commentable_id' => $ficha_pago->id],['descripcion' => $observaciones]);
        $ficha_pago->update([
          'destacado' => 0,
        ]);
      }
    }

    public function calcularMoraInteresCapital($monto, $ficha_pago){
      //Se calcula cuanto de interes. mora y capital pago

      $mora = ($monto - $ficha_pago->mora <= 0) ? $monto : $ficha_pago->mora;
      $sobrante = $monto - $mora;
      $interes = ($sobrante - $ficha_pago->interes <= 0) ? $sobrante : $ficha_pago->interes;
      $sobrante = $sobrante - $interes;
      $capital = ($sobrante - $ficha_pago->capital <= 0) ? $sobrante : $ficha_pago->capital;

      $calculos = array(
        'mora' => $mora,
        'interes' => $interes,
        'capital' => $capital
      );
      return $calculos;
    }
    //Verificar Pagos
    public function verificarPago(Request $request){
      //Obtenemos el monto que pago
      // dd($request);
      $monto = $request->input('cuota');
      $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
                              where('no_dia', $request->input('no_cuota'))->
                              where('estado', 0)->first();
      // dd($ficha_pago);
      //La cuota que se tenia que pagar
      $ajuste = $ficha_pago->cuota;
       // dd($ajuste);
      //Se crea una variable para ver si el monto pagado y la cuota que se pago es cero
      // if($monto=="")
      // {
      //   $monto=0;
      // }

      $esCero = $ajuste - $monto;
      // dd($esCero);
// dd($esCero);
      //Si es cero se pago completo, de lo contrario se hizo un pago parcial
      if($esCero == 0){
        $this->pagoCuota($request);
      }else if($esCero > 0){
        $this->pagoCuota($request);
      }

      else{
        $this->pagoMas($request);
      }
    }

    //Pagos de prestamos activos
    public function esUltimaCuota(Request $request){
      // dd($request);
      $ficha_pago = Prestamo::find($request->input('cliente'))
                    ->ficha_pago->where('no_dia', $request->input('no_cuota'))->first();

      // if($ficha_pago->estado_p==5)
      // {
      //   $pago->delete();
      // }
      //$ficha_pago_ultimo = Prestamo::find($request->input('cliente'))->ficha_pago->last();
      // $prestamoFirst=Prestamo::where('id', $request->input('cliente'))->first();
      // $ficha_pago_ultimo=$prestamoFirst->ficha_pago->where('no_dia', $request->input('no_cuota'))->first();
      $ficha_pago_ultimo = Prestamo::find($request->input('cliente'))->ficha_pago->last();

      if($ficha_pago_ultimo->no_dia == $ficha_pago->no_dia){
        switch($request->submitbutton) {
            case 'Pago':
                //Enlazando con ficha de pagos
                  $ficha_pago->update([
                    'estado_p' => 2,
                    'estado' => 1,
                  ]);

                  $monto = $request->input('cuota');
                  $ajuste = $ficha_pago->cuota;
                  $esCero = $ajuste - $monto;
                  $interes_actual = $ficha_pago->prestamo->interes;
                  $capital_actual = $ficha_pago->prestamo->capital_actual;
                  $mora_actual = $ficha_pago->prestamo->mora;
                  $mora_recuperada = $ficha_pago->prestamo->mora_recuperada;

                  $this->refreshClasi($ficha_pago->prestamo);

                  if($esCero == 0){
                    $estado = 1;
                    $estado_prestamo = 9;
                    $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
                      "total_capital" => $ficha_pago->prestamo->ruta->first()->hoja_ruta->total_capital - $ficha_pago->prestamo->monto,
                    ]);

                    $ficha_pago->prestamo->update([

                    ]);
                  }else{
                    $estado = 3;
                    $estado_prestamo = 10;
                    $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
                      "total_capital" => $ficha_pago->prestamo->ruta->first()->hoja_ruta->total_capital - $ficha_pago->prestamo->monto,
                      "mora" => $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $ajuste,
                    ]);
                  }

                  $calculos = $this->calcularMoraInteresCapital($monto, $ficha_pago);
                  $mora = $calculos['mora'];
                  $interes = $calculos['interes'];
                  $capital = $calculos['capital'];


                  //Creacion de pago
                  //Aca si tiene que ir un update
                  if($ficha_pago_ultimo->estado_p==5)
                  {
                    $pago=$ficha_pago->pago;
                    $pago->update([
                      'monto'=>$pago->monto + $monto,
                      'updated_at'=>Carbon::now()->toDateString()
                    ]);
                  }
                  elseif($ficha_pago_ultimo->estado_p==3)
                  {
                    $pago=$ficha_pago->pago;
                    $pago->update([
                      'monto'=>$pago->monto + $monto,
                      'updated_at'=>Carbon::now()->toDateString()
                    ]);
                  }
                  else {

                    // dd('pagoCreate2');
                    $pago = Pago::create([
                      'descripcion'     =>    $request->input('observaciones'),
                      'monto'           =>    $monto,
                      'interes'         =>    $interes,
                      'capital'         =>    $capital,
                      'mora'            =>    $mora,
                      'fecha'           =>    Carbon::now()->toDateString(),
                      'prestamo_id'        => $ficha_pago->prestamo->id,
                    ]);
                  }

                  //Actualiza la ficha de pago con el pago conrrespondiente
                  $ficha_pago->update([
                              'estado'  => 1,
                              'estado_p'  => $estado,
                              'pago_id' => $pago->id,
                  ]);

                  $ficha_pago->prestamo->update([
                              'estado_p_id' => $estado_prestamo,
                              'pagado'      => $pago->prestamo->pagado + $pago->monto,
                              'interes'     => $interes_actual + $interes,
                              'capital'     => $capital_actual + $capital,
                              'capital_recuperado' => $ficha_pago->prestamo->capital_recuperado + $pago->capital,
                              'saldo' => $ficha_pago->cuota - $pago->monto,

                  ]);

                //  $nuevaFicha = $ficha_pago->prestamo->ficha_pago()->create([
                //       'no_dia'=> 123,
                //       'fecha'=> Carbon::now()->format('Y-m-d'),
                //       'cuota'=> $ficha_pago->cuota - $pago->monto,
                //       'interes' => $ficha_pago->interes - $pago->interes,
                //       'capital' => $ficha_pago->capital - $pago->capital,
                //       'total'   => $ficha_pago->cuota - $pago->monto,
                //       'mora' => $ficha_pago->mora - $pago->mora,
                //       'tipo'  =>0,
                //     ]);

                  $agencia = Agencia::find(1);
                  $agencia->update([
                      'capital'   => $agencia->capital + $pago->capital,
                  ]);
                  $this->refreshClasi($ficha_pago->prestamo);
                  // $ficha_pago->prestamo->cliente->update([
                  //   'clasificacion_id'  => $ficha_pago->prestamo->_id,
                  // ]);

                  return 1;
            break;
            case 'No pago':

            $ficha_pago->update([
              'estado_p' => 2,
              'estado' => 1,
            ]);

            $ficha_pago->save();

            $estado_prestamo = 10;

            $ficha_pago->prestamo->update([
                        'estado_p_id' => $estado_prestamo,
            ]);



              return 1;
            break;
      }}else {
        # code...
      }{
        return 0;
      }
    }

    public function actualizarPrestamo($prestamo, $monto, $interes, $capital, $mora, $mora_recuperada, $capital_pagado){
      echo '<pre>';
        var_dump($mora);
      echo '</pre>';
      $prestamo->update([
        'pagado'          => $prestamo->pagado + $monto,
        'interes'         => $prestamo->interes + $interes,
        'capital'         => $prestamo->capital_activo + $capital,
        'mora'            => $prestamo->mora + $mora,
        'mora_recuperada' => $prestamo->mora_recuperada + $mora_recuperada,
        'capital_recuperado' => $prestamo->capital_recuperado + $capital_pagado,
      ]);

      $prestamo->save();
    }



    public function pagoCuota(Request $request){
      //Buscamos la ficha de pago actual donde se pago
      $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
                              where('no_dia', $request->input('no_cuota'))->
                              where('estado', 0)->first();

     $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->count();
     $this->refreshClasi($ficha_pago->prestamo);




      //Inicializamos variables
      $monto = 0;
      $ajuste = 0;
      $esCero = 0;
      $estado = 0;
      $interes = 0;
      $sobrante = 0;
      $mora = 0;
      $capital = 0;
      $interes_actual = 0;
      $capital_actual = 0;
      $moraa = 0;
      $mora2 = 0;
      $cont = 0;


      //Obtenemos el monto que pago
      $monto = $request->input('cuota');

      //La cuota que se tenia que pagar
      $ajuste = $ficha_pago->cuota;

      //Se crea una variable para ver si el monto pagado y la cuota que se pago es cero
      $esCero = $ajuste - $monto;

      //Si es cero se pago completo, de lo contrario se hizo un pago parcial
      if($esCero == 0){
        $estado = 1;
      }else{
        $estado = 3;
      }

      $calculos = $this->calcularMoraInteresCapital($monto, $ficha_pago);
      $mora = $calculos['mora'];
      $interes = $calculos['interes'];
      $capital = $calculos['capital'];

      //Creacion de pago
      $fichaUpdate=Prestamo::find($request->input('cliente'))->ficha_pago->
                              where('no_dia', $request->input('no_cuota'))->
                              where('estado', 0);
      // if($ficha_pago->estado_p==5)
      // {
      //   $pago=$ficha_pago->pago;
      //   $pago->update([
      //     'monto'=>$pago->monto + $monto,
      //     'updated_at'=> Carbon::now()->toDateString(),
      //   ]);
      // }
      // else {
        // dd('pagoCreate3');
        $pago = Pago::create([
          'descripcion'     =>    $request->input('observaciones'),
          'monto'           =>    $monto,
          'interes'         =>    $interes,
          'capital'         =>    $capital,
          'mora'            =>    $mora,
          'fecha'           =>    Carbon::now()->toDateString(),
          'prestamo_id'        => $request->input('cliente'),
        ]);
      // }


      //Actualiza la ficha de pago con el pago correspondiente
      $ficha_pago->update([
                  'estado'  => 1,
                  'estado_p'  => $estado,
                  'pago_id' => $pago->id,
      ]);

      $plan_ficha = $ficha_pago->prestamo->plan->nombre;



      //Se agrega lo que falta a la fecha siguiente
      if($plan_ficha == "Semanal" && $esCero != 0 || $plan_ficha == "Quincena" && $esCero != 0){
          $ficha_pago_siguiente = $ficha_pago->subFicha_siguiente();
        }else{
          $ficha_pago_siguiente = $ficha_pago->siguiente();
        }

      $ficha_no_pagada = $ficha_pago->subficha_anterior() ? $ficha_pago->subficha_anterior()->estado_p : 0;

      if($ficha_no_pagada == '2' || $ficha_no_pagada == '3'){
        if($estado == 1){
          $moraa = 0;
        }
        elseif($estado == 3){
          // dd($ficha_pago->total);
          if($monto>=$ficha_pago->total)
          {
            $moraa =  0;
            $mora2 = 0;
          }
          elseif($monto<$ficha_pago->cuota) {
            $moraa =  $ficha_pago->prestamo->plan->mora + $ficha_pago->mora - $mora;
            $mora2 = $ficha_pago->prestamo->plan->mora;
          }
        }
        else{
          $moraa =  $ficha_pago->prestamo->plan->mora + $ficha_pago->mora - $mora;
          $mora2 = $ficha_pago->prestamo->plan->mora;
        }
        if($plan_ficha == "Semanal" && $esCero != 0 || $plan_ficha == "Quincena" && $esCero != 0){
          if($ficha_pago->cont == 1){
            $moraa = $ficha_pago->prestamo->plan->mora - $mora;
            $mora2 = 0;
            $cont= 1;
          }else{
            $moraa = $ficha_pago->mora + $ficha_pago->prestamo->plan->mora;
            $mora2 = $ficha_pago->prestamo->plan->mora;
            $cont = 1;
          }
        }
      }

      if($ficha_pago_siguiente->tipo == 1){
        $cont=0;
      }


      $cuota_siguiente = $ficha_pago_siguiente->cuota +  $esCero;

      $this->actualizarPrestamo($ficha_pago->prestamo ,$pago->monto, $interes, $capital, $mora2, $pago->mora, $pago->capital);

      $recargo_mora = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $mora2;
      $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
        'mora'  => $recargo_mora,
      ]);

      $ficha_pago_siguiente->update([
        'ajuste'    => $mora2 ? $mora2 : 0,
        'interes'   => $ficha_pago_siguiente->interes + $ficha_pago->interes - $interes,
        'mora'      => $mora2 ? $mora2 : 0,
        'capital'   => $ficha_pago_siguiente->capital + $ficha_pago->capital - $capital,
        'cuota'     => $cuota_siguiente + $mora2,
        'tipo'      => 1,
        'cont'      => $cont,
      ]);

      $agencia = Agencia::find(1);
      $agencia->update([
          'capital'   => $agencia->capital + $pago->capital,
      ]);

    }

    //Pago prestamo vencido
    public function pagoPrestamoVencido(Request $request){
      $id = $request->input('prestamo_v');
      $prestamo = Prestamo::find($id);
      // dd($request);
      $monto = $request->input('total_v');
      $ulFicha = $prestamo->ficha_pago->last();
      $cuota = $ulFicha->cuota;
      $saldo = $cuota - $monto;
// dd($saldo);
      if($saldo == 0){
        // dd('if');
        $estado = 9;
        //Creacion de pago
        // dd('pagoCreate4');
        $mora_pendiente=$prestamo->mora - $prestamo->mora_recuperada;
        $primerPaso=100*$prestamo->plan->interes;
        $interesPorcentaje=($primerPaso/$prestamo->plan->total)/100;
        $capitalPorcentaje=1-$interesPorcentaje;
        if(is_null($ulFicha->pago_id))
        {
          // dd($saldo);
          $pago = Pago::create([
            'descripcion'     =>    $request->input('observaciones'),
            'monto'           =>    $monto,
            'interes'         =>    $ulFicha->interes,
            'capital'         =>    $ulFicha->capital,
            'mora'            =>    $ulFicha->mora,
            'fecha'           =>    Carbon::now()->toDateString(),
            'prestamo_id'        => $id,
          ]);
          $ficha_extra=Ficha_pago::create([
            'no_dia'=>$ulFicha->no_dia,
            'fecha'=>Carbon::now()->toDateString(),
            'cuota'=>$ulFicha->cuota,
            'mora'=>0,
            'total'=>$ulFicha->cuota,
            'interes'=>$interesPorcentaje*$monto,
            'capital'=>$capitalPorcentaje*$monto,
            'ajuste'=>0,
            'total_pago'=>$monto,
            'prestamo_id'=>$id,
            'pago_id'=>$pago->id,
            'estado_p'=>1,
            'estado'=>1,
            'tipo'=>1,
            'cont'=>1,
          ]);

          //Se actualiza el prestamo
          $interes_actual = $ulFicha->prestamo->interes;
          $capital_actual = $ulFicha->prestamo->capital_recuperado;
          $mora_actual = $prestamo->mora;


          // Se actualiza el prestamo con lo que se a pagado
          $prestamo->update([
                      'pagado'  => $ulFicha->prestamo->pagado + $pago->monto,
                      'interes' => $interes_actual + $pago->interes,
                      'capital_recuperado' => $capital_actual + $pago->capital,
                      'mora_recuperada'  => $mora_actual,
                      'estado_p_id' => '9',
          ]);

          $agencia = Agencia::find(1);
          $agencia->update([
              'capital'   => $agencia->capital + $pago->capital,
          ]);
        }
        else
        {
          $pago=$ulFicha->pago;
          // dd($pago);
          $montoU=$pago->monto + $monto;
          $interesU=$pago->interes+$ulFicha->interes;
          $capitalU=$pago->capital+$ulFicha->capital;
          $moraU=$pago->mora+$ulFicha->mora;
          $pago->update([
            'descripcion'     =>    $request->input('observaciones'),
            'monto'           =>    $montoU,
            'interes'         =>    $interesU,
            'capital'         =>    $capitalU,
            'mora'            =>    $moraU,
            'fecha'           =>    Carbon::now()->toDateString(),
            'prestamo_id'        => $id,
          ]);
          $ficha_extra=Ficha_pago::create([
            'no_dia'=>$ulFicha->no_dia,
            'fecha'=>Carbon::now()->toDateString(),
            'cuota'=>$ulFicha->cuota,
            'mora'=>0,
            'total'=>$ulFicha->cuota,
            'interes'=>$interesPorcentaje*$monto,
            'capital'=>$capitalPorcentaje*$monto,
            'ajuste'=>0,
            'total_pago'=>$monto,
            'prestamo_id'=>$id,
            'pago_id'=>$pago->id,
            'estado_p'=>1,
            'estado'=>1,
            'tipo'=>1,
            'cont'=>1,
          ]);

          //Se actualiza el prestamo
          $interes_actual = $ulFicha->prestamo->interes;
          $capital_actual = $ulFicha->prestamo->capital_recuperado;
          $mora_actual = $prestamo->mora;


          // Se actualiza el prestamo con lo que se a pagado
          $prestamo->update([
                      'pagado'  => $ulFicha->prestamo->pagado + $pago->monto,
                      'interes' => $interes_actual + $pago->interes,
                      'capital_recuperado' => $capital_actual + $pago->capital,
                      'mora_recuperada'  => $mora_actual,
                      'estado_p_id' => '9',
          ]);

          $agencia = Agencia::find(1);
          $agencia->update([
              'capital'   => $agencia->capital + $pago->capital,
          ]);
        }
      }else{
        $estado = 9;
        //Creacion de pago
        // dd('pagoCreate4');
        $mora_pendiente=$prestamo->mora - $prestamo->mora_recuperada;
        $primerPaso=100*$prestamo->plan->interes;
        $interesPorcentaje=($primerPaso/$prestamo->plan->total)/100;
        $capitalPorcentaje=1-$interesPorcentaje;
        if(is_null($ulFicha->pago_id))
        {
          // dd($saldo);
          $pago = Pago::create([
            'descripcion'     =>    $request->input('observaciones'),
            'monto'           =>    $monto,
            'interes'         =>    $ulFicha->interes,
            'capital'         =>    $ulFicha->capital,
            'mora'            =>    $ulFicha->mora,
            'fecha'           =>    Carbon::now()->toDateString(),
            'prestamo_id'        => $id,
          ]);
          $ficha_extra=Ficha_pago::create([
            'no_dia'=>$ulFicha->no_dia,
            'fecha'=>Carbon::now()->toDateString(),
            'cuota'=>$ulFicha->cuota,
            'mora'=>0,
            'total'=>$ulFicha->cuota,
            'interes'=>$interesPorcentaje*$monto,
            'capital'=>$capitalPorcentaje*$monto,
            'ajuste'=>0,
            'total_pago'=>$monto,
            'prestamo_id'=>$id,
            'pago_id'=>$pago->id,
            'estado_p'=>1,
            'estado'=>1,
            'tipo'=>1,
            'cont'=>1,
          ]);

          //Se actualiza el prestamo
          $interes_actual = $ulFicha->prestamo->interes;
          $capital_actual = $ulFicha->prestamo->capital_recuperado;
          $mora_actual = $prestamo->mora;


          // Se actualiza el prestamo con lo que se a pagado
          $prestamo->update([
                      'pagado'  => $ulFicha->prestamo->pagado + $pago->monto,
                      'interes' => $interes_actual + $pago->interes,
                      'capital_recuperado' => $capital_actual + $pago->capital,
                      'mora_recuperada'  => $mora_actual,
                      'estado_p_id' => '9',
          ]);

          $agencia = Agencia::find(1);
          $agencia->update([
              'capital'   => $agencia->capital + $pago->capital,
          ]);
        }
        else
        {
          $pago=$ulFicha->pago;
          // dd($pago);
          $montoU=$pago->monto + $monto;
          $interesU=$pago->interes+$ulFicha->interes;
          $capitalU=$pago->capital+$ulFicha->capital;
          $moraU=$pago->mora+$ulFicha->mora;
          $pago->update([
            'descripcion'     =>    $request->input('observaciones'),
            'monto'           =>    $montoU,
            'interes'         =>    $interesU,
            'capital'         =>    $capitalU,
            'mora'            =>    $moraU,
            'fecha'           =>    Carbon::now()->toDateString(),
            'prestamo_id'        => $id,
          ]);
          $ficha_extra=Ficha_pago::create([
            'no_dia'=>$ulFicha->no_dia,
            'fecha'=>Carbon::now()->toDateString(),
            'cuota'=>$ulFicha->cuota,
            'mora'=>0,
            'total'=>$ulFicha->cuota,
            'interes'=>$interesPorcentaje*$monto,
            'capital'=>$capitalPorcentaje*$monto,
            'ajuste'=>0,
            'total_pago'=>$monto,
            'prestamo_id'=>$id,
            'pago_id'=>$pago->id,
            'estado_p'=>1,
            'estado'=>1,
            'tipo'=>1,
            'cont'=>1,
          ]);

          //Se actualiza el prestamo
          $interes_actual = $ulFicha->prestamo->interes;
          $capital_actual = $ulFicha->prestamo->capital_recuperado;
          $mora_actual = $prestamo->mora;


          // Se actualiza el prestamo con lo que se a pagado
          $prestamo->update([
                      'pagado'  => $ulFicha->prestamo->pagado + $pago->monto,
                      'interes' => $interes_actual + $pago->interes,
                      'capital_recuperado' => $capital_actual + $pago->capital,
                      'mora_recuperada'  => $mora_actual,
                      'estado_p_id' => '9',
          ]);

          $agencia = Agencia::find(1);
          $agencia->update([
              'capital'   => $agencia->capital + $pago->capital,
          ]);
        // }
      }


    }
    return redirect()->back();
  }
    //No pago ya sea semanal o quincenal, que son diferentes.
    public function noPagoOtro(Request $request, $ficha_pago){

      //Se obtiene el interes y las cuotas actuales de las fichas
      $interes = $ficha_pago->interes;
      $mora = $ficha_pago->mora;
      $capital = $ficha_pago->capital;
      $cuota = $ficha_pago->cuota;

      //Se obtiene ficha siguiente del prestamo
      $ficha_pago_siguiente = $ficha_pago->subficha_siguiente();

      //Se obtiene el numero de fichas no pagadas
      $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->where('no_dia',$ficha_pago->no_dia)->count();

      if($ficha_no_pagadas >= 1){

        $this->refreshClasi($ficha_pago->prestamo);


        $ficha_pago->update([
          'estado_p' => 2,
          'estado' => 1,
        ]);
        $ficha_pago->save();

        $moraa = $ficha_pago->mora;
        if($ficha_pago->cont == 1){
          // dd('if uno');
          $moraa = $moraa;
          $mora2 = 0;
          $cont= 1;
        } else{
          // dd('else');
          $moraa = $moraa + $ficha_pago->prestamo->plan->mora;
          $mora2 = $ficha_pago->prestamo->plan->mora;
          $cont = 1;
        }

        // if($ficha_pago->tipo == 1){
        //   $cont=1;
        // }
        if(isset($ficha_pago_siguiente))
        {
          if($ficha_pago_siguiente->tipo == 1){
              // dd('if dos');
            $cont=0;
          }

          $ficha_pago_siguiente->update([
            'interes'     => $ficha_pago_siguiente->interes + $interes,
            'mora'        => $moraa,
            'capital'     => $ficha_pago_siguiente->capital + $capital,
            'ajuste'      => $mora2,
            'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora2,
            'tipo'        => 1,
            'cont'        => $cont,
          ]);
        }
        $mora_a_recargar = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $mora2;
        $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
          'mora'  =>  $mora_a_recargar,
        ]);

        $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora2,0,0);
      } else {
        $this->refreshClasi($ficha_pago->prestamo);
        // $ficha_pago->prestamo->update([
        //   "clasificacion_id"  => 1,
        // ]);

        $ficha_pago->update([
          'estado_p' => '2',
          'estado' => 1,
        ]);
        $ficha_pago->save();
        if(isset($ficha_pago_siguiente))
        {
          $ficha_pago_siguiente->update([
            'interes'     => $ficha_pago_siguiente->interes + $interes,
            'mora'        => $ficha_pago_siguiente->mora + $mora,
            'capital'     => $ficha_pago_siguiente->capital + $capital,
            'ajuste'      => 0,
            'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
            'tipo'        => 1,
          ]);
        }
      }


    }

    //Metodo cuando paga de mas.
    public function pagoMas(Request $request){
      //Nuevo estado de pago adelantado.
      $estado_pagado = 1;
      $estado_parcial = 4; //Parcial adelantado

      $ficha_pago = Prestamo::find($request->input('cliente'))->ficha_pago->
                              where('no_dia', $request->input('no_cuota'))->
                              where('estado', 0)->first();

      $ficha_pago->update([
        'total_pago' => $request->cuota,
      ]);

      $this->refreshClasi($ficha_pago->prestamo);

      //Obtenemos el monto que pago
      $monto = $request->input('cuota');

      //La cuota que se tenia que pagar
      $cuota = $ficha_pago->cuota;

      //Se crea una variable para ver si el monto pagado y la cuota que se pago es cero
      $diferencia = $monto - $cuota;

      //Division para determinar cuantos pagos se han realizado con ese monto
      $cuota_definida = $ficha_pago->prestamo->ficha_pago->first()->cuota;
      $div = $diferencia / $cuota_definida;

      //Se divide en dos para determinar cuando fichas de pago de va a cubrir y cual sera la diferencia
      $numero = explode('.',$div);

      $fichas_pagadas = (int)$numero[0];
      $ficha_parcial = (count($numero)==2) ? $numero[1] : 0;

      $detallePago = array(
        'monto'   => $ficha_pago->cuota,
        'interes' => $ficha_pago->interes,
        'capital' => $ficha_pago->capital,
        'mora'    => $ficha_pago->mora,
      );

      //Se realiza lo que cubre de un pago
      // if($ficha_pago->estado_p==5)
      // {
      //   $pago=$ficha_pago->pago;
      //   $pago->update([
      //     'monto'=>$pago->monto + $monto,
      //     'updated_at'=> Carbon::now()->toDateString(),
      //   ]);
      // }
      // else {
        // dd('crearPago1');
        $this->crearPago($request, $ficha_pago, $estado_pagado, $detallePago, 0);
      // }

      for ($i=0; $i < $fichas_pagadas; $i++) {
        $ficha_pago = $ficha_pago->siguiente();
        $detallePago = array(
          'monto'   => $ficha_pago->cuota,
          'interes' => $ficha_pago->interes,
          'capital' => $ficha_pago->capital,
          'mora'    => $ficha_pago->mora,
        );
        //Se realiza lo que cubre de un pago
        // if($ficha_pago->estado_p==5)
        // {
        //   $pago=$ficha_pago->pago;
        //   $pago->update([
        //     'monto'=>$pago->monto + $monto,
        //     'updated_at'=> Carbon::now()->toDateString(),
        //   ]);
        // }
        // else {
          // dd('crearPago2');
          $this->crearPago($request, $ficha_pago, 4, $detallePago, 0);
        // }
      }

      if($ficha_parcial > 0){
          $diferencia = $diferencia - ($cuota_definida * $fichas_pagadas);
          $ficha_prueba=$ficha_pago->siguiente();
          if(isset($ficha_prueba))
          {
            $ficha_pago = $ficha_pago->siguiente();
          }
          else
          {
            $ficha_pago=$ficha_pago;
          }

          $estado = 5;
// dd($ficha_pago);
          $calculos = $this->calcularMoraInteresCapital($diferencia, $ficha_pago);
          $mora = $calculos['mora'];
          $interes = $calculos['interes'];
          $capital = $calculos['capital'];

          $detallePagoParcial = array(
            'monto' => $diferencia,
            'interes' => $interes,
            'mora'  => $mora,
            'capital' => $capital,
          );
            // if($ficha_pago->estado_p==5)
            // {
            //   $pago=$ficha_pago->pago;
            //   $pago->update([
            //     'monto'=>$pago->monto + $monto,
            //     'updated_at'=> Carbon::now()->toDateString(),
            //   ]);
            // }
            // else {
              // dd('crearPago3');
              $pago = $this->crearPago($request, $ficha_pago, $estado, $detallePagoParcial, 1);
            // }

          $ficha_pago->update([
            'interes' => $ficha_pago->interes - $pago->interes,
            'mora' => 0,
            'capital' => $ficha_pago->capital - $pago->capital,
            'cuota' => $ficha_pago->cuota - $pago->monto,
          ]);
      }
    }

    public function crearPago($request ,$ficha_pago, $estado, $detallePago, $tipo){
      //Creacion de pago
      // dd($ficha_pago);
      // dd('pagoCreate1');

      if($ficha_pago->estado_p==5)
      {
        $pago=$ficha_pago->pago;
        $pago->update([
          'monto'=>$pago->monto + $detallePago['monto'],
          'updated_at'=> Carbon::now()->toDateString(),
        ]);
      }
      elseif($ficha_pago->estado_p==3)
      {
        $pago=$ficha_pago->pago;
        $pago->update([
          'monto'=>$pago->monto + $monto,
          'updated_at'=>Carbon::now()->toDateString()
        ]);
      }
      else {
        // dd('crearPago2');
        $pago = Pago::create([
          'descripcion'     =>    $request->input('observaciones'),
          'monto'           =>    $detallePago['monto'],
          'interes'         =>    $detallePago['interes'],
          'capital'         =>    $detallePago['capital'],
          'mora'            =>    $detallePago['mora'],
          'fecha'           =>    Carbon::now()->toDateString(),
          'prestamo_id'        => $request->input('cliente'),
        ]);
      }




      $est = 1;
      if($tipo==0){
        $est = 1;
      }else{
        $est = 0;
      }
      //Se actualiza fecha
      $ficha_pago->update([
        'estado'  => $est,
        'estado_p'  => $estado,
        'pago_id' => $pago->id,
      ]);

      //Se actualiza el prestamo
      $interes_actual = $ficha_pago->prestamo->interes;
      $capital_actual = $ficha_pago->prestamo->capital_recuperado;
      $mora_actual = $ficha_pago->prestamo->mora_recuperada;

      // Se actualiza el prestamo con lo que se a pagado
      $ficha_pago->prestamo->update([
                  'pagado'  => $pago->prestamo->pagado + $pago->monto,
                  'interes' => $interes_actual + $pago->interes,
                  'capital_recuperado' => $capital_actual + $pago->capital,
                  'mora_recuperada'  => $mora_actual + $pago->mora,
      ]);

      return $pago;
    }

    public function revertirPagoHoy(Request $request)
    {
      $id_ficha = $request->get('valor');
      $ficha_pago = Ficha_pago::find($id_ficha);
      $estado_p = $ficha_pago->estado_p;
      $estado = $ficha_pago->estado;

      $ficha_pago->update([
        'estado_p'=>0,
        'estado'=>0,
      ]);
      $ficha_pago->save();
    }

    public function eliminarPago(Request $request)
    {

      $id_ficha = $request->get('valor');
      $ficha_pago = Ficha_pago::find($id_ficha);
      $pago = $ficha_pago->pago;
      $estado = $ficha_pago->estado_p;
      $prestamo = $ficha_pago->prestamo;
      $plan = $ficha_pago->prestamo->plan->nombre;


      if($estado != 2){
      //Revertir actualizacion de prestamo
      //**INICIO**
        $pagado_actual = $prestamo->pagado;
        $interes_actual = $prestamo->interes;
        $capital_actual = $ficha_pago->prestamo->capital_recuperado;
        $mora_recuperada_actual = $prestamo->mora_recuperada;
        $mora_actual = $prestamo->mora;
        $ficha_pago_siguiente = $ficha_pago->siguiente();

        //**FIN

        //Revertir capital
        //**INICIO**
        if (isset($pago)) {
          $agencia = Agencia::find(1);
          $agencia->update([
              'capital'   => $agencia->capital - $pago->capital,
          ]);
        }
        else {
          // code...
        }

          //**FIN**

        //Revertir cambios en fichas de pago [Actual y siguiente]

        $ficha_pago->update([
          'estado_p' => '0',
          'estado' => 0,
          'pago_id' => null,
        ]);
        $ficha_pago->save();

        $ficha_predeterminada = $ficha_pago->prestamo->ficha_pago->first();

        if($plan=='Diario'){
          $ficha_pago_siguiente = $ficha_pago->siguiente();
          if (isset($ficha_pago_siguiente)) {
            $ficha_pago_siguiente->update([
              'interes'     => $ficha_predeterminada->interes,
              'mora'        => $ficha_predeterminada->mora,
              'capital'     => $ficha_predeterminada->capital,
              'ajuste'      => 0,
              'cuota'       => $ficha_predeterminada->cuota,
              'tipo'        => 1,
            ]);

            $pago->delete();
          }
          else {

            $pago->delete();
          }


        }else {

        }

      }else {
        $pagado_actual = $prestamo->pagado;
        $interes_actual = $prestamo->interes;
        $capital_actual = $prestamo->capital_recuperado;
        $mora_actual = $prestamo->mora_recuperada;


        //Revertir estado de ficha de pago
        $ficha_pago->update([
          'estado_p' => 0,
          'estado' => 0,
        ]);

        if($plan=='Diario'){

          $ficha_predeterminada = $ficha_pago->prestamo->ficha_pago->first();
          $ficha_pago_siguiente = $ficha_pago->siguiente();
          $mora_actual = $prestamo->mora;

          $prestamo->update([
            'mora'  => $mora_actual - $ficha_pago_siguiente->ajuste,
          ]);


          $ficha_pago_siguiente->update([
            'interes'     => $ficha_predeterminada->interes,
            'mora'        => $ficha_predeterminada->mora,
            'capital'     => $ficha_predeterminada->capital,
            'ajuste'      => 0,
            'cuota'       => $ficha_predeterminada->cuota,
            'tipo'        => 1,
          ]);

        }
      }









      $ficha_pago_eliminar=Ficha_pago::find($request->get('valor'));
      if(is_null($ficha_pago_eliminar->pago_id))
      {
        $ficha_pago_eliminar->update([

          'cuota'=>0,
          'mora'=>0,
          'total'=>0,
          'interes'=>0,
          'capital'=>0,
          'ajuste'=>0,
          'pago_id'=>NULL,
          'estado_p'=>0,
          'estado'=>0,
          'tipo'=>0,
          'cont'=>0,
        ]);
      }
      else {
        $pago_eliminar = Pago::find($ficha_pago_eliminar->pago_id);
        $pago_eliminar->delete();
        $ficha_pago_eliminar->update([

          'cuota'=>0,
          'mora'=>0,
          'total'=>0,
          'interes'=>0,
          'capital'=>0,
          'ajuste'=>0,
          'pago_id'=>NULL,
          'estado_p'=>0,
          'estado'=>0,
          'tipo'=>0,
          'cont'=>0,
        ]);
      }
    }

    /**
    *Metodo para revertir el pago, revierte los cambios que establece un pago
    *
    *@param Request $request que contiene la informacion del form lo importante es el id de ficha de pago
    */

    public function revertirCompleto(Request $request)
    {
      $ficha_inicial=Ficha_pago::where('prestamo_id', $request->get('valor'))->first();
      $plan = $ficha_inicial->prestamo->plan->nombre;
      if($plan=='Diario')
      {
        $fichas_diarias=Ficha_pago::where('prestamo_id', $request->get('valor'));
        $pagos_diarios=Pago::where('prestamo_id', $request->get('valor'));
        $fichas_diarias->update([
          'interes'     => $ficha_inicial->interes,
          'mora'        => $ficha_inicial->mora,
          'capital'     => $ficha_inicial->capital,
          'ajuste'      => 0,
          'cuota'       => $ficha_inicial->cuota,
          'tipo'        => 1,
          'estado_p'    => 0,
          'pago_id'     => NULL
        ]);

        // $ficha_inicial->update([
        //   'tipo'        => 1,
        // ]);

        $pagos_diarios->delete();
      }
    }


    public function revertirPago(Request $request) {
        $id_ficha = $request->get('valor');
        $ficha_pago = Ficha_pago::find($id_ficha);
        $pago = $ficha_pago->pago;
        // $pagosTotales=Pago::where('')
        $estado = $ficha_pago->estado_p;
        $prestamo = $ficha_pago->prestamo;
        $plan = $ficha_pago->prestamo->plan->nombre;


        if($estado != 2){
        //Revertir actualizacion de prestamo
        //**INICIO**
          $pagado_actual = $prestamo->pagado;
          $interes_actual = $prestamo->interes;
          $capital_actual = $ficha_pago->prestamo->capital_recuperado;
          $mora_recuperada_actual = $prestamo->mora_recuperada;
          $mora_actual = $prestamo->mora;
          $ficha_pago_siguiente = $ficha_pago->siguiente();

          if (isset($ficha_pago_siguiente)) {
            $prestamo->update([
                        'pagado'              => $pagado_actual - $pago->monto,
                        'interes'             => $interes_actual - $pago->interes,
                        'capital_recuperado'  => $capital_actual - $pago->capital,
                        'mora_recuperada'     => $mora_recuperada_actual - $pago->mora,
                        'mora'                => $mora_actual - $ficha_pago_siguiente->ajuste,
            ]);
          }
          else {
            // dd($pagado_actual);
            // dd($pago);
            // dd($pago);
            if(isset($pago))
            {
              $prestamo->update([

                          'pagado'              => $pagado_actual - $pago->monto,
                          'interes'             => $interes_actual - $pago->interes,
                          'capital_recuperado'  => $capital_actual - $pago->capital,
                          'mora_recuperada'     => $mora_recuperada_actual - $pago->mora,
              ]);
            }
          }

          //**FIN

          //Revertir capital
          //**INICIO**
          if(isset($pago))
          {
            $agencia = Agencia::find(1);
            $agencia->update([
                'capital'   => $agencia->capital - $pago->capital,
            ]);
          }
            //**FIN**

          //Revertir cambios en fichas de pago [Actual y siguiente]

          $ficha_pago->update([
            'estado_p' => '0',
            'estado' => 0,
            'pago_id' => null,
          ]);
          $ficha_pago->save();

          $ficha_predeterminada = $ficha_pago->prestamo->ficha_pago->first();

          if($plan=='Diario'){
            $ficha_pago_siguiente = $ficha_pago->siguiente();
            if (isset($ficha_pago_siguiente)) {
              $ficha_pago_siguiente->update([
                'interes'     => $ficha_predeterminada->interes,
                'mora'        => $ficha_predeterminada->mora,
                'capital'     => $ficha_predeterminada->capital,
                'ajuste'      => 0,
                'cuota'       => $ficha_predeterminada->cuota,
                'tipo'        => 1,
              ]);
              if($ficha_pago_siguiente->estado_p==5)
              {
                $ficha_pago_siguiente->update([
                  'estado_p' => '0',
                  'estado' => 0,
                  'pago_id' => null,
                ]);
              }

              $pago->delete();
            }
            else {

              $pago->delete();
            }


          }else {
            $ficha_predeterminada = $ficha_pago->prestamo->ficha_pago->first();
            $ficha_pago_siguiente = $ficha_pago->siguiente();
            $mora_actual = $prestamo->mora;

            if (isset($ficha_pago_siguiente)) {
              $prestamo->update([
                'mora'  => $mora_actual - $ficha_pago_siguiente->ajuste,
              ]);


              $ficha_pago_siguiente->update([
                'interes'     => $ficha_predeterminada->interes,
                'mora'        => $ficha_predeterminada->mora,
                'capital'     => $ficha_predeterminada->capital,
                'ajuste'      => 0,
                'estado_p'    => 0,
                'cuota'       => $ficha_predeterminada->cuota,
                'pago_id'     => NULL,
                'tipo'        => 1,
              ]);
            }
            if(isset($pago))
            {
              $pago->delete();
            }

          }

        }else {
          $pagado_actual = $prestamo->pagado;
          $interes_actual = $prestamo->interes;
          $capital_actual = $prestamo->capital_recuperado;
          $mora_actual = $prestamo->mora_recuperada;


          //Revertir estado de ficha de pago
          $ficha_pago->update([
            'estado_p' => 0,
            'estado' => 0,
            'cont' => 0,
          ]);

          if($plan=='Diario'){

            $ficha_predeterminada = $ficha_pago->prestamo->ficha_pago->first();
            $ficha_pago_siguiente = $ficha_pago->siguiente();
            $mora_actual = $prestamo->mora;

            $prestamo->update([
              'mora'  => $mora_actual - $ficha_pago_siguiente->ajuste,
            ]);


            $ficha_pago_siguiente->update([
              'interes'     => $ficha_predeterminada->interes,
              'mora'        => $ficha_predeterminada->mora,
              'capital'     => $ficha_predeterminada->capital,
              'ajuste'      => 0,
              'cuota'       => $ficha_predeterminada->cuota,
              'tipo'        => 1,
            ]);

          }
          else {
            $ficha_predeterminada = $ficha_pago->prestamo->ficha_pago->first();
            $ficha_pago_siguiente = $ficha_pago->siguiente();
            $mora_actual = $prestamo->mora;

            if (isset($ficha_pago_siguiente)) {
              $prestamo->update([
                'mora'  => $mora_actual - $ficha_pago_siguiente->ajuste,
              ]);


              $ficha_pago_siguiente->update([
                'interes'     => $ficha_predeterminada->interes,
                'mora'        => $ficha_predeterminada->mora,
                'capital'     => $ficha_predeterminada->capital,
                'ajuste'      => 0,
                'cuota'       => $ficha_predeterminada->cuota,
                'tipo'        => 1,
              ]);
            }

          }
          if(isset($pago))
          {
            $pago->delete();
          }
        }
        if(isset($pago))
        {
          $pago->delete();
        }
        // return $pago;
       // return redirect()->back();
    }

    /**
    *Meodo creado para identificar la mora
    *
    *
    */
    public function establecerMoraDiario($ficha_anterior, $ficha_pago, $ficha_pago_siguiente){
      //Se actualiza en hoja ruta , prestamo , ficha de pago, prestamo
      $mora = $ficha_pago->mora; //Obtenemos la mora actual de la ficha de pago
      $recargo_mora = 0;
      $hoja_ruta = $ficha_pago->prestamo->ruta->first()->hoja_ruta;
      $prestamo = $ficha_pago->prestamo;
      if($ficha_anterior->cont == 0){
        $mora_a_recargar = $ficha_pago->prestamo->plan->mora; //Se obtiene el valor a recargar de la mora
        $recargo_mora = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $ficha_pago->prestamo->plan->mora;
        $cont = 1;
        $hoja_ruta->update([
          'mora'  => $recargo_mora,
        ]);
        $hoja_ruta->save();
      }else {
        $mora_a_recargar = 0;
        $cont = 0;
      }

      $ficha_pago->update([
        'cont' => $cont,
      ]);
      //Se actualiza la ficha de pago
      $ficha_pago_siguiente->update([
        'mora'        => $mora + $mora_a_recargar, //Se recarga la mora
        'ajuste'      => $mora_a_recargar, //Auxiliar que lleva la mora recargada por ficha de pago
      ]);

      return $mora_a_recargar;
    }



    public function establecerClasi($prestamo, $ficha_pago){
// dd($prestamo->ficha_pago->where('ajuste','>',0)->count());
     if($ficha_pago->no_dia == 1){
       $prestamo->update([
         'clasificacion_id'  => 1,
       ]);
     }else{

       $ficha_no_pagadas = $prestamo->ficha_pago->where('cont',1)->count();
        // dd($ficha_no_pagadas);
       $estado = 1;
       if($ficha_no_pagadas == 0){
         $estado = 1;
       }
       if($ficha_no_pagadas == 1 || $ficha_no_pagadas == 2){
         $estado = 2;
       }
       if($ficha_no_pagadas >= 3){
         $estado = 3;
       }
       $prestamo->update([
         'clasificacion_id'  => $estado,
       ]);

     }
     return "funciona";
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function infoPagoPendiente($id)
    {
      $ficha_pago = Ficha_pago::find($id);
      $credito = $ficha_pago->prestamo;
      $datos = response()->json([
        'cuota' => $ficha_pago->cuota,
        'no_cuota' => $ficha_pago->no_dia,
        'cliente'  => $credito->cliente->persona->nombre . " " . $credito->cliente->persona->apellido,
        'prestamo_id' => $credito->id,
        'prestamo' => $credito->id,
        'no_prestamo' => $credito->cliente->prestamos->count(),
        'mora'  => $ficha_pago->mora,
        'total' => $ficha_pago->cuota,
        'dia_perdon'  => $ficha_pago->cont,
      ]);

      return $datos;
    }

    public function cambioCartera(Request $request ,$id){
      $rutaActual=Hoja_ruta::whereHas('ruta.prestamo', function($query) use($id){
        $query->where('id', $id);
      })->first();
      $prestamo=Prestamo::find($id);
      $rutas=Hoja_ruta::all();
      $esta=$rutaActual->toArray();
      $todas=$rutas->toArray();
      return response(['actual'=>$rutaActual->nombre, 'todas'=>$rutas, 'prestamo'=>$id]);
    }

    public function actualizarCartera(Request $request){
      $nueva=$request->nueva_ruta;
      $prestamo=$request->prestamo_new;
      $rutaActual=Ruta::where('prestamo_id',$prestamo)->first();
      $rutaActual->update([
        'hoja_ruta_id'=>$nueva,
        'updated_at'=>Carbon::now(),
      ]);
      return redirect()->back();
    }

    public function infoPago(Request $request ,$id){
      // dd($id);

      if($request->tipo == 2){
        $ficha_pago = Ficha_pago::find($id);
        $credito = $ficha_pago->prestamo;
        $datos = response()->json([
          'cuota' => $ficha_pago->cuota,
          'no_cuota' => $ficha_pago->no_dia,
          'cliente'  => $credito->cliente->persona->nombre . " " . $credito->cliente->persona->apellido,
          'prestamo_id' => $credito->id,
          'prestamo' => $credito->id,
          'no_prestamo' => $credito->cliente->prestamos->count(),
          'mora'  => $ficha_pago->mora,
          'total' => $ficha_pago->cuota,
          'dia_perdon'  => $ficha_pago->cont,
        ]);
      }else{
        $credito = Prestamo::find($id);
        $ficha_pagoLast = $credito->ficha_pago->last();
        $ficha_pago = $credito->ficha_pago->where('estado', '0')->where('tipo','1')->first();
        // $ficha_ultima=Ficha_pago::where('prestamo_id',$ficha_pago_pre->prestamo_id)->orderBy('id', 'DESC')->first();
        if($credito->ficha_pago==null){
          $datos = "";
        } elseif($credito->estado_p_id==10) {
          $pagosSaldo = Pago::where('prestamo_id',$id)->sum('monto');
          $pagosMora = Pago::where('prestamo_id',$id)->sum('mora');
          $totalPagosFicha=Ficha_pago::where('prestamo_id',$id)->whereNotNull('pago_id')->get();
          $sumaTotalPagos=0;
          foreach ($totalPagosFicha as $pagosList) {
            $listadoPagos[]=Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
            // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
          }
          if(isset($listadoPagos))
          {
            foreach ($listadoPagos as $sumaPagos) {
              $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
            }
          }
          else {
            $sumaTotalPagos=0;
          }
          // $totalRealPago=\App\Ficha_pago::where('')
           $saldoTotal=$sumaTotalPagos-$pagosMora;
           $final=round(($credito->monto - $saldoTotal)+($credito->mora - $credito->mora_recuperada), 2);
          $datos = response()->json([
            'cuota' => $final,
            'no_cuota' => $ficha_pagoLast->no_dia,
            'cliente'  => $credito->cliente->persona->nombre . " " . $credito->cliente->persona->apellido,
            'prestamo' => $credito->id,
            'no_prestamo' => $credito->cliente->prestamos->count(),
            'mora'  => $ficha_pagoLast->mora,
            'total' => $ficha_pagoLast->cuota,
            'dia_perdon'  => $ficha_pagoLast->cont,
          ]);
      }
      else {
        $datos = response()->json([
            'cuota' => $ficha_pago->cuota,
            'no_cuota' => $ficha_pago->no_dia,
            'cliente'  => $credito->cliente->persona->nombre . " " . $credito->cliente->persona->apellido,
            'prestamo' => $credito->id,
            'no_prestamo' => $credito->cliente->prestamos->count(),
            'mora'  => $ficha_pago->mora,
            'total' => $ficha_pago->cuota,
            'dia_perdon'  => $ficha_pago->cont,
          ]);
      }
    }
      return $datos;
    }

    public function pagoEmpleado(Request $request, $id){
      $usuario = User::find($id);

      $sueldo_base = $request->input("sueldo_base");
      $comision_clientes =  $request->input('comision_cliente');
      $comision_capital = $request->input("comision_capital");
      $descuento_mora = $request->input("descuento");
      $combustible = $request->input("combustible");
      $total = $request->input("total");

      $pagoNew = Boleta_pago::create([
        'fecha'             =>  Carbon::now(),
        'sueldo_base'       =>  $sueldo_base,
        'comision_clientes' =>  $comision_clientes,
        'comision_capital'  =>  $comision_capital,
        'descuento_mora'    =>  $descuento_mora,
        'combustible'       =>  $combustible,
        'total'             =>  $total,
        "users_id"          =>  $usuario->id,
      ]);

      $agencia = Agencia::find(1);
      $agencia->update([
        'capital' => $agencia->capital - $total,
      ]);

      $usuario->update([
        'comision_cliente_nuevo' => $usuario->comision_cliente_nuevo - $comision_clientes,
      ]);
      return redirect()->route('usuarios.show', ['id' => $usuario->id]);
    }



    public function RealizarPagoModal(Request $request, $id)
    {
      $credito = Prestamo::find($id);
      // $this->refreshClasi($credito);
      $pagos = Pago::where("prestamo_id",$id)->get();
      return view('Creditos.show',compact('credito', 'pagos'));
    }


    public function ModalTablaPago($id)
    {
      return view('Pagos.partials.modal-tabla-pago', compact('id'));
    }


    public function DatatablePendientes($id)
    {
      $prestamo = Prestamo::find($id);
      $ficha_pago = Ficha_pago::where('prestamo_id', $prestamo->id)
                     ->whereDate('fecha', '<=', Carbon::now())
                     ->where('tipo','=',1)
                     ->where('estado_p', '=',0);

       return Datatables::of($ficha_pago)
       ->addColumn('no_dia', function ($ficha) {return $ficha->no_dia;})
       ->addColumn('fecha',  function ($ficha) {return Carbon::parse($ficha->fecha)->formatLocalized('%A') ." " . Carbon::parse($ficha->fecha)->format('d-m-y');})
       ->addColumn('cuota',  function ($ficha) {return $ficha->cuota - $ficha->mora;})
       ->addColumn('mora',   function ($ficha) {return $ficha->mora;})
       ->addColumn('total_pago', function ($ficha) {return $ficha->cuota;})
       ->addColumn('estado', function ($ficha) {return "<td><a><span class='badge badge-warning pull-center'>Pendiente</span></a></td>";})
       ->addColumn('monto_pagado', function ($ficha) {return 0;})
       ->addColumn('Fecha_ingreso', function ($ficha) use($prestamo) {$this->refreshClasi($prestamo); return isset($ficha_pago->pago) ? $ficha_pago->pago->created_at : "Sin fecha";})
       ->addColumn('accion', function ($ficha) use ($ficha_pago) {
         return view('Pagos.partials.modal-table-action', compact('ficha', 'ficha_pago'));
       })
       ->rawColumns(['estado', 'accion'])
       ->setRowClass('text-center')
       ->make(true);
    }

    public function pagosVencidos2()
    {
      $fecha_hoy=Carbon::now();
      $fichas_totales=Ficha_pago::all();
      $fichas_pasadas=Ficha_pago::where('fecha', '<', $fecha_hoy->toDateString())->where('estado_p',0)->get();
      // $fichas_pasadas->chunk(1000,function($cunk){
        foreach ($fichas_pasadas as $ficha_pago) {
          $ficha_pago_ultimo = Prestamo::find($ficha_pago->prestamo_id)->ficha_pago->last();
          if($ficha_pago_ultimo->no_dia == $ficha_pago->no_dia){
              $ficha_pago->update([
                'estado_p' => 2,
                'estado' => 1,
              ]);

              $ficha_pago->save();
              $estado_prestamo = 10;
              $ficha_pago->prestamo->update([
                          'estado_p_id' => $estado_prestamo,
              ]);

            }
            else {
              if($ficha_pago->prestamo->plan->nombre=="Diario")
              {
                $interes = $ficha_pago->interes;
                $capital = $ficha_pago->capital;
                $cuota = $ficha_pago->cuota;

                //Se obtiene ficha siguiente del prestamo
                $ficha_pago_siguiente = $ficha_pago->siguiente();

                //Se obtiene el numero de fichas no pagadas
                $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->count();
                $ficha_no_pagada = $ficha_pago->subficha_anterior() ? $ficha_pago->subficha_anterior()->estado_p : 0;

                if($ficha_no_pagada == '2' || $ficha_no_pagada == '3' ){

                  $this->refreshClasi($ficha_pago->prestamo);


                  $ficha_pago->update([
                    'estado_p' => 2,
                    'estado' => 1,
                    'updated_at' => Carbon::now(),
                  ]);

                  $ficha_pago->save();


                  // $mora_a_recargar = $ficha_pago->prestamo->plan->mora;
                  $mora_a_recargar = $this->establecerMoraDiario($ficha_pago->subficha_anterior(), $ficha_pago, $ficha_pago_siguiente);

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    // 'mora'        => $mora + $mora_a_recargar,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    // 'ajuste'      => $mora_a_recargar,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora_a_recargar,
                  ]);
                  // $recargo_mora = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $ficha_pago->prestamo->plan->mora;
                  // $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
                  //   'mora'  => $recargo_mora,
                  // ]);


                  $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora_a_recargar,0,0);

                } else {
                 $this->refreshClasi($ficha_pago->prestamo);
                  // $ficha_pago->prestamo->update([
                  //   "clasificacion_id"  => 1,
                  // ]);
                  $ficha_pago->update([
                    'estado_p' => '2',
                    'estado' => 1,
                    'updated_at' => Carbon::now(),
                  ]);
                  $ficha_pago->save();

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    'mora'        => 0,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    'ajuste'      => 0,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
                  ]);

                }
              }

              if($ficha_pago->prestamo->plan->nombre=="Semanal" || $ficha_pago->prestamo->plan->nombre=="Quincena")
              {
                $interes = $ficha_pago->interes;
                $mora = $ficha_pago->mora;
                $capital = $ficha_pago->capital;
                $cuota = $ficha_pago->cuota;

                //Se obtiene ficha siguiente del prestamo
                $ficha_pago_siguiente = $ficha_pago->subficha_siguiente();

                //Se obtiene el numero de fichas no pagadas
                $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->where('no_dia',$ficha_pago->no_dia)->count();

                if($ficha_no_pagadas >= 1){

                  $this->refreshClasi($ficha_pago->prestamo);


                  $ficha_pago->update([
                    'estado_p' => 2,
                    'estado' => 1,
                  ]);
                  $ficha_pago->save();

                  $moraa = $ficha_pago->mora;
                  if($ficha_pago->cont == 1){
                    // dd('if uno');
                    $moraa = $moraa;
                    $mora2 = 0;
                    $cont= 1;
                  } else{
                    // dd('else');
                    $moraa = $moraa + $ficha_pago->prestamo->plan->mora;
                    $mora2 = $ficha_pago->prestamo->plan->mora;
                    $cont = 1;
                  }

                  // if($ficha_pago->tipo == 1){
                  //   $cont=1;
                  // }
                  if($ficha_pago_siguiente->tipo == 1){
                      // dd('if dos');
                    $cont=0;
                  }

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    'mora'        => $moraa,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    'ajuste'      => $mora2,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora2,
                    'tipo'        => 1,
                    'cont'        => $cont,
                  ]);

                  $mora_a_recargar = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $mora2;
                  $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
                    'mora'  =>  $mora_a_recargar,
                  ]);

                  $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora2,0,0);
                } else {
                  $this->refreshClasi($ficha_pago->prestamo);
                  // $ficha_pago->prestamo->update([
                  //   "clasificacion_id"  => 1,
                  // ]);

                  $ficha_pago->update([
                    'estado_p' => '2',
                    'estado' => 1,
                  ]);
                  $ficha_pago->save();

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    'mora'        => $ficha_pago_siguiente->mora + $mora,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    'ajuste'      => 0,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
                    'tipo'        => 1,
                  ]);

                }
              }
            }
        }
      // });

      return response()->json(['Exito'=>'Exito']);
    }

    public function pagosVencidos()
    {
      $fecha_hoy=Carbon::now();
      for ($contador=0; $contador < 1; $contador++) {

      }
      $fichas_pasadas=Ficha_pago::where('prestamo_id',$ficha_pago->prestamo_id)->where('fecha', '<', $fecha_hoy->toDateString())->where('estado_p',0)->get();
      foreach ($fichas_pasadas as $ficha_pago) {
        $ficha_pago_ultimo = Prestamo::find($ficha_pago->prestamo_id)->ficha_pago->last();
        if($ficha_pago_ultimo->id == $ficha_pago->id){
        $ficha_pago->update([
                      'estado_p' => 2,
                      'estado' => 1,
                    ]);

                    $ficha_pago->save();

                    $estado_prestamo = 10;

                    $ficha_pago->prestamo->update([
                                'estado_p_id' => $estado_prestamo,
                    ]);
        }
        else {

          if($ficha_pago->prestamo->plan->nombre == "Semanal" || $ficha_pago->prestamo->plan->nombre == "Quincena")
          {
            $interes = $ficha_pago->interes;
            $mora = $ficha_pago->mora;
            $capital = $ficha_pago->capital;
            $cuota = $ficha_pago->cuota;

            //Se obtiene ficha siguiente del prestamo
            $ficha_pago_siguiente = $ficha_pago->subficha_siguiente();

            //Se obtiene el numero de fichas no pagadas
            $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->where('no_dia',$ficha_pago->no_dia)->count();

            if($ficha_no_pagadas >= 1){

              $this->refreshClasi($ficha_pago->prestamo);


              $ficha_pago->update([
                'estado_p' => 2,
                'estado' => 1,
              ]);
              $ficha_pago->save();

              $moraa = $ficha_pago->mora;
              if($ficha_pago->cont == 1){
                // dd('if uno');
                $moraa = $moraa;
                $mora2 = 0;
                $cont= 1;
              } else{
                // dd('else');
                $moraa = $moraa + $ficha_pago->prestamo->plan->mora;
                $mora2 = $ficha_pago->prestamo->plan->mora;
                $cont = 1;
              }

              // if($ficha_pago->tipo == 1){
              //   $cont=1;
              // }
              if($ficha_pago_siguiente->tipo == 1){
                  // dd('if dos');
                $cont=0;
              }

              $ficha_pago_siguiente->update([
                'interes'     => $ficha_pago_siguiente->interes + $interes,
                'mora'        => $moraa,
                'capital'     => $ficha_pago_siguiente->capital + $capital,
                'ajuste'      => $mora2,
                'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora2,
                'tipo'        => 1,
                'cont'        => $cont,
              ]);

              $mora_a_recargar = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $mora2;
              $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
                'mora'  =>  $mora_a_recargar,
              ]);

              $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora2,0,0);
            } else {
              $this->refreshClasi($ficha_pago->prestamo);
              // $ficha_pago->prestamo->update([
              //   "clasificacion_id"  => 1,
              // ]);

              $ficha_pago->update([
                'estado_p' => '2',
                'estado' => 1,
              ]);
              $ficha_pago->save();

              $ficha_pago_siguiente->update([
                'interes'     => $ficha_pago_siguiente->interes + $interes,
                'mora'        => $ficha_pago_siguiente->mora + $mora,
                'capital'     => $ficha_pago_siguiente->capital + $capital,
                'ajuste'      => 0,
                'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
                'tipo'        => 1,
              ]);

            }
          }
          else
          {
            $interes = $ficha_pago->interes;
            $capital = $ficha_pago->capital;
            $cuota = $ficha_pago->cuota;

            //Se obtiene ficha siguiente del prestamo
            $ficha_pago_siguiente = $ficha_pago->siguiente();

            //Se obtiene el numero de fichas no pagadas
            $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->count();
            $ficha_no_pagada = $ficha_pago->subficha_anterior() ? $ficha_pago->subficha_anterior()->estado_p : 0;

            if($ficha_no_pagada == '2' || $ficha_no_pagada == '3' ){

              $this->refreshClasi($ficha_pago->prestamo);


              $ficha_pago->update([
                'estado_p' => 2,
                'estado' => 1,
                'updated_at' => Carbon::now(),
              ]);

              $ficha_pago->save();


              // $mora_a_recargar = $ficha_pago->prestamo->plan->mora;
              $mora_a_recargar = $this->establecerMoraDiario($ficha_pago->subficha_anterior(), $ficha_pago, $ficha_pago_siguiente);

              $ficha_pago_siguiente->update([
                'interes'     => $ficha_pago_siguiente->interes + $interes,
                // 'mora'        => $mora + $mora_a_recargar,
                'capital'     => $ficha_pago_siguiente->capital + $capital,
                // 'ajuste'      => $mora_a_recargar,
                'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora_a_recargar,
              ]);
              // $recargo_mora = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $ficha_pago->prestamo->plan->mora;
              // $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
              //   'mora'  => $recargo_mora,
              // ]);


              $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora_a_recargar,0,0);

            } else {
             $this->refreshClasi($ficha_pago->prestamo);
              // $ficha_pago->prestamo->update([
              //   "clasificacion_id"  => 1,
              // ]);
              $ficha_pago->update([
                'estado_p' => '2',
                'estado' => 1,
                'updated_at' => Carbon::now(),
              ]);
              $ficha_pago->save();

              $ficha_pago_siguiente->update([
                'interes'     => $ficha_pago_siguiente->interes + $interes,
                'mora'        => 0,
                'capital'     => $ficha_pago_siguiente->capital + $capital,
                'ajuste'      => 0,
                'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
              ]);

            }
          }

        }


        }
      // dd($prestamo);
        return response()->json(['Exito'=>'ECSITO']);

    }

    public function cuentaFichas()
    {
      $fecha_hoy=Carbon::now();
      $fichas_pasadas=Ficha_pago::where('fecha', '<', $fecha_hoy->toDateString())->where('estado_p', 0)->get();
      $cuenta=count($fichas_pasadas);
      return response()->json($cuenta);
    }

    public function intentoTres()
    {
      $fecha_hoy=Carbon::now();
      $planesDiarios=Plan::where('nombre','Diario')->get();
      foreach ($planesDiarios as $plan)
      {
        $creditosDiarios=Prestamo::where('plan_id',$plan->id)->where('estado_p_id',5)->where('activo', '!=', 0)->get();
        foreach ($creditosDiarios as $credito)
        {
          $gg = Ficha_pago::where('prestamo_id',$credito->id)
                                      ->where('prestamo_id', '!=', null)
                                      ->where('fecha','<',$fecha_hoy)
                                      ->where('estado_p', 0)
                                      ->orderBy('prestamo_id')
                                      ->get();
          if($gg->isNotEmpty())
          {
            foreach($gg as $gg)
          $fichas_pasadas=$gg;
          // $fichas_pasadas->update([
          //
          // ]);
          }
          $creditosDiarios2=$gg = Ficha_pago::where('prestamo_id',$credito->id)
                                      ->where('prestamo_id', '!=', null)
                                      ->where('fecha','<',$fecha_hoy)
                                      ->where('estado_p', 0)
                                      ->orderBy('prestamo_id');
          $credito=$creditosDiarios2;
        }
      }
      // dd($fichas_pasadas);
      return response()->json($fichas_pasadas);
    }

    public function primerPasoDiario()
    {
      $fecha_hoy=Carbon::now();
      // $ficha_pasada=Ficha_pago::where('fecha', '<', $fecha_hoy->toDateString())->where('estado_p', 0)->orderBy('prestamo_id')->first();
      $ficha_pasada = Ficha_pago::where('estado_p', 0)
                              ->where('fecha', '<', Carbon::now()->toDateString())
                              ->whereHas('prestamo.plan', function ($query)
                              {
                                $query->where('nombre', 'like', '%Diario%');
                              })
                              ->first();
      // dd($ficha_pasada);
      if(isset($ficha_pasada))
      {
        $this->segundoPasoDiario($ficha_pasada->prestamo_id);
      }
      else {
        return response()->json('fin');
      }
    }

    public function segundoPasoDiario($prestamo_id)
    {
      $fecha_hoy=Carbon::now();
      $ficha_pago=Ficha_pago::where('fecha', '<', $fecha_hoy->toDateString())->where('estado_p',0)->where('prestamo_id', $prestamo_id)->first();
      // $fichas_pasadas->chunk(1000,function($cunk){
        // foreach ($fichas_pasadas as $ficha_pago) {
          $ficha_pago_ultimo = Prestamo::find($ficha_pago->prestamo_id)->ficha_pago->last();
          if($ficha_pago_ultimo->no_dia == $ficha_pago->no_dia){
              $ficha_pago->update([
                'estado_p' => 2,
                'estado' => 1,
              ]);

              $ficha_pago->save();
              $estado_prestamo = 10;
              $ficha_pago->prestamo->update([
                          'estado_p_id' => $estado_prestamo,
              ]);

            }
            else {
              if($ficha_pago->prestamo->plan->nombre=="Diario")
              {
                $interes = $ficha_pago->interes;
                $capital = $ficha_pago->capital;
                $cuota = $ficha_pago->cuota;

                //Se obtiene ficha siguiente del prestamo
                $ficha_pago_siguiente = $ficha_pago->siguiente();

                //Se obtiene el numero de fichas no pagadas
                $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->count();
                $ficha_no_pagada = $ficha_pago->subficha_anterior() ? $ficha_pago->subficha_anterior()->estado_p : 0;

                if($ficha_no_pagada == '2' || $ficha_no_pagada == '3' ){

                  $this->refreshClasi($ficha_pago->prestamo);


                  $ficha_pago->update([
                    'estado_p' => 2,
                    'estado' => 1,
                    'updated_at' => Carbon::now(),
                  ]);

                  $ficha_pago->save();


                  // $mora_a_recargar = $ficha_pago->prestamo->plan->mora;
                  $mora_a_recargar = $this->establecerMoraDiario($ficha_pago->subficha_anterior(), $ficha_pago, $ficha_pago_siguiente);

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    // 'mora'        => $mora + $mora_a_recargar,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    // 'ajuste'      => $mora_a_recargar,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora_a_recargar,
                  ]);
                  // $recargo_mora = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $ficha_pago->prestamo->plan->mora;
                  // $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
                  //   'mora'  => $recargo_mora,
                  // ]);


                  $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora_a_recargar,0,0);

                } else {
                 $this->refreshClasi($ficha_pago->prestamo);
                  // $ficha_pago->prestamo->update([
                  //   "clasificacion_id"  => 1,
                  // ]);
                  $ficha_pago->update([
                    'estado_p' => '2',
                    'estado' => 1,
                    'updated_at' => Carbon::now(),
                  ]);
                  $ficha_pago->save();

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    'mora'        => 0,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    'ajuste'      => 0,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
                  ]);

                }
              }

            }
        // }
      // });
      $this->primerPasoDiario();
      // return response()->json(['Exito'=>'Exito']);
    }

    public function fichasSemanales()
    {
      set_time_limit(0);
      $ficha_pago_pre = Ficha_pago::where('estado_p', 0)
                              ->where('fecha', '<', Carbon::now()->toDateString())
                              ->whereHas('prestamo.plan', function ($query)
                              {
                                $query->where('nombre', 'like', '%Semanal%');
                              })
                              // ->where('prestamo_id',76)
                              ->first();
// dd($ficha_pago_pre);
    if(!is_null($ficha_pago_pre))
    {
      $ficha_primaria=Ficha_pago::where('prestamo_id',$ficha_pago_pre->prestamo_id)->where('no_dia',$ficha_pago_pre->no_dia)->first();
      $ficha_ultima=Ficha_pago::where('prestamo_id',$ficha_pago_pre->prestamo_id)->orderBy('id', 'DESC')->first();

      if($ficha_primaria->id==$ficha_ultima->id)
      {
        $ficha_pago = Ficha_pago::where('estado_p', 0)
                                ->where('fecha', '<', Carbon::now()->toDateString())
                                // ->where('prestamo_id',76)
                                ->whereHas('prestamo.plan', function ($query)
                                {
                                  $query->where('nombre', 'like', '%Semanal%');
                                })
                                ->first();

      }
      elseif($ficha_pago_pre->id==$ficha_primaria->id && $ficha_pago_pre->estado_p==0)
      {
        $ficha_pago = Ficha_pago::where('estado_p', 0)
                                ->where('fecha', '<', Carbon::now()->toDateString())
                                // ->where('prestamo_id',76)
                                ->whereHas('prestamo.plan', function ($query)
                                {
                                  $query->where('nombre', 'like', '%Semanal%');
                                })
                                ->first();
      }
      else{
        $ficha_pago = Ficha_pago::where('estado_p', 0)
                                ->where('fecha', '<', Carbon::now()->toDateString())
                                // ->where('prestamo_id',76)
                                ->whereHas('prestamo.plan', function ($query)
                                {
                                  $query->where('nombre', 'like', '%Semanal%');
                                })
                                ->first();
      }
      //TENGO QUE VALIDAR PORQUE NO ENTRA< CUANDO LA FICHA PAGO ES IGUAL A LA FICHA PAGO PRIMARIA DE LA CUOTA, VOLVER A PONER EL SCRIPT DE BD
// dd($ficha_primaria);
// dd($ficha_pago);

      if(count($ficha_pago)>0)
      {
      // foreach ($fichas_semana as $ficha_pago) {

        if($ficha_pago->id==$ficha_primaria->id && $ficha_pago->estado_p==0)
        {
          // dd($ficha_pago);
          $todas_semanales = $ficha_pago;
          // return response()->json(['Exito1'=>$ficha_pago]);
          $this->segundoPasoSemanal($ficha_pago);
        }
        elseif($ficha_pago->id==$ficha_ultima->id)
        {
          // dd('dos');
          $todas_semanales = $ficha_pago;
          // return response()->json(['Exito1'=>$ficha_pago]);
          $this->segundoPasoSemanal($ficha_pago);
        }
        else {

// dd("tres");
        if(isset($ficha_pago->subficha_anterior()->id))
        {

          if($ficha_pago->subficha_anterior()->estado_p != 1 && $ficha_pago->subficha_anterior()->no_dia == $ficha_pago->no_dia)
          {
            $todas_semanales = $ficha_pago;
            // return response()->json(['Exito1'=>$ficha_pago]);
            $this->segundoPasoSemanal($ficha_pago);
          }
          else {

            $ficha_pago->update([
              'estado_p'=>6
            ]);
            $this->fichasSemanales();
            // return response()->json(['Exito2'=>$ficha_pago]);
          }
        }
        else {
          $ficha_pago->update([
            'estado_p'=>6
          ]);
          $this->fichasSemanales();
          // return response()->json(['Exito3'=>$ficha_pago]);
        }
      }
    // }
    // return response()->json(['Exito'=>$todas_semanales]);
      }
      else {
        return response()->json(['Exito'=>'Ya no hay']);
      }
    }
    else {
      return response()->json(['Exito'=>'Ya no hay']);
    }
  }

    public function primerPasoSemanal()
    {
      $fecha_hoy=Carbon::now();
      // $ficha_pasada=Ficha_pago::where('fecha', '<', $fecha_hoy->toDateString())->where('estado_p', 0)->orderBy('prestamo_id')->first();
      $ficha_pasada = Ficha_pago::where('estado_p', 0)
                              ->where('fecha', '<', Carbon::now()->toDateString())
                              ->whereHas('prestamo.plan', function ($query)
                              {
                                $query->where('nombre', 'like', '%Diario%');
                              })
                              ->get();
      // dd($ficha_pasada);
      if(isset($ficha_pasada))
      {
        $this->segundoPasoSemanal($ficha_pasada->prestamo_id);
      }
      else {
        return response()->json('fin');
      }
    }

    public function segundoPasoSemanal($id)
    {
      set_time_limit(0);
      // dd($id);
      $fecha_hoy=Carbon::now();
      $ficha_pago=Ficha_pago::where('id',$id->id)->first();
      $prestamo=Prestamo::where('id', $id->prestamo_id)->first();
      // $fichas_pasadas->chunk(1000,function($cunk){
        // foreach ($fichas_pasadas as $ficha_pago) {
          $ficha_pago_ultimo = Prestamo::find($prestamo->id)->ficha_pago->last();
          if($ficha_pago_ultimo->no_dia == $ficha_pago->no_dia){
              $ficha_pago->update([
                'estado_p' => 2,
                'estado' => 1,
              ]);

              $ficha_pago->save();
              $estado_prestamo = 10;
              $ficha_pago->prestamo->update([
                          'estado_p_id' => $estado_prestamo,
              ]);

            }
            else {
              if($ficha_pago->prestamo->plan->nombre=="Diario")
              {
                $interes = $ficha_pago->interes;
                $capital = $ficha_pago->capital;
                $cuota = $ficha_pago->cuota;

                //Se obtiene ficha siguiente del prestamo
                $ficha_pago_siguiente = $ficha_pago->siguiente();

                //Se obtiene el numero de fichas no pagadas
                $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->count();
                $ficha_no_pagada = $ficha_pago->subficha_anterior() ? $ficha_pago->subficha_anterior()->estado_p : 0;

                if($ficha_no_pagada == '2' || $ficha_no_pagada == '3' ){

                  $this->refreshClasi($ficha_pago->prestamo);


                  $ficha_pago->update([
                    'estado_p' => 2,
                    'estado' => 1,
                    'updated_at' => Carbon::now(),
                  ]);

                  $ficha_pago->save();


                  // $mora_a_recargar = $ficha_pago->prestamo->plan->mora;
                  $mora_a_recargar = $this->establecerMoraDiario($ficha_pago->subficha_anterior(), $ficha_pago, $ficha_pago_siguiente);

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    // 'mora'        => $mora + $mora_a_recargar,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    // 'ajuste'      => $mora_a_recargar,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora_a_recargar,
                  ]);
                  // $recargo_mora = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $ficha_pago->prestamo->plan->mora;
                  // $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
                  //   'mora'  => $recargo_mora,
                  // ]);


                  $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora_a_recargar,0,0);

                } else {
                 $this->refreshClasi($ficha_pago->prestamo);
                  // $ficha_pago->prestamo->update([
                  //   "clasificacion_id"  => 1,
                  // ]);
                  $ficha_pago->update([
                    'estado_p' => '2',
                    'estado' => 1,
                    'updated_at' => Carbon::now(),
                  ]);
                  $ficha_pago->save();

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    'mora'        => 0,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    'ajuste'      => 0,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
                  ]);

                }
              }

              if($ficha_pago->prestamo->plan->nombre=="Semanal" || $ficha_pago->prestamo->plan->nombre=="Quincena")
              {
                $interes = $ficha_pago->interes;
                $mora = $ficha_pago->mora;
                $capital = $ficha_pago->capital;
                $cuota = $ficha_pago->cuota;

                //Se obtiene ficha siguiente del prestamo
                $ficha_pago_siguiente = $ficha_pago->subficha_siguiente();

                //Se obtiene el numero de fichas no pagadas
                $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->where('no_dia',$ficha_pago->no_dia)->count();

                if($ficha_no_pagadas >= 1){

                  $this->refreshClasi($ficha_pago->prestamo);


                  $ficha_pago->update([
                    'estado_p' => 2,
                    'estado' => 1,
                  ]);
                  $ficha_pago->save();

                  $moraa = $ficha_pago->mora;
                  if($ficha_pago->cont == 1){
                    // dd('if uno');
                    $moraa = $moraa;
                    $mora2 = 0;
                    $cont= 1;
                  } else{
                    // dd('else');
                    $moraa = $moraa + $ficha_pago->prestamo->plan->mora;
                    $mora2 = $ficha_pago->prestamo->plan->mora;
                    $cont = 1;
                  }

                  // if($ficha_pago->tipo == 1){
                  //   $cont=1;
                  // }
                  if($ficha_pago_siguiente->tipo == 1){
                      // dd('if dos');
                    $cont=0;
                  }

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    'mora'        => $moraa,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    'ajuste'      => $mora2,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora2,
                    'tipo'        => 1,
                    'cont'        => $cont,
                  ]);

                  $mora_a_recargar = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $mora2;
                  $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
                    'mora'  =>  $mora_a_recargar,
                  ]);

                  $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora2,0,0);
                } else {
                  $this->refreshClasi($ficha_pago->prestamo);
                  // $ficha_pago->prestamo->update([
                  //   "clasificacion_id"  => 1,
                  // ]);

                  $ficha_pago->update([
                    'estado_p' => '2',
                    'estado' => 1,
                  ]);
                  $ficha_pago->save();

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    'mora'        => $ficha_pago_siguiente->mora + $mora,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    'ajuste'      => 0,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
                    'tipo'        => 1,
                  ]);

                }
              }
            }
        // }
      // });
      $this->fichasSemanales();
      // return response()->json(['Exito'=>'Exito']);
    }



    public function primerPasoQuincena()
    {
      set_time_limit(0);
      $fecha_hoy=Carbon::now();
      // $ficha_pasada=Ficha_pago::where('fecha', '<', $fecha_hoy->toDateString())->where('estado_p', 0)->orderBy('prestamo_id')->first();
      $fichas_semana = Ficha_pago::where('estado_p', 0)
                              ->where('fecha', '<', Carbon::now()->toDateString())
                              ->whereHas('prestamo.plan', function ($query)
                              {
                                $query->where('nombre', 'like', '%Quincena%');
                              })
                              ->first();
      // dd($ficha_pasada);
      if(isset($fichas_semana))
      {
        $this->segundoPasoQuincena($fichas_semana->prestamo_id);
      }
      else {
        return response()->json('fin');
      }
    }

    public function fichasQuincena()
    {
      set_time_limit(0);
      $ficha_pago_pre = Ficha_pago::where('estado_p', 0)
                              ->where('fecha', '<', Carbon::now()->toDateString())
                              // ->where('prestamo_id',222)
                              ->whereHas('prestamo.plan', function ($query)
                              {
                                $query->where('nombre', 'like', '%Quincena%');
                              })
                              ->first();
                              // dd($ficha_pago_pre);
      if(!is_null($ficha_pago_pre))
      {
          $ficha_primaria=Ficha_pago::where('prestamo_id',$ficha_pago_pre->prestamo_id)->where('no_dia',$ficha_pago_pre->no_dia)->first();
          $ficha_ultima=Ficha_pago::where('prestamo_id',$ficha_pago_pre->prestamo_id)->orderBy('id', 'DESC')->first();
          // $ficha_ultima_real=Ficha_pago::where('prestamo_id',$ficha_pago_pre->prestamo_id)->where('id','!=',$ficha_ultima->id)->orderBy('id', 'DESC')->first();

          if($ficha_primaria->id==$ficha_ultima->id)
          {
            $ficha_pago = Ficha_pago::where('estado_p', 0)
                                    ->where('fecha', '<', Carbon::now()->toDateString())
                                    ->where('id','!=',$ficha_ultima->id)
                                    // ->where('prestamo_id',222)
                                    ->whereHas('prestamo.plan', function ($query)
                                    {
                                      $query->where('nombre', 'like', '%Quincena%');
                                    })
                                    ->first();
                                    // dd('uno');

          }
          elseif($ficha_pago_pre->id==$ficha_primaria->id && $ficha_pago_pre->estado_p==0)
          {
            $ficha_pago = Ficha_pago::where('estado_p', 0)
                                    ->where('fecha', '<', Carbon::now()->toDateString())
                                    ->where('id','!=',$ficha_ultima->id)
                                    // ->where('prestamo_id',222)
                                    ->whereHas('prestamo.plan', function ($query)
                                    {
                                      $query->where('nombre', 'like', '%Quincena%');
                                    })
                                    ->first();
                                    // dd('dos');
          }
          else{
            $ficha_pago = Ficha_pago::where('estado_p', 0)
                                    ->where('fecha', '<', Carbon::now()->toDateString())
                                    // ->where('id','!=',$ficha_ultima->id)
                                    // ->where('prestamo_id',222)
                                    ->whereHas('prestamo.plan', function ($query)
                                    {
                                      $query->where('nombre', 'like', '%Quincena%');
                                    })
                                    ->first();
                                    // dd('tres');
          }
          // dd($ficha_pago);
          if(count($ficha_pago)>0)
          {
          // foreach ($fichas_semana as $ficha_pago) {

            if($ficha_pago->id==$ficha_primaria->id && $ficha_pago->estado_p==0)
            {
              // dd('uno');
              $todas_semanales = $ficha_pago;
              // return response()->json(['Exito1'=>$ficha_pago]);
              $this->segundoPasoQuincena($ficha_pago);
            }
            elseif($ficha_pago->id==$ficha_ultima->id)
            {
              // dd('dos');
              $todas_semanales = $ficha_pago;
              // return response()->json(['Exito1'=>$ficha_pago]);
              $this->segundoPasoQuincena($ficha_pago);
            }
            else {

          // dd("tres");
            if(isset($ficha_pago->subficha_anterior()->id))
            {

              if($ficha_pago->subficha_anterior()->estado_p != 1 && $ficha_pago->subficha_anterior()->no_dia == $ficha_pago->no_dia)
              {
                $todas_semanales = $ficha_pago;
                // return response()->json(['Exito1'=>$ficha_pago]);
                $this->segundoPasoQuincena($ficha_pago);
              }
              else {
                // dd('cuatro');
                $ficha_pago->update([
                  'estado_p'=>6
                ]);
                $this->fichasQuincena();
                // return response()->json(['Exito2'=>$ficha_pago]);
              }
            }
            else {
              // dd('cinco');
              $ficha_pago->update([
                'estado_p'=>6
              ]);
              $this->fichasQuincena();
              // return response()->json(['Exito3'=>$ficha_pago]);
            }
          }
          // }
          // return response()->json(['Exito'=>$todas_semanales]);
          }
          else {
            return response()->json(['Exito'=>'Ya no hay']);
          }
      }
      else {
        return response()->json(['Exito'=>'Ya no hay']);
      }
}

    public function segundoPasoQuincena($ficha)
    {
      $fecha_hoy=Carbon::now();
      $ficha_pago=Ficha_pago::where('id', $ficha->id)->first();
      // $fichas_pasadas->chunk(1000,function($cunk){
        // foreach ($fichas_pasadas as $ficha_pago) {
        $ficha_ultima=Ficha_pago::where('prestamo_id',$ficha_pago->prestamo_id)->orderBy('id', 'DESC')->first();
        // $ficha_ultima_real=Ficha_pago::where('prestamo_id',$ficha_pago->prestamo_id)->where('id','!=',$ficha_ultima->id)->orderBy('id', 'DESC')->first();
        // $ficha_ultima_real=Ficha_pago::where('prestamo_id',$ficha_pago->prestamo_id)->where('id','!=',$ficha_ultima->id)->orderBy('id', 'DESC')->first();
          if($ficha_ultima->no_dia == $ficha_pago->no_dia){
              $ficha_pago->update([
                'estado_p' => 2,
                'estado' => 1,
              ]);

              $ficha_pago->save();
              $estado_prestamo = 10;
              $ficha_pago->prestamo->update([
                          'estado_p_id' => $estado_prestamo,
              ]);

            }
            else {
              if($ficha_pago->prestamo->plan->nombre=="Diario")
              {
                $interes = $ficha_pago->interes;
                $capital = $ficha_pago->capital;
                $cuota = $ficha_pago->cuota;

                //Se obtiene ficha siguiente del prestamo
                $ficha_pago_siguiente = $ficha_pago->siguiente();

                //Se obtiene el numero de fichas no pagadas
                $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->count();
                $ficha_no_pagada = $ficha_pago->subficha_anterior() ? $ficha_pago->subficha_anterior()->estado_p : 0;

                if($ficha_no_pagada == '2' || $ficha_no_pagada == '3' ){

                  $this->refreshClasi($ficha_pago->prestamo);


                  $ficha_pago->update([
                    'estado_p' => 2,
                    'estado' => 1,
                    'updated_at' => Carbon::now(),
                  ]);

                  $ficha_pago->save();


                  // $mora_a_recargar = $ficha_pago->prestamo->plan->mora;
                  $mora_a_recargar = $this->establecerMoraDiario($ficha_pago->subficha_anterior(), $ficha_pago, $ficha_pago_siguiente);

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    // 'mora'        => $mora + $mora_a_recargar,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    // 'ajuste'      => $mora_a_recargar,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora_a_recargar,
                  ]);
                  // $recargo_mora = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $ficha_pago->prestamo->plan->mora;
                  // $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
                  //   'mora'  => $recargo_mora,
                  // ]);


                  $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora_a_recargar,0,0);

                } else {
                 $this->refreshClasi($ficha_pago->prestamo);
                  // $ficha_pago->prestamo->update([
                  //   "clasificacion_id"  => 1,
                  // ]);
                  $ficha_pago->update([
                    'estado_p' => '2',
                    'estado' => 1,
                    'updated_at' => Carbon::now(),
                  ]);
                  $ficha_pago->save();

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    'mora'        => 0,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    'ajuste'      => 0,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
                  ]);

                }
              }

              if($ficha_pago->prestamo->plan->nombre=="Semanal" || $ficha_pago->prestamo->plan->nombre=="Quincena")
              {
                $interes = $ficha_pago->interes;
                $mora = $ficha_pago->mora;
                $capital = $ficha_pago->capital;
                $cuota = $ficha_pago->cuota;

                //Se obtiene ficha siguiente del prestamo
                $ficha_pago_siguiente = $ficha_pago->subficha_siguiente();

                //Se obtiene el numero de fichas no pagadas
                $ficha_no_pagadas = $ficha_pago->prestamo->ficha_pago->whereIn('estado_p',[2,3])->where('no_dia',$ficha_pago->no_dia)->count();

                if($ficha_no_pagadas >= 1){

                  $this->refreshClasi($ficha_pago->prestamo);


                  $ficha_pago->update([
                    'estado_p' => 2,
                    'estado' => 1,
                  ]);
                  $ficha_pago->save();

                  $moraa = $ficha_pago->mora;
                  if($ficha_pago->cont == 1){
                    // dd('if uno');
                    $moraa = $moraa;
                    $mora2 = 0;
                    $cont= 1;
                  } else{
                    // dd('else');
                    $moraa = $moraa + $ficha_pago->prestamo->plan->mora;
                    $mora2 = $ficha_pago->prestamo->plan->mora;
                    $cont = 1;
                  }

                  // if($ficha_pago->tipo == 1){
                  //   $cont=1;
                  // }
                  if($ficha_pago_siguiente->tipo == 1){
                      // dd('if dos');
                    $cont=0;
                  }

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    'mora'        => $moraa,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    'ajuste'      => $mora2,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota + $mora2,
                    'tipo'        => 1,
                    'cont'        => $cont,
                  ]);

                  $mora_a_recargar = $ficha_pago->prestamo->ruta->first()->hoja_ruta->mora + $mora2;
                  $ficha_pago->prestamo->ruta->first()->hoja_ruta->update([
                    'mora'  =>  $mora_a_recargar,
                  ]);

                  $this->actualizarPrestamo($ficha_pago->prestamo, 0,0,0,$mora2,0,0);
                } else {
                  $this->refreshClasi($ficha_pago->prestamo);
                  // $ficha_pago->prestamo->update([
                  //   "clasificacion_id"  => 1,
                  // ]);

                  $ficha_pago->update([
                    'estado_p' => '2',
                    'estado' => 1,
                  ]);
                  $ficha_pago->save();

                  $ficha_pago_siguiente->update([
                    'interes'     => $ficha_pago_siguiente->interes + $interes,
                    'mora'        => $ficha_pago_siguiente->mora + $mora,
                    'capital'     => $ficha_pago_siguiente->capital + $capital,
                    'ajuste'      => 0,
                    'cuota'       => $ficha_pago_siguiente->cuota + $cuota,
                    'tipo'        => 1,
                  ]);

                }
              }
            }
        // }
      // });
      $this->fichasQuincena();
      // return response()->json(['Exito'=>'Exito']);
    }





  public function fichasDiarias()
  {
    $fichas_dia = Ficha_pago::where('estado_p', 0)
                            ->where('fecha', '<', Carbon::now()->toDateString())
                            ->whereHas('prestamo.plan', function ($query)
                            {
                              $query->where('nombre', 'like', '%Diario%');
                            })
                            ->get();

    if(count($fichas_dia)>0)
    {
  return response()->json(['Exito'=>$fichas_dia]);
  }
  else {
    return response()->json(['Exito'=>'Ya no hay']);
  }
  }

  public function elminiarFichasQuincena()
  {
    set_time_limit(0);
    $fichas_quincena = Ficha_pago::whereHas('prestamo.plan', function ($query)
                            {
                              $query->where('nombre', 'like', '%Quincena%');
                            })
                            ->get();

    $creditos=Prestamo::whereHas('plan', function ($query)
                            {
                              $query->where('nombre', 'like', '%Quincena%');
                            })->get();

    foreach ($creditos as $credito) {
      $ficha_ultima=Ficha_pago::where('prestamo_id',$credito->id)->orderBy('id', 'DESC')->first();
      if(!is_null($ficha_ultima))
      {
        $cuenta=count(Ficha_pago::where('prestamo_id', $ficha_ultima->prestamo_id)->where('no_dia',$ficha_ultima->no_dia)->get());
        $fichas[]=$ficha_ultima->prestamo_id;
        if($cuenta==2)
        {
          $ficha_ultima->delete();
        }
      }
      // dd($ficha_ultima->prestamo_id);
      //
      // $credito2=Prestamo::find($ficha_ultima->prestamo_id);
    }


    // $fichas_extras=Ficha_pago::where('prestamo_id',$ficha_pago->prestamo_id)->orderBy('id', 'DESC')->first();
    return response()->json($fichas);
  }

  public function moraSinSaldo()
  {
    $creditos=Prestamo::where('estado_p_id',10)->get();
    foreach ($creditos as $credito) {
      $pagosSaldo = \App\Pago::where('prestamo_id',$credito->id)->sum('monto');
      $pagosMora = \App\Pago::where('prestamo_id',$credito->id)->sum('mora');
      $totalPagosFicha=\App\Ficha_pago::where('prestamo_id',$credito->id)->whereNotNull('pago_id')->get();
      $sumaTotalPagos=0;
      $listadoPagos=[];
      $saldoTotal=0;
      $final=0;
      $creditosArreglar=[];
      foreach ($totalPagosFicha as $pagosList) {
        $listadoPagos[]=\App\Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
        // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
      }
      // dd($listadoPagos);
      if(isset($listadoPagos))
      {
        foreach ($listadoPagos as $sumaPagos) {
          $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
        }
      }
      else {
        $sumaTotalPagos=0;
      }
      // $totalRealPago=\App\Ficha_pago::where('')
       $saldoTotal=$sumaTotalPagos-$pagosMora;
       $final=round(($credito->monto - $saldoTotal)+($credito->mora - $credito->mora_recuperada), 2);
       if($final<=0)
       {
         $creditosArreglar[]=$credito;
         $credito->update([
           'estado_p_id'=>9,
         ]);
       }
     }
     return response()->json($creditosArreglar);
  }



}
