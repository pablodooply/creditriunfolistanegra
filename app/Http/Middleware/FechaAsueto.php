<?php

namespace App\Http\Middleware;
use App\Asueto;
use Closure;
use Carbon\Carbon;
use DateTime;
class FechaAsueto
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // $hoy = date('d-m-Y');    
      
    // $asueto = Asueto::where('fecha_inicio','>=', $hoy )->where('fecha_fin','<=', $hoy)->first();
    // if($asueto){
    //     abort(511);
    // }else{
    //     return $next($request);
    // }
    public function handle($request, Closure $next)
    {
        
        $hoy = Carbon::now()->format('Y-m-d');     
        $asueto = Asueto::wheredate('fecha_inicio','<=', $hoy)->wheredate('fecha_fin','>=', $hoy)->first();
        if($asueto){
            abort(511);
        }else{
            return $next($request);
        }
        
    }
}
