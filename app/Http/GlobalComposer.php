<?php


namespace App\Http;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User;



class GlobalComposer {


  /**
   * Bind data to the view.
   *
   * @param  View $view
   * @return void
   */
  public function compose(View $view)
  {
    if(\Auth::check()){
      $id = Auth::user()->id;
      $user = DB::table('users')->where('id', $id)->first();

      $view->with('user', $user);
    }
  }


}
