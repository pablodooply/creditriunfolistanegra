<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Hoja_ruta extends Model
{
    //
    protected $table = 'hoja_ruta';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'nombre', 'user_id', 'activa', 'total_capital', 'capital_activo', 'comision', 'mora','capital_vencido'
    ];

    protected $guarded = [
      'id'
     ];

     public function user()
     {
         return $this->belongsTo('App\User');
     }

     public function ruta()
     {
       return $this->hasMany('App\Ruta');
     }

     public function prospectos()
     {
       return $this->hasMany('App\Prospecto');
     }

     public function supervisor(){
       return $this->belongsTo('App\User','supervisor_id');
     }

     public function prestamos()
     {
       return $this->belongsToMany('App\Prestamo', 'ruta', 'hoja_ruta_id', 'prestamo_id');
     }

     public function prestamos2($id)
     {
       return $this->belongsToMany('App\Prestamo', 'ruta', 'hoja_ruta_id', 'prestamo_id')->where('prestamo_id',$id);
     }


     public function mora_recuperada($inicio = null, $fin = null){
       return $this->with(['prestamos.pagos' => function($query) use($inicio,$fin){
         return $query->wherebetween('fecha', [$inicio, $fin]);
       }])->get();
     }

     public function pagos($inicio = null, $fin = null){
       if($inicio == null && $fin == null){
         return DB::table('pago')
                    ->join('prestamo', 'prestamo.id', '=', 'pago.prestamo_id')
                    ->join('ruta', 'prestamo.id', '=', 'ruta.prestamo_id')
                    ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                    ->where('hoja_ruta.id','=',$this->id)
                    ->select('pago.*')->get();
       } else if($inicio !=null && $fin == null){
         return DB::table('pago')
                    ->join('prestamo', 'prestamo.id', '=', 'pago.prestamo_id')
                    ->join('ruta', 'prestamo.id', '=', 'ruta.prestamo_id')
                    ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                    ->where('hoja_ruta.id','=',$this->id)
                    ->where('pago.fecha', $inicio)
                    ->select('pago.*')->get();
       } else{
         return DB::table('pago')
                    ->join('prestamo', 'prestamo.id', '=', 'pago.prestamo_id')
                    ->join('ruta', 'prestamo.id', '=', 'ruta.prestamo_id')
                    ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                    ->where('hoja_ruta.id','=',$this->id)
                    ->whereBetween('pago.fecha', [$inicio, $fin])
                    ->select('pago.*')->get();
       }

     }
}
