<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Garantia extends Model
{
  //

  protected $table      = 'garantia';
  protected $primaryKey = 'id';
  public    $timestamps = true;


  protected $fillable = [
    'nombre', 'descripcion', 'cantidad', 'valoracion_cliente', 'valoracion_empresa', 'estado_producto_cliente',
    'estado_producto_empresa', 'imagen_uno', 'imagen_dos', 'prestamo_id', 'categoria_id', 'estado_garantida_id', 'created_at', 'updated_at','created_by', 'estado', 'observacion'
  ];


  protected $guarded = [
    'id'
   ];

   public function prestamo()
   {
       return $this->belongsTo('App\Prestamo');
   }

   public function categoria()
   {
       return $this->belongsTo('App\Categoria');
   }

   public function estado_garantia()
   {
       return $this->belongsTo('App\Estado_garantia');
   }
}
