<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Ficha_pago extends Model
{
    //
    protected $table      = 'ficha_pago';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'no_dia', 'fecha', 'cuota', 'mora', 'prestamo_id', 'pago_id', 'estado', 'estado_p',
      'interes', 'capital', 'ajuste', 'total', 'tipo', 'cont', 'destacado', 'updated_at', 'total_pago'
    ];

    protected $guarded = [
      'id'
     ];

     public function prestamo()
     {
         return $this->belongsTo('App\Prestamo');
     }

     public function pago()
     {
         return $this->belongsTo('App\Pago');
     }

     public function comentarios()
      {
          return $this->morphMany('App\Comentario', 'commentable');
      }

     public function anterior(){
        return Ficha_pago::where('prestamo_id', '=', $this->prestamo_id)->where('no_dia', '=', $this->no_dia-1)->first();
     }

     public function siguiente(){
       return Ficha_pago::where('prestamo_id', '=', $this->prestamo_id)->where('no_dia', '=', $this->no_dia+1)->first();
     }

     public function subficha_siguiente(){
       return Ficha_pago::where('prestamo_id', '=', $this->prestamo_id)->where('id', '>', $this->id)->first();
     }

     public function subficha_anterior(){
       return Ficha_pago::where('prestamo_id', '=', $this->prestamo_id)->where('tipo', 1)->where('id', '<', $this->id)->orderBy('id', 'DESC')->first();
     }

     public function pagados(){
        return Ficha_pago::where('estado', '=', '1');
     }

     public function ficha_actual(){
        return Ficha_pago::where('fecha', '=', Carbon::now()->toDateString());
     }

     public function estadoActual(){
       $estado = "";
       switch ($this->estado_p) {
         case 0:
           // code...
           $estado = "<td><a><span class='badge badge-warning pull-center'>Pendiente</span></a></td>";
           break;
         case 1:
           $estado = '<td><a><span class="badge badge-primary pull-center">Pagado</span></a></td>';
           break;
         case 2:
           $estado = '<td><a><span class="badge badge-danger pull-center">No Pagado</span></a></td>';
            break;
         case 3:
           $estado = '<td><a><span class="badge badge-info pull-center">Pago parcial</span></a></td>';
           break;
         case 4:
           $estado = '<td><a><span class="badge badge-success pull-center">Pago adelantado</span></a></td>';
          break;
          case 5:
            $estado = '<td><a><span class="badge badge-success pull-center">Parcial adelantado</span></a></td>';
           break;
       }
       return $estado;
     }

}
