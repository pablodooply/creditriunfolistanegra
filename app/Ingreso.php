<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
  protected $table = 'ingreso';
  protected $primaryKey = 'id';
  public    $timestamps = true;
  protected $fillable = [
    'monto', 'descripcion', 'tipo_ingreso_id', 'proveedor', 'cantidad', 'foto_recibo', 'created_by', 'created_at', 'updated_by', 'updated_at'
  ];
  protected $guarded = [
    'id'
  ];
}
