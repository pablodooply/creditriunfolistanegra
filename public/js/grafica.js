function cargar_grafica_line(datos){
  console.log('funciona aqui');
  console.log(datos);
  var cont=0;
  for (var i = 0; i < datos.length; i++) {
    console.log(datos[i]);
    datos[i] = datos[i].map(function(each_element){
      return each_element;
  });
}

  Highcharts.chart('container', {

      title: {
          text: 'Grafica'
      },

      xAxis: {
          categories: datos[0],
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
      },


      series: [{
          name: 'Capital total',
          data: datos[1],
        },{
          name: 'Intereses',
          data: datos[2],
        }, {
          name: 'Moras',
          data: datos[3],
        }, {
          name: 'Capital recuperado',
          data: datos[4],
        }, {
          name: 'Ganacias',
          data: datos[5],
        }],

      responsive: {
          rules: [{
              condition: {
                  maxWidth: 500
              },
              chartOptions: {
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  }
              }
          }]
      }
  });
}

function cargar_grafica_barra(datos){
    var chart = Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafica de datos recuperados'
    },
    xAxis: {
        categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Capital total',
        data: [5, 3, 4, 7, 2]
    }, {
        name: 'Capital activo',
        data: [2, -2, -3, 2, 1]
    }, {
        name: 'Mora',
        data: [3, 4, 4, -2, 5]
    }]
});
}
