


// $("#fecha_nacimiento").change(function() {
//   verificarEdad();
//   console.log("PENDEJO");
// });



// $('#fecha_nacimiento').onChange(function() {
//   console.log("cambio");
// });

function verificarEdad() {

var fecha_nacimiento = $('#fecha_nacimiento').val();

var url = "{{url('/credito/verificarEdad')}}";
$.get({
  data: {
    fecha_nacimiento: fecha_nacimiento,
  },
  url: url,
  success: function(response) {
    if (response == "false") {
      swal("No es mayor de edad", "Verifique el dato ingresado", "error");

      $('#fecha_nacimiento').val('1990-01-01');
    }
  }
});
}
var form = $("#form-credito");
form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        dpi: {
            required: true,
            // number: true
        },

        nombre: "required",
        apellido: "required",

        email: {
            required: true,
            email: true,
        },

        telefono: {
            required: true,
        },

        direccion: {
            required: true,
        },

        nombre_r1: {
            required: true,
        },

        telefono_r1: {
            required: true,
        },

        nombre_r2: {
            required: true,
        },

        telefono_r2: {
            required: true,
        },

        cantidad: {
            required: true,
            number: true,
        },

        no_pagos: {
            required: true,
            number: true,
        },

        interes: {
            required: true,
            number: true,
        },

        mora: {
            required: true,
            number: true,
        },

        recibo_luz: {
            required: true,
        },

        direccion_luz: {
            required: true,
        },

        salario: {
            number: true,
        },

        direccion_cobrar: {
            required: true,
        },
        hora: {
            required: true,
        },


    },
    messages: {
      nombre: "El nombre es requerido",

      apellido: "El apellido es requerido",

      dpi: {
        required: "El DPI es requerido",
        // number: "Solo puede ser numeros",
      },

      telefono: {
          required: "El telefono es requerido"
      },

      direccion: {
          required: "La direccion es requerida",
      },

      cantidad: {
          required: "La cantidad es requerida",
          number: "Tiene que ser valor numerico",
      },

      no_pagos: {
          required: "El No. de pagos es requerido",
          number: "Tiene que ser valor numerico",      },

      interes: {
          required: "El interes es requerido",
          number: "Tiene que ser valor numerico",      },

      mora: {
          required: "La mora es requerida",
          number: "Tiene que ser valor numerico",      },

      recibo_luz: {
          required: "El nombre de recibo de luz es requerido",
      },

      direccion_luz: {
          required: "La direccion del recibo es requerida",
      },

      direccion_cobrar: {
          required: "La direccion a cobrar es requerida",
      },

      hora: {
          required: "Se requiere la hora",
      },
    }
});

form.children("div").steps({
  headerTag: "h3",
  bodyTag: "section",
  transitionEffect: "slideLeft",
  labels:{
    previous: "Anterior",
    next: "Siguiente",
    finish: "Registrar",
  },
  onStepChanging: function (event, currentIndex, newIndex)
  {
    // Allways allow previous action even if the current form is not valid!
    if (currentIndex > newIndex)
    {
        return true;
    }
    // Forbid next action on "Warning" step if the user is to young
    if (newIndex === 3 && Number($("#age-2").val()) < 18)
    {
        return false;
    }
    // Needed in some cases if the user went back (clean up)
    if (currentIndex < newIndex)
    {
        // To remove error styles
        form.find(".body:eq(" + newIndex + ") label.error").remove();
        form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
    }
    form.validate().settings.ignore = ":disabled,:hidden";
    return form.valid();
  },
  onFinishing: function (event, currentIndex)
  {
      obtenerInfo();
      form.validate().settings.ignore = ":disabled";
      return form.valid();
  },
  onFinished: function (event, currentIndex)
  {
    form.submit();

  }
});

$("#departamento").change(function(){
  obtenerDatos();
});

function obtenerDatos(){
  var valor = $("#departamento :selected").attr("value"); // The text content of the selected option
  var url = '{{ route("api.municipios", ":id") }}';
  url = url.replace(':id', valor);
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $("#city").empty();
        $.each(response, function(id,name){
            $("#city").append('<option value="'+name+'">'+ id  +'</option>');
          });
      }
  });
}

obtenerDatos();

$("#tipoPlan").change(function(){
  obtenerNoPagos();
});

$("#no_pagos").change(function(){
  obtenerMontos();
});

$("#monto").change(function(){
  obtenerInfo();
});


obtenerNoPagos();


function obtenerNoPagos(){
  var plan = $("#tipoPlan :selected").attr("value");
  $.ajax({
    type: "GET",
    url: "{{route('info.nopagos')}}",
    data: {
      tipo: plan,
    },
    success: function( response ) {
      $("#no_pagos").empty();
      $.each(response, function(id,tiempo){
          $("#no_pagos").append('<option value="'+ tiempo.id+'">'+  tiempo.tiempo +'</option>');
        });
        obtenerMontos();

    }
  });

}

function obtenerMontos(){
  var periodo = $("#no_pagos :selected").attr("value"); // The text content of the selected option

  $.ajax({
    type: 'GET',
    url: "{{route('info.montos')}}",
    data: {
      tipo: periodo,
    },
    success: function( response ) {
      $("#monto").empty();
      $.each(response, function(id,total){
          $("#monto").append('<option value="'+total.id+'">'+ total.capital  +'</option>');
        });
        obtenerInfo();
    }
  });
}

var valores;
function obtenerInfo(){
  var plan = $("#monto :selected").attr("value");
  $.ajax({
    type: "GET",
    url: "{{route('info.infoPlan')}}",
    data: {
      tipo: plan
    },
    success: function( response ) {
      valores = response;
      $('#interes').val(response['interes']);
      $('#mora').val(response['mora']);
    },
  });
}
// obtenerMontos();

$('#timepicker').datetimepicker({
      format: 'hh:ii',
      language:  'es',
      weekStart: 1,
      todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 1,
  minView: 0,
  maxView: 1,
  forceParse: 0
  });


  $("#ruta").change(function(){
    var valor = $("#ruta :selected").val(); // The text content of the selected option
    var url = '{{ route("ruta.promotor", ":id") }}';
    url = url.replace(':id', valor);
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          console.log(response);
          $("#promotor_ruta").val(response);
        }
    });
  });

  $("#dpi").change(function(){
    verificarDPI();
  });

  function verificarDPI(){
    console.log('entro');
    var dpi = $('#dpi').val();
    var url = "{{url('verificar/dpi')}}";
    $.get({
      data:{
        dpi: dpi,
      },
      url: url,
      success: function( response ) {
        if(response=="true"){
        swal("DPI ya en uso","Dirijase a listado de clientes","error");}
      }
    });
  }

function cuiIsValid(cui) {
  var console = window.console;
  if (!cui) {
      console.log("CUI vacío");
      return true;
  }

  var cuiRegExp = /^[0-9]{4}\s?[0-9]{5}\s?[0-9]{4}$/;

  if (!cuiRegExp.test(cui)) {
      console.log("CUI con formato inválido");
      return false;
  }

  cui = cui.replace(/\s/, '');
  cui = cui.replace(/\s/, '');
  var depto = parseInt(cui.substring(9, 11), 10);
  var muni = parseInt(cui.substring(11, 13));
  var numero = cui.substring(0, 8);
  var verificador = parseInt(cui.substring(8, 9));

  // Se asume que la codificación de Municipios y
  // departamentos es la misma que esta publicada en
  // http://goo.gl/EsxN1a

  // Listado de municipios actualizado segun:
  // http://goo.gl/QLNglm

  // Este listado contiene la cantidad de municipios
  // existentes en cada departamento para poder
  // determinar el código máximo aceptado por cada
  // uno de los departamentos.
  var munisPorDepto = [
      /* 01 - Guatemala tiene:      */ 17 /* municipios. */,
      /* 02 - El Progreso tiene:    */  8 /* municipios. */,
      /* 03 - Sacatepéquez tiene:   */ 16 /* municipios. */,
      /* 04 - Chimaltenango tiene:  */ 16 /* municipios. */,
      /* 05 - Escuintla tiene:      */ 13 /* municipios. */,
      /* 06 - Santa Rosa tiene:     */ 14 /* municipios. */,
      /* 07 - Sololá tiene:         */ 19 /* municipios. */,
      /* 08 - Totonicapán tiene:    */  8 /* municipios. */,
      /* 09 - Quetzaltenango tiene: */ 24 /* municipios. */,
      /* 10 - Suchitepéquez tiene:  */ 21 /* municipios. */,
      /* 11 - Retalhuleu tiene:     */  9 /* municipios. */,
      /* 12 - San Marcos tiene:     */ 30 /* municipios. */,
      /* 13 - Huehuetenango tiene:  */ 32 /* municipios. */,
      /* 14 - Quiché tiene:         */ 21 /* municipios. */,
      /* 15 - Baja Verapaz tiene:   */  8 /* municipios. */,
      /* 16 - Alta Verapaz tiene:   */ 17 /* municipios. */,
      /* 17 - Petén tiene:          */ 14 /* municipios. */,
      /* 18 - Izabal tiene:         */  5 /* municipios. */,
      /* 19 - Zacapa tiene:         */ 11 /* municipios. */,
      /* 20 - Chiquimula tiene:     */ 11 /* municipios. */,
      /* 21 - Jalapa tiene:         */  7 /* municipios. */,
      /* 22 - Jutiapa tiene:        */ 17 /* municipios. */
  ];

  if (depto === 0 || muni === 0)
  {
      console.log("CUI con código de municipio o departamento inválido.");
      return false;
  }

  if (depto > munisPorDepto.length)
  {
      console.log("CUI con código de departamento inválido.");
      return false;
  }

  if (muni > munisPorDepto[depto -1])
  {
      console.log("CUI con código de municipio inválido.");
      return false;
  }

  // Se verifica el correlativo con base
  // en el algoritmo del complemento 11.
  var total = 0;

  for (var i = 0; i < numero.length; i++)
  {
      total += numero[i] * (i + 2);
  }

  var modulo = (total % 11);

  console.log("CUI con módulo: " + modulo);
  return modulo === verificador;
}

$('#dpi').bind('change paste keyup', function (e) {
  var $this = $(this);
  var $parent = $this.parent();
  var $next = $this.next();
  var cui = $this.val();

  if (cui && cuiIsValid(cui)) {
      $parent.addClass('has-success');
      $next.addClass('glyphicon-ok');
      $parent.removeClass('has-error');
      $next.removeClass('glyphicon-remove');
  } else if (cui) {
      $parent.addClass('has-error');
      $next.addClass('glyphicon-remove');
      $parent.removeClass('has-success');
      $next.removeClass('glyphicon-ok');
  } else {
      $parent.removeClass('has-error');
      $next.removeClass('glyphicon-remove');
      $parent.removeClass('has-success');
      $next.removeClass('glyphicon-ok');
  }
});
