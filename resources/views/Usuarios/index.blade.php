@extends('layouts.app')

@section('title', 'Colaboradores')

@section('link')

@endsection

@section('content')

@section('nombre','Colaboradores')
@section('ruta')
  <li class="active">
      <strong>Listado de colaboradores</strong>
  </li>
@endsection

<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
      <div class="col-sm-8">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <h5>Listado de colaboradores</h5>
          </div>
          <div class="ibox-content">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#activos">Colaboradores activos</a></li>
              <li><a data-toggle="tab" href="#no_activos">Colaboradores no activos</a></li>
            </ul>
            <div class="tab-content">
              <div id="activos" class="tab-pane active">
                <div class="input-group">
                  <input type="text" placeholder="Codigo: Emp-123, Nombre: Nombre" class="input form-control" id="busqueda">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                  </span>
                  <div id="aqui">

                  </div>
                </div>
                <ul class="nav">
                </ul>
                <div class="table-responsive">
                  <table class='table table-striped' style="width:100%" id="tabla-usuarios">
                  <thead>
                    <tr>
                      <th>Código</th>
                      <th>Nombre</th>
                      <th>No. Prestamos</th>
                      <th>Estado</th>
                      <th>Puesto</th>
                      <th></th>
                    </tr>
                  </thead>
                  {{-- <tbody>
                    @if($empleados_activos->isEmpty())
                      <h2> No hay colaboradores</h2>
                    @else
                      @foreach ($empleados_activos as $empleado)
                        <tr>
                          <td><a class="client-link" value='{{$empleado->id}}'>Emp-{{$empleado->id}}</a></td>
                          <td>{{$empleado->persona->nombre}} {{$empleado->persona->apellido}}</td>
                          @if(is_null($empleado->hoja_ruta))
                            <td>0</td>
                          @else
                            @php
                            $pres=0;
                            @endphp
                            @foreach ($empleado->hoja_ruta as $hoja_ruta)
                              @php
                              $pres = $pres + count($hoja_ruta->ruta);
                              @endphp
                            @endforeach
                            <td>0</td>
                          @endif
                          @if($empleado->estado==1)
                            <td><a><span class="badge badge-primary pull-center">Activo</span></a></td></td>
                          @else
                            <td><a><span class="badge badge-default pull-center">No activo</span></a></td></td>
                          @endif
                          <td>{{$empleado->roles->first()->nombre}}</td>
                          <td class="project-actions">
                            <a href="{{route('usuarios.show',$empleado->id)}}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>
                            @if(Auth::user()->hasRole('Administrador'))
                              <a href="{{route('usuarios.edit',$empleado->id)}}" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>
                            @endif
                          </td>
                        </tr>
                      @endforeach
                    @endif
                  </tbody> --}}

                </table>
                </div>
              </div>
              <div id="no_activos"class="tab-pane">
                <div class="input-group">
                  <input type="text" placeholder="Buscar empleado " class="input form-control" id="busqueda-noActivos">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                  </span>
                  <div id="aqui">

                  </div>
                </div>
                <ul class="nav">
                </ul>
                <div class="table-responsive">
                  <table class='table table-striped' style="width:100%" id="tabla-usuarios-noActivos">
                  <thead>
                    <tr>
                      <th>Código</th>
                      <th>Nombre</th>
                      <th>No. Prestamos</th>
                      <th>Estado</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($empleados_no_activos->isEmpty())
                      <h2>No hay Colaboradores</h2>
                    @else
                      @foreach ($empleados_no_activos as $empleado)
                      <tr>
                        <td><a class="client-link" value='{{$empleado->id}}'>Emp-{{$empleado->id}}</a></td>
                        <td>{{$empleado->persona->nombre}} {{$empleado->persona->apellido}}</td>
                        @if(is_null($empleado->hoja_ruta))
                          <td>0</td>
                        @else
                          @php
                          $pres=0;
                          @endphp
                          @foreach ($empleado->hoja_ruta as $hoja_ruta)
                            @php
                            $pres = $pres + count($hoja_ruta->ruta);
                            @endphp
                          @endforeach
                          <td>{{$pres}}</td>
                        @endif
                        @if($empleado->estado==1)
                          <td><a><span class="badge badge-primary pull-center">Activo</span></a></td></td>
                        @else
                          <td><a><span class="badge badge-default pull-center">No activo</span></a></td></td>
                        @endif
                        <td class="project-actions">
                          <a href="{{route('usuarios.show',$empleado->id)}}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>
                          @if(Auth::user()->hasRole('Administrador'))
                            <a href="{{route('usuarios.edit',$empleado->id)}}" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>
                          @endif
                        </td>
                      </tr>
                      @endforeach
                    @endif
                  </tbody>

                </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {{-- Inicio de vista previa de empleado --}}
      <div class="col-sm-4">
        <div class="ibox ">
            <div id='resumen_perfil' class="ibox-content">
              <div class="tab-content">
                @if($empleados_activos->isEmpty())
                  <h3>No hay colaboradores</h3>
                @else
                  <div id="contact-1" class="tab-pane active">
                  <div class="row m-b-lg">
                      <div class="col-lg-12 text-center">
                          <h2>{{$empleados_activos->first()->persona->nombre}} {{$empleados_activos->first()->persona->apellido}}</h2>
                      </div>
                  </div>
                  <div class="client-detail">
                    <div class="full-height-scroll">
                        <strong>Datos Personales</strong>

                        <ul class="list-group clear-list">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">{{Carbon\Carbon::parse($empleados_activos->first()->persona->fecha_nacimiento)->age}}</span>
                                Edad
                            </li>
                            <li class="list-group-item fist-item">
                                <span id="dpi" class="pull-right">{{$empleados_activos->first()->persona->dpi}} </span>
                                Dpi
                            </li>
                            <li class="list-group-item">
                                <span id="telefono" class="pull-right">{{$empleados_activos->first()->persona->telefono}}</span>
                                Telefono
                            </li>
                            <li class="list-group-item">
                                <span id="celular1" class="pull-right">{{$empleados_activos->first()->persona->celular1}}</span>
                                Celular 1
                            </li>
                            <li class="list-group-item">
                                <span id="celular2" class="pull-right">{{$empleados_activos->first()->persona->celular2}}</span>
                                Celular 2
                            </li>
                        </ul>
                        <strong>Rutas:</strong>
                        <table class='table table-striped table-hover'>
                          <thead>
                            <tr>
                              <th>Nombre</th>
                              <th>Estado</th>
                            </tr>
                          </thead>
                          <tbody>
                            @if($empleados_activos->first()->hoja_ruta->isEmpty())
                            @else
                              @foreach ($empleados_activos->first()->hoja_ruta as $hoja_ruta)
                                <tr>
                                  <td id="hoja_nombre">{{$hoja_ruta->nombre}}</td>
                                  @if($hoja_ruta->activa == 1)
                                    <td><a><span class="badge badge-primary pull-center">Activo</span></a></td>
                                  @else
                                    <td><a><span class="badge badge-danger pull-center">No activo</span></a></td>
                                  @endif
                                </tr>
                              @endforeach
                            @endif
                          </tbody>

                        </table>

                    </div>
                  </div>
              </div>
                @endif

              </div>
          </div>
        </div>
      </div>
      {{-- Fin de vista previa de empleado --}}
  </div>
</div>
@endsection

@section('scripts')

  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

<script type="text/javascript">

$(document).ready(function(){

    var oTable = $('#tabla-usuarios').DataTable({
      "language": {
          "url": "{{asset('fonts/dataTablesEsp.json')}}",
      },
      "paging":   true,
      "info":     false,
      'dom' : 'tip',
      serverSide: true,
      ajax: {
        url: '{{url('dataEmpleados')}}',
        data: {
          'tipo': 1
        }
      },
      columns: [
          {data: 'Codigo', name: 'Codigo'},
          {data: 'Nombre', name: 'Nombre'},
          {data: 'No. Prestamos', name: 'No. Prestamos'},
          {data: 'Estado', name: 'Prestamo'},
          {data: 'Puesto', name: 'Puesto'},
          {data: 'Acciones', name: 'Acciones'},
      ]

    });

    $('#busqueda').keyup(function(){
          oTable.search($(this).val()).draw() ;
    });

    var oTable2 = $('#tabla-usuarios-noActivos').DataTable({
      "language": {
          "url": "{{asset('fonts/dataTablesEsp.json')}}",
      },
      "paging":   true,
      "info":     false,
      'dom' : 'tip',
    });

    $('#busqueda-noActivos').keyup(function(){
          oTable2.search($(this).val()).draw() ;
    });

    $(".dataTables_filter input").addClass('input form-control');

  	$("tbody").on('click','.client-link',function(){
        var valor = $(this).attr("value");
        var url = '{{ route("usuarios.show", ":id") }}';
        url = url.replace(':id', valor);
        $.ajax({
            type: "GET",
            url: url,
            success: function( response ) {
              $('#resumen_perfil').html(response);

            }
        });

  	});
});

</script>

@endsection
