@extends('layouts.app')

@section('title', 'Posiciones')

@section('link')
  <link href="{{asset('css/rangedatepicker/daterangepicker.css')}}" rel="stylesheet"  media="screen">
@endsection
@section('content')

@section('nombre','Posiciones')
@section('ruta')
  <li class="active">
      <strong>Posiciones colaboradores</strong>
  </li>
@endsection
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Posiciones</h5>
        </div>
        <div class="ibox-content">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select class="form-control" name="" id="filtro-general">
                <option value="1">Capital Activo</option>
                <option value="2">Mora recuperada</option>
              </select>
            </div>
            {{-- <div class="col-md-3 col-sm-3 col-xs-12">
                <input id="rangedate" class="form-control" type="text" name="datefilter" value="">
            </div> --}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input placeholder="Busqueda de colaborador" class="form-control" id="busqueda-rutas">
            </div>
            <ul class="nav">
              <p><span class="pull-left text-muted">{{$posiciones->count()}} colaboradores</span></p>
            </ul>
            <div class="col-xs-12">
              <div class="table-responsive">
                <table id="tabla-posiciones" class='table table-striped table-hover'>
                  <thead>
                    <tr>
                      <th>Puesto</th>
                      <th>Código</th>
                      <th>Colaborador</th>
                      <th id="filtro-1">Capital activo</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                      $cont = 1;
                    @endphp
                    @foreach ($posiciones as $promotor)
                      <tr>
                        <td>{{$cont}}</td>
                        <td>Emp-{{$promotor['codigo']}}</td>
                        <td>{{$promotor['nombre']}}</td>
                        <td>{{$promotor['total']}}</td>
                      </tr>
                      @php
                        $cont = $cont +1;
                      @endphp
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/moment/moment.js')}}"></script>
<script type="text/javascript" src="{{asset('js/rangedatepicker/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
<script type="text/javascript">
  var datos;
  var fecha;
  var oTable = $('#tabla-posiciones').DataTable({
    "language": {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    "paging":   true,
    "info":     false,
    'dom' : 'tip'
  });

  $('#busqueda-rutas').keyup(function(){
        oTable.search($(this).val()).draw() ;
  });

  $("#filtro-general").change(function(){
    actualizar();
  })

    function compare(a,b) {
      if (a.total < b.total)
        return -1;
      if (a.total > b.total)
        return 1;
      return 0;
    }

  $("#rangedate").daterangepicker({
    "opens": "center",
    "showWeekNumbers": true,
    "locale": {
      "format": "DD/MM/YYYY",
      "fromLabel": "Desde",
      "toLabel": "Hasta",
      "applyLabel": "Aplicar",
      "cancelLabel": "Cancelar",
      "daysOfWeek": [
        "Dom",
        "Lun",
        "Mar",
        "Mie",
        "Jue",
        "Vie",
        "Sab"
      ],
      "monthNames": [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
      ],
    }
  });

  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      actualizar();
  });

  function actualizar(){
    var url = "{{route('usuarios.posicionesView')}}";
    var pru = $('#rangedate').data('daterangepicker');
    var tipo = $("#filtro-general option:selected").val();
    var inicio = pru.startDate.format('YYYY-MM-DD');
    var fin = pru.endDate.format('YYYY-MM-DD');
    if(tipo == 1){
      $('#filtro-1').text('Capital activo');
    }else {
      $('#filtro-1').text('Mora recuperada');
    }

    $.ajax({
      type: "GET",
      url: url,
      data: {
        tipo: tipo,
        inicio: inicio,
        fin: fin,
      },
      success: function(response){
        console.log('LLEGUE');

        var table = $('#tabla-posiciones').DataTable();
        datos = response;
        console.log(response);
        table.clear().draw();
        var cont = 1;
        var nuevo = Object.keys(datos).map(key => datos[key]);

        // Object.keys(datos).sort().reverse().forEach(function(v, i) {
          nuevo.forEach(datos_ac => {
            table.row.add([
              cont,
              'Emp-' + datos_ac.codigo,
              datos_ac.nombre,
              datos_ac.total,
              "",
          ]).draw();
          cont = cont +1;
          });
      //   for (var i = 0; i < nuevo.length; i++) {
      //     var datos_ac = nuevo[i];
      //     table.row.add([
      //         cont,
      //         'Emp-' + datos_ac.codigo,
      //         datos_ac.nombre,
      //         datos_ac.total,
      //         "",
      //     ]).draw();
      //     cont = cont +1;
      //  };

      },
    });
  }
</script>
@endsection
