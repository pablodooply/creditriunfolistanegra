@extends('layouts.app')

@section('title', 'Perfil empleado')

@section('content')

@section('nombre','Perfil empleado')
@section('ruta')
  <li class="active">
      <strong>Perfil de empleado</strong>
  </li>
@endsection
<div class="row">
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5> Pago </h5>
                </div>
                <div class="ibox-content">
                  {!! Form::open(['route' => ['pagos.empleado',$usuario->id], 'method' => 'POST']) !!}
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-6">
                            {{ Form::label('name', 'Sueldo base') }}
                            {{ Form::text('sueldo_base', $usuario->sueldo_base ? $usuario->sueldo_base : 0, ['class' => 'form-control valor', 'id' => 'valor', 'required']) }}
                        </div>
                        <div class="form-group col-sm-6">
                            {{ Form::label('name', 'Comision cliente nuevo') }}
                            {{ Form::text('comision_cliente', $usuario->comision_cliente_nuevo ? $usuario->comision_cliente_nuevo : 0 , ['class' => 'form-control valor', 'id' => 'valor', 'required' ]) }}
                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-6">
                            {{ Form::label('name', 'Comision capital activo') }}
                            {{ Form::text('comision_capital', $comision_1 ? $comision_1 : 0, ['class' => 'form-control valor', 'id' => 'valor', 'required' ]) }}
                        </div>
                        <div class="form-group col-sm-6">
                            {{ Form::label('name', 'Combustible') }}
                            {{ Form::text('combustible', $usuario->combustible ? $usuario->combustible : 0 , ['class' => 'form-control valor', 'id' => 'valor', 'required' ]) }}
                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-6">
                            {{ Form::label('name', 'Descuento mora') }}
                            {{ Form::text('descuento', $comision_2 ? $comision_2 : 0, ['class' => 'form-control valor', 'id' => 'valor', 'required' ]) }}
                        </div>
                        <div class="form-group col-sm-6">
                            {{ Form::label('name', 'Otros') }}
                            {{ Form::text('otros', $usuario->otros ? $usuario->otros : 0 , ['class' => 'form-control valor', 'required' ]) }}
                        </div>
                        <div class="form-group col-sm-12">
                            {{ Form::label('name', 'Total') }}
                            {{ Form::text('total',"0", ['class' => 'form-control total', 'id' => 'total', 'readonly', 'required' ]) }}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          {{ Form::submit('Pagar', ['class' => 'btn btn-md btn-primary']) }}
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                      </div>
                    </div>
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                  <div class="col-lg-4">
                      <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Perfil de empleado</h5>
                            </div>
                            <div>
                                <div class="ibox-content profile-content">
                                  <div class="col-xs-12">
                                    <div class="profile-image">
                                      <img src="{{($usuario->persona->foto_perfil ?  asset('images/empleados/perfil/')."/".$usuario->persona->foto_perfil :asset('/images/default/perfil.jpg/'))}}" class="pull-center img-circle circle-border m-b-md" alt="profile">
                                    </div>
                                  </div>
                                  <h4>{{$usuario->persona->nombre}}  {{$usuario->persona->apellido}}</h4>
                                  <div class="clear">

                                  </div>
                                  <strong class="text-success">Datos Personales</strong>

                                  <ul class="list-group clear-list">
                                    <li class="list-group-item fist-item">
                                        <span class="pull-right"> {{$usuario->obtenerEdad()}} </span>
                                        Edad
                                    </li>
                                      <li class="list-group-item">
                                          <span class="pull-right"> {{$usuario->persona->dpi}} </span>
                                          DPI
                                      </li>
                                      <li class="list-group-item">
                                          <span class="pull-right"> {{$usuario->persona->telefono}} </span>
                                          Teléfono
                                      </li>
                                      <li class="list-group-item">
                                          <span class="pull-right"> {{$usuario->persona->celular1}}</span>
                                          Celular 1
                                      </li>
                                      <li class="list-group-item">
                                          <span class="pull-right"> {{$usuario->persona->celular2}} </span>
                                          Celular 2
                                      </li>
                                  </ul>
                                </div>
                            </div>
                        </div>
                      </div>
                      {{-- <div class="col-lg-12">
                          <div class="ibox float-e-margins">
                              <div class="ibox-title">
                                  <h5>Comision</h5>
                                  <div class="ibox-tools">
                                      <a class="collapse-link">
                                          <i class="fa fa-chevron-up"></i>
                                      </a>
                                  </div>
                              </div>
                              <div class="ibox-content">
                                <ul class="list-group clear-list">
                                  <li class="list-group-item fist-item">
                                      Sueldo base
                                     <span class="pull-right"> Q. {{$usuario->sueldo_base ? $usuario->sueldo_base : 0}} </span>
                                  </li>
                                  <li class="list-group-item">
                                      Comision por cliente nuevo
                                     <span class="pull-right"> Q. {{$comision_1 ? $comision_1 : 0}} </span>
                                  </li>
                                    <li class="list-group-item">
                                      Comision por capital activo
                                        <span class="pull-right"> Q. {{$usuario->comision_capital_activo ? $usuario->comision_capital_activo : 0}} </span>
                                        <li class="list-group-item">
                                          Combustible
                                            <span class="pull-right"> Q. {{$usuario->combustible ? $usuario->combustible : 0}} </span>
                                        </li>
                                    </li>
                                    <li class="list-group-item">
                                        Descuento por mora
                                       <span class="pull-right"> Q. {{$comision_2 ? $comision_2 : 0}} </span>
                                    </li>
                                    <li class="list-group-item">
                                        Total
                                       <span class="pull-right"> Q. {{$usuario->sueldo_base +  $usuario->comision_cliente_nuevo
                                       + $usuario->comision_capital_activo + $usuario->combustible + $usuario->descuento_mora + $comision_2}} </span>
                                    </li>
                                    <li class="list-group-item text-danger">
                                      Pagado este mes:
                                      <span class="pull-right"> Q. {{$total_pagado}} </span>
                                    </li>
                                </ul>
                                @if(Auth::user()->hasRole('Administrador'))
                                  <div class="user-button">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button id="pagar" type="button" class="btn btn-primary btn-sm btn-block"
                                            data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-envelope"></i> Pagar</button>
                                        </div>
                                    </div>
                                </div>
                                @endif
                              </div>
                          </div>
                      </div> --}}
                  </div>

                  <div class="col-md-8">
                      <div class="ibox float-e-margins">
                          <div class="ibox-title">
                              <h5>Rutas</h5>
                              <div class="ibox-tools">
                                  <a class="collapse-link">
                                      <i class="fa fa-chevron-up"></i>
                                  </a>
                                  <a class="close-link">
                                      <i class="fa fa-times"></i>
                                  </a>
                              </div>
                          </div>
                          <div class="ibox-content">
                            <div class="tabs-container">
                              <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#rutas">Rutas</a></li>
                                <li><a data-toggle="tab" href="#calculos">Contraseña</a></li>
                                <li><a data-toggle="tab" href="#documentos">Documentos</a></li>
                                {{-- <li><a data-toggle="tab" href="#pagos">Pagos</a></li> --}}
                              </ul>
                              <div class="tab-content">
                                <div id="rutas" class="tab-pane active">
                                  <div class="table-responsive">
                                    <table class='table table-striped table-hover'>
                                      <thead>
                                        <tr>
                                          <th>Código</th>
                                          <th>Estado</th>
                                          <th>Nombre</th>
                                          <th>No. Prestamos</th>
                                          <th>Acciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($usuario->hoja_ruta as $hoja_ruta)
                                          <tr>
                                            <td>{{$hoja_ruta->id}}</td>
                                            @if($hoja_ruta->activa == 1)
                                              <td><a><span class="badge badge-primary pull-center">activo</span></a></td>
                                            @else
                                              <td><a><span class="badge badge-danger pull-center">terminado</span></a></td>
                                            @endif
                                            <td>{{$hoja_ruta->nombre}}</td>
                                            <td> {{count($hoja_ruta->ruta)}}
                                              <td>
                                                <a href="{{route('rutas.show', $hoja_ruta->id)}}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                              </td>
                                            </tr>
                                          @endforeach
                                        </tbody>

                                      </table>
                                    </div>
                                  </div>
                                <div id="calculos" class="tab-pane">
                                  <form method="post" action="{{route('colaborador.contrasena')}}">

   {{csrf_field()}}

                                    <br>
                                <p>Cambio de contraseña.</p>
                                <div class="form-group row"><label class="col-lg-2 col-form-label">Contraseña Actual</label>

                                    <div class="col-lg-10"><input type="password" name="mypassword" placeholder="Password" class="form-control"></div>
                                </div>
                                <div class="form-group row"><label class="col-lg-2 col-form-label">Nueva Contraseña</label>

                                    <div class="col-lg-10"><input type="password" name="password" placeholder="Password" class="form-control"></div>
                                </div>
                                <div class="form-group row"><label class="col-lg-2 col-form-label">Confirmar Nueva Contraseña</label>

                                    <div class="col-lg-10"><input type="password" name="password_confirmation" placeholder="Password" class="form-control"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <div class="i-checks"><label class=""> <div class="icheckbox_square-green" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div><i></i> Remember me </label></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-offset-2 col-lg-10">
                                      {!! Form::button("<i class='fa fa-save'></i> Actualizar Contraseña",["class"=>"btn btn-accent m-btn m-btn--air m-btn--custom","id"=>"btn_MDoc", 'type' => 'submit'])!!}&nbsp;&nbsp;

                                      <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancelar</button>

                                    </div>
                                </div>
                            </form>
                                </div>
                                <div id="documentos" class="tab-pane">
                                  <h4>No tiene documentos</h4>
                                </div>
                                <div id="pagos" class="tab-pane">
                                  <div class="table-responsive">
                                    <table class='table table-striped table-hover' id='table-pagos'>
                                      <thead>
                                        <tr>
                                          <th>No.</th>
                                          <th class="sum">Sueldo</th>
                                          <th class="sum">Comisión 1</th>
                                          <th class="sum">Comisión 2</th>
                                          <th class="sum">Descuento</th>
                                          <th class="sum">Combustible</th>
                                          <th class="sum">Total</th>
                                          <th class="sum">Impresión</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                            @if($usuario->boleta_pago->isEmpty())
                                              <h3> No se tiene pagos registrados </h3>
                                            @else
                                              @php
                                                $cont = 1;
                                              @endphp
                                              @foreach ($usuario->boleta_pago as $pago)
                                                <tr>
                                                    <td>{{$cont}}</td>
                                                    <td>{{$pago->sueldo_base ? $pago->sueldo_base : 0}}</td>
                                                    <td>{{$pago->comision_clientes ? $pago->comision_clientes : 0}}</td>
                                                    <td>{{$pago->comision_capital ? $pago->comision_capital : 0}}</td>
                                                    <td>{{$pago->descuento_mora ? $pago->descuento_mora : 0}}</td>
                                                    <td>{{$pago->combustible ? $pago->combustible : 0}}</td>
                                                    <td>{{$pago->total ? $pago->total : 0 }}</td>
                                                    <td><a href="{{route('printPagoColaborador', $hoja_ruta->id)}}" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-print"></i></a></td>
                                                  </tr>
                                                @php
                                                  $cont++;
                                                @endphp
                                              @endforeach
                                            @endif
                                        </tbody>
                                        <tfoot>
                                          <tr>
                                            <th>Total: </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                          </tr>
                                        </tfoot>
                                      </table>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          </div>
                      </div>

                  </div>

            </div>
@endsection


@section('scripts')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

<script type="text/javascript">

var oTable = $('#table-pagos').DataTable({
  "language": {
      "url": "{{asset('fonts/dataTablesEsp.json')}}",
  },
  "paging":   true,
  "info":     false,
  'dom' : 'tip',
  "footerCallback": function ( row, data, start, end, display ) {
    var api = this.api(), data;

    var intVal = function ( i ) {
        return typeof i === 'string' ?
            i *1 :
            typeof i === 'number' ?
                i : 0;
    };

    // Total over all pages
    total = api
      .column( 1 )
      .data()
      .reduce( function (a, b) {
          return intVal(a) + intVal(b);
      }, 0 );

      // Update footer
      var que = api.columns('.sum', { page: 'current'}).every( function () {
        var sum = this
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );

        this.footer().innerHTML = sum;
      } );
    }
});

  $('#dar_baja').click(function(){
    swal({
      title: "Esta seguro de que quiere dar de baja al usuario?",
      text: "el usuario sera dado de baja",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Usuario dado de baja", {
          icon: "success",
        });
        $("#formulario-del").submit();
      } else {
        swal("Usuario seguira activo", {
          icon: "warning"
        });
      }
    });
  });

  $('#registrar').click(function(){
    swal({
      title: "Esta seguro de que quiere volver a registrar al usuario?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Usuario contratado!", {
          icon: "success",
        });
        $("#formulario-del").submit();
      } else {
        swal("Usuario sigue no activo", {
          icon: "warning"
        });
      }
    });
  });



  $("#pagar").click(function(){
    console.log("comienza");
    calc_total();
  });

  $(".valor").change(function(){
    console.log("cambiando");
    calc_total();
  });

  function calc_total(){
    var sum = 0;
    $(".valor").each(function(){
      sum += parseFloat($(this).val());
      console.log(sum);
    });
    $('.total').val(sum);
  }

</script>

@endsection
