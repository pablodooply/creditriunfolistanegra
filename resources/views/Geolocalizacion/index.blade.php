@extends('layouts.app')

@section('title', 'Geolocalizacion')

@section('content')

  @section('nombre','Geolocalizacion')
  @section('ruta')
    <li class="active">
        <strong>Localizacion</strong>
    </li>
  @endsection
  <style>
  .map-info-window{
 background:#333;
 border-radius:4px;
 box-shadow:8px 8px 16px #222;
 color:#fff;
 max-width:200px;
 max-height:300px;
 text-align:center;
 padding:5px 20px 10px;
 overflow:hidden;
 position:absolute;
 text-transform:uppercase;
}
  </style>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-md-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Mapa</h5>
        </div>
        <div class="ibox-content">
          <div class="row">
            <div class="col-xs-12">
            <div class="form-group col-md-6">
              <label>Promotor</label>
                <select class="form-control" name="" id="promotor">
                  @if($empleados->isEmpty())
                    <option value="">No hay promotores</option>
                  @else
                    @foreach ($empleados as $promotor )
                      <option value="{{$promotor->id}}">{{$promotor->persona->nombre}} {{$promotor->persona->apellido}}</option>
                    @endforeach
                  @endif
                </select>
            </div>
            <div class="form-group col-md-6">
              <label>Ruta</label>
              <div class="input-group">
                <select class="form-control" name="" id="rutas">
                  @if($rutas->isEmpty())
                    <option value="">No hay rutas</option>
                  @else
                  @endif
                </select>
                <span class="input-group-btn">
                        <button id="localizar" type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Localizar</button>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="google-map" style="height: 500px;" id="map1"></div>
        </div>
    </div>
</div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6igmhHiOWFAOGcb9OJ8nlicsEIpWkgtg"></script>


<script type="text/javascript">
google.maps.event.addDomListener(window, 'load', init);
var mapa;
var marcador;
var clientes = [];
var datos;
var markerCluster;
var posicion = [];
var center =  {lat: 14.704493399999999, lng: 268.151373};

var iconoMoto = {
  url: "{{ asset('images/moto.png') }}",
  size: new google.maps.Size(45, 45),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(0, 32),
  scaledSize: new google.maps.Size(25, 25)
};

//Icono casa prestamo activo
var iconoCasa_activo = {
    url: "{{ asset('images/casa-icon-activo.png') }}",
    size: new google.maps.Size(45, 45),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 32),
    scaledSize: new google.maps.Size(45, 45)
  };

//Icono casa prestamo mora
var iconoCasa_vencido_mora = {
    url: "{{ asset('images/casa-icon-vencido-mora.png') }}",
    size: new google.maps.Size(45, 45),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 32),
    scaledSize: new google.maps.Size(45, 45)
  };

//Icono casa prestamo vencido cancelado
var iconoCasa_vencido_cancelado = {
    url: "{{ asset('images/casa-icon-vencido-cancelado.png') }}",
    size: new google.maps.Size(45, 45),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 32),
    scaledSize: new google.maps.Size(45, 45)
  };

var shape = {
coords: [1, 1, 1, 20, 18, 20, 18, 1],
type: 'poly'
};

function init() {
  var mapOptions1 = {
      zoom: 15,
      center: new google.maps.LatLng(center),
      disableDefaultUI: true,
      zoomControl: true,
      mapTypeControl: true,
      scaleControl: true,
      streetViewControl: true,
      rotateControl: true,
      fullscreenControl: true
  };

  var mapElement1 = document.getElementById('map1');
  mapa = new google.maps.Map(mapElement1, mapOptions1);

  @php
    $colors = array("red", "fuchsia", "blue", "green", "purple", "dodgerblue", "sienna", "black", "aqua", "orange", "red", "fuchsia", "blue");
    $count = 0; //temporary variable counting number of tracklog (each user hawe own Polyline)
  @endphp

  //Marcadores de ubicacion de los empleados
    marcador = new google.maps.Marker();
    actualizar();

    //Marcadores de ubicacion de los clientes
    @foreach($rutas as $ruta)
      @if(isset($ruta->prestamo->cliente->gps))
        @if($ruta->prestamo->estado_p_id == 10) // vencido en mora
          var icon_casa = iconoCasa_vencido_mora;
        @elseif($ruta->prestamo->estado_p_id == 9) //Cancelado
          var icon_casa = iconoCasa_vencido_cancelado;
        @elseif($ruta->prestamo->estado_p_id == 5 || $ruta->prestamo->estado_p_id == 3) //activo
          var icon_casa = iconoCasa_activo;
        @endif
        var marker = new google.maps.Marker({position: new google.maps.LatLng({{$ruta->prestamo->cliente->gps->coords_lat}},
        {{$ruta->prestamo->cliente->gps->coords_lon}}),
        map: mapa,
        size: new google.maps.Size(10, 10),
        shape: shape,
        icon: icon_casa,
        title: "{{$ruta->prestamo->cliente->persona->nombre}}"  });
        clientes.push(marker);
              google.maps.event.addListener(marker,'click', function() {
            var id = '{{$ruta->prestamo->id}}';
            createInfo(id,this);
          });
      @endif
    @endforeach

};

function actualizar(){
  marcador.setMap(null);
  var promo = $('#promotor option:selected').val();
  $.ajax({
      type: "GET",
      url: "api/obtener_posiciones/" + promo,
      success: function( data ) {
        marcador = new google.maps.Marker({
        position: new google.maps.LatLng(data['latitud'],
        data['longitud']),
        map: mapa,
        size: new google.maps.Size(10, 10),
        shape: shape,
        icon: iconoMoto,
        title: data['nombre'],
        animation: google.maps.Animation.BOUNCE});
        centrarPromotor(parseFloat(data['latitud']),parseFloat(data['longitud']));
      }
  });


};


function actualizar2(){
  marcador.setMap(null);
  var promo = $('#promotor option:selected').val();
  $.ajax({
      type: "GET",
      url: "api/obtener_posiciones/" + promo,
      success: function( data ) {
        marcador = new google.maps.Marker({
        position: new google.maps.LatLng(data['latitud'],
        data['longitud']),
        map: mapa,
        size: new google.maps.Size(10, 10),
        shape: shape,
        icon: iconoMoto,
        title: data['nombre'],
        animation: google.maps.Animation.BOUNCE});
      }
  });


};

function centrarPromotor(lat,lon){
  center =  {lat: parseFloat(lat), lng: parseFloat(lon)};

  mapa.setCenter(center);
}

var infowindow;

function reiniciarMarcadores(){
  for (var i = 0; i < clientes.length; i++) {
      clientes[i].setMap(null);
  }
}


function actualizarMarcadores(data){
  reiniciarMarcadores();
  console.log(data);
  for (var i = 0; i < data.length; i+=1) {
    if(data[i][0].estado == 10)
    {
      var icon_casa = iconoCasa;
    }
    elseif(data[i][0].estado == 9) //Cancelado
    {
      var icon_casa = iconoCasa_vencido_cancelado;
    }
    elseif(data[i][0].estado == 5 || data[i][0].estado == 3) //activo
    {
      var icon_casa = iconoCasa_activo;
    }
    var marker = new google.maps.Marker({
    position: new google.maps.LatLng(data[i][0].lat,
    data[i][0].lon),
    map: mapa,
    size: new google.maps.Size(10, 10),
    shape: shape,
    animation: google.maps.Animation.DROP,
    icon: icon_casa,
    title: data[i][0].nombre,
    id: data[i][0].id_cliente,
  });

  clientes.push(marker);
  google.maps.event.addListener(marker,'click', function() {
      var id = this.id;
      createInfo(id,this);
    });

}

}

function createInfo(id, marker){
  var url = "{{route('info.cliente', ":id")}}";
  var datos;
  url = url.replace(':id', id);
  var url_cliente = "{{route('clientes.show',":id")}}";
  url_cliente = url_cliente.replace(':id', id);
  $.ajax({
      type: "GET",
      url: url,
      success: function( data ) {
        var contentString =
        '<div id="contentInfoWindow" style="width: 200px;">'
          +'<table class="table">'
            +'<tbody>'
              +'<tr>'
                +'<th> Nombre: </th>'
                +'<td>' + data.nombre + '</td>'
              +'</tr>'
              +'<tr>'
                +'<th> Prestamo: </th>'
                +'<td>' + data.prestamo + '</td>'
              +'</tr>'
              +'<tr>'
                +'<th> Monto: </th>'
                +'<td>' + data.monto + '</td>'
              +'</tr>'
            +'<tbody>'
          +'</table>'
          +'<a href=' + url_cliente + ' class="btn btn-outline btn-sm btn-primary"><span class="fa fa-eye"></span></a>'
        +'</div>';
        infowindow = new google.maps.InfoWindow({
          content: contentString
        });
        infowindow.open(mapa, marker);
      }
  });
}

$('#localizar').click(function(){
  var promo = $('#promotor option:selected').val();
  actualizar();
  $.ajax({
      type: "GET",
      url: "api/obtener_clientes/" + promo,
      success: function( data ) {
        actualizarMarcadores(data);
      }
      });
        centrarPromotor();
});

$('#promotor').change(function(){
  obtenerPromoRutas();
});


function obtenerPromoRutas()
{
  var promo = $('#promotor option:selected').val();
  var url = "{{route('info.promoRutas')}}";
  $.ajax({
    type : "GET",
    url : url,
    data: {
      id: promo,
    },
    success: function( data ) {
      $("#rutas").empty();
      $.each(data, function(id,arreglo){
          $("#rutas").append('<option value="'+arreglo.id+'">'+ arreglo.nombre  +'</option>');
        });
    },
  });
}

obtenerPromoRutas();

window.setInterval("actualizar2()", 30000);

</script>

@endsection
