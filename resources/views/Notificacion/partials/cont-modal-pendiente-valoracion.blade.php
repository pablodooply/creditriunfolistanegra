<style>
#cargando {
  border: 6px solid #f3f3f3;
  border-radius: 50%;
  border-top: 6px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4 class="modal-title">Aprobación de garantías del prestamo <span class="text-success">CRE-{{str_pad($prestamo->id,6,"0",STR_PAD_LEFT)}}</span></h4>
  {{-- <small class="font-bold"><a href="/empleado/perfil/{{$usuario->id}}">(EMP-{{str_pad($usuario->id,6,"0",STR_PAD_LEFT)}})</a> {{$usuario->persona->nombre}} {{$usuario->persona->apellido}}</small> --}}
</div>
<div class="modal-body">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <div class="form-group">
              @php
                $i = 0;
                $con = 0;
              @endphp
              @foreach ($garantias as $garantia)
                @php
                  $i = $i + 1;
                @endphp
              <div class="col-md-6">

                  <div class="social-feed-box">
                          <div class="social-avatar">
                              <div class="media-body">
                                  <a href="#">
                                      Garantía #{{$i}}
                                  </a>
                                  <small class="text-muted">{{$garantia->created_at}}</small>
                              </div>
                          </div>
                          <div class="social-body">
                              <p>
                                <strong>Código:</strong>
                                  {{str_pad($garantia->id,6,"0",STR_PAD_LEFT)}}
                              </p>
                              <p>
                                <strong>Nombre:</strong>
                                  {{$garantia->nombre}}
                              </p>
                              <p>
                                <strong>Descripción:</strong>
                                  {{$garantia->descripcion}}
                              </p>
                              <p>
                                <strong>Categoría:</strong>
                                  {{$garantia->categoria->nombre}}
                              </p>
                              <p>
                                <strong>Cantidad:</strong>
                                  {{$garantia->cantidad}}
                              </p>
                              <p>
                                <strong>Valorado en (Cliente):</strong>
                                  Q. {{$garantia->valoracion_cliente}}
                              </p>
                              <p>
                                <strong>Estado del producto (cliente):</strong>
                                  {{$garantia->estado_producto_cliente}} <strong class="text-success">/10</strong>
                              </p>
                              <div id="carousel-generic{{$garantia->id}}" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="carousel-generic{{$garantia->id}}" data-slide-to="0" class="active"></li>
                                </ol>
                                <div class="carousel-inner">
                                      @foreach($imagenes_garantia as $key => $slider)
                                      @if($garantia->id == $slider->garantia_id)
                                      <div class="item {{$con == 0 ? 'active' : '' }}">
                                       <img class="img-thumbnail" src="{{url($slider->ruta)}}">
                                     </div>
                                     @php
                                      $con++;
                                     @endphp
                                     @else
                                     @php
                                       $con = 0;
                                     @endphp
                                       @endif
                                       @endforeach
                                    <a class="left carousel-control" href="#carousel-generic{{$garantia->id}}" role="button" data-slide="prev">
                                     <span class="glyphicon glyphicon-chevron-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-generic{{$garantia->id}}" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                </div>
                            </div>
                          </div>
                          <div class="mail-body">
            <p>
              </p><h5><strong>Formulario de garantía</strong></h5>
                <div class="row">
                  <div class="form-group col-md-12">
                    <label>Valorado en Q.</label>
                    <input name="valoracion_empresa" value="{{$garantia->valoracion_empresa}}" required="" type="number"  placeholder="Ingrese valor en Q" class="form-control input-garantia text-valoracion{{$garantia->id}} sum-garantia" onkeyup="keyupValoracion('{{$garantia->id}}')">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-12">
                    <label>Estado producto de 1-10</label>
                    <input name="estado_producto_empresa" value="{{$garantia->estado_producto_empresa}}" required="" min="1" max="10" type="number" placeholder="Ingrese estado de producto" class="form-control input-garantia text-estado-producto{{$garantia->id}}" onkeyup="keyupValoracion('{{$garantia->id}}')">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-12">
                    <label>Observación</label>
                    <textarea name="observacion"  required="" rows="4" placeholder="Ingrese Observación" class="form-control input-garantia text-observacion{{$garantia->id}}" onkeyup="keyupValoracion('{{$garantia->id}}')">{{$garantia->observacion}}</textarea>
                  </div>
                </div>
          </div>
            </div>

              </div>
            @endforeach

            <h3 class="float-right" style="float:right;">Valoración total: Q. <label id="lblVessel" for="">0.00</label></h3>
            </div>
        </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <div id="cargando" style="display:none; float: left;" ></div>
  <button type="button" class="btn btn-warning" id="btnaprobar" data-dismiss="modal" onclick="aprobarValoracion('{{$garantias->first()->prestamo->id}}')"><span class="fa fa-check"></span> Aprobar</button>
  <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
</div>


<script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>

  @include('sweet::alert')

<script type="text/javascript">
  function keyupValoracion(id)
  {
    if( !$(".text-valoracion"+id).val() || !$(".text-estado-producto"+id).val()  || !$(".text-observacion"+id).val()) {
          // $(this).parents('p').addClass('warning');
      $("#btnaprobar").attr('disabled','disabled');

    }
    else {
      $("#btnaprobar").removeAttr('disabled');
    }


    $("#cargando").css("display", "inline");
    console.log($(".text-valoracion"+id).val());
    var url = '{{route("garantia.modal.valoracion", ":slug")}}';
    url = url.replace(':slug', id);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
     type: "PUT",
     url: url,
     data: {
       _token: CSRF_TOKEN,
       valoracion_empresa : $(".text-valoracion"+id).val(),
       estado_producto_empresa : $(".text-estado-producto"+id).val(),
       observacion : $(".text-observacion"+id).val()
     },
     cache: false,
     success: function(data) {
       $("#cargando").css("display", "none");
     }
   });
  }


  function aprobarValoracion(id)
  {
    console.log(id)
    var url = '{{route("garantia.aprobar", ":slug")}}';
    url = url.replace(':slug', id);
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          swal("Aprobado", "Se ha aprobado las garantias de este credito", "success")

          location.reload();
        }
    });
  }
</script>

<script type="text/javascript">
  $( ".sum-garantia" ).keyup(function() {
    var sum = 0;
    $("input[class *= 'sum-garantia']").each(function(){
       sum += +$(this).val();
    });
    // $(".total").val(sum);
    // this.value = parseFloat(this.value).toFixed(2);
    $('#lblVessel').text(sum.toFixed(2));
    console.log(sum);
  });

  $(document).ready(function(){
    var sum = 0;
    $("input[class *= 'sum-garantia']").each(function(){
       sum += +$(this).val();
    });
    // $(".total").val(sum);
    // this.value = parseFloat(this.value).toFixed(2);
    $('#lblVessel').text(sum.toFixed(2));
    console.log(sum);
  });



</script>
