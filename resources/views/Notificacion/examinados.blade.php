@extends('layouts.app')

@section('title', 'Examinados')

@section('link')
  <link href="{{asset('css/ladda/ladda-themeless.min.css')}}" rel="stylesheet">
@endsection
@section('content')

@section('nombre',"Prestamos examinados")

@section('ruta')

<li class="active">
    <strong>Examinados</strong>
</li>
@endsection

<body class="pace-done">

<div class="wrapper wrapper-content">
<div class="row">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-content mailbox-content">
                <div class="file-manager">
                    <div class="space-25"></div>
                    <h5>Prestamos</h5>
                    <ul class="folder-list m-b-md" style="padding: 0">
                        <li><a href="{{route('Notificacion.examinados')}}" class="examinados"> <i class="fa fa-inbox "></i> Examinados <span class="label label-warning pull-right">{{$prestamos_examinados->count()}}</span> </a></li>
                        <li><a href="{{route('Notificacion.pendientes')}}" class="pendientes"> <i class="fa fa-inbox "></i> Pendientes <span class="label label-basic pull-right">{{$prestamos->count()}}</span> </a></li>
                        <li><a href="{{route('Notificacion.proximos')}}" class="pendientes"> <i class="fa fa-inbox "></i> Próximos a vencer <span class="label label-basic pull-right">{{$proximos->count()}}</span></a></li>
                        <li><a href="{{route('Notificacion.mora')}}" class="pendientes"> <i class="fa fa-inbox "></i>Mora pendiente <span class="label label-basic pull-right">{{$mora->count()}}</span></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-9 animated fadeInRight">
      <div class="mail-box-header">
          <h2 id="titulo">
              Examinados
          </h2>
      </div>
    <div class="mail-box">
      <div id=detalle_credito>
        @include('Notificacion.index', ['prestamos' => $prestamos_examinados])
      </div>
    </div>
  </div>
</div>
</div>

</body>
@endsection


@section('scripts')
  <script src="{{asset('js/ladda/spin.min.js')}}"></script>
  <script src="{{asset('js/ladda/ladda.min.js')}}"></script>
  <script src="{{asset('js/ladda/ladda.jquery.min.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function(){
      	$(".pendiente-link").click(function(){
            var valor = $(this).attr("value");
            var url = '{{ route("notificacion.show", ":id") }}';
            url = url.replace(':id', valor);
            $.ajax({
                type: "GET",
                url: url,
                success: function( response ) {
                  $('#detalle_credito').html(response);
                }
            });

      	});

        $(".crear-notificacion").click(function(){
            var url = '{{ route("notificacion.create") }}';
            $.ajax({
                type: "GET",
                url: url,
                success: function( response ) {
                  console.log(url);
                  $('#detalle_credito').html(response);
                  $('#titulo').text('Creacion de notificacion');
                }
            });
      	});
  });



</script>

@endsection
