@php
  $proximos
@endphp
<table class="table table-hover">

  <tbody>

    @if(!isset($proximos))

      @if(Auth::user()->hasRole('Administrador'))

        <h5>No hay préstamos próximos a vencer</h5>

      @else

        <h5>No hay prestamos próximos a vencer</h5>

      @endif

    @else

      @foreach ($proximos as $prestamo)

        <tr class="unread">

          <td>

          </td>

          <td value='{{$prestamo->id}}' class="mail-ontact"><a>Crédito <span class="text-success">cre-{{$prestamo->id}}</span> </a></td>

          <td class="mail-subject">El crédito está próximo a vencer</td>


          <td class="text-center"><span class="text-warning text-center">RUTA-{{$prestamo->ruta->first()->hoja_ruta->nombre}}</span></td>

          <td class="text-right mail-date">{{$prestamo->fecha_fin}}</td>
          <td><a href="{{route('creditos.show', $prestamo->id)}}" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-eye"> Ver</i></a></td>
        </tr>

      @endforeach

    @endif

  </tbody>

</table>
