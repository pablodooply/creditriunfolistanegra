@extends('layouts.app')

@section('title', 'Creditos')

@section('content')

@section('nombre','Creditos')
@section('ruta')
  <li class="active">
      <strong>Creditos a entregar</strong>
  </li>
@endsection
    <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                      <div class="ibox float-e-margins">
                        <div class="ibox-title">
                          <h5>Prestamos a entregar</h5>
                        </div>
                        <div class="ibox-content">
                          <div class="input-group">
                              <input type="text" placeholder="Buscar credito " class="input form-control">
                              <span class="input-group-btn">
                                      <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                              </span>
                          </div>
                          <ul class="nav">
                            <p><span class="pull-left text-muted">Pagos</span></p>
                          </ul>
                          <div class="table-responsive">
                            <table class='table table-striped table-hover'>
                              <thead>
                                <tr>
                                  <th>Prestamo</th>
                                  <th>Monto</th>
                                  <th>Cliente</th>
                                  <th>Cod. Cliente</th>
                                  <th>Accion</th>
                                </tr>
                              </thead>
                              <tbody>
                                 @foreach ($prestamos as $prestamo)
                                  <tr>
                                    <td><a href="{{route('creditos.show', $prestamo->id)}}" class="client-link">Cre-{{$prestamo->id}}</a></td>
                                    <td>{{$prestamo->monto}}</td>
                                    <td>{{$prestamo->cliente->persona->nombre}} {{$prestamo->cliente->persona->apellido}}</td>
                                    <td><a href="{{route('clientes.show',$prestamo->cliente->id)}}" class="client-link">Cli-{{$prestamo->cliente->id}}</a></td>
                                    <td><a id="#btnbus" href="#" value="{{$prestamo->cliente->id}}" class="btn btn-outline  btn-info entregar"><i class="fa fa-truck entregar-hijo" value="{{$prestamo->id}}"></i></a></td>
                                  </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>


                    </div>
                </div>
            </div>
@endsection

@section('scripts')

<script type="text/javascript">
  var idCliente;
  var idPrestamo;
  $(document).ready(function(){
    $(".entregar").click(function(){
      idCliente = $(this).attr("value");
      idPrestamo = $(this).children('.entregar-hijo').attr("value");
      console.log(idPrestamo);
      if (navigator.geolocation)
        {
        /* la geolocalizacion esta disponible */
          navigator.geolocation.getCurrentPosition(gpsActivado, errorMetodo, {enableHightAccuracy: true, timeout:Infinity, maximage:0});
        }
      else
        {
          console.log('Ocurrio un error, intentelo otra vez');
        }
})

    function gpsActivado(position){
      var latitud = position.coords.latitude;
      var longitud = position.coords.longitude;
      var precision = position.coords.accuracy;
      var tiempo = position.timestamp;
      console.log(latitud, longitud, precision, tiempo, idPrestamo);
      sendLocation(latitud, longitud, idCliente);
    }

    function errorMetodo(){
      alert('GPS no activado');
    }

    function sendLocation(lat, lon, id){
      // var id = $(this).attr("value");
      url = '{{url('coordenadas/cliente/')}}';
      $.ajax({
          type: "GET",
          url: url,
          data: {
            'latitude' : lat,
            'longitude': lon,
            'secret'  : id,
            'prestamoid' : idPrestamo
          },success: function(html){
            location.reload();
          }
      });
      console.log('ENTRO');
    }
  });
</script>

@endsection
