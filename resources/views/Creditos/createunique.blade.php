@extends('layouts.app')

@section('title', 'Registro')

@section('link')
<link href="{{asset('css/steps/jquery.steps.css')}}" rel="stylesheet">

<link href="{{asset('css/tool/complemento.css')}}" rel="stylesheet" media="screen">
<link href="{{asset('css/timepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">
<link href="{{asset('css/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">

<style media="screen">
  #div-garantias {
    height: 800px;
    overflow: auto;
  }
</style>
@endsection

@section('content')

@section('nombre','Solicitud de credito')
@section('ruta')
<li class="active">
  <strong>Creación de Cliente</strong>
</li>
@endsection
<div class="wrapper wrapper-content animated fadeInRight">

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="ibox-title">
          <h5>Solicitud</h5>
        </div>
        {!! Form::open(['id'=> 'form-credito','route' => 'cliente.unique.store', 'method' => 'POST', 'files' => true, 'enctype' => 'multipart/form-data']) !!}
        <div class="ibox-content">
          {{--*************************************************--}}

          {{--*************************************************--}}


          <h3>Datos Personales uno</h3>
          <section>
            <div class="form-row col-sm-12">
              <!--*****************Nombre******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Nombre') }}
                {{ Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
              <!--*****************Apellido******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Apellido') }}
                {{ Form::text('apellido', null, ['class' => 'form-control', 'id' => 'apellido' ]) }}
              </div>
              <!--*****************DPI******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'DPI') }}
                {{-- {{ Form::text('dpi', null, ['class' => 'form-control', 'id' => 'dpi','data-mask' => '9999 99999 9999' ]) }} --}}

                <div class="form-group  has-feedback">

                  {{ Form::text('dpi', null, ['class' => 'form-control', 'id' => 'dpi','data-mask' => '9999 99999 9999' ]) }}

                  <span class="glyphicon form-control-feedback"></span>
                </div>
              </div>
            </div>
            <div class="form-row col-sm-12">
              <!--*****************NIT******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'NIT') }}
                {{ Form::text('nit', null, ['class' => 'form-control', 'id' => 'nit', 'maxlength' => '11' ]) }}
              </div>
              @php
              $month = date('m');
              $day = date('d');
              $year = date('Y');
              $today = $year . '-' . $month . '-' . $day;
              @endphp
              <!--*****************Fecha_nacimiento******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Fecha de Nacimiento') }}
                <input type="date" id="fecha_nacimiento" class="form-control" name="fecha_nacimiento" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="{{$today}}">
              </div>
              <!--*****************Genero******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Genero') }}
                <select class="form-control" name="genero">
                  <option value="1">Hombre</option>
                  <option value="0">Mujer</option>
                </select>
              </div>
            </div>
            <div class="form-row col-sm-12">
              <!--*****************estado******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Departamento') }}
                <select name="state" id="departamento" class="form-control">
                  @foreach ($states as $state)
                  <option value="{{$state->id}}">{{$state->name}}</option>
                  @endforeach
                  {{-- <option value="1">Quetzaltenango</option> --}}
                </select>
              </div>
              <!--*****************ciudad******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Municipio') }}
                <select name="city" id="city" class="form-control">
                  <option value="1">Quetzaltenango</option>
                </select>
              </div>
              <!--*****************Nacionalidad******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Nacionalidad') }}
                <select name="nacionalidad" id="city" class="form-control">
                  <option value="Guatemalteca">Guatemalteca</option>
                  <option value="Beliceña">Mexicana</option>
                  <option value="Estadounidense">Estadounidense</option>
                  <option value="Costarricense">Costarricense</option>
                  <option value="Hondureña">Hondureña</option>
                  <option value="Nicaraguense">Nicaraguense</option>
                  <option value="Panameña">Panameña</option>
                  <option value="Salvadoreña">Salvadoreña</option>
                  <option value="Beliceña">Beliceña</option>
                </select>
              </div>
            </div>
            <div class="form-row col-sm-12">
              <!--*****************Telefono 1******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'No. hijos') }}
                {{ Form::number('no_hijos', null, ['class' => 'form-control', 'id' => 'telefono1' ]) }}
              </div>
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Telefono 1') }}
                {{ Form::text('telefono', null, ['class' => 'form-control', 'id' => 'telefono1' ]) }}
              </div>
              <!--*****************Celular1******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Telefono Móvil 1') }}
                {{ Form::text('celular1', null, ['class' => 'form-control', 'id' => 'celular1' ]) }}
              </div>
              {{-- <!--*****************Celular2******************-->
                              <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Telefono Móvil 2') }}
              {{ Form::text('celular2', null, ['class' => 'form-control', 'id' => 'celular2' ]) }}
            </div> --}}

        </div>
        <div class="form-row col-sm-12">
          <!--*****************Estado civil******************-->
          <div class="form-group col-sm-4">
            {{ Form::label('foto', 'Estado civil') }}
            <select class="form-control" name="estado_civil">
              <option value="Soltero">Soltero</option>
              <option value="Casado">Casado</option>
              <option value="Divorciado">Divorciado</option>
              <option value="Viudo">Viudo</option>
              <option value="Unión de hecho">Unión de hecho</option>
            </select>
          </div>
          <!--*****************Tipo de casa******************-->
          <div class="form-group col-sm-4">
            {{ Form::label('name', 'Tipo de casa') }}
            {{ Form::text('tipo_casa', null, ['class' => 'form-control', 'id' => 'nombre' ])}}
          </div>
          <!--*****************Actividad economica******************-->
          <div class="form-group col-sm-4">
            {{ Form::label('name', 'Actividad economica') }}
            {{ Form::text('actividad', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
          </div>
        </div>
        </section>
        <h3>Datos personales dos</h3>
        <section>
          <!--*****************Direccion Domicilio******************-->
          <div class="form-row col-sm-12">
            <div class="form-group col-sm-12">
              {{ Form::label('name', 'Dirección domicilio') }}
              {{ Form::text('direccion', null, ['class' => 'form-control', 'id' => 'direccion' ]) }}
            </div>
            <div class="form-group col-sm-12">
              {{ Form::label('name', 'Dirección a cobrar') }}
              {{ Form::text('direccion_cobrar', null, ['class' => 'form-control', 'id' => 'direccion']) }}
            </div>
          </div>

          <div class="form-row col-sm-12">
            <!--*****************Foto de dpi******************-->
            <div class="form-group col-sm-4">
              {{ Form::label('foto', 'Imagen de DPI') }}
              {{ Form::file('foto_dpi', ['class' => 'form-control']) }}
            </div>
            <!--*****************Imagen recibo de luz******************-->
            <div class="form-group col-sm-4">
              {{ Form::label('foto', 'Imagen de recibo de luz') }}
              {{ Form::file('foto_recibo', ['class' => 'form-control']) }}
            </div>
            <!--*****************Imagen recibo de luz******************-->
            <div class="form-group col-sm-4">
              {{ Form::label('foto', 'Imagen de solicitud') }}
              {{ Form::file('foto_firma', ['class' => 'form-control']) }}
            </div>
          </div>
          <div class="form-row col-sm-12">
            <!--*****************Nombre recibo de luz******************-->
            <div class="form-group col-sm-6">
              {{ Form::label('name', 'Nombre recibo de luz o referencia COCODE') }}
              {{ Form::text('recibo_luz', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
            </div>
            <!--*****************Direccion Recibo Luz******************-->
            <div class="form-group col-sm-6">
              {{ Form::label('name', 'Dirección recibo de luz') }}
              {{ Form::text('direccion_luz', null, ['class' => 'form-control', 'id' => 'direccion' ]) }}
            </div>
          </div>
        </section>

        {{--*************************************************--}}
        <h3>Referencias personaless</h3>
        <section>
          <div class="form-row col-sm-12">
            <div class="form-group">
              <!--****************Referencia Personal 1******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Nombre Referencia uno') }}
                {{ Form::text('nombre_r1', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
              <!--****************Referencia Telefono 1******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Telefono Referencia uno') }}
                {{ Form::text('telefono_r1', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
              <!--****************Referencia Direccion 1******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Direccion referencia uno') }}
                {{ Form::text('direccion_r1', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
            </div>
          </div>
          <div class="form-row col-sm-12">
            <div class="form-group">
              <!--****************Referencia Personal 2******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Nombre Referencia dos') }}
                {{ Form::text('nombre_r2', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
              <!--*****************Telefono Referencia 2******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Telefono Referencia dos') }}
                {{ Form::text('telefono_r2', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
              <!--****************Referencia Direccion 2******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Direccion referencia dos') }}
                {{ Form::text('direccion_r2', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
            </div>
          </div>
        </section>


        {{--*********************************************** --}}
        <h3>Datos de Trabajo</h3>
        <section>
          <div class="form-row col-sm-12">
            <div class="form-group">
              <!--****************Empresa******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Empresa') }}
                {{ Form::text('empresa_trabajo', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
              <!--*****************Telefono empresa******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Telefono empresa') }}
                {{ Form::text('telefono_empresa', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
              <!--*****************Tiempo trabajando******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Tiempo Trabajando') }}
                {{ Form::text('tiempo_trabajando', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
            </div>
          </div>
          <div class="form-row col-sm-12">
            <div class="form-group">
              <!--****************Salario******************-->
              <div class="form-group col-sm-4">
                {{ Form::label('name', 'Salario') }}
                {{ Form::text('salario', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
              <!--*****************Observaciones******************-->
              <div class="form-group col-sm-8">
                {{ Form::label('name', 'Observaciones') }}
                {{ Form::text('observaciones', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
              </div>
            </div>
          </div>
        </section>

        {{-- ******* --}}
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
</div>
@endsection

@section('scripts')

<script src="{{asset ('js/steps/jquery.steps.js')}}"></script>
<script src="{{asset ('js/validate/jquery.validate.min.js')}}"></script>
<script src="{{asset ('js/timepicker/bootstrap-datetimepicker.js')}}"></script>

{{-- <script scr="{{asset ('js/sweetalert/sweetalert.min.js')}}"></script> --}}

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@include('sweet::alert')
<!-- Input Mask-->
<script src="{{asset('js/jasny/jasny-bootstrap.min.js')}}"></script>

<script type="text/javascript">
  // $("#fecha_nacimiento").change(function() {
  //   verificarEdad();
  //   console.log("PENDEJO");
  // });



  // $('#fecha_nacimiento').onChange(function() {
  //   console.log("cambio");
  // });

  function verificarEdad() {

    var fecha_nacimiento = $('#fecha_nacimiento').val();

    var url = "{{url('/credito/verificarEdad')}}";
    $.get({
      data: {
        fecha_nacimiento: fecha_nacimiento,
      },
      url: url,
      success: function(response) {
        if (response == "false") {
          swal("No es mayor de edad", "Verifique el dato ingresado", "error");

          $('#fecha_nacimiento').val('1990-01-01');
        }
      }
    });
  }
</script>

<script type="text/javascript">
  var form = $("#form-credito");
  form.validate({
    errorPlacement: function errorPlacement(error, element) {
      element.before(error);
    },
    rules: {
      dpi: {
        required: true,
        // number: true
      },

      nombre: "required",
      apellido: "required",

      email: {
        required: true,
        email: true,
      },

      telefono: {
        required: true,
      },

      direccion: {
        required: true,
      },

      nombre_r1: {
        required: true,
      },

      telefono_r1: {
        required: true,
      },

      nombre_r2: {
        required: true,
      },

      telefono_r2: {
        required: true,
      },

      cantidad: {
        required: true,
        number: true,
      },

      no_pagos: {
        required: true,
        number: true,
      },

      interes: {
        required: true,
        number: true,
      },

      mora: {
        required: true,
        number: true,
      },

      recibo_luz: {
        required: true,
      },

      direccion_luz: {
        required: true,
      },

      salario: {
        number: true,
      },

      direccion_cobrar: {
        required: true,
      },
      hora: {
        required: true,
      },


    },
    messages: {
      nombre: "El nombre es requerido",

      apellido: "El apellido es requerido",

      dpi: {
        required: "El DPI es requerido",
        // number: "Solo puede ser numeros",
      },

      telefono: {
        required: "El telefono es requerido"
      },

      direccion: {
        required: "La direccion es requerida",
      },

      cantidad: {
        required: "La cantidad es requerida",
        number: "Tiene que ser valor numerico",
      },

      no_pagos: {
        required: "El No. de pagos es requerido",
        number: "Tiene que ser valor numerico",
      },

      interes: {
        required: "El interes es requerido",
        number: "Tiene que ser valor numerico",
      },

      mora: {
        required: "La mora es requerida",
        number: "Tiene que ser valor numerico",
      },

      recibo_luz: {
        required: "El nombre de recibo de luz es requerido",
      },

      direccion_luz: {
        required: "La direccion del recibo es requerida",
      },

      direccion_cobrar: {
        required: "La direccion a cobrar es requerida",
      },

      hora: {
        required: "Se requiere la hora",
      },
    }
  });

  form.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    labels: {
      previous: "Anterior",
      next: "Siguiente",
      finish: "Registrar",
    },
    onStepChanging: function(event, currentIndex, newIndex) {
      // Allways allow previous action even if the current form is not valid!
      if (currentIndex > newIndex) {
        return true;
      }
      // Forbid next action on "Warning" step if the user is to young
      if (newIndex === 3 && Number($("#age-2").val()) < 18) {
        return false;
      }
      // Needed in some cases if the user went back (clean up)
      if (currentIndex < newIndex) {
        // To remove error styles
        form.find(".body:eq(" + newIndex + ") label.error").remove();
        form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
      }
      form.validate().settings.ignore = ":disabled,:hidden";
      return form.valid();
    },
    onFinishing: function(event, currentIndex) {
      obtenerInfo();
      form.validate().settings.ignore = ":disabled";
      return form.valid();
    },
    onFinished: function(event, currentIndex) {
      form.submit();

    }
  });

  $("#departamento").change(function() {
    obtenerDatos();
  });

  function obtenerDatos() {
    var valor = $("#departamento :selected").attr("value"); // The text content of the selected option
    var url = '{{ route("api.municipios", ":id") }}';
    url = url.replace(':id', valor);
    $.ajax({
      type: "GET",
      url: url,
      success: function(response) {
        $("#city").empty();
        $.each(response, function(id, name) {
          $("#city").append('<option value="' + name + '">' + id + '</option>');
        });
      }
    });
  }

  obtenerDatos();

  $("#tipoPlan").change(function() {
    obtenerNoPagos();
  });

  $("#no_pagos").change(function() {
    obtenerMontos();
  });

  $("#monto").change(function() {
    obtenerInfo();
  });


  obtenerNoPagos();


  function obtenerNoPagos() {
    var plan = $("#tipoPlan :selected").attr("value");
    $.ajax({
      type: "GET",
      url: "{{route('info.nopagos')}}",
      data: {
        tipo: plan,
      },
      success: function(response) {
        $("#no_pagos").empty();
        $.each(response, function(id, tiempo) {
          $("#no_pagos").append('<option value="' + tiempo.id + '">' + tiempo.tiempo + '</option>');
        });
        obtenerMontos();

      }
    });

  }

  function obtenerMontos() {
    var periodo = $("#no_pagos :selected").attr("value"); // The text content of the selected option

    $.ajax({
      type: 'GET',
      url: "{{route('info.montos')}}",
      data: {
        tipo: periodo,
      },
      success: function(response) {
        $("#monto").empty();
        $.each(response, function(id, total) {
          $("#monto").append('<option value="' + total.id + '">' + total.capital + '</option>');
        });
        obtenerInfo();
      }
    });
  }

  var valores;

  function obtenerInfo() {
    var plan = $("#monto :selected").attr("value");
    $.ajax({
      type: "GET",
      url: "{{route('info.infoPlan')}}",
      data: {
        tipo: plan
      },
      success: function(response) {
        valores = response;
        $('#interes').val(response['interes']);
        $('#mora2').val(response['mora']);
      },
    });
  }
  // obtenerMontos();

  $('#timepicker').datetimepicker({
    format: 'hh:ii',
    language: 'es',
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0
  });


  $("#ruta").change(function() {
    var valor = $("#ruta :selected").val(); // The text content of the selected option
    var url = '{{ route("ruta.promotor", ":id") }}';
    url = url.replace(':id', valor);
    $.ajax({
      type: "GET",
      url: url,
      success: function(response) {
        console.log(response);
        $("#promotor_ruta").val(response);
      }
    });
  });

  $("#dpi").change(function() {
    verificarDPI();
  });

  function verificarDPI() {
    console.log('entro');
    var dpi = $('#dpi').val();
    var url = "{{url('verificar/dpi')}}";
    $.get({
      data: {
        dpi: dpi,
      },
      url: url,
      success: function(response) {
        if (response == "true") {
          swal("DPI ya en uso", "Dirijase a listado de clientes", "error");
        }
      }
    });
  }
</script>


<script type="text/javascript">
  function cuiIsValid(cui) {
    var console = window.console;
    if (!cui) {
      console.log("CUI vacío");
      return true;
    }

    var cuiRegExp = /^[0-9]{4}\s?[0-9]{5}\s?[0-9]{4}$/;

    if (!cuiRegExp.test(cui)) {
      console.log("CUI con formato inválido");
      return false;
    }

    cui = cui.replace(/\s/, '');
    cui = cui.replace(/\s/, '');
    var depto = parseInt(cui.substring(9, 11), 10);
    var muni = parseInt(cui.substring(11, 13));
    var numero = cui.substring(0, 8);
    var verificador = parseInt(cui.substring(8, 9));

    // Se asume que la codificación de Municipios y
    // departamentos es la misma que esta publicada en
    // http://goo.gl/EsxN1a

    // Listado de municipios actualizado segun:
    // http://goo.gl/QLNglm

    // Este listado contiene la cantidad de municipios
    // existentes en cada departamento para poder
    // determinar el código máximo aceptado por cada
    // uno de los departamentos.
    var munisPorDepto = [
      /* 01 - Guatemala tiene:      */
      17 /* municipios. */ ,
      /* 02 - El Progreso tiene:    */
      8 /* municipios. */ ,
      /* 03 - Sacatepéquez tiene:   */
      16 /* municipios. */ ,
      /* 04 - Chimaltenango tiene:  */
      16 /* municipios. */ ,
      /* 05 - Escuintla tiene:      */
      13 /* municipios. */ ,
      /* 06 - Santa Rosa tiene:     */
      14 /* municipios. */ ,
      /* 07 - Sololá tiene:         */
      19 /* municipios. */ ,
      /* 08 - Totonicapán tiene:    */
      8 /* municipios. */ ,
      /* 09 - Quetzaltenango tiene: */
      24 /* municipios. */ ,
      /* 10 - Suchitepéquez tiene:  */
      21 /* municipios. */ ,
      /* 11 - Retalhuleu tiene:     */
      9 /* municipios. */ ,
      /* 12 - San Marcos tiene:     */
      30 /* municipios. */ ,
      /* 13 - Huehuetenango tiene:  */
      32 /* municipios. */ ,
      /* 14 - Quiché tiene:         */
      21 /* municipios. */ ,
      /* 15 - Baja Verapaz tiene:   */
      8 /* municipios. */ ,
      /* 16 - Alta Verapaz tiene:   */
      17 /* municipios. */ ,
      /* 17 - Petén tiene:          */
      14 /* municipios. */ ,
      /* 18 - Izabal tiene:         */
      5 /* municipios. */ ,
      /* 19 - Zacapa tiene:         */
      11 /* municipios. */ ,
      /* 20 - Chiquimula tiene:     */
      11 /* municipios. */ ,
      /* 21 - Jalapa tiene:         */
      7 /* municipios. */ ,
      /* 22 - Jutiapa tiene:        */
      17 /* municipios. */
    ];

    if (depto === 0 || muni === 0) {
      console.log("CUI con código de municipio o departamento inválido.");
      return false;
    }

    if (depto > munisPorDepto.length) {
      console.log("CUI con código de departamento inválido.");
      return false;
    }

    if (muni > munisPorDepto[depto - 1]) {
      console.log("CUI con código de municipio inválido.");
      return false;
    }

    // Se verifica el correlativo con base
    // en el algoritmo del complemento 11.
    var total = 0;

    for (var i = 0; i < numero.length; i++) {
      total += numero[i] * (i + 2);
    }

    var modulo = (total % 11);

    console.log("CUI con módulo: " + modulo);
    return modulo === verificador;
  }

  $('#dpi').bind('change paste keyup', function(e) {
    var $this = $(this);
    var $parent = $this.parent();
    var $next = $this.next();
    var cui = $this.val();

    if (cui && cuiIsValid(cui)) {
      $parent.addClass('has-success');
      $next.addClass('glyphicon-ok');
      $parent.removeClass('has-error');
      $next.removeClass('glyphicon-remove');
    } else if (cui) {
      $parent.addClass('has-error');
      $next.addClass('glyphicon-remove');
      $parent.removeClass('has-success');
      $next.removeClass('glyphicon-ok');
    } else {
      $parent.removeClass('has-error');
      $next.removeClass('glyphicon-remove');
      $parent.removeClass('has-success');
      $next.removeClass('glyphicon-ok');
    }
  });
</script>


<script type="text/javascript">
  $(function() {
    $("#chkPassport").click(function() {
      if ($(this).is(":checked")) {

        $("#puede-continuar").hide("slow");
        $("#div-garantias").show("slow");
      } else {
        $("#puede-continuar").show("slow");
        $("#div-garantias").hide("slow");
      }
    });
  });
</script>


<script type="text/javascript">
  var i = 0;

  $("#add").click(function() {
    var url = '{{route("garantia.div.create")}}';

    ++i;

    $.ajax({
      type: "GET",
      url: url,
      data: {
        id_div: i
      },
      success: function(response) {
        $('#dynamicTable').append(response).show('slow');
      }
    });

    // $("#dynamicTable").append('<tr><td><input type="text" name="addmore['+i+'][name]" placeholder="Enter your Name" class="form-control" /></td><td><input type="text" name="addmore['+i+'][qty]" placeholder="Enter your Qty" class="form-control" /></td><td><input type="text" name="addmore['+i+'][price]" placeholder="Enter your Price" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
  });

  function eliminardivcreate(div_id) {
    $('#div-create-' + div_id).fadeOut("slow", function() {
      // After animation completed:
      $('#div-create-' + div_id).remove();
    });
  }


  $(document).on('click', '.remove-tr', function() {
    $(this).parents('tr').remove();
  });

  // $("#boton-siguiente").steps("next",{});



  // var sum = 0;


  // function keygarantia()
  // {
  // var  sum=0;
  //       // sum += parseFloat(this.value);
  //       // sum += parseFloat($(".sum-garantia").val());
  //       $(".sum-garantia").each(function() {
  //
  //         sum =$(this).val();
  //       });
  //       console.log(sum);
  // }

  // $( ".sum-garantia" ).keyup(function() {
  //   // alert( "Handler for .keyup() called." );
  //   console.log("pendejo");
  // });

  // $(document).ready(function() {
  //     $(".sum-garantia").keyup(function(){
  //       console.log('keyup');
  //       // $("input .sum-garantia").css("background-color", "pink");
  //     });
  //     });
</script>

@endsection
