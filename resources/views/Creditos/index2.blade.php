@extends('layouts.app')

@section('title', 'Creditos')

@section('link')
<link rel="stylesheet" href="{{asset('css/datatables/datatables.min.css')}}">
@endsection

@section('content')

@section('nombre','Creditos')
@section('ruta')
  <li class="active">
      <strong>Listado de creditos</strong>
  </li>
@endsection

@php
  use Carbon\Carbon;
@endphp

<div id="espacio-modal">
  @include('Creditos.pago')
  @include('Pagos.pagoVencido')
  @include('Creditos.partials.cambioCartera')
</div>
    <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-8">
                      <div class="ibox float-e-margins">
                        <div class="ibox-title">
                          <h5>Listado de creditos</h5>
                        </div>
                        <div class="ibox-content">
                          <ul class="nav nav-tabs">
                            @if(Auth::user()->hasAnyRole(['Administrador','Secretaria']))
                            <li class="active"><a data-toggle="tab" href="#todos">Todos</a></li>

                            <li><a data-toggle="tab" href="#home">Activos</a></li>
                            <li><a data-toggle="tab" href="#menu1">Vencidos</a></li>
                            <li><a data-toggle="tab" href="#menu2">En mora</a></li>

                              <li><a data-toggle="tab" href="#menu3">Pendientes</a></li>
                              <li><a data-toggle="tab" href="#menu4">Aprobados hoy</a></li>

                            @elseif (Auth::user()->hasAnyRole(['Promotor']))
                              <li class="active"><a data-toggle="tab" href="#todos">Todos</a></li>
                              <li><a data-toggle="tab" href="#home">Activos</a></li>
                              <li><a data-toggle="tab" href="#menu1">Vencidos</a></li>
                              <li><a data-toggle="tab" href="#menu2">En mora</a></li>


                          @elseif (Auth::user()->hasAnyRole(['Supervisor']))
                            <li class="active"><a data-toggle="tab" href="#todos">Todos</a></li>
                            <li><a data-toggle="tab" href="#home">Activos</a></li>
                            <li><a data-toggle="tab" href="#menu1">Vencidos</a></li>
                            <li><a data-toggle="tab" href="#menu2">En mora</a></li>
                          @endif

                          </ul>
                          <div class="tab-content">
                            @if(Auth::user()->hasAnyRole(['Administrador','Secretaria']))
                            <div id="todos" class="tab-pane fade in active">
                              <br>
                                <div class="row">
                                  <div class="col-md-6">
                                    <label>Busqueda:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <input type="text" placeholder="Ejemplo Cliente: Nombre, Ruta: Alfa, Cod.: Cre-123, Estado: Pendiente" class="input form-control" id="busqueda-creditos-todos">
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_fecha" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                                <div class="col-md-6">

                                  <label>Plan:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <select data-column="5" class="form-control clickSelect" name="tipo" id="plan-todos">
                                        <option value="">Todos</option>
                                        <option value="Diario">Diario</option>
                                        <option value="Semanal">Semanal</option>
                                        <option value="Quincena">Quincena</option>
                                      </select>
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-6">
                                  <label>Ruta:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <select data-column="6" class="form-control clickRuta" name="tipo" id="ruta-todos">
                                        <option value="">Todas</option>
                                        @foreach ($rutas as $ruta)
                                          <option value="{{$ruta->nombre}}">{{$ruta->nombre}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <ul class="nav">

                                <p><span class="pull-left text-muted">{{count($prestamos)}} Creditos</span></p>

                              </ul>
                              <div class="row">
                              {{--  <a class="btn btn-default btn-sm pull-right" href="{{url('formatCreditos', 1)}}" target="_blank">Print</a>
                                <a class="btn btn-danger btn-sm pull-right" href="{{url('formatCreditos', 2)}}" target="_blank">PDF</a> --}}
                                <div class="col-xs-12 table-responsive">
                                  <table class='table table-striped table-hover' id="tabla-creditos-todos" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Código</th>
                                        <th>Monto</th>
                                        <th>Estado</th>
                                        <th>Clasificación</th>
                                        <th>Tipo crédito</th>
                                        <th>Plan</th>
                                        <th>Ruta</th>
                                        <th>Cliente</th>
                                        <th>Acciones</th>
                                      </tr>
                                    </thead>

                                  </table>
                                  </div>
                              </div>
                            </div>

                            <div id="home" class="tab-pane fade">


                              <br>
                                <div class="row">
                                  <div class="col-md-6">
                                    <label>Busqueda:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <input type="text" placeholder="Ejemplo Cliente: Nombre, Ruta: Alfa, Cod.: Cre-123, Estado: Pendiente" class="input form-control" id="busqueda-creditos">
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_fecha" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                                <div class="col-md-6">

                                  <label>Plan:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <select data-column="5" class="form-control clickSelect" name="tipo" id="plan-activos">
                                        <option value="">Todos</option>
                                        <option value="Diario">Diario</option>
                                        <option value="Semanal">Semanal</option>
                                        <option value="Quincena">Quincena</option>
                                      </select>
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-6">
                                  <label>Ruta:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <select data-column="6" class="form-control clickRuta" name="tipo" id="ruta-activos">
                                        <option value="">Todas</option>
                                        @foreach ($rutas as $ruta)
                                          <option value="{{$ruta->nombre}}">{{$ruta->nombre}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <ul class="nav">
                                <p><span class="pull-left text-muted">{{count($creditos_activos)}} Creditos</span></p>
                              </ul>
                              <div class="row">
                              {{--  <a class="btn btn-default btn-sm pull-right" href="{{url('formatCreditos', 1)}}" target="_blank">Print</a>
                                <a class="btn btn-danger btn-sm pull-right" href="{{url('formatCreditos', 2)}}" target="_blank">PDF</a> --}}
                                <div class="col-xs-12 table-responsive">
                                  <table class='table table-striped table-hover' id="tabla-creditos" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Código</th>
                                        <th>Monto</th>
                                        <th>Estado</th>
                                        <th>Clasificación</th>
                                        <th>Tipo crédito</th>
                                        <th>Plan</th>
                                        <th>Ruta</th>
                                        <th>Cliente</th>
                                        <th>Acciones</th>
                                      </tr>
                                    </thead>

                                  </table>
                                  </div>
                              </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">


                              <br>
                                <div class="row">
                                  <div class="col-md-6">
                                    <label>Busqueda:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <input type="text" placeholder="Ejemplo Cliente: Nombre, Ruta: Alfa, Cod.: Cre-123, Estado: Pendiente" class="input form-control" id="busqueda-creditos-2">
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_fecha" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                                <div class="col-md-6">

                                  <label>Plan:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <select data-column="5" class="form-control clickSelect" name="tipo" id="plan-vencidos">
                                        <option value="">Todos</option>
                                        <option value="Diario">Diario</option>
                                        <option value="Semanal">Semanal</option>
                                        <option value="Quincena">Quincena</option>
                                      </select>
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-6">
                                  <label>Ruta:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <select data-column="6" class="form-control clickRuta" name="tipo" id="ruta-vencidos">
                                        <option value="">Todas</option>
                                        @foreach ($rutas as $ruta)
                                          <option value="{{$ruta->nombre}}">{{$ruta->nombre}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <ul class="nav">
                                <p><span class="pull-left text-muted">{{count($creditos_vencidos)}} Creditos</span></p>
                              </ul>
                              <div class="row">
                                <div class="col-xs-12 table-responsive">
                                  <table class='table table-striped table-hover' id="tabla-creditos-2" style="width: 100%">
                                    <thead>
                                      <tr>
                                        <th>Código</th>
                                        <th>Monto</th>
                                        <th>Estado</th>
                                        <th>Clasificación</th>
                                        <th>Tipo crédito</th>
                                        <th>Plan</th>
                                        <th>Ruta</th>
                                        <th>Cliente</th>
                                        <th>Acciones</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @if($creditos_vencidos->isEmpty())
                                        <h1> No hay creditos<h1>
                                        @else
                                          @foreach ($creditos_vencidos as $credito)
                                            <tr>
                                              <td><a class="client-link" value='{{$credito->id}}'>Cre-{{$credito->id}}</a></td>
                                              <td>{{$credito->monto}}</td>
                                              {!!$credito->obtenerEstado()!!}
                                              {!!$credito->obtenerClasificacion()!!}
                                              <td>{{$credito->plan->nombre}}</td>
                                              <td>{{$credito->cliente->persona->nombre}} </td>
                                              <td class="project-actions">
                                                <a href="{{route('creditos.show',$credito->id)}}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                              </td>
                                            </tr>
                                          @endforeach
                                        @endif
                                      </tbody>
                                    </table>
                                  </div>
                              </div>
                            </div>
                            <div id="menu2" class="tab-pane fade">

                              <br>
                                <div class="row">
                                  <div class="col-md-6">
                                    <label>Busqueda:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <input type="text" placeholder="Ejemplo Cliente: Nombre, Ruta: Alfa, Cod.: Cre-123, Estado: Pendiente" class="input form-control" id="busqueda-creditos-3">
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_fecha" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                                <div class="col-md-6">

                                  <label>Plan:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <select data-column="5" class="form-control clickSelect" name="tipo" id="plan-3">
                                        <option value="">Todos</option>
                                        <option value="Diario">Diario</option>
                                        <option value="Semanal">Semanal</option>
                                        <option value="Quincena">Quincena</option>
                                      </select>
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-6">
                                  <label>Ruta:</label>
                                  <div class="input-group">
                                    <div class="form-group">
                                      <select data-column="6" class="form-control clickRuta" name="tipo" id="ruta-3">
                                        <option value="">Todas</option>
                                        @foreach ($rutas as $ruta)
                                          <option value="{{$ruta->nombre}}">{{$ruta->nombre}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                    <span class="input-group-btn">
                                      <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <ul class="nav">
                                <p><span class="pull-left text-muted">{{count($creditos_vencidos_mora)}} Creditos</span></p>
                              </ul>
                              <div class="row">
                                <div class="col-xs-12 table-responsive">
                                <table class='table table-striped table-hover' id="tabla-creditos-3" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th>Código</th>
                                      <th>Monto</th>
                                      <th>Estado</th>
                                      <th>Clasificación</th>
                                      <th>Tipo crédito</th>
                                      <th>Plan</th>
                                      <th>Ruta</th>
                                      <th>Cliente</th>
                                      <th>Acciones</th>
                                    </tr>
                                  </thead>

                                  </table>
                                </div>
                              </div>

                            </div>
                            @if(Auth::user()->hasAnyRole(['Administrador','Secreatria','Supervisor']))
                              <div id="menu3" class="tab-pane fade">
                                <br>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label>Busqueda:</label>
                                    <div class="input-group">
                                      <div class="form-group">
                                        <input type="text" placeholder="Ejemplo Cliente: Nombre, Ruta: Alfa, Cod.: Cre-123, Estado: Pendiente" class="input form-control" id="busqueda-creditos-4">
                                      </div>
                                      <span class="input-group-btn">
                                        <a id="busqueda_fecha" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="col-md-6">

                                    <label>Plan:</label>
                                    <div class="input-group">
                                      <div class="form-group">
                                        <select data-column="5" class="form-control clickSelect" name="tipo" id="plan-4">
                                          <option value="">Todos</option>
                                          <option value="Diario">Diario</option>
                                          <option value="Semanal">Semanal</option>
                                          <option value="Quincena">Quincena</option>
                                        </select>
                                      </div>
                                      <span class="input-group-btn">
                                        <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                      </span>
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-6">
                                    <label>Ruta:</label>
                                    <div class="input-group">
                                      <div class="form-group">
                                        <select data-column="6" class="form-control clickRuta" name="tipo" id="ruta-4">
                                          <option value="">Todas</option>
                                          @foreach ($rutas as $ruta)
                                            <option value="{{$ruta->nombre}}">{{$ruta->nombre}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                      <span class="input-group-btn">
                                        <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                  <ul class="nav">
                                    <p><span class="pull-left text-muted">{{$creditos_pendientes ? count($creditos_pendientes) : 0}} Creditos</span></p>
                                  </ul>
                                  <div class="row">
                                    <div class="col-xs-12 table-responsive">
                                    <table class='table table-striped table-hover' id="tabla-creditos-4" style="width:100%">
                                      <thead>
                                        <tr>
                                          <th>Código</th>
                                          <th>Monto</th>
                                          <th>Estado</th>
                                          <th>Clasificación</th>
                                          <th>Tipo crédito</th>
                                          <th>Plan</th>
                                          <th>Ruta</th>
                                          <th>Cliente</th>
                                          <th>Acciones</th>
                                        </tr>
                                      </thead>

                                    </table>
                                    </div>
                                  </div>
                              </div>
                              <div id="menu4" class="tab-pane fade">
                                <br>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label>Busqueda:</label>
                                    <div class="input-group">
                                      <div class="form-group">
                                        <input type="text" placeholder="Ejemplo Cliente: Nombre, Ruta: Alfa, Cod.: Cre-123, Estado: Pendiente" class="input form-control" id="busqueda-creditos-apro">
                                      </div>
                                      <span class="input-group-btn">
                                        <a id="busqueda_fecha" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="col-md-6">

                                    <label>Plan:</label>
                                    <div class="input-group">
                                      <div class="form-group">
                                        <select data-column="5" class="form-control clickSelect" name="tipo" id="plan-5">
                                          <option value="">Todos</option>
                                          <option value="Diario">Diario</option>
                                          <option value="Semanal">Semanal</option>
                                          <option value="Quincena">Quincena</option>
                                        </select>
                                      </div>
                                      <span class="input-group-btn">
                                        <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                      </span>
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-6">
                                    <label>Ruta:</label>
                                    <div class="input-group">
                                      <div class="form-group">
                                        <select data-column="6" class="form-control clickRuta" name="tipo" id="ruta-5">
                                          <option value="">Todas</option>
                                          @foreach ($rutas as $ruta)
                                            <option value="{{$ruta->nombre}}">{{$ruta->nombre}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                      <span class="input-group-btn">
                                        <a id="busqueda_plan" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <ul class="nav">
                                  <p><span class="pull-left text-muted">{{count($creditos_aprobado_hoy)}} Creditos</span></p>
                                </ul>
                                <div class="table-responsive">
                                  <table class='table table-striped table-hover' id="tabla-creditos-apro" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Código</th>
                                        <th>Monto</th>
                                        <th>Estado</th>
                                        <th>Clasificación</th>
                                        <th>Tipo crédito</th>
                                        <th>Plan</th>
                                        <th>Ruta</th>
                                        <th>Cliente</th>
                                        <th>Acciones</th>
                                      </tr>
                                    </thead>

                                    </table>
                                  </div>
                              </div>
                            @endif
                          @else
                            <div id="todos" class="tab-pane fade in active">
                              <div class="input-group">
                                <input type="text" placeholder="Código: Cre-123, Cliente: Nombre del cliente" class="input form-control" id="busqueda-creditos-todos">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i>Busqueda</button>
                                </span>
                              </div>
                              <ul class="nav">

                                <p><span class="pull-left text-muted">{{count($todos)}} Creditos</span></p>

                              </ul>
                              <div class="row">
                              {{--  <a class="btn btn-default btn-sm pull-right" href="{{url('formatCreditos', 1)}}" target="_blank">Print</a>
                                <a class="btn btn-danger btn-sm pull-right" href="{{url('formatCreditos', 2)}}" target="_blank">PDF</a> --}}
                                <div class="col-xs-12 table-responsive">
                                  <table class='table table-striped table-hover' id="tabla-creditos-todos" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Código</th>
                                        <th>Monto</th>
                                        <th>Estado</th>
                                        <th>Clasificación</th>
                                        <th>Tipo crédito</th>
                                        <th>Plan</th>
                                        <th>Ruta</th>
                                        <th>Cliente</th>
                                        <th>Acciones</th>
                                      </tr>
                                    </thead>

                                  </table>
                                  </div>
                              </div>
                            </div>

                            <div id="home" class="tab-pane fade">
                              <div class="input-group">
                                <input type="text" placeholder="Código: Cre-123, Cliente: Nombre del cliente" class="input form-control" id="busqueda-creditos">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i>Busqueda</button>
                                </span>
                              </div>
                              <ul class="nav">
                                <p><span class="pull-left text-muted">{{count($creditos_activos)}} Creditos</span></p>
                              </ul>
                              <div class="row">
                              {{--  <a class="btn btn-default btn-sm pull-right" href="{{url('formatCreditos', 1)}}" target="_blank">Print</a>
                                <a class="btn btn-danger btn-sm pull-right" href="{{url('formatCreditos', 2)}}" target="_blank">PDF</a> --}}
                                <div class="col-xs-12 table-responsive">
                                  <table class='table table-striped table-hover' id="tabla-creditos" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Código</th>
                                        <th>Monto</th>
                                        <th>Estado</th>
                                        <th>Clasificación</th>
                                        <th>Tipo crédito</th>
                                        <th>Plan</th>
                                        <th>Ruta</th>
                                        <th>Cliente</th>
                                        <th>Acciones</th>
                                      </tr>
                                    </thead>

                                  </table>
                                  </div>
                              </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                              <div class="input-group">
                                <input type="text" placeholder="Código: Cre-123, Cliente: Nombre del cliente " class="input form-control" id="busqueda-creditos-2">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                                </span>
                              </div>
                              <ul class="nav">
                                <p><span class="pull-left text-muted">{{count($creditos_vencidos)}} Creditos</span></p>
                              </ul>
                              <div class="row">
                                <div class="col-xs-12 table-responsive">
                                  <table class='table table-striped table-hover' id="tabla-creditos-2" style="width: 100%">
                                    <thead>
                                      <tr>
                                        <th>Código</th>
                                        <th>Monto</th>
                                        <th>Estado</th>
                                        <th>Clasificación</th>
                                        <th>Tipo crédito</th>
                                        <th>Plan</th>
                                        <th>Ruta</th>
                                        <th>Cliente</th>
                                        <th>Acciones</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @if($creditos_vencidos->isEmpty())
                                        <h1> No hay creditos<h1>
                                        @else
                                          @foreach ($creditos_vencidos as $credito)
                                            <tr>
                                              <td><a class="client-link" value='{{$credito->id}}'>Cre-{{$credito->id}}</a></td>
                                              <td>{{$credito->monto}}</td>
                                              {!!$credito->obtenerEstado()!!}
                                              {!!$credito->obtenerClasificacion()!!}
                                              <td>{{$credito->plan->nombre}}</td>
                                              <td>{{$credito->cliente->persona->nombre}} </td>
                                              <td class="project-actions">
                                                <a href="{{route('creditos.show',$credito->id)}}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                              </td>
                                            </tr>
                                          @endforeach
                                        @endif
                                      </tbody>
                                    </table>
                                  </div>
                              </div>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                              <div class="input-group">
                                <input type="text" placeholder="Código: Cre-123, Cliente: Nombre del cliente" class="input form-control" id="busqueda-creditos-3">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                                </span>
                              </div>
                              <ul class="nav">
                                <p><span class="pull-left text-muted">{{count($creditos_vencidos_mora)}} Creditos</span></p>
                              </ul>
                              <div class="row">
                                <div class="col-xs-12 table-responsive">
                                <table class='table table-striped table-hover' id="tabla-creditos-3" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th>Código</th>
                                      <th>Monto</th>
                                      <th>Estado</th>
                                      <th>Clasificación</th>
                                      <th>Tipo crédito</th>
                                      <th>Plan</th>
                                      <th>Ruta</th>
                                      <th>Cliente</th>
                                      <th>Acciones</th>
                                    </tr>
                                  </thead>

                                  </table>
                                </div>
                              </div>

                            </div>
                            @if(Auth::user()->hasAnyRole(['Administrador','Secreatria','Supervisor']))
                              <div id="menu3" class="tab-pane fade">
                                  <div class="input-group">
                                    <input type="text" placeholder="Código: Cre-123, Cliente: Nombre del cliente" class="input form-control" id="busqueda-creditos-4">
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                                    </span>
                                  </div>
                                  <ul class="nav">
                                    <p><span class="pull-left text-muted">{{$creditos_pendientes ? count($creditos_pendientes) : 0}} Creditos</span></p>
                                  </ul>
                                  <div class="row">
                                    <div class="col-xs-12 table-responsive">
                                    <table class='table table-striped table-hover' id="tabla-creditos-4" style="width:100%">
                                      <thead>
                                        <tr>
                                          <th>Código</th>
                                          <th>Monto</th>
                                          <th>Estado</th>
                                          <th>Clasificación</th>
                                          <th>Tipo crédito</th>
                                          <th>Plan</th>
                                          <th>Ruta</th>
                                          <th>Cliente</th>
                                          <th>Acciones</th>
                                        </tr>
                                      </thead>

                                    </table>
                                    </div>
                                  </div>
                              </div>
                              <div id="menu4" class="tab-pane fade">
                                <div class="input-group">
                                  <input type="text" placeholder="Código: Cre-123, Cliente: Nombre del cliente" class="input form-control" id="busqueda-creditos-apro">
                                  <span class="input-group-btn">
                                    <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                                  </span>
                                </div>
                                <ul class="nav">
                                  <p><span class="pull-left text-muted">{{count($creditos_aprobado_hoy)}} Creditos</span></p>
                                </ul>
                                <div class="table-responsive">
                                  <table class='table table-striped table-hover' id="tabla-creditos-apro" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Código</th>
                                        <th>Monto</th>
                                        <th>Estado</th>
                                        <th>Clasificación</th>
                                        <th>Tipo crédito</th>
                                        <th>Plan</th>
                                        <th>Ruta</th>
                                        <th>Cliente</th>
                                        <th>Acciones</th>
                                      </tr>
                                    </thead>

                                    </table>
                                  </div>
                              </div>
                            @endif
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="ibox ">
                        <div id='resumen_perfil' class="ibox-content">
                          @if($creditos_activos->isEmpty())
                            <h3>No hay creditos</h3>
                          @else
                            <div class="tab-content">
                              <div id="contact-1" class="tab-pane active">
                                <div class="row m-b-lg">
                                    <div class="col-lg-12 text-center">
                                        <h2>Código: Cre-{{$creditos_activos->first()->id}}</h2>
                                    </div>
                                </div>
                                  <div class="client-detail">
                                    <div class="full-height-scroll">
                                        <strong>Datos de credito</strong>
                                        <ul class="list-group clear-list">
                                          <li class="list-group-item">
                                            <span class="pull-right">{{$creditos_activos->first()->cliente->persona->nombre}}</span>
                                            Cliente
                                          </li>
                                            <li class="list-group-item fist-item">
                                                <span class="pull-right">{{$creditos_activos->first()->monto}} </span>
                                                Monto
                                            </li>
                                            @php
                                              $abonado = $creditos_activos->first()->capital_recuperado + $creditos_activos->first()->interes;
                                            @endphp
                                            <li class="list-group-item">
                                                <span class="pull-right">{{$abonado}}</span>
                                                Abonado:
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right">{{$creditos_activos->first()->mora}}</span>
                                                Mora actual
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right">{{$creditos_activos->first()->fecha_desembolso}}</span>
                                                Fecha de desembolso:
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right">{{$creditos_activos->first()->fecha_inicio}}</span>
                                                Fecha de Inicio:
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right">{{$creditos_activos->first()->fecha_fin}}</span>
                                                Fecha de Final:
                                            </li>
                                        </ul>
                                        <strong>Prestamos</strong>
                                        <table class='table table-striped table-hover'>
                                          <thead>
                                            <tr>
                                              <th>Fecha</th>
                                              <th>Estado</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            @foreach ($creditos_activos->first()->ficha_pago->where('tipo',1)->where('fecha','>=',Carbon::now()->format('Y-m-d'))->take(5) as $ficha_pago)
                                              <tr>
                                                <td>{{$ficha_pago->fecha}}</td>
                                                <td>{!!$ficha_pago->estadoActual()!!}</td>
                                              </tr>
                                            @endforeach
                                          </tbody>
                                        </table>
                                    </div>
                                  </div>
                              </div>

                            </div>
                          @endif
                        </div>
                      </div>
                    </div>
                </div>
            </div>
@endsection

@section('scripts')

<script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>


<script type="text/javascript">
  $(document).ready(function(){
        //Tabla de activos
        $.fn.dataTable.ext.errMode = 'none';

        $('#busqueda-creditos-todos').keyup(function(){
              oTable6.search($(this).val()).draw();
        });

        $('#plan-todos').change(function (){
          oTable6.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        $('#ruta-todos').change(function (){
          oTable6.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        var oTable6 = $('#tabla-creditos-todos').DataTable({
          "order": [[ 0, "desc" ]],
          "language": {
              "url": "{{asset('fonts/dataTablesEsp.json')}}",
          },
          "paging":   true,
          "info":     false,
          'dom' : 'tip',
          processing: true,
          serverSide: true,
          ajax: {
            "url": '{{url('dataCreditos')}}',
            "data": {
              'tipo': 6
            }
          },
          columns: [
              {data: 'Codigo', name: 'Codigo'},
              {data: 'Monto', name: 'Monto'},
              {data: 'Estado', name: 'Estado'},
              {data: 'Clasificacion', name: 'Clasificacion'},
              {data: 'Tipo', name: 'Tipo'},
              {data: 'Plan', name: 'Plan'},
              {data: 'Ruta', name: 'Ruta'},
              {data: 'Cliente', name: 'Cliente'},
              {data: 'Acciones', name: 'Acciones'}
          ]
        });

        var oTable = $('#tabla-creditos').DataTable({
          "order": [[ 0, "desc" ]],
          "language": {
              "url": "{{asset('fonts/dataTablesEsp.json')}}",
          },
          "paging":   true,
          "info":     false,
          'dom' : 'tip',
          processing: true,
          serverSide: true,
          ajax: {
            "url": '{{url('dataCreditos')}}',
            "data": {
              'tipo': 1
            }
          },
          columns: [
              {data: 'Codigo', name: 'Codigo'},
              {data: 'Monto', name: 'Monto'},
              {data: 'Estado', name: 'Estado'},
              {data: 'Clasificacion', name: 'Clasificacion'},
              {data: 'Tipo', name: 'Tipo'},
              {data: 'Plan', name: 'Plan'},
              {data: 'Ruta', name: 'Ruta'},
              {data: 'Cliente', name: 'Cliente'},
              {data: 'Acciones', name: 'Acciones'}
          ]
        },
      );

        $('#busqueda-creditos').keyup(function(){
              oTable.search($(this).val()).draw();
        });

        $('#plan-activos').change(function (){
          oTable.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        $('#ruta-activos').change(function (){
          oTable.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        //Tabla de vencidos
        var oTable2 = $('#tabla-creditos-2').DataTable({
          "order": [[ 0, "desc" ]],
          "language": {
              "url": "{{asset('fonts/dataTablesEsp.json')}}",
          },
          "paging":   true,
          "info":     false,
          'dom' : 'tip',
          processing: true,
          serverSide: true,
          ajax: {
            "url": '{{url('dataCreditos')}}',
            "data": {
              'tipo': 3
            }
          },
          columns: [
              {data: 'Codigo', name: 'Codigo'},
              {data: 'Monto', name: 'Monto'},
              {data: 'Estado', name: 'Estado'},
              {data: 'Clasificacion', name: 'Clasificacion'},
              {data: 'Tipo', name: 'Tipo'},
              {data: 'Plan', name: 'Plan'},
              {data: 'Ruta', name: 'Ruta'},
              {data: 'Cliente', name: 'Cliente'},
              {data: 'Acciones', name: 'Acciones'}
          ]
        });

        $('#busqueda-creditos-2').keyup(function(){
              oTable2.search($(this).val()).draw();
        });

        $('#plan-vencidos').change(function (){
          oTable2.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        $('#ruta-vencidos').change(function (){
          oTable2.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        //Tabla en mora
        var oTable3 = $('#tabla-creditos-3').DataTable({
          "order": [[ 0, "desc" ]],
          "language": {
              "url": "{{asset('fonts/dataTablesEsp.json')}}",
          },
          "paging":   true,
          "info":     false,
          'dom' : 'tip',
          processing: true,
          serverSide: true,
          ajax: {
            "url": '{{url('dataCreditos')}}',
            "data": {
              'tipo': 5
            }
          },
          columns: [
              {data: 'Codigo', name: 'Codigo'},
              {data: 'Monto', name: 'Monto'},
              {data: 'Estado', name: 'Estado'},
              {data: 'Clasificacion', name: 'Clasificacion'},
              {data: 'Tipo', name: 'Tipo'},
              {data: 'Plan', name: 'Plan'},
              {data: 'Ruta', name: 'Ruta'},
              {data: 'Cliente', name: 'Cliente'},
              {data: 'Acciones', name: 'Acciones'}
          ]
        });

        $('#busqueda-creditos-3').keyup(function(){
              oTable3.search($(this).val()).draw();
        });

        $('#plan-3').change(function (){
          oTable3.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        $('#ruta-3').change(function (){
          oTable3.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        //Tabla de pendientes
        var oTable4 = $('#tabla-creditos-4').DataTable({
          "order": [[ 0, "desc" ]],
          "language": {
              "url": "{{asset('fonts/dataTablesEsp.json')}}",
          },
          "paging":   true,
          "info":     false,
          'dom' : 'tip',
          processing: true,
          serverSide: true,
          ajax: {
            "url": '{{url('dataCreditos')}}',
            "data": {
              'tipo': 2
            }
          },
          columns: [
              {data: 'Codigo', name: 'Codigo'},
              {data: 'Monto', name: 'Monto'},
              {data: 'Estado', name: 'Estado'},
              {data: 'Clasificacion', name: 'Clasificacion'},
              {data: 'Tipo', name: 'Tipo'},
              {data: 'Plan', name: 'Plan'},
              {data: 'Ruta', name: 'Ruta'},
              {data: 'Cliente', name: 'Cliente'},
              {data: 'Acciones', name: 'Acciones'}
          ]
        });

        $('#busqueda-creditos-4').keyup(function(){
              oTable4.search($(this).val()).draw();
        });
        $('#plan-4').change(function (){
          oTable4.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        $('#ruta-4').change(function (){
          oTable4.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        var oTable5 = $('#tabla-creditos-apro').DataTable({
          "language": {
              "url": "{{asset('fonts/dataTablesEsp.json')}}",
          },
          "paging":   true,
          "info":     false,
          'dom' : 'tip',
          processing: true,
          serverSide: true,
          ajax: {
            "url": '{{url('dataCreditos')}}',
            "data": {
              'tipo': 4
            }
          },
          columns: [
              {data: 'Codigo', name: 'Codigo'},
              {data: 'Monto', name: 'Monto'},
              {data: 'Estado', name: 'Estado'},
              {data: 'Clasificacion', name: 'Clasificacion'},
              {data: 'Tipo', name: 'Tipo'},
              {data: 'Plan', name: 'Plan'},
              {data: 'Ruta', name: 'Ruta'},
              {data: 'Cliente', name: 'Cliente'},
              {data: 'Acciones', name: 'Acciones'}
          ]
        });

        $('#busqueda-creditos-apro').keyup(function(){
              oTable5.search($(this).val()).draw();
        });
        $('#plan-5').change(function (){
          oTable5.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });
        $('#ruta-5').change(function (){
          oTable5.column($(this).data('column'))
          .search($(this).val())
          .draw();
        });

        $("tbody").on('click','.client-link',function(){
            var valor = $(this).attr("value");
            var url = '{{ route("creditos.show", ":id") }}';
            url = url.replace(':id', valor);
            $.ajax({
                type: "GET",
                url: url,
                success: function( response ) {
                  $('#resumen_perfil').html(response);
                }
            });

        });

        $("tbody").on('click', '#pagar-prestamo', function(){
          var valor = $(this).attr("value");
          var url = '{{ route("pagos.info", ":id") }}';
          url = url.replace(':id', valor);
          $.ajax({
              type: "GET",
              url: url,
              success: function( response ) {
                $('#nom_cliente').val(response['cliente']);
                $('#cliente').val(response['prestamo']);
                $('#no_cuota').val(response['no_cuota']);
                $('#total').val(response['cuota']);
                $('#prestamo').val(response['prestamo']);
                $('#modal-pago').modal('show');
              }
          });
        });


        $("tbody").on('click', '#cambiar-cartera', function(){
          var valor = $(this).attr("value");
          var url = '{{ route("pagos.cambio", ":id") }}';
          url = url.replace(':id', valor);
          $.ajax({
              type: "GET",
              url: url,
              success: function( response ) {
                $('#nueva_ruta').html("");
                $('#ruta_antigua').val(response['actual']);
                $('#antigua').val(response['actual']);
                $('#prestamo_new').val(response['prestamo']);
                $('#prestamo_old').val(response['prestamo']);
                $.each(response['todas'], function(id, nombre){
                    var option = new Option(nombre.id, nombre.id);
                    $(option).html(nombre.nombre);
                    $("#nueva_ruta").append(option);
                });

                $('#modal-cambio').modal('show');
              }
          });
        });

  $("tbody").on('click', '#pago-vencido', function(){
    var valor = $(this).attr("value");
    var url = '{{ route("info.credito", ":id") }}';
    console.log(valor);
    url = url.replace(':id', valor);
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          console.log(response);
          $('#nom_cliente_v').val(response['cliente']);
          $('#prestamo_v').val(response['prestamo']);
          $('#no_prestamo_v').val(response['no_prestamo']);
          $('#capital_v').val(response['capital_vencido']);
          $('#mora_v').val(response['mora']);
          $('#interes_v').val(response['interes']);
          $('#total_v').val(response['cuota']);
        }
    });
  });


});
function clickeliminar(){
$(".eliminarcredito").click(function(){
  var valor = $(this).attr('value');
  eliminarcredito(valor);
});
}

function eliminarcredito(valor){
  console.log('click');
  var url = "{{route('creditos.eliminarcredito')}}";
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  swal({
    type: 'info',
    title: "Esta seguro que desea eliminar el crédito?",
    text: "Se eliminaran toda la información de éste crédito, los pagos, la ficha de pago y la ruta!. Además, no se podran recuperar estos datos!",
    icon: "warning",
    buttons: true,
    dangerMode: false,
    footer: '<a href>Why do I have this issue?</a>',
  }).then((willDelete) => {
    if (willDelete) {
      $.ajax({
        method: 'GET',
        url: url,
        data: {
          valor : valor,
        },
        success: function( response ) {
          console.log(response);
          console.log('peligro');
          swal("Se eliminó toda la información del crédito, los pagos, la ficha de pago y la ruta.", {
            icon: "success",
          });
          setTimeout("location.reload()",1000);

        },
        error: function( response ) {
          console.log(response);
          console.log('peligro');
          swal("Lo sentimos, Ocurrió un error!", {
            icon: "warning",
          });

        }
      });
    } else {
      swal("No se eliminó el crédito",{icon: "warning",});
    }
  });
}

</script>

@endsection
