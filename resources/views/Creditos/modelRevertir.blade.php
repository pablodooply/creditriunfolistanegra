<div class="row">
  <div id="modal-revertir" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
          <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Cambiar estado pago</h5>
        </div>
        <div class="ibox-content">
          {!! Form::  open(['id'=> 'formulario','route' => 'pagos.store', 'method' => 'POST']) !!}
          <div class="form-row col-sm-12">
              <div class="form-group col-sm-6">
                {{ Form::label('name', 'Cuota') }}
                {{ Form::text('cuota', null, ['class' => 'form-control', 'id' => 'rev_cuota']) }}
              </div>
              <div class="form-group col-sm-6">
                {{ Form::label('name', 'No. cuota') }}
                {{ Form::text('no_cuota', null, ['class' => 'form-control', 'id' => 'no_cuota']) }}
              </div>
          </div>

          <div class="form-row">
              <div class="form-group" id="guardar">
                {{-- {{ Form::submit('Pago', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton', 'value' => 'pago']) }} --}}
                {{-- {{ Form::submit('No pago',['class' => 'btn btn-md btn-danger','name' => 'submitbutton', 'value' => 'no_pago'])}} --}}
                {{-- {{ Form::button('Pendiente',['class' => 'btn btn-md btn-warning','name' => 'submitbutton', 'value' => 'no_pago'])}} --}}
            </div>
          </div>
        </div>
      {!! Form::close() !!}
      </div>
    </div>
        </div>
      </div>
    </div>
  </div>
</div>
