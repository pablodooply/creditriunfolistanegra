<div class="row">
  <div id="modal-pago" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Pago</h5>
                </div>
                <div class="ibox-content">
                  {!! Form::  open(['id'=> 'formulario', 'method' => 'POST','route' => 'pagos.store']) !!}
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-6">
                            {{ Form::label('name', 'Cliente') }}
                            {{ Form::text('nom_cliente','Alex', ['class' => 'form-control', 'id' => 'nom_cliente', 'readonly']) }}
                            {{ Form::hidden('cliente', null, array('id' => 'cliente')) }}
                        </div>
                        <div class="form-group col-sm-6">
                            {{ Form::label('name', 'Prestamo') }}
                            {{ Form::text('prestamo','3', ['class' => 'form-control', 'id' => 'prestamo', 'readonly']) }}
                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-6">
                            {{ Form::label('name', 'No. Cuota') }}
                            {{ Form::text('no_cuota', '10', ['class' => 'form-control', 'id' => 'no_cuota' , 'readonly']) }}
                        </div>
                        <div class="form-group col-sm-6">
                            {{ Form::label('name', 'Total') }}
                            {{-- {{ Form::number('cuota', '240' , ['min'=>'0,01', 'class' => 'form-control', 'id' => 'total']) }} --}}
                            <input type="text" name="cuota" class="form-control valor" id="total">
                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-12">
                            {{ Form::label('name', 'Observaciones') }}
                            {{ Form::text('observaciones', null, ['class' => 'form-control', 'id' => 'observacion' ]) }}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          {{ Form::submit('Pago', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton', 'value' => 'pago']) }}
                          {{ Form::submit('No pago',['class' => 'btn btn-md btn-danger','name' => 'submitbutton', 'value' => 'no_pago'])}}

                          {{-- {{ Form::submit('Dia de perdon',['id' => 'btn-perdon','class' => 'btn btn-md btn-warning','name' => 'submitbutton', 'value' => 'pendiente'])}} --}}
                      </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
