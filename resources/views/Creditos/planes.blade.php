@extends('layouts.app')

@section('title', 'Planes')

@section('content')

@section('nombre','Planes')
@section('ruta')
  <li class="active">
      <strong>Planes</strong>
  </li>
@endsection
@include('Creditos.modalPlan')
  <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox">
                <div class="ibox-title">
                  <h5>Planes:</h5>
                </div>
                <div class="ibox-content">
                  <h5 class="text-success">Agregar nuevo plan:</h5>
                  <hr>
                  <div class="row">
                    {!! Form::  open(['id'=> 'form-credito', 'route' => 'creditos.planesStore','method' => 'POST']) !!}
                    <div class="form-group col-sm-4">
                      {{ Form::label('name', 'Plan:') }}
                      <select class="form-control" name="plan">
                        <option value="Diario">Diario</option>
                        <option value="Semanal">Semanal</option>
                        <option value="Quincena">Quincena</option>
                      </select>
                    </div>
                    <div class="form-group col-sm-4">
                      {{ Form::label('name', 'Monto') }}
                      {{ Form::text('monto', 0, ['class' => 'form-control', 'id' => 'monto', 'required']) }}
                    </div>
                    <div class="form-group col-sm-4">
                      {{ Form::label('name', 'Interes') }}
                      {{ Form::text('interes', null, ['class' => 'form-control', 'id' => 'interes', 'required']) }}
                    </div>
                    <div class="form-group col-sm-4">
                      {{ Form::label('name', 'Mora') }}
                      {{ Form::text('mora', null, ['class' => 'form-control', 'id' => 'mora', 'required']) }}
                    </div>
                    <div class="form-group col-sm-4">
                      {{ Form::label('name', 'No. Pagos') }}
                      {{ Form::text('no_pagos', 0, ['class' => 'form-control', 'id' => 'no_pagos', 'required']) }}
                    </div>
                    <div class="form-group col-sm-4">
                      {{ Form::label('name', 'Cuota') }}
                      {{ Form::text('cuota', null, ['class' => 'form-control', 'id' => 'cuota', 'disabled']) }}
                    </div>
                    {!! Form::submit('Crear', ['class' => 'btn btn-info pull-right']) !!}
                    {{Form::close()}}
                    <div class="col-md-12">
                      <h5 class="text-info">Planes actuales:</h5>
                      <div class="table-responsive">
                      <table id="tabla-planes" class="table table-striped">
                        <thead>
                          <tr>
                            <th>Plan</th>
                            <th>Total</th>
                            <th>Capital</th>
                            <th>Interes</th>
                            <th>Mora</th>
                            <th>Cuota</th>
                            <th>No pagos</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($planes as $plan)
                            <tr>
                              <td>{{$plan->nombre}}</td>
                              <td>Q.{{$plan->total}}</td>
                              <td>Q.{{$plan->total-$plan->interes}}</td>
                              <td>Q.{{$plan->interes}}</td>
                              <td>Q.{{$plan->mora}}</td>
                              <td>Q.{{$plan->cuota}}</td>
                              <td>{{$plan->periodo->tiempo}}</td>
                              <td><a value ="{{$plan->id}}"class="btn btn-success btn-outline btn-sm plan_info" data-toggle="modal" data-target="#modal-plan">
                                  <i class="fa fa-pencil"></i></a></td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

  <script type="text/javascript">

  var datos;
  var oTable = $('#tabla-planes').DataTable({
    "language": {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    "paging":   true,
    "info":     false,
    'dom' : 'tip'

  });
  $('#monto').change(function(){
    calcCuota();
  });
  $('#no_pagos').change(function(){
    calcCuota();
  });
  $('#interes').change(function(){
    calcCuota();
  });

    function calcCuota(){
      var monto =  parseFloat($('#monto').val());
      var interes = parseFloat($('#interes').val());
      var tiempo = parseFloat($('#no_pagos').val());

      var cuota = (monto+interes)/tiempo;
      $('#cuota').val(cuota);
    }

    calcCuota();

    $('tbody tr').on('click', 'td .plan_info',function(){
      var valor = $(this).attr('value');
      infoPlan(valor);
    })

    function infoPlan(valor){
      var url = "{{route('creditos.editar_plan')}}";
      var opciones;
      $.ajax({
        type: "GET",
        url: url,
        data: {id: valor},
        success: function( response ){
          if(response.plan == "Diario"){
            opciones = {"Diario": "Diario", "Semanal": "Semanal", "Quicena": "Quincena"};
          } else if(response.plan == "Semanal"){
            opciones = {"Semanal":"Semanal", "Diario": "Diario", "Quincena": "Quincena"};
          } else{
            opciones = {"Quincena": "Quincena", "Diario": "Diario", "Semanal": "Semanal"};
          }

          var $select_edit = $('#plan_plan');
          $select_edit.empty();
          $.each(opciones, function(key,value) {
            $select_edit.append($("<option></option>")
               .attr("value", value).text(key));
          });
          $('#capital_e').val(response.capital);
          $('#interes_e').val(response.interes);
          $('#mora_e').val(response.mora);
          $('#nopagos_e').val(response.nopagos);
          $('#plan_edit').val(response.id);
          calcCuota_edit();
        }
      });
      return datos;
    }
  </script>


  <script type="text/javascript">


  function calcCuota_edit(){
    var monto =  parseFloat($('#capital_e').val());
    var interes = parseFloat($('#interes_e').val());
    var tiempo = parseFloat($('#nopagos_e').val());

    var cuota = (monto+interes)/tiempo;
    $('#cuota_e').val(cuota);
  }

  $('#capital_e').change(function(){
    calcCuota_edit();
  });

  $('#interes_e').change(function(){
    calcCuota_edit();
  });

  $('#nopagos_e').change(function(){
    calcCuota_edit();
  })

  </script>

@endsection
