<div class="row">
  <div id="modal-cambio" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Cambio de Ruta</h5>
                </div>
                <div class="ibox-content">
                  {!! Form::  open(['id'=> 'formulario', 'method' => 'POST', "route" => "cambio.cartera"]) !!}
                  {{-- {{ Form::text('prestamo','3', ['class' => 'form-control', 'id' => 'ejemplo', 'readonly']) }} --}}
                  {{-- <textarea name="textarea" rows="10" cols="50" id="ejemplo">Write something here</textarea> --}}
                  <div class="form-row col-sm-12">
                    <div class="form-group col-sm-6">
                      {{ Form::label('name', 'Prestamo') }}
                      {{ Form::text('prestamo_new',null, ['class' => 'form-control', 'id' => 'prestamo_new', 'readonly']) }}
                      {{ Form::hidden('prestamo_old', null, array('id' => 'prestamo_old')) }}
                    </div>
                      <div class="form-group col-sm-6">
                        {{ Form::label('name', 'Ruta antigua') }}
                        {{ Form::text('ruta_antigua',null, ['class' => 'form-control', 'id' => 'ruta_antigua', 'readonly']) }}
                        {{ Form::hidden('antigua', null, array('id' => 'antigua')) }}
                      </div>

                      <div class="form-group col-sm-6">
                        {{ Form::label('name', 'Seleccione ruta nueva') }}
                        <select id="nueva_ruta" name="nueva_ruta" class="form-control">
                        </select>
                      </div>
                  </div>
                  <br>
                  <div class="form-row">
                      <div class="form-group" id="guardar">
                        {{ Form::submit('Actualizar', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton']) }}
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>



                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
