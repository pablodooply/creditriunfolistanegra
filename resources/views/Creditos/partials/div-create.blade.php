  <link href="{{asset('css/dropzone/dropzone.css')}}" rel="stylesheet" media="screen">
<div class="col-lg-6 " id="div-create-{{$id_div}}">
  <div class="ibox ">
    <div class="ibox-title">
      <h5>Garantía #{{$id_div}}</h5>
      <div class="ibox-tools">
        <a class="collapse-link">
          <i class="fa fa-chevron-up"></i>
        </a>
        <a class="close-link" onclick="eliminardivcreate('{{$id_div}}')">
          <i class="fa fa-times"></i>
        </a>
      </div>
    </div>
    <div class="ibox-content">
      <div class="animated fadeInRight">
        <div class="mail-box-header">
          <h2>
            <strong>Información de garantía</strong>
          </h2>
          <div class="mail-tools tooltip-demo m-t-md">
            <h5>
              <p><span class="float-right font-normal">Fecha: </span>{{\Carbon\Carbon::now()}}</p>
              <p><span class="font-normal">Lugar: </span>Mazatenango, Guatemala</p>
            </h5>
          </div>
          <div class="float-right tooltip-demo">
            <a href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Print email"><i class="fa fa-print"></i> </a>
            <a onclick="eliminardivcreate('{{$id_div}}')" href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </a>
          </div>
        </div>
        <div class="mail-box">
          <div class="mail-body">
            <p>
              <h5><strong>Formulario de garantía</strong></h5>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label>Nombre</label>
                    <input name="nombre_garantia[]" required  type="text" placeholder="Ingrese nombre" class="form-control input-garantia">
                    {{-- {{ Form::text('nombre_garantia[]', null, ['class' => 'form-control', 'id' => 'nombre' ]) }} --}}
                  </div>
                  <div class="form-group col-md-6">
                    <label>Cantidad</label>
                    <input name="cantidad_garantia[]" required  type="number" placeholder="Ingrese cantidad" class="form-control input-garantia" >
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label>Valorado en Q. (cliente)</label>
                    <input name="valoracion_garantia[]" required  type="number" placeholder="Ingrese valoración" class="form-control input-garantia  sum-garantia">
                  </div>
                  <div class="form-group col-md-6">
                    <label>Estado producto de 1-10 (cliente)</label>
                    <input name="estado_producto_garantia[]" required min="1" max="10" type="number" placeholder="Ingrese estado de producto" class="form-control input-garantia">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-12">
                    <label for="">Categoría</label>
                    {!! Form::select('categoria_garantia_id[]', $categorias, 0, ['class' => 'form-control input-garantia', 'id'=>'categorias']) !!}

                  </div>
                </div>
                <div class="row">
                  </form>
                  <div class="form-group col-md-12">
                    <label>Comentario</label>
                    {{-- <input type="email" placeholder="Enter email" class="form-control"> --}}
                    <textarea name="descripcion_garantia[]" required  rows="4" placeholder="Ingrese Descripción" class="form-control input-garantia"></textarea>
                  </div>
                </div>
                <div id="row{{$id_div}}" class="row">
                  {{-- <div class="form-group col-md-6">
                    <label>Imagen 1</label>
                    <input name="imagen_uno[]" required  type="file" placeholder="Ingrese imagen" class="form-control input-garantia">
                  </div>
                  <div class="form-group col-md-6">
                    <label>Imagen 2</label>
                    <input name="imagen_dos[]" required  type="file" placeholder="Ingrese imagen" class="form-control input-garantia">
                  </div> --}}
                </div>
          </div>


          {{-- <input type="text" class="total" value="" /> --}}

          <div class="mail-body text-right tooltip-demo">
            {{-- <a class="btn btn-sm btn-white" href="mail_compose.html"><i class="fa fa-reply"></i> Comentario</a> --}}
            <button title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="Print" class="btn btn-sm btn-white"><i class="fa fa-print"></i> Imprimir</button>
            <button onclick="eliminardivcreate('{{$id_div}}')" title="" data-placement="top" data-toggle="tooltip" data-original-title="Trash" class="btn btn-sm btn-white"><i class="fa fa-trash-o"></i> Eliminar</button>
          </div>
          <div class="clearfix">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
// function keygarantia()
// {
// var  sum=0;
//       // sum += parseFloat(this.value);
//       // sum += parseFloat($(".sum-garantia").val());
//       $(".sum-garantia").each(function() {
//
//         sum =$(this).val();
//       });
//       console.log(sum);
// }

$( ".sum-garantia" ).keyup(function() {
  var sum = 0;
   $("input[class *= 'sum-garantia']").each(function(){
       sum += +$(this).val();
   });
   // $(".total").val(sum);
   // this.value = parseFloat(this.value).toFixed(2);
$('#lblVessel').text(sum.toFixed(2));
   console.log(sum);
});
</script>
