@extends('layouts.app')
@section('title', 'Renovacion')
@section('link')
  <link href="{{asset('css/timepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">
  <link href="{{asset('css/imageuploader/image-uploader.min.css')}}" rel="stylesheet">
@endsection
@section('content')
  <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
          <div class="col-sm-12">
            <div class="col-sm-4">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Detalle de cliente</h5>
                </div>
                <div class="ibox-content">
                  <div class="client-detail">
                    <div class="full-height-scroll">
                        <strong>Datos Personales</strong>
                        <ul class="list-group clear-list">
                          <li class="list-group-item fist-item">
                              <span class="pull-right">{{$cliente->persona->nombre}} {{$cliente->persona->apellido}}</span>
                              Nombre
                          </li>
                            <li class="list-group-item">
                                <span class="pull-right"> {{$cliente->obtenerEdad()}} </span>
                                Edad
                            </li>
                            <li class="list-group-item ">
                                <span class="pull-right">{{$cliente->persona->dpi}}</span>
                                Dpi
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">{{$cliente->persona->nit}}</span>
                                Nit
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">{{$cliente->persona->telefono}}</span>
                                Telefono
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">{{$cliente->persona->celular1}}</span>
                                Celular 1
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">{{$cliente->persona->celular2}}</span>
                                Celular 2
                            </li>
                        </ul>
                        <strong>Prestamos</strong>
                        <table class='table table-striped table-hover'>
                          <thead>
                            <tr>
                              <th>Código</th>
                              <th>Estado</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($cliente->prestamos as $prestamo)
                              <tr>
                                <td><a class="client-link" href="{{route('creditos.show', $prestamo->id)}}">Cre-{{$prestamo->id}}</a></td>
                                {!!$prestamo->obtenerEstado()!!}
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Renovacion</h5>
                </div>
                  <div class="ibox-content">
                    {{--*************************************************  --}}
                    {!! Form::  open(['id'=> 'form-renovacion','route' => 'creditos.renoCreate', 'method' => 'POST','files' => true, 'enctype' => 'multipart/form-data']) !!}
                    <h5 class='text-success'>Datos de Prestamo</h5>
                      <div class="form-row col-sm-12">
                        <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Hora') }}
                            {{ Form::time('hora', null, ['class' => 'form-control', 'id' => 'timepicker', 'autocomplete' => 'off'])}}
                        </div>
                        <!--*****************FEcha Desembolso******************-->
                        @php
                            $month = date('m');
                            $day = date('d');
                            $year = date('Y');
                            $today = $year . '-' . $month . '-' . $day;
                        @endphp
                        <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Fecha posible desembolso') }}
                            <input type="date" id="bday" class="form-control" name="fecha_desembolso" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="{{$today}}">
                        </div>
                        <!--*****************FEcha Desembolso******************-->
                        <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Fecha posible 1er. Pago') }}
                            <input type="date" id="bday" class="form-control" name="fecha_inicio" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="{{$today}}">
                        </div>
                      </div>
                      <div class="form-row col-sm-12">
                        <div class="form-group">
                          <div class="form-group col-sm-4">
                          <!--*****************Promotor******************-->
                              {{ Form::label('name', 'Promotor quien coloco:') }}
                              <select class="form-control" name="promotor">
                                @foreach ($promotores as $promotor)
                                  <option value="{{$promotor->id}}">{{$promotor->persona->nombre}} {{$promotor->persona->apellido}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Ruta') }}
                              <select class="form-control" name="ruta" id="ruta">
                                @foreach ($hoja_ruta as $hoja)
                                  <option value="{{$hoja->id}}">{{$hoja->nombre}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="form-group col-sm-4">
                          <!--*****************Promotor******************-->
                              {{ Form::label('name', 'Promotor de la ruta:') }}
                              {{ Form::text('promotor_ruta', $hoja_ruta->first()->user->persona->nombre . $hoja_ruta->first()->user->persona->apellido, ['class' => 'form-control', 'id' => 'promotor_ruta', 'readonly']) }}
                          </div>
                        </div>
                      </div>
                      <div class="form-row col-sm-12">
                        <div class="form-group">
                          <!--*****************Plan******************-->
                          <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Plan') }}
                            <select id="tipoPlan"class="form-control" name="plan">
                              <option id="1" value="Diario">Diario</option>
                              <option id="2" value="Semanal">Semanal</option>
                              <option id="3" value="Quincena">Quincena</option>
                            </select>
                          </div>
                          <!--*****************No. de pagos******************-->
                              <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'No. pagos') }}
                                  <select class="form-control" name="no_pagos" id="no_pagos">
                                  </select>
                                </div>
                          <!--*****************Cantidad de prestamo******************-->
                          <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Cantidad') }}
                              <select id="monto" class="form-control" name="monto">
                              </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-row col-sm-12">
                          <div class="form-group">
                            <!--****************Interes******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Interes') }}
                                {{ Form::text('interes', null, ['class' => 'form-control', 'readonly', 'id' => 'interes']) }}
                            </div>
                            <!--*****************Mora******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Mora') }}
                                {{ Form::text('mora', null, ['class' => 'form-control', 'readonly', 'id' => 'mora2'])}}
                            </div>
                            <!--*****************Saldo total pendiente******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Saldo Total Pendiente', ['class' => 'text-danger']) }}
                                {{ Form::text('sadototalpendiente', $saldototal, ['class' => 'form-control text-danger', 'readonly'])}}
                            </div>
                          </div>
                      </div>
                      <section>
                        <label for="chkPassport">
                          <input type="checkbox" value="1" name="validacion_garantia" id="chkPassport" checked/>
                          Desea agregar garantías?
                        </label>
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="" id="puede-continuar" style="display:none;">
                              <div class="ibox">
                                <div class="ibox-content profile-content">
                                <h4 class="text-center"><strong>No agregará garantías?</strong></h4>
                                <h5 class="text-center">
                                  Puede darle click al botón de "Siguiente" para continuar.
                                </h5>
                              </div>
                              </div>
                            </div>
                            <div  id="div-garantias" >
                              <button type="button" name="add" id="add" class="btn btn-success btn-block">
                                <span class="fa fa-plus"></span>
                                Agregar Garantía
                              </button>
                              <div id="dynamicTable"></div>
                            </div>
                            <h3 class="float-right" style="float:right;">Valoración total (cliente): Q. <label id="lblVessel" for="">0.00</label></h3>
                          </div>
                        </div>
                      </section>
                    {{ Form::hidden('cliente', $cliente->id) }}
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          @if(Auth::user()->hasAnyRole(['Administrador','Supervisor', 'Secretaria']))
                            {{ Form::submit('Registrar', ['class' => 'btn btn-md btn-primary']) }}
                            {{ Form::button('Cancelar',['class' => 'btn btn-md btn-danger'])}}
                          @else
                            @if($saldototal > 0)
                            @elseif($saldototal <= 0)
                              {{ Form::submit('Registrar', ['class' => 'btn btn-md btn-primary']) }}
                              {{ Form::button('Cancelar',['class' => 'btn btn-md btn-danger'])}}
                            @endif
                          @endif
                      </div>
                    </div>
                    {!! Form::close() !!}
                  </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{asset ('js/validate/jquery.validate.min.js')}}"></script>
<script src="{{asset ('js/timepicker/bootstrap-datetimepicker.js')}}"></script>
  <script src="{{asset('js/imageuploader/image-uploader.min.js')}}"></script>
<script type="text/javascript">
var form = $("#form-renovacion");
form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        cantidad: {
            required: true,
            number:   true,
        },
        no_pagos: {
            required: true,
            number: true
        },
        interes: {
            required: true,
            number: true
        },
        mora: {
            required: true,
            number: true
        },
        hora: {
            required: true,
        },
    }
});
$("#tipoPlan").change(function(){
  obtenerNoPagos();
});
$("#no_pagos").change(function(){
  obtenerMontos();
});
$("#monto").change(function(){
  obtenerInfo();
});
obtenerNoPagos();
function obtenerNoPagos(){
  var plan = $("#tipoPlan :selected").attr("value");
  $.ajax({
    type: "GET",
    url: "{{route('info.nopagos')}}",
    data: {
      tipo: plan,
    },
    success: function( response ) {
      $("#no_pagos").empty();
      $.each(response, function(id,tiempo){
          $("#no_pagos").append('<option value="'+ tiempo.id+'">'+  tiempo.tiempo +'</option>');
        });
        obtenerMontos();
    }
  });
}
function obtenerMontos(){
  var periodo = $("#no_pagos :selected").attr("value"); // The text content of the selected option
  $.ajax({
    type: 'GET',
    url: "{{route('info.montos')}}",
    data: {
      tipo: periodo,
    },
    success: function( response ) {
      $("#monto").empty();
      $.each(response, function(id,total){
          $("#monto").append('<option value="'+total.id+'">'+ total.capital  +'</option>');
        });
        obtenerInfo();
    }
  });
}
var valores;
function obtenerInfo(){
  var plan = $("#monto :selected").attr("value");
  $.ajax({
    type: "GET",
    url: "{{route('info.infoPlan')}}",
    data: {
      tipo: plan
    },
    success: function( response ) {
      valores = response;
      console.log(valores);
      $('#interes').val(response['interes']);
      $('#mora2').val(response['mora']);
    },
  });
}
$('#timepicker').datetimepicker({
      format: 'hh:ii',
      language:  'es',
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 1,
      minView: 0,
      maxView: 1,
      forceParse: 0
  });
$("#ruta").change(function(){
  var valor = $("#ruta :selected").val(); // The text content of the selected option
  var url = '{{ route("ruta.promotor", ":id") }}';
  url = url.replace(':id', valor);
  $.ajax({
      type: "GET",
      url: url,
      success: function(response) {
        console.log(response);
        $("#promotor_ruta").val(response);
      }
  });
});
</script>
<script type="text/javascript">
$(function () {
      $("#chkPassport").click(function () {
          if ($(this).is(":checked")) {
            $("#puede-continuar").hide("slow");
            $("#div-garantias").show("slow");
          } else {
              $("#puede-continuar").show("slow");
              $("#div-garantias").hide("slow");
          }
      });
  });
</script>
<script type="text/javascript">
    var i = 0;
    $("#add").click(function(){
      var url = '{{route("garantia.div.create")}}';
        ++i;
        $.ajax({
            type: "GET",
            url: url,
            data: {
                id_div : i
            },
            success: function( response ) {
              $('#dynamicTable').append(response).show('slow');
              var e = $('<div class="" id="input-images'+ i+'"></div>');
              $('#row'+i).append(e);
              $("#input-images"+ i).addClass("input-images").imageUploader({ label:'Arrastre o agregue sus imágenes.',imagesInputName:'photos'+i, maxFiles: 15});
            }
        });
        // $("#dynamicTable").append('<tr><td><input type="text" name="addmore['+i+'][name]" placeholder="Enter your Name" class="form-control" /></td><td><input type="text" name="addmore['+i+'][qty]" placeholder="Enter your Qty" class="form-control" /></td><td><input type="text" name="addmore['+i+'][price]" placeholder="Enter your Price" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
    }
    );
    function eliminardivcreate(div_id)
    {
      $('#div-create-' + div_id).fadeOut("slow", function() {
      // After animation completed:
      $('#div-create-' + div_id).remove();
    });
    }
    $(document).on('click', '.remove-tr', function(){
         $(this).parents('tr').remove();
    });
    // $("#boton-siguiente").steps("next",{});
    // var sum = 0;
    // function keygarantia()
    // {
    // var  sum=0;
    //       // sum += parseFloat(this.value);
    //       // sum += parseFloat($(".sum-garantia").val());
    //       $(".sum-garantia").each(function() {
    //
    //         sum =$(this).val();
    //       });
    //       console.log(sum);
    // }
    // $( ".sum-garantia" ).keyup(function() {
    //   // alert( "Handler for .keyup() called." );
    //   console.log("pendejo");
    // });
// $(document).ready(function() {
//     $(".sum-garantia").keyup(function(){
//       console.log('keyup');
//       // $("input .sum-garantia").css("background-color", "pink");
//     });
//     });
</script>
@endsection
