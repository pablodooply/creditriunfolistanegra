@extends('layouts.app')

@section('title', 'Fechas especiales')

@section('content')

  @section('nombre','Creditos')
  @section('ruta')
    <li class="active">
        <strong>Fechas especiales</strong>
    </li>
  @endsection
  <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox">
                <div class="ibox-title">
                  <h5>Fecha especiales</h5>
                </div>
                <div class="ibox-content">
                  <h5 class="text-success">Agregar fecha:</h5>
                  <hr>
                  <div class="row">
                    {!! Form::  open(['id'=> 'form-credito', 'route'  => 'creditos.fecha_create','method' => 'POST']) !!}
                    <div class="form-group col-sm-4">
                      {{ Form::label('name', 'Fecha') }}
                      {{ Form::Date('fecha', \Carbon\Carbon::now(), ['class' => 'form-control', 'id' => 'nombre' ]) }}
                    </div>
                    <div class="form-group col-sm-8">
                      {{ Form::label('name', 'Descripcion') }}
                      {{ Form::text('descripcion', null, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                    </div>
                    {!! Form::submit('Crear', ['class' => 'btn btn-info']) !!}
                    {{Form::close()}}
                    <div class="col-md-12">
                      <h5 class="text-info">Fechas actuales:</h5>
                      <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>Fecha</th>
                            <th>Descripcion</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($fechas as $fecha)
                            <tr>
                              <td>{{$fecha->id}}</td>
                              <td>{{$fecha->fecha}}</td>
                              <td>{{$fecha->descripcion}}</td>
                            </tr>
                          @endforeach

                        </tbody>
                      </table>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
