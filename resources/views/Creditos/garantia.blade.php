<div class="ibox ">
    <div class="ibox-title">
        <h5>Garantía #1</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
      <div class="animated fadeInRight">
        <div class="mail-box-header">
            <h2>
                <strong>Información de garantía</strong>
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">
                <h5>
                    <p><span class="float-right font-normal">Fecha: </span>{{\Carbon\Carbon::now()}}</p>
                    <p><span class="font-normal">Lugar: </span>Mazatenango, Guatemala</p>
                </h5>
            </div>
            <div class="float-right tooltip-demo">
                <a href="mail_compose.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Reply"><i class="fa fa-reply"></i> Comentario</a>
                <a href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Print email"><i class="fa fa-print"></i> </a>
                <a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </a>
            </div>

        </div>
            <div class="mail-box">


            <div class="mail-body">
                <p>
                    <h5><strong>Formulario de garantía</strong></h5>
                    <div class="row">
                      <div class="form-group col-md-6">
                        <label>Nombre</label>
                        <input type="email" placeholder="Enter email" class="form-control">
                      </div>
                      <div class="form-group col-md-6">
                        <label>Cantidad</label>
                        <input type="email" placeholder="Enter email" class="form-control">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-md-6">
                        <label>Valoración (cliente)</label>
                        <input type="email" placeholder="Enter email" class="form-control">
                      </div>
                      <div class="form-group col-md-6">
                        <label>Estado producto (cliente)</label>
                        <input type="email" placeholder="Enter email" class="form-control">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-md-12">
                        <label>Descripción</label>
                        {{-- <input type="email" placeholder="Enter email" class="form-control"> --}}
                        <textarea rows="4" placeholder="Enter email" class="form-control">
                          At w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies.
                        </textarea>
                      </div>
                    </div>
            </div>
                <div class="mail-attachment">
                    <p>
                        <span><i class="fa fa-paperclip"></i> 2 Adjuntos - </span>
                        <a href="#">Descargar Todol</a>
                        |
                        <a href="#">Ver todas las imágenes</a>
                    </p>

                    <div class="attachment">
                      <td class="project-people">
                        <a href=""><img alt="image" class="" src="img/a3.jpg"></a>
                        <a href=""><img alt="image" class="" src="img/a1.jpg"></a>
                        <a href=""><img alt="image" class="" src="img/a2.jpg"></a>
                        <a href=""><img alt="image" class="" src="img/a4.jpg"></a>
                        <a href=""><img alt="image" class="" src="img/a5.jpg"></a>
                      </td>
                    </div>
                    </div>
                    <div class="mail-body text-right tooltip-demo">
                            <a class="btn btn-sm btn-white" href="mail_compose.html"><i class="fa fa-reply"></i> Comentario</a>
                            <button title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="Print" class="btn btn-sm btn-white"><i class="fa fa-print"></i> Imprimir</button>
                            <button title="" data-placement="top" data-toggle="tooltip" data-original-title="Trash" class="btn btn-sm btn-white"><i class="fa fa-trash-o"></i> Eliminar</button>
                    </div>
                    <div class="clearfix"></div>


            </div>
        </div>
    </div>
</div>
