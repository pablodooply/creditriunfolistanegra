@extends('layouts.app')

@section('title', 'Cartera')

@section('content')

@section('nombre','Cartera')
@section('ruta')
  <li class="active">
      <strong>Cartera</strong>
  </li>
@endsection
    <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                  <div class="col-lg-4">
                      <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Cartera</h5>
                            </div>
                            <div>
                                <div class="ibox-content profile-content">
                                  <h4>{{$cartera->prestamos->count()}}</h4>
                                  <strong class="text-success">Datos de cartera</strong>

                                  <ul class="list-group clear-list">
                                    <li class="list-group-item fist-item">
                                        <span class="pull-right">{{$cartera->nombre}}</span>
                                        Nombre
                                    </li>
                                      <li class="list-group-item">
                                          <span class="pull-right">{{$cartera->user}}</span>
                                          Promotor
                                      </li>
                                      <li class="list-group-item">
                                          <span class="pull-right"> {{$cartera->activa}} </span>
                                          Estado
                                      </li>
                                  </ul>
                                    <div class="user-button">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-primary btn-sm btn-block"><i class="fa fa-envelope"></i> Documentos</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>

                  <div class="col-md-8">
                      <div class="ibox float-e-margins">
                          <div class="ibox-title">
                              <h5>Carteras</h5>
                              <div class="ibox-tools">
                                  <a class="collapse-link">
                                      <i class="fa fa-chevron-up"></i>
                                  </a>
                                  <a class="close-link">
                                      <i class="fa fa-times"></i>
                                  </a>
                              </div>
                          </div>
                          <div class="ibox-content">
                            <div class="table-responsive">
                              <table class='table table-striped table-hover'>
                                <thead>
                                  <tr>
                                    <th>Código</th>
                                    <th>Monto</th>
                                    <th>Estado</th>
                                    <th>Cliente</th>
                                  </tr>
                                </thead>
                                <tbody>

                                    @foreach ($cartera->prestamos as $prestamo)
                                    <tr>
                                      <td>{{$prestamo->id}}</td>
                                      <td>{{$prestamo->monto}}</td>
                                      @if($prestamo->activo === 1)
                                        <td><a><span class="badge badge-primary pull-center">activo</span></a></td>
                                      @else
                                        <td><a><span class="badge badge-danger pull-center">terminado</span></a></td>
                                      @endif
                                      <td>{{$hoja_ruta->nombre}}</td>
                                      <td> {{count($hoja_ruta->ruta)}}
                                      <td class="project-actions">
                                          <a href="#" class="btn btn-success btn-sm"><i class="fa fa-folder"></i> Ver </a>
                                      </td>
                                    </tr>
                                    @endforeach
                                </tbody>

                              </table>
                            </div>

                          </div>

                          </div>
                      </div>

                  </div>

            </div>
@endsection
