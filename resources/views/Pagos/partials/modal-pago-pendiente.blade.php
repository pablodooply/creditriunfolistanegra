<div class="row">
  <div id="pagomodalpendiente" class="modal fade bd-modal-pago-pendiente" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5> Pago </h5>
                </div>
                <div class="ibox-content">
                  {!! Form::open(['method' => 'POST', 'id' => 'PagoModalPendiente']) !!}
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Cliente') }}
                            {{ Form::text('nom_cliente',null , ['class' => 'form-control valor', 'id' => 'nom_clienteee', "readonly"]) }}
                            {{ Form::hidden('cliente', '', ['id' => 'prestamooo']) }}
                        </div>
                        <div class="form-group col-sm-4">
                          {{ Form::label('name', 'No. Prestamo') }}
                          {{ Form::text('no_prestamo', null, ['class' => 'form-control valor', 'id' => 'no_prestamooo', "readonly" ]) }}
                        </div>
                        <div class="form-group col-sm-4">
                            {{ Form::label('name', 'No. Cuota') }}
                            {{ Form::text('no_cuota',null, ['class' => 'form-control valor', 'id' => 'no_cuotaaa', "readonly" ]) }}
                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                      <div class="form-group col-sm-4">
                        {{ Form::label('name', 'Mora') }}
                        {{ Form::text('mora',null, ['class' => 'form-control valor', 'id' => 'moraaa' , "readonly"]) }}
                      </div>
                      <div class="form-group col-sm-8">
                        {{ Form::label('name', 'Total') }}
                        {{ Form::text('cuota','', ['class' => 'form-control valor', 'id' => 'totalll']) }}
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          {{ Form::submit('Pago', ['class' => 'btn btn-md btn-primary', 'id' => 'botonSiPago', 'name' => 'submitbutton', 'value' => 'pago']) }}
                          {{ Form::submit('No pago',['class' => 'btn btn-md btn-danger','id' => 'botonNoPago', 'name' => 'submitbutton', 'value' => 'no_pago'])}}
                          {{-- {{ Form::submit('Dia de perdon',['class' => 'btn btn-md btn-warning','name' => 'submitbutton', 'value' => 'pendiente'])}} --}}
                      </div>
                    </div>
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
