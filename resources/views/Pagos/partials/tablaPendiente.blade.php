<div class="panel-body">
  <div class="ibox ">
    <div class="ibox-title">

    {{-- <input type="text" placeholder="Ejemplo Cliente: Nombre, Ruta: Alfa, Cod.: Cre-123, Estado: Pendiente" class="input form-control" id="busqueda-creditos-hoy2"> --}}
    <div class="ibox-content">

      <div class="table-responsive">
        <table id="tabla_pendientes" class="table table-striped table-bordered table-hover dataTables-example text-center">
          <thead>
            <tr>
              <th>DPI</th>
              <th>Cliente</th>
              <th>Ruta</th>
              <th>Plan</th>
              <th>Hora</th>
              <th>Cod. prestamo</th>
              <th>Cuotas atrasadas</th>
              <th class="sum">Total</th>
              <th>Estado</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
          <tfoot>
            <tr>
              <th>DPI</th>
              <th>Cliente</th>
              <th>Ruta</th>
              <th>Plan</th>
              <th>Hora</th>
              <th>Cod. prestamo</th>
              <th>Cuotas atrasadas</th>
              <th class="sum">Total</th>
              <th>Estado</th>
              <th>Acciones</th>
            </tr>
          </tfoot>
        </table>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
var tabla=$('#tabla_pendientes').DataTable({
  order: [[ 0, "desc" ]],
  language: {
      "url": "{{asset('fonts/dataTablesEsp.json')}}",
  },
  paging: true,
  info: false,
  dom : 'tip',
  processing: true,
  serverSide: true,
  ajax: {
    "url": '{{route('pagos.apiPendientes')}}'
  },
  pageLength: 10,
  responsive: true,
  dom: '<"html5buttons"B>lTfgitp',
  buttons: [
    {extend: 'copy'},
    {extend: 'csv'},
    {extend: 'excel', title: 'Creditos'},
    {extend: 'pdf', title: 'Creditos'},
    {extend: 'print',
       customize: function (win)
       {
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
                  .addClass('compact')
                  .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
      {data: 'DPI', name: 'DPI'},
      {data: 'Cliente', name: 'Cliente'},
      {data: 'Ruta', name: 'Ruta'},
      {data: 'Plan', name: 'Plan'},
      {data: 'Hora', name: 'Hora'},
      {data: 'Prestamo', name: 'Prestamo'},
      {data: 'Numero', name: 'Numero'},
      {data: 'Cuota', name: 'Cuota'},
      {data: 'Estado', name: 'Estado'},
      {data: 'Acciones', name: 'Acciones'},
  ]
});



$("tbody").on('click', '#pagar-prestamo', function(){

  var valor = $(this).attr("value");

  var url = '{{ route("pagos.info", ":id") }}';

  url = url.replace(':id', valor);

  $.ajax({

      type: "GET",

      url: url,

      success: function( response ) {

        $('#nom_cliente').val(response['cliente']);

        $('#cliente').val(response['prestamo']);

        $('#no_cuota').val(response['no_cuota']);

        $('#total').val(response['cuota']);

        $('#prestamo').val(response['prestamo']);

        if(response['dia_perdon'] == '1'){

          $('#btn-perdon').hide();

        } else{

          $('#btn-perdon').show();

        }

        $('#modal-pago').modal('show');

      }

  });

});
</script>
