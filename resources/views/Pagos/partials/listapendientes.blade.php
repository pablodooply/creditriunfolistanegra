<div id="creditos_hoy" class="tab-pane fade in active">
  <div class="input-group">
    <input type="text" placeholder="Ejemplo Cliente: Nombre, Ruta: Alfa, Cod.: Cre-123, Estado: Pendiente" class="input form-control" id="busqueda-creditos-hoy">
    <span class="input-group-btn">
      <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
    </span>
  </div>

  <div class="table-responsive">
    <table class='table table-striped table-hover' id="tabla-creditos-hoy">
      <thead>
        <tr>
          <th>DPI</th>
          <th>Cliente</th>
          <th>Ruta</th>
          <th>Plan</th>
          <th>Hora</th>
          <th>Cod. prestamo</th>
          <th>No. cuota</th>
          <th class="sum">Cuota</th>
          <th>Estado</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        @if($fichas_pago_hoy->isEmpty())
          <h1> No hay creditos<h1>
          @else
            @foreach ($fichas_pago_hoy->where('estado_p', 0) as $ficha_pago)
              <tr>
                <td>{{$ficha_pago->prestamo->cliente->persona->dpi}}</td>
                <td><a class="client-link" href="{{route('clientes.show',$ficha_pago->prestamo->cliente->id)}}">{{$ficha_pago->prestamo->cliente->persona->nombre ." " .
                  $ficha_pago->prestamo->cliente->persona->apellido}}</a></td>
                  <td><a class="client-link" href="{{route('rutas.show',$ficha_pago->prestamo->ruta->first()->hoja_ruta->id)}}">
                    {{$ficha_pago->prestamo->ruta->first()->hoja_ruta->nombre}}</a></td>
                    @if($ficha_pago->prestamo->plan->nombre=='Diario')
                      <td><span class="badge badge-primary">{{$ficha_pago->prestamo->plan->nombre}}</span></td>
                    @elseif($ficha_pago->prestamo->plan->nombre=='Semanal')
                      <td><span class="badge badge-success">{{$ficha_pago->prestamo->plan->nombre}}</span></td>
                    @elseif($ficha_pago->prestamo->plan->nombre=='Quincena')
                      <td><span class="badge badge-info">{{$ficha_pago->prestamo->plan->nombre}}</span></td>
                    @endif
                    <td>{{$ficha_pago->prestamo->ruta->first()->hora}}</td>
                    <td><a class="client-link" href="{{route('creditos.show', $ficha_pago->prestamo->id)}}">Cre-{{$ficha_pago->prestamo->id}}</a></td>
                    <td>{{$ficha_pago->no_dia}}</td>
                    <td>{{$ficha_pago->cuota}}</td>
                    @switch($ficha_pago->estado_p)
                      @case (0)
                      <td><a><span class='badge badge-warning pull-center'>Pendiente</span></a></td>
                      @break
                      @case (1)
                      <td><a><span class="badge badge-primary pull-center">Pagado</span></a></td>
                      @break
                      @case (2)
                      @if($ficha_pago->cont == 1)
                        <td><a><span class="badge badge-danger pull-center">No Pagado</span></a></td>
                      @else
                        <td><a><span class="badge badge pull-center">Perdon</span></a></td>
                      @endif
                      @break
                      @case (3)
                      <td><a><span class="badge badge-info pull-center">Pago parcial</span></a></td>
                      @break
                      @case (4)
                      <td><a><span class="badge badge-success pull-center">Pago adelantado</span></a></td>
                      @break
                      @case (5)
                      <td><a><span class="badge badge-success pull-center">Parcial adelantado</span></a></td>
                      @break;
                    @endswitch
                    <td>
                      @php
                        $ficha_coment = App\Ficha_pago::where('prestamo_id', $ficha_pago->prestamo->id)->where('no_dia',$ficha_pago->no_dia)->first();
                        $advertencia = App\Ficha_pago::where('prestamo_id', $ficha_pago->prestamo->id)->where('tipo','=',1)->where('no_dia','<',$ficha_pago->no_dia)->where('estado_p', '=',0)->count();
                      @endphp
                      @if(($ficha_pago->estado_p==0 || $ficha_pago->estado_p==5) && $advertencia == 0)
                        <a id="pagar-prestamo" value="{{$ficha_pago->prestamo->id}}" class="btn btn-outline btn-primary btn-sm"><i class="fa"><strong>Q</strong></i></a>
                      {{-- @if($ficha_pago->estado_p==0 || $ficha_pago->estado_p==5 && $advertencia > 0)
                        --}}
                      @else
                        <a href="#" data-toggle="modal" data-target="#modal-tabla-pago" id="" value="{{$ficha_pago->prestamo->id}}" onclick="ModalTabla('{{$ficha_pago->prestamo->id}}')" class="btn btn-outline btn-primary btn-sm"><i class="fa"><strong>Q</strong></i></a>
                        {{-- <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa">Q</i></a> --}}
                      @endif
                      @if($ficha_coment->comentarios->isNotEmpty())
                        <a id="comentario" value="{{$ficha_coment->comentarios->first()->id}}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-comment-o"></i></a>
                      @endif
                      @if($advertencia > 0)
                        <a onclick="swal('','Existen pagos pendientes fuera de fecha','warning')"  class="btn btn-danger btn-sm"><i class="fa fa-exclamation"></i></a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              @endif
            </tbody>
            <tfoot>
              <tr>
                <th></th>
                <th></th>
                <th></th>
                <th>Total:</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>



      @section('scripts')

        <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>

      <script type="text/javascript">
      function ModalTabla(valor)
      {
        var id = $(this).attr('value');
        var url = '{{route("pago.modal.tabla.pago", ":slug")}}';
        url = url.replace(':slug', valor);
        $('#cont-modal-tabla-pago').html(' ');
        console.log('modal pago');
        $.ajax({
            type: "GET",
            url: url,
            success: function( response ) {
              $('#cont-modal-tabla-pago').html(response);
            }
        });
      }
      </script>
      </script>
      <script>

      $(document).ready(function(){


            var oTable3 = $('#tabla-creditos-hoy').DataTable({
              "language": {
                  "url": "{{asset('fonts/dataTablesEsp.json')}}",
              },
              "paging":   true,
              "ordering": false,
              "info":     false,
              'dom' : 'tip',
              "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i *1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                console.log(total);

                // Update footer
                api.columns('.sum', { page: 'current'}).every( function () {
                  var sum = this
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                  this.footer().innerHTML = parseFloat(sum).toFixed(2);
                } );
              }
            });

            $('#busqueda-creditos-hoy').keyup(function(){
                  oTable3.search($(this).val()).draw();
            });

            $("tbody").on('click', '#pagar-prestamo', function(){
              var valor = $(this).attr("value");
              var url = '{{ route("pagos.info", ":id") }}';
              url = url.replace(':id', valor);
              $.ajax({
                  type: "GET",
                  url: url,
                  success: function( response ) {
                    $('#nom_cliente').val(response['cliente']);
                    $('#cliente').val(response['prestamo']);
                    $('#no_cuota').val(response['no_cuota']);
                    $('#total').val(response['cuota']);
                    $('#prestamo').val(response['prestamo']);
                    if(response['dia_perdon'] == '1'){
                      $('#btn-perdon').hide();
                    } else{
                      $('#btn-perdon').show();
                    }
                    $('#modal-pago').modal('show');
                  }
              });
            });
          });


      $("tbody").on('click', '#pago-vencido', function(){
      var valor = $(this).attr("value");
      var url = '{{ route("info.credito", ":id") }}';
      console.log(valor);
      url = url.replace(':id', valor);
      $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        console.log(response);
        $('#nom_cliente_v').val(response['cliente']);
        $('#prestamo_v').val(response['prestamo']);
        $('#no_prestamo_v').val(response['no_prestamo']);
        $('#capital_v').val(response['capital_vencido']);
        $('#mora_v').val(response['mora']);
        $('#interes_v').val(response['interes']);
        $('#total_v').val(response['cuota']);

      }
      });
      });

          $('tr td').on('click', '#comentario', function(){
            var id = $(this).attr('value');
            $.ajax({
              url: '{{route('pagos.updComentario')}}',
              type: 'GET',
              data: {id: id},
              success: function (data) {
                comentario(data,id);
              }
            })
          })

          function updateComentario(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var contenido = $('#txt-observacion').val();
            var id = $('#txt-observacion').attr('name');
            $.ajax({
                  /* the route pointing to the post function */
                  url: '{{route('pagos.updComentario')}}',
                  type: 'POST',
                  /* send the csrf-token and the input to the controller */
                  data: {_token: CSRF_TOKEN, observacion: contenido, id: id},
                  // dataType: 'JSON',
                  /* remind that 'data' is the response of the AjaxController */
                  success: function (data) {
                    swal("Observacion actualizada", {
                      icon: "success",
                    });
                  }
              });
          }


          function comentario(texto, value){
            swal({
              'title': "Observacion",
              content: {
                element: "input",
                attributes: {
                  placeholder: texto,
                  name: value,
                  id: 'txt-observacion',
                },
              },
              buttons: {
              cancel: 'Cancelar',
              confirm: "Actualizar",
              },
            })
            .then((confirm) => {
              if (confirm) {
                updateComentario();
              } else {
                swal("No se actualizo la observacion");
              }
            });
          }
      </script>

      <script type="text/javascript">
      $('.revertir').click(function(){
        var valor = $(this).attr('value');
        revertir(valor);
      })

      function revertir(valor){
        console.log(valor);
        var url = "{{route('pagos.revertir.hoy')}}";
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        swal({
          title: "Esta seguro de cambiar el estado a pendiente?",
          text: "Se van a revertir los cambios!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
          if (willDelete) {
            $.ajax({
              method: 'GET',
              url: url,
              data: {
                valor : valor,
              },
              success: function( response ) {
                console.log(response);
                console.log('peligro');
                swal("Cambios revertidos, estado a pendiente", {
                  icon: "success",
                });
                 location.reload();
              }
            });
          } else {
            swal("No se cambio el estado",{icon: "warning",});
          }
        });
      }
      </script>


      <script type="text/javascript">
      function refreshPendientes(){
        var url = '{{ route("refresh.pendientes") }}';
        $('#resumen-refresh-pendientes').html(' ');
        $.ajax({
            type: "GET",
            url: url,
            success: function( response ) {
              $('#resumen-refresh-pendientes').html(response);
            }
        });
      }

        $("#botonSiPago").click(function(event)
        {
          event.preventDefault();
          var form = $("#PagoModalPendiente");
          var data = form.serialize();
          var url = "{{route('pagos.store.SIpago')}}";
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

           swal({
             icon: 'info',
             title: '¿Desea realizar éste pago?',
             buttons: [true, "Do it!"],
           }).then((result) => {
               $.ajax({
                 method: 'POST',
                 url: url,
                 data: data,
                 cache: false,
                 success: function( response ) {
                   // console.log(response);
                   swal({
                     icon: 'success',
                     title: 'Pagado!',
                     text: "Se efectuó el pago con éxito!",
                     timer: 1000
                   });
                   // DatatablesExtensionButtons.t.draw('page');
                   $('#PagoModalPendiente').modal('hide');
                   var t = $("#datatable-pagos-pendientes").DataTable();
                   t.ajax.reload( null, false );
                   // var t1 = $("#tabla-creditos-hoy").DataTable();
                   // t1.reload();
                   refreshPendientes();

                 },
                 error: function( response ) {
                   console.log(response);
                   console.log('peligro');
                   swal({
                     icon: 'error',
                     title: 'Lo sentimos...',
                     text: 'Ocurrió un error!',
                     timer: 1500
                   });

                 }
               });

           })
        });


        $("#botonNoPago").click(function(event)
        {
          event.preventDefault();
          var form = $("#PagoModalPendiente");
          var data = form.serialize();
          var url = "{{route('pagos.store.NOpago')}}";
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

           swal({
             icon: 'info',
             title: '¿Desea realizar éste pago?',
             buttons: [true, "Do it!"],
           }).then((result) => {
               $.ajax({
                 method: 'POST',
                 url: url,
                 data: data,
                 cache: false,
                 success: function( response ) {
                   // console.log(response);
                   swal({
                     icon: 'success',
                     title: 'Pagado!',
                     text: "Se efectuó el pago con éxito!",
                     timer: 1000
                   });
                   // DatatablesExtensionButtons.t.draw('page');
                   $("#PagoModalPendiente").find('form')[0].reset();
                   $('#PagoModalPendiente').modal('hide');
                   var t = $("#datatable-pagos-pendientes").DataTable();
                   t.ajax.reload( null, false );
                   // var t1 = $("#tabla-creditos-hoy").DataTable();
                   // t1.reload();
                   refreshPendientes();


                 },
                 error: function( response ) {
                   console.log(response);
                   console.log('peligro');
                   swal({
                     icon: 'error',
                     title: 'Lo sentimos...',
                     text: 'Ocurrió un error!',
                     timer: 1500
                   });

                 }
               });

           })
        });
      </script>



      @endsection
