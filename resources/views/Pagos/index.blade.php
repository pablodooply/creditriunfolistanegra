@extends('layouts.app')

@section('title', 'Pagos')

@section('content')

@section('nombre','Pagos')
@section('ruta')
  <li class="active">
      <strong>Historial de pagos</strong>
  </li>
@endsection
    <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                      <div class="ibox float-e-margins">
                        <div class="ibox-title">
                          <h5>Historial de pagos</h5>
                        </div>
                        <div class="ibox-content">
                          <input type="text" placeholder="Cliente: Nombre, Prestamo: Cre-123, Plan: Diario" class="input form-control" id="busqueda-pagos">
                          <ul class="nav">
                            <p><span class="pull-left text-muted">{{count($pagos)}} Pagos</span></p>
                          </ul>
                          <div class="table-responsive">
                            <table class='table table-striped table-hover' id="tabla-pagos">
                              <thead>
                                <tr>
                                  <th>DPI</th>
                                  <th>Código</th>
                                  <th>Cliente</th>
                                  <th>Prestamo</th>
                                  <th>Promotor</th>
                                  <th>Ruta</th>
                                  <th>Plan</th>
                                  <th class="sum">Monto</th>
                                  <th>Fecha</th>
                                </tr>
                              </thead>
                              <tbody>
                                @if($pagos->isEmpty())
                                  <h1> No hay creditos<h1>
                                @else
                                  @foreach ($pagos->where('prestamo_id', '!=', null) as $pago)
                                  <tr>
                                    <td>{{$pago->prestamo->cliente->persona->dpi}}</td>
                                    <td>Cli-{{$pago->prestamo->cliente->id}}</td>
                                    <td><a class="client-link" href="{{route('clientes.show', $pago->prestamo->cliente->id)}}">{{$pago->prestamo->cliente->persona->nombre}} {{$pago->prestamo->cliente->persona->apellido}}</a></td>
                                    <td><a class="client-link" href="{{route('creditos.show', $pago->prestamo->id)}}">Cre-{{$pago->prestamo->id}}<a/></td>
                                    <td>{{$pago->prestamo->ruta->first()->hoja_ruta->user->persona->nombre}}</td>
                                    <td><span class="text-success">{{$pago->prestamo->ruta->first()->hoja_ruta->nombre}}</span></td>
                                    <td>{{$pago->prestamo->plan->nombre}}</td>
                                    <td>{{$pago->monto}}</td>
                                    <td>{{$pago->created_at}}</td>
                                  </tr>
                                  @endforeach
                                @endif
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th>Total:</th>
                                  <th></th>
                                  <th></th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
@endsection

@section('scripts')

<script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function(){

        var oTable = $('#tabla-pagos').DataTable({
          "paging":   true,
          "info":     false,
          "order": [[ 7, "desc" ]],
          'dom' : 'tip',
          dom: '<"html5buttons" B>tip',
          buttons: [
              {extend: 'excel',
              title: 'Pagos',
              footer: true,
              },
              {extend: 'pdf',
              title: 'Pagos',
            }],
          "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i *1 :
                typeof i === 'number' ?
                    i : 0;
        };

      // Total over all pages
      total = api
          .column( 5 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );

          // Update footer
          $( api.column( 5 ).footer() ).html(
              total
          );
    }
  });

        $('#busqueda-pagos').keyup(function(){
              oTable.search($(this).val()).draw();
        });
});
</script>

@endsection
