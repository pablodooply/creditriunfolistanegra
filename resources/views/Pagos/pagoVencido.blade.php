<div class="row">
  <div class="modal fade bd-example-modal-lg pago-vencido" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5> Pago </h5>
                </div>
                <div class="ibox-content">
                  {!! Form::open(['method' => 'POST', "route" => "pagos.vencidos"]) !!}
                    <div class="form-row col-sm-12">
                      <div class="form-group col-sm-4">
                          {{ Form::label('name', 'Cliente') }}
                          {{ Form::text('nom_cliente',null , ['class' => 'form-control valor', 'id' => 'nom_cliente_v', "readonly"]) }}
                          {{ Form::hidden('prestamo_v', null, ['id' => 'prestamo_v']) }}
                      </div>
                      <div class="form-group col-sm-4">
                        {{ Form::label('name', 'No. Prestamo') }}
                        {{ Form::text('no_prestamo_v', null, ['class' => 'form-control valor', 'id' => 'no_prestamo_v', "readonly" ]) }}
                      </div>
                      <div class="form-group col-sm-4">
                          {{ Form::label('name', 'Capital Vencido') }}
                          {{ Form::text('capital_v',null, ['class' => 'form-control valor', 'id' => 'capital_v', "readonly" ]) }}
                      </div>
                    </div>
                    <div class="form-row col-sm-12">
                      <div class="form-group col-sm-4">
                        {{ Form::label('name', 'Mora') }}
                        {{ Form::text('mora_v',null, ['class' => 'form-control valor', 'id' => 'mora_v' , "readonly"]) }}
                      </div>
                      <div class="form-group col-sm-4">
                        {{ Form::label('name', 'Interes') }}
                        {{ Form::text('interes_v',null, ['class' => 'form-control valor', 'id' => 'interes_v' , "readonly"]) }}
                      </div>
                      <div class="form-group col-sm-4">
                        {{ Form::label('name', 'Total') }}
                        {{ Form::text('total_v','', ['class' => 'form-control valor', 'id' => 'total_v' ,  "readonly"]) }}
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          {{ Form::submit('Pago', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton']) }}
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                      </div>
                    </div>
                {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
