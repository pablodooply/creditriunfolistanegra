@extends('layouts.app')

@section('title', 'Perfil cliente')

@section("link")
<link href="{{asset('css/rangedatepicker/daterangepicker.css')}}" rel="stylesheet" media="screen">
@endsection

@section('content')

@section('nombre','Grafica')
@section('ruta')
<li class="active">
  <strong>Grafica</strong>
</li>
@endsection
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Grafica</h5>
        </div>
        <div class="ibox-content">
          <div class="row">
            <div class="col-md-6">
              <label>Rango:</label>
              <div class="input-group">
                <div class="form-group">
                  <input id="rangedate" class="form-control" type="text" name="" value="">
                </div>
                <span class="input-group-btn">
                  <a id="busqueda" class="btn btn-primary"> <i class="fa fa-bar-chart-o"></i> Graficar</a>
                </span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div id="container"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{!! asset('js/slimscroll/jquery.slimscroll.min.js') !!}"></script>

{{-- <script src="{!! asset('js/graph/highcharts.js')!!}"></script> --}}
{{-- <script src="{!! asset('js/graph/series-label.js')!!}"></script>
<script src="{!! asset('js/graph/exporting.js')!!}"></script> --}}

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script type="text/javascript" src="{!! asset('js/grafica.js')!!}"></script>
<script type="text/javascript" src="{{asset('js/moment/moment.js')}}"></script>
<script type="text/javascript" src="{{asset('js/rangedatepicker/daterangepicker.js')}}"></script>

<script type="text/javascript">
  var variable;
  $(document).ready(function() {


    $("#rangedate").daterangepicker({
      "locale": {
        "format": "DD/MM/YYYY",
        "fromLabel": "Desde",
        "toLabel": "Hasta",
        "applyLabel": "Aplicar",
        "cancelLabel": "Cancelar",
        "daysOfWeek": [
          "Dom",
          "Lun",
          "Mar",
          "Mie",
          "Jue",
          "Vie",
          "Sab"
        ],
        "monthNames": [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
        ],
      }
    });


    $("#busqueda").click(function() {
      console.log('entro');
      graficar();
    });

    function graficar() {
      // cargar_grafica_line();
      var rango_fechas = $("#rangedate").val();
      var valores = rango_fechas.split(' - ');
      var inicio = valores[0];
      var fin = valores[1];
      var url = "{{url('api/datosGrafica')}}";
      $.ajax({
        type: "GET",
        url: url,
        data: {
          'inicio': inicio,
          'fin': fin,
        },
        success: function(response) {
          // console.log(response);
          variable = response;
          cargar_grafica_line(response);
        }
      });
    }
    // graficar();
  });
</script>


@endsection
