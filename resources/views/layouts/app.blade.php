<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @yield('meta')

    <title> @yield('title') </title>





    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />

    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />





    @yield('link')

</head>

<body>



  <!-- Wrapper-->

    <div id="wrapper">



        <!-- Navigation -->

        @include('layouts.navigation')



        <!-- Page wraper -->

        <div id="page-wrapper" class="gray-bg">



            <!-- Page wrapper -->

            @include('layouts.topnavbar')



            @include('layouts.ruta')

            <!-- Main view  -->

            @yield('content')



            <!-- Footer -->

            @include('layouts.footer')



        </div>

        <!-- End page wrapper-->



    </div>

    <!-- End wrapper-->



<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>



<script type="text/javascript">

    function actualizarNotificacion(){

        var url='{{url('api/notificacion/num')}}';

        $.ajax({

            type: "GET",

            url: url,

            success: function( response ) {

              $("#actualizar").text(response);

            }

        });

      }

      actualizarNotificacion();



      function actualizarNotificacion_valoracion(){
          var url='{{url('api/notificacion_valoracion/num')}}';
          $.ajax({
              type: "GET",
              url: url,
              success: function( response ) {
                $("#actualizar_valoracion").text(response);
              }
          });
        }
        actualizarNotificacion_valoracion();

        function actualizar_proximos(){
            var url='{{url('api/notificacion/proximo')}}';
            $.ajax({
                type: "GET",
                url: url,
                success: function( response ) {
                  $("#proximo").text(response);
                }
            });
          }
          actualizar_proximos();


          function actualizar_mora(){
              var url='{{url('api/notificacion/mora')}}';
              $.ajax({
                  type: "GET",
                  url: url,
                  success: function( response ) {
                    $("#mora").text(response);
                  }
              });
            }
            actualizar_mora();

</script>


<script src="js/plugins/pace/pace.min.js"></script>
@section('scripts')



@show



</body>

</html>

