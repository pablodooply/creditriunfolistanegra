<li class="{{ isActiveRoute('home') }}">
    <a href="{{route('home')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Principal</span></a>
</li>
<li class="{{ isActiveRoute('clientes.index') . isActiveRoute('clientes.cumple') }}">
    <a><i class="fa fa-address-book-o"></i>
      <span class="nav-label">Clientes</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="{{ isActiveRoute('clientes.index')}}"><a href="{{route('clientes.index')}}">Listado de clientes</a></li>
        <li class="{{ isActiveRoute('clientes.cumple')}}"><a href="{{route('clientes.cumple')}}">Cumpleaņeros</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('creditos.index') . isActiveRoute('creditos.finalizar') }}">
    <a><i class="fa fa-folder"></i>
      <span class="nav-label">Creditos</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
      <li class="{{isActiveRoute('creditos.create')}}"><a href="{{route('creditos.create')}}">Nueva solicitud</a></li>
      <li class="{{ isActiveRoute('creditos.index')}}"><a href="{{route('creditos.index')}}">Listado de creditos</a></li>
      <!--<li><a href="{{route('creditos.entregados')}}">Listado de entregables</a></li>-->
      <li class="{{ isActiveRoute('creditos.finalizar')}}"><a href="{{route('creditos.finalizar')}}">Creditos a finalizar</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('prospecto.index') }}">
    <a href="{{ route('prospecto.index') }}"><i class="fa fa-archive"></i> <span class="nav-label">Prospectos</span> </a>
</li>
<li class="{{ isActiveRoute('geo.promotor') }}">
    <a href="{{ route('geo.promotor') }}"><i class="fa fa-map-marker"></i> <span class="nav-label">Geolocalizacion</span> </a>
</li>
{{-- <li class="{{ isActiveRoute('') }}">
    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Graficas</span> </a>
</li> --}}
<li class="{{ isActiveRoute('graficas.index') . isActiveRoute('graficas.promotor') . isActiveRoute('graficas.ruta') }}">
  <a><i class="fa fa-bar-chart-o"></i>
    <span class="nav-label">Graficas</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li class="{{ isActiveRoute('graficas.promotor')}}"><a href="{{ route('graficas.promotor')}}">Promotores</a></li>
      <li class="{{ isActiveRoute('graficas.ruta')}}"><a href="{{ route('graficas.ruta')}}">Ruta</a></li>
  </ul>
</li>
<li class="{{ isActiveRoute('colaborador.perfil') }}">
    <a href="{{route('colaborador.perfil')}}"><i class="fa fa-user"></i> <span class="nav-label">Perfil</span></a>
</li>
