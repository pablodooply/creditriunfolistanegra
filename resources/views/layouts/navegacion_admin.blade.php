<li class="{{ isActiveRoute('home') }}">
    <a href="{{route('home')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Principal</span></a>
</li>
<li class="{{ isActiveRoute('usuarios.create') . isActiveRoute('usuarios.index') . isActiveRoute('usuarios.show') . isActiveRoute('usuarios.posicionesView')}}">
    <a ><i class="fa fa-address-book"></i>
       <span class="nav-label">Colaboradores</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="{{isActiveRoute('usuarios.create')}}"><a href="{{route('usuarios.create')}}">Registrar colaborador</a></li>
        <li class="{{isActiveRoute('usuarios.index')}}"><a href="{{route('usuarios.index')}}">Listado de colaboradores</a></li>
        <li class="{{isActiveRoute('usuarios.posicionesView')}}"><a href="{{route('usuarios.posicionesView')}}">Posiciones</a></li>

    </ul>
</li>
<li class="{{ isActiveRoute('clientes.index') . isActiveRoute('clientes.cumple') }}">
    <a><i class="fa fa-address-book-o"></i>
      <span class="nav-label">Clientes</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="{{isActiveRoute('clientes.index')}}"><a href="{{route('clientes.index')}}">Listado de clientes</a></li>
        <li class="{{isActiveRoute('clientes.cumple')}}"><a href="{{route('clientes.cumple')}}">Cumpleañeros</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('creditos.create') . isActiveRoute('creditos.index') . isActiveRoute('creditos.entregados')
             . isActiveRoute('creditos.finalizar') . isActiveRoute('creditos.fecha') . isActiveRoute('creditos.planes')}}">
    <a><i class="fa fa-folder"></i>
      <span class="nav-label">Créditos</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="{{isActiveRoute('creditos.create')}}"><a href="{{route('creditos.create')}}">Nueva solicitud</a></li>
        <li class="{{isActiveRoute('creditos.index')}}"><a href="{{route('creditos.index')}}">Listado de créditos</a></li>
        <li class="{{isActiveRoute('creditos.entregados')}}"><a href="{{route('creditos.entregados')}}">Créditos a entregar</a></li>
        <li class="{{isActiveRoute('creditos.finalizar')}}"><a href="{{route('creditos.finalizar')}}">Créditos a finalizar</a></li>
        <li class="{{isActiveRoute('creditos.fecha')}}"><a href="{{route('creditos.fecha')}}">Fechas de descanso</a></li>
        <li class="{{isActiveRoute('creditos.planes')}}"><a href="{{route('creditos.planes')}}">Planes</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('pagos.create') . isActiveRoute('pagos.index') . isActiveRoute('prospecto.index') }}">
  <a><i class="fa fa-edit"></i>
    <span class="nav-label">Pagos</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li class="{{ isActiveRoute('pagos.create')}}"><a href="{{ route('pagos.create') }}">Efectuar pago</a></li>
      <li class="{{ isActiveRoute('pagos.index')}}"><a href="{{ route('pagos.index')}}">Historial de pagos</a></li>
      <li class="{{ isActiveRoute('prospecto.index')}}"><a href="{{ route('prospecto.index')}}">Prospecto</a></li>
  </ul>
</li>

<li class="{{ isActiveRoute('garantia.index') . isActiveRoute('categoria.index') }}">
  <a><i class="fa fa-database"></i>
    <span class="nav-label">Garantías</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li class="{{ isActiveRoute('garantia.index')}}"><a href="{{ route('garantia.index') }}">Inventario</a></li>
      <li class="{{ isActiveRoute('categoria.index')}}"><a href="{{ route('categoria.index')}}">Categorías</a></li>
  </ul>
</li>

<li class="{{ isActiveRoute('rutas.index') . isActiveRoute('rutas.create')  }}">
  <a><i class="fa fa-arrows"></i>
    <span class="nav-label">Rutas</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li class="{{ isActiveRoute('rutas.index')}}"><a href="{{ route('rutas.index') }}">Listado de rutas</a></li>
      <li class="{{ isActiveRoute('rutas.create')}}"><a href="{{ route('rutas.create')}}">Nueva ruta</a></li>
  </ul>
</li>

<li class="{{ isActiveRoute('gastos.index') . isActiveRoute('ingreso.index') . isActiveRoute('capital.secundario.index')}}">
    <a><i class="fa fa-external-link"></i>
      <span class="nav-label">Extras</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="{{isActiveRoute('ingreso.index')}}"><a href="{{route('ingreso.index')}}">Ingresos</a></li>
        <li class="{{isActiveRoute('asueto.index')}}"><a href="{{route('asueto.index')}}">Fecha de Asueto</a></li>
        <li class="{{isActiveRoute('gastos.index')}}"><a href="{{route('gastos.index')}}">Gastos</a></li>
        <li class="{{isActiveRoute('capital.secundario.index')}}"><a href="{{route('capital.secundario.index')}}">Capital secundario</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('reportes.index') }}">
  <a><i class="fa fa-file-text"></i>
    <span class="nav-label">Reportes</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li class="{{ (isActiveRoute('reportes.index',1) == 1) ? 'active' : ' '}}"><a href="{{ route('reportes.index', 1) }}">General</a></li>
      <li class="{{ (isActiveRoute('reportes.index',2) == 2) ? 'active' : ' '}}"><a href="{{ route('reportes.index', 2) }}">Clientes</a></li>
      <li class="{{ (isActiveRoute('reportes.index',3) == 3) ? 'active' : ' '}}"><a href="{{ route('reportes.index', 3) }}">Promotores</a></li>
      <li class="{{ (isActiveRoute('reportes.index',4) == 4) ? 'active' : ' '}}"><a href="{{ route('reportes.index', 4) }}">Moras</a></li>
      <li class="{{ (isActiveRoute('reportes.index',5) == 5) ? 'active' : ' '}}"><a href="{{ route('reportes.index', 5) }}">Clasificación</a></li>
      <li class="{{ (isActiveRoute('reportes.index',6) == 6) ? 'active' : ' '}}"><a href="{{ route('reportes.index', 6) }}">Ruta</a></li>
  </ul>
</li>
<li class="{{ isActiveRoute('geolocalizacion.index') . isActiveRoute('geolocalizacion.ruta')  }}">
  <a><i class="fa fa-map-marker"></i>
    <span class="nav-label">Geolocalización</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li class="{{isActiveRoute('geolocalizacion.index')}}"><a href="{{ route('geolocalizacion.index') }}">Localizar promotor</a></li>
      <li class="{{isActiveRoute('geolocalizacion.ruta')}}"><a href="{{ route('geolocalizacion.ruta') }}">Ruta recorrida</a></li>
  </ul>
</li>
<li class="{{ isActiveRoute('graficas.index') . isActiveRoute('graficas.promotor') . isActiveRoute('graficas.ruta') }}">
  <a><i class="fa fa-bar-chart-o"></i>
    <span class="nav-label">Graficas</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li class="{{isActiveRoute('graficas.index')}}"><a href="{{ route('graficas.index') }}">General</a></li>
      <li class="{{isActiveRoute('graficas.promotor')}}"><a href="{{ route('graficas.promotor')}}">Promotores</a></li>
      <li class="{{isActiveRoute('graficas.ruta')}}"><a href="{{ route('graficas.ruta')}}">Ruta</a></li>
  </ul>
</li>
<li class="{{ isActiveRoute('colaborador.perfil') }}">
    <a href="{{route('colaborador.perfil')}}"><i class="fa fa-user"></i> <span class="nav-label">Perfil</span></a>
</li>
