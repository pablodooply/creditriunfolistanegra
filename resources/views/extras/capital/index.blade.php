@extends('layouts.app')
@section('title', 'Capital secundario')
@section('link')
  <link rel="stylesheet" href="{{asset('css/datatables/datatables.min.css')}}">
@endsection
@section('content')
@section('nombre','Gastos')

@section('ruta')
<li class="active">
  <strong>Capital secundario</strong>
</li>
@endsection

@php
    $fecha = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
    $fecha2 = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
@endphp
<div class="panel-body">
  <div class="ibox ">
    <div class="ibox-title">
      <h5>Capital secundario</h5>

    </div>
    <div class="ibox-content">
      <div class="row">
        <form method="POST" id="search-form-total">
          <div class="input-daterange form-row" data-plugin="datepicker" data-option="{}" id="datepicker">
            <div class="form-group col-md-12" id="data_1">
                <div class="form-group col-md-6">
                  {{ Form::label('name', 'Desde' ) }} <input type="date" id="fecha1" class="form-control" name="fecha1" value="{{$fecha}}">
                </div>
                <div class="form-group col-md-6">
                  {{ Form::label('name', 'Hasta ' ) }}<input type="date" id="fecha2" class="form-control" name="fecha2" value="{{$fecha2}}">
                </div>
            </div>
          </div>
        </form>
      </div>
      <div class="row">
        <div class="table-responsive">
          <table id="tabla_capital_secundario" class="table table-striped table-bordered table-hover dataTables-example text-center">
            <thead>
              <tr>
                <th>Capital</th>
                <th>Total Ingresos</th>
                <th>Total Gastos</th>
              </tr>
            </thead>
            <tfoot>
                <tr>
                  <th>Capital</th>
                  <th>Total Ingresos</th>
                  <th>Total Gastos</th>
                </tr>
            </tfoot>
            <tbody>

            </tbody>

          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
  <script type="text/javascript">
  $(document).ready(function(){


     $('#search-form-total').on('change', function(e) {
       var a = $('input[name=fecha1]').val();
       var b = $('input[name=fecha2]').val();
       rGeneral.draw();

     });

    var rGeneral=$('#tabla_capital_secundario').DataTable({
      order: [[ 0, "asc" ]],
      language: {
          "url": "{{asset('fonts/dataTablesEsp.json')}}",
      },
      paging: true,
      info: false,
      dom : 'tip',
      processing: true,
      serverSide: true,
      ajax: {
        "url": '{{route('capital.secundario.api')}}',
        'beforeSend': function (request) {
         request.setRequestHeader("X-CSRF-TOKEN", '{{ csrf_token() }}');
         },
         data: function(d) {


         d.fecha1 = $('input[name=fecha1]').val();
         d.fecha2 = $('input[name=fecha2]').val();
         }
      },
      pageLength: 10,
      responsive: true,
      dom: '<"html5buttons"B>lTfgitp',
      buttons: [
        {extend: 'copy'},
        {extend: 'csv'},
        {extend: 'excel', title: 'Cuadre'},
        {extend: 'pdf', title: 'Cuadre'},
        {extend: 'print',
           customize: function (win)
           {
              $(win.document.body).addClass('white-bg');
              $(win.document.body).css('font-size', '10px');

              $(win.document.body).find('table')
                      .addClass('compact')
                      .css('font-size', 'inherit');
          }
        }
      ],
      columns: [
          {data: 'Capital', name: 'Capital'},
          {data: 'Ingreso', name: 'Ingreso'},
          {data: 'Gasto', name: 'Gasto'},
      ],
    //   "footerCallback": function( tfoot, data, start, end, display ) {
    //   var api = this.api();
    //   $( api.column( 5 ).footer() ).html(
    //       api.column( 5 ).data().reduce( function ( a, b ) {
    //           return a + b;
    //       }, 0 )
    //   );
    // }
    });
  });
  </script>
@endsection
