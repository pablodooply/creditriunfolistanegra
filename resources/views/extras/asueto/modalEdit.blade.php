
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title"> <strong>Creación de Fecha de Asueto</strong> </h4>
    </div>
    {!! Form::open(["id"=>"form-asueto-edit",'files' => true, 'enctype'=>"multipart/form-data" , 'data-parsley-validate'=> "data-parsley-validate" ]) !!}
    <div class="modal-body">
      <div class="row">

            <div class="col-lg-12">
              <div id="alertaso" role="alert"  style="display:none" class="alert alert-danger alert-dismissible"></div>
                  
                  <div class="form-group col-sm-12">
                    {{ Form::label('name', 'Titulo') }}
                    {{ Form::text('titulo', $asueto->titulo, ['class' => 'form-control', 'id' => 'nombre']) }}
                  </div>
                  <div class="form-group col-sm-6">
                    {{ Form::label('name', 'Fecha Inicio:') }}
                    {{ Form::date('fecha_inicio', Carbon\Carbon::parse($asueto->fecha_inicio), ['class' => 'form-control', 'id' => 'nombre']) }}
                  </div>
                 
                  <div class="form-group col-sm-6">
                    {{ Form::label('name', 'Fecha Fin:') }}
                    {{ Form::date('fecha_fin', Carbon\Carbon::parse($asueto->fecha_fin), ['class' => 'form-control', 'id' => 'nombre']) }}
                
                  </div>
                  <div class="form-group col-sm-12">
                    {{ Form::label('name', 'Descripción') }}
                    {{ Form::text('descripcion', $asueto->descripcion, ['class' => 'form-control', 'id' => 'nombre']) }}
                  </div>
               
            
                  <br>
                
                  
                  
                </div>
              </div>
              
              
              
            </div>
            
            
            <div class="modal-footer">
              {{ Form::submit('Registrar', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton', 'value' => 'pago' ,'id'=>'boton-asueto-editar']) }}
              
              <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            </div>
            {{Form::close()}}


    <script>


      $("#form-asueto-edit").submit(function (event) {
  
          $("#boton-asueto-editar").attr('disabled', 'disabled').html("<i class='glyphicon glyphicon-floppy-disk'></i> Ingresando...");
          event.preventDefault();
          $("#updateForm input").css("pointer-events", "none");
          $('.loading').show();
          var form = $(this);
          var data = form.serialize();
          var url = form.attr("action");;
          $.ajax({
              type: "POST",
              url: url,
              data: new FormData($("#form-asueto-edit")[0]),
              cache: false,
              contentType: false,
              processData: false,  
              success: function (data) {
                if(data.errors)
                {
                  jQuery('.alert-danger').html('');
  
                  jQuery.each(data.errors, function(key, value){
                    jQuery('.alert-danger').show();
                    jQuery('.alert-danger').append('<li> '+value+'</li>');
                  });
                } else {
                      $(".has-error").removeClass("has-error");
                      $(".help-block").empty();
                      var oTablepr = $("#tabla-asueto").DataTable();
                        oTablepr.draw();
  
                      $("#asueto-modal-creacion").find('form')[0].reset();
                      $('#asueto-modal-creacion').modal('hide');
                      swal("Fecha de asueto","La fecha de asueto ha sido editada correctamente","success");
                   
  
  
                  }
                  $('.loading').hide();
                  $("#boton-asueto-editar").removeAttr('disabled').html("<i class='glyphicon glyphicon-floppy-disk'></i> Guardar");
                  $("#form-asueto-edit input").css("pointer-events", "");
              },
              error: function (xhr, textStatus, errorThrown) {
                  alert(textStatus);
                  $("#boton-asueto-editar").removeAttr('disabled').html("<i class='glyphicon glyphicon-floppy-disk'></i> Guardar");
                  $("#form-asueto-edit input").css("pointer-events", "");
              }
          });
          return false;
  
  
  
      });
  </script>