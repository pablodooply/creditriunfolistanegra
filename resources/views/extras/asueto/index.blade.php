@extends('layouts.app')

@section('title', 'Fechas de Asueto')

@section('link')

@endsection

@section('content')

@section('nombre','Fechas de Asueto')
@section('ruta')
  <li class="active">
      <strong>Listado de Fechas de Asueto</strong>
  </li>
@endsection

<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
      <div class="col-sm-12">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <h5>Listado de Fechas</h5>

          </div>


          <div class="ibox-content">
            <a type="button" class="btn btn btn-primary" onclick="create()" data-toggle="modal" data-target="#asueto-modal-creacion">  Nueva Fecha</a>
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#activos">Fechas</a></li>

            </ul>
            <div class="tab-content">
              <div id="activos" class="tab-pane active">
                <div class="input-group">
                  <input type="text" placeholder="Codigo: Emp-123, Nombre: Nombre" class="input form-control" id="busqueda">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                  </span>
                  <div id="aqui">

                  </div>
                </div>
                <ul class="nav">
                </ul>

                    <div class="table-responsive">
                      <table id="tabla-asueto" class="table table-striped table-bordered table-hover dataTables-example text-center">
                        <thead>
                          <tr>
                            <th>Código</th>
                            <th>Titulo</th>
                            <th>Fecha inicio</th>
                            <th>Fecha fin</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th>Código</th>
                            <th>Titulo</th>
                            <th>Fecha inicio</th>
                            <th>Fecha fin</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>

<div class="modal inmodal fade" id="asueto-modal-creacion" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div id="asueto-modal-create">
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')

  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
  @include('sweet::alert')
  <script>
       function create()
  {

    var url = '{{route("asueto.create")}}';
    $('#asueto-modal-create').html(' ');
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $('#asueto-modal-create').html(response);
        }
    });
  }

  function editar(id)
  {

    var url = '{{route("asueto.editar", ":slug")}}';
    url = url.replace(':slug', id);
    $('#asueto-modal-create').html(' ');
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $('#asueto-modal-create').html(response);
        }
    });
  }

  function eliminar(id)
  {

    var url = '{{ route("asueto.eliminar", ":slug") }}';
    url = url.replace(':slug', id);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    swal({
     icon: "warning",
     title: '¿Desea eliminar?',
     text: "Si se eliminar esta fecha de asueto, ya no aparecerá en el sistema.",
     buttons: true,
     dangerMode: true,
    }).then((result) => {
     if (result) {
       $.ajax({
         method: 'DELETE',
         url: url,
         data: {
           valor : id,
           _token: CSRF_TOKEN,
         },
         success: function( response ) {
           console.log(response);
           swal({
             icon: 'success',
             title: 'Eliminada!',
             text: "Se eliminó ésta fecha de asueto con éxito!",
             timer: 1000
           });
           // DatatablesExtensionButtons.t.draw('page');
           var oTablepr = $("#tabla-asueto").DataTable();
           oTablepr.draw();


         },
         error: function( response ) {
           console.log(response);
           console.log('peligro');
           swal({
             icon: 'error',
             title: 'Lo sentimos...',
             text: 'Ocurrió un error!',
             timer: 1500
           });

         }
       });
     }
     else {
       swal('Cancelado',
        'Ésta categoría no se eliminará.',
        'info');
     }
    })
  }
  </script>
<script type="text/javascript">
    $(document).ready(function(){
      $('#tabla-asueto').DataTable({
        order: [[ 0, "desc" ]],
        language: {
            "url": "{{asset('fonts/dataTablesEsp.json')}}",
        },
        paging: true,
        info: false,
        dom : 'tip',
        processing: true,
        serverSide: true,
        ajax: {
          "url": '{{route('api.asueto')}}'
        },
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTgitp',
        buttons: [
          {extend: 'copy'},
          {extend: 'csv'},
          {extend: 'excel', title: 'Asueto'},
          {extend: 'pdf', title: 'Asueto'},
          {extend: 'print',
             customize: function (win)
             {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');

                $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
            }
          }
        ],
        columns: [
            {data: 'codigo', name: 'codigo'},
            {data: 'titulo', name: 'Titulo'},
            {data: 'fecha_inicio', name: 'fecha_inicio'},
            {data: 'fecha_fin', name: 'fecha_fin'},
            {data: 'descripcion', name: 'descripcion'},
            {data: 'acciones', name: 'acciones'},

        ]
      });
    });
    </script>

@endsection
