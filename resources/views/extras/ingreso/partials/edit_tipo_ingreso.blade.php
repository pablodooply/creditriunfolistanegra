<div class="row">
  <div id="modal_tipo_edit" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Editar tipo de ingreso</h5>
                </div>
                <div class="ibox-content">
                  {!! Form::open(array('route' => 'tipo.ingreso.update', 'method' => 'POST', 'id' => 'tipo_ingreso')) !!}
                  <input type="hidden" name="tipo_ingreso_id" id="tipo_ingreso_id" value="">
                    <div class="form-group">
                      {{ Form::label('name', 'Tipo de ingreso') }}
                      {{ Form::text('nombretipo', null, ['class' => 'form-control', 'id' => 'nombretipo']) }}
                    </div>
                    <div class="form-group">
                      {{ Form::label('name', 'Descripcion') }}
                      {{ Form::text('descripcion_tipo', null, ['class' => 'form-control', 'id' => 'descripcion_tipo']) }}
                    </div>
                  <br>
                  <div class="row">
                    <div class="col-sm-4" align="center">
                    </div>
                    <div class="col-sm-4" align="center">
                      <br>
                      {{-- <a class="btn btn-primary" style="text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ingresar gastos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> --}}
                      <input type="submit" class="btn btn-primary" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Editar tipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                    </div>
                  </div>

                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
