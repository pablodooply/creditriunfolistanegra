<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4 class="modal-title">Tipo de Ingreso</h4>
  {{-- <small class="font-bold"><a href="/empleado/perfil/{{$usuario->id}}">(EMP-{{str_pad($usuario->id,6,"0",STR_PAD_LEFT)}})</a> {{$usuario->persona->nombre}} {{$usuario->persona->apellido}}</small> --}}
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
        <div class="panel-body">
          {{-- {!! Form::model($categoria, ['route' => ['categoria.modal.create', $categoria->id],  'method' => 'POST', 'id' => 'form-create-categoria']) !!} --}}
          {!! Form::open(array('route' => 'tipo_ingreso.modal.create', 'method' => 'POST', 'id' => 'tipo_ingreso')) !!}
            <div class="form-group">
              {{ Form::label('name', 'Tipo de ingreso') }}
              {{ Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombrecat']) }}
            </div>
            <div class="form-group">
              {{ Form::label('name', 'Descripcion') }}
              {{ Form::text('descripcion', null, ['class' => 'form-control', 'id' => 'descripcion']) }}
            </div>
            <div class="row">
              <div class="col-md-6">
              {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
              {!! Form::close() !!}
              </div>
              <div class="col-md-6">
              </div>
            </div>
        </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

</div>

<script type="text/javascript">
$("#tipo_ingreso").submit(function(event)
{
  console.log('entro al form')
  event.preventDefault();
  var form = $(this);
  var data = form.serialize();
  var url = form.attr("action");
  console.log(url);

  $.ajax({
   type: "POST",
   url: url,
   data: data,
   cache: false,
   success: function(data) {
     console.log('entro al success');
     if (data.errors) {
     } else {
       $('#modal_tipo_gasto').modal('hide');
       // var t = $("#tabla-categorias").DataTable();
       // t.ajax.reload( null, false );
       swal({
         type: 'success',
         title: 'Creado',
         text: 'Se creó un tipo de gasto',
         timer: 1200
       })
       swal("Creado", "Se creó un tipo de gasto", "success")
     }

   },
   error: function(xhr, textStatus, errorThrown) {
   }
 })
});
</script>
