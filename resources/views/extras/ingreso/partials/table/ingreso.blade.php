<div class="table-responsive">
  <table id="tabla_ingresos" class="table table-striped table-bordered table-hover dataTables-example text-center">
    <thead>
      <tr>
        <th>Codigo</th>
        <th>Monto</th>
        <th>Descripcion</th>
        <th>Tipo Ingreso</th>
        <th>Proveedor</th>
        <th>Cantidad</th>
        <th>Creado por</th>
        <th>Fecha de creacion</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>

    </tbody>
    <tfoot>
      <tr>
        <th>Codigo</th>
        <th>Monto</th>
        <th>Descripcion</th>
        <th>Tipo Ingreso</th>
        <th>Proveedor</th>
        <th>Cantidad</th>
        <th>Creado por</th>
        <th>Fecha de creacion</th>
        <th>Acciones</th>
      </tr>
    </tfoot>
  </table>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#tabla_ingresos').DataTable({
    order: [[ 0, "desc" ]],
    language: {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    paging: true,
    info: false,
    dom : 'tip',
    processing: true,
    serverSide: true,
    ajax: {
      "url": '{{route('ingresos.api')}}'
    },
    pageLength: 10,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
      {extend: 'copy'},
      {extend: 'csv'},
      {extend: 'excel', title: 'Creditos'},
      {extend: 'pdf', title: 'Creditos'},
      {extend: 'print',
         customize: function (win)
         {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');

            $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
        }
      }
    ],
    columns: [
        {data: 'id', name: 'id'},
        {data: 'monto', name: 'monto'},
        {data: 'descripcion', name: 'descripcion'},
        {data: 'tipo_ingreso_id', name: 'tipo_ingreso_id'},
        {data: 'proveedor', name: 'proveedor'},
        {data: 'cantidad', name: 'cantidad'},
        {data: 'created_by', name: 'created_by'},
        {data: 'created_at', name: 'created_at'},
        {data: 'acciones', name: 'acciones'},
    ]
  });
});


$("tbody").on('click', '#edit_ingreso', function(){
  var valor = $(this).attr("value");
  console.log(valor);
  var url = '{{ route("ingreso.edit", ":id") }}';
  url = url.replace(':id', valor);
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#ingreso_id').val(response['ingreso_id']);
        $("div.selectChampetIngreso select").val(response['tipo']);
        $('#monto_ingreso').val(response['total']);
        $('#descripcion_ingreso').val(response['descripcion']);
        $('#cantidad_ingreso').val(response['cantidad']);
        $('#proveedor').val(response['proveedor']);

        $('#edit_ingreso').modal('show');
      }
  });
});

$("tbody").on('click', '#foto_ingreso', function(){
  var valor = $(this).attr("value");
  console.log(valor);
  var url = '{{ route("ingreso.foto", ":id") }}';
  url = url.replace(':id', valor);
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $("#img_foto_ingreso").attr("src",response['foto']);
        $('#modal_foto_ingreso').modal('show');
      }
  });
});
</script>
