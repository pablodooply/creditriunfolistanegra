<div class="table-responsive">
  <table id="tabla_tipo" class="table table-striped table-bordered table-hover dataTables-example text-center">
    <thead>
      <tr>
        <th>Codigo</th>
        <th>Nombre</th>
        <th>Descripcion</th>
        <th>Fecha de creacion</th>
        <th>Creado por</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>

    </tbody>
    <tfoot>
      <tr>
        <th>Codigo</th>
        <th>Nombre</th>
        <th>Descripcion</th>
        <th>Fecha de creacion</th>
        <th>Creado por</th>
        <th>Acciones</th>
      </tr>
    </tfoot>
  </table>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#tabla_tipo').DataTable({
    order: [[ 0, "desc" ]],
    language: {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    paging: true,
    info: false,
    dom : 'tip',
    processing: true,
    serverSide: true,
    ajax: {
      "url": '{{route('tipo.ingresos.api')}}'
    },
    pageLength: 10,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
      {extend: 'copy'},
      {extend: 'csv'},
      {extend: 'excel', title: 'Creditos'},
      {extend: 'pdf', title: 'Creditos'},
      {extend: 'print',
         customize: function (win)
         {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');

            $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
        }
      }
    ],
    columns: [
        {data: 'id', name: 'id'},
        {data: 'nombre', name: 'nombre'},
        {data: 'descripcion', name: 'descripcion'},
        {data: 'created_at', name: 'created_at'},
        {data: 'created_by', name: 'created_by'},
        {data: 'acciones', name: 'acciones'},
    ]
  });
});

$("tbody").on('click', '#edit_tipo', function(){
  var valor = $(this).attr("value");
  console.log(valor);
  var url = '{{ route("tipo.ingreso.edit", ":id") }}';
  url = url.replace(':id', valor);
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {

        $('#tipo_ingreso_id').val(response['tipo_id']);
        $('#nombretipo').val(response['nombre']);
        $('#descripcion_tipo').val(response['descripcion']);

        $('#modal_tipo_edit').modal('show');
      }
  });
});
</script>
