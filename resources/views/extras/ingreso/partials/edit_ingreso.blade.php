<div class="row">
  <div id="edit_ingreso" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Editar ingreso</h5>
                </div>
                <div class="ibox-content">
                  {!! Form::  open(['id'=> 'formulario_edit_ingreso', 'method' => 'POST','route' => 'ingreso.update', 'files' => true, 'enctype' => 'multipart/form-data']) !!}
                  <input type="hidden" name="ingreso_id" id="ingreso_id" value="">
                  <div class="row">
                    <div class="col-sm-4">
                      <label for="">Tipo de ingreso (*)</label>
                        <div class="form-group selectChampetIngreso">
                          <select class="input form-control" name="tipo_ingreso_edit" id="tipo_ingreso_edit">
                            @foreach ($tipo_ingreso as $tipo)
                              <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                      <label for="">Total (*)</label>
                      <input type="text" placeholder="" class="input form-control" id="monto_ingreso" name="monto_ingreso" required>
                    </div>
                    <div class="col-sm-4" id="cant">
                      <label for="">Descripción (*)</label>
                      <input type="text" placeholder="" class="input form-control" id="descripcion_ingreso" name="descripcion_ingreso" required>
                    </div>
                  </div>
                  <br>
                  <div class="row">

                    <div class="col-sm-4">
                      <label for="">Cantidad (*)</label>
                      <input type="text" placeholder="" class="input form-control" value="1" id="cantidad_ingreso" name="cantidad_ingreso" required>
                    </div>
                    <div class="col-sm-4">
                      {{ Form::label('foto', 'Foto de recibo') }}
                      {{ Form::file('recibo_ingreso_edit', ['class' => 'form-control']) }}
                    </div>
                    <div class="col-sm-4" id='prov'>
                      <label for="">Proveedor</label>
                      <input type="text" placeholder="" class="input form-control" id="proveedor_edit_ingreso" name="proveedor_edit_ingreso">
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-sm-4" align="center">
                    </div>
                    <div class="col-sm-4" align="center">
                      <br>
                      {{-- <a class="btn btn-primary" style="text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ingresar gastos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> --}}
                      <input type="submit" class="btn btn-primary" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Editar ingreso&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                    </div>
                  </div>

                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
