@extends('layouts.app')
@section('title', 'Ingresos')
@section('link')
  <link rel="stylesheet" href="{{asset('css/datatables/datatables.min.css')}}">
@endsection
@section('content')
@section('nombre','Ingresos')

@section('ruta')
<li class="active">
  <strong>Ingresos</strong>
</li>
@endsection

@include('extras.ingreso.partials.edit_ingreso')
@include('extras.ingreso.partials.modal_foto_ingreso')
@include('extras.ingreso.partials.edit_tipo_ingreso')
<div class="wrapper wrapper-content">
  @if(session()->has('message'))
      <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <strong>{{ session()->get('message') }}</strong>
      </div>
  @endif
  <div class="row">
    <div class="col-sm-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <div class="row">
            <div class="col-sm-4">
              <h5>Crear ingreso</h5>
            </div>
          </div>
        </div>

        <div class="ibox-content">
          {!! Form::open(array('route' => 'ingreso.store', 'method' => 'POST', 'id' => 'ingreso_store', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
          <div class="row">
            <div class="col-sm-4">
              <label for="">Tipo de ingreso (*)</label>

              <div class="input-group">
                <div class="form-group">
                  <select class="input form-control" name="tipo_ingreso" id="tipo_ingreso">
                    @foreach ($tipo_ingreso as $tipo)
                      <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                    @endforeach
                  </select>
                </div>
                <span class="input-group-btn">
                  <a id="crear_tipo" class="btn btn-primary" onclick="create_tipo()" data-toggle="modal" data-target="#modal_tipo_gasto"> <i class="fa fa-plus"></i></a>
                </span>
              </div>
            </div>
            <div class="col-sm-4">
              <label for="">Total (*)</label>
              <input type="text" placeholder="" class="input form-control" id="monto" name="monto" required>
            </div>
            <div class="col-sm-4" id="cant">
              <label for="">Descripción (*)</label>
              <input type="text" placeholder="" class="input form-control" id="descripcion" name="descripcion" required>
            </div>
          </div>
          <br>
          <div class="row">

            <div class="col-sm-4">
              <label for="">Cantidad (*)</label>
              <input type="text" placeholder="" class="input form-control" value="1" id="cantidad" name="cantidad" required>
            </div>
            <div class="col-sm-4">
              {{ Form::label('foto', 'Foto de recibo') }}
              {{ Form::file('recibo', ['class' => 'form-control']) }}
            </div>
            <div class="col-sm-4" id='prov'>
              <label for="">Proveedor</label>
              <input type="text" placeholder="" class="input form-control" id="proveedor" name="proveedor">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-sm-4" align="center">
            </div>
            <div class="col-sm-4" align="center">
              <br>
              {{-- <a class="btn btn-primary" style="text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ingresar gastos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> --}}
              <input type="submit" class="btn btn-primary" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Crear ingreso&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <div class="row">
                <div class="col-sm-4">
                  <h5>Ingresos</h5>
                </div>
              </div>
            </div>
            <div class="ibox-content">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" id='clic_ingreso' href="#todos">Ingresos</a></li>
                <li><a data-toggle="tab" id='clic_tipo' href="#home">Tipo de ingresos</a></li>
              </ul>
              <div class="tab-content">

                <div id="todos" class="tab-pane fade in active">
                  <div id="tab_ingresos">

                  </div>
                </div>


                <div id="home" class="tab-pane fade">
                  <div id="tab_tipo_ingresos">

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>



<div class="modal inmodal fade" id="modal_tipo_gasto" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div id="cont_modal_tipo_gasto">
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
@include('sweet::alert')


  <script type="text/javascript">
  $(document).ready(function(){
    $('#clic_ingreso').trigger('click');
  });

  $("#clic_ingreso").on('click', function(){
    $('#tab_ingresos').html(' ');
    var url = '{{ route("tabla.ingreso") }}';
    console.log(url);

    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $('#tab_ingresos').html(response);
        }
    });
  });

  $("#clic_tipo").on('click', function(){
    $('#tab_tipo_ingresos').html(' ');
    var url = '{{ route("tabla.tipo.ingreso") }}';
    console.log(url);

    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $('#tab_tipo_ingresos').html(response);
        }
    });
  });

  function create_tipo()
  {
    var url = '{{route("tipo_ingreso.modal.create")}}';
    // $('#cont-modal-categoria').html(' ');
    // $('#contenido').load('#skeleton');
     $.ajax({
         type: "GET",
         url: url,
         success: function( response ) {
           $('#cont_modal_tipo_gasto').html(response);
         }
     });
  }
  </script>

  <script type="text/javascript">
  var tIngreso=$('#tabla_ingreso').DataTable({
    order: [[ 0, "asc" ]],
    language: {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    paging: true,
    info: false,
    dom : 'tip',
    processing: true,
    serverSide: true,
    ajax: {
      "url": '{{route('gastos.api')}}',
      'beforeSend': function (request) {
       request.setRequestHeader("X-CSRF-TOKEN", '{{ csrf_token() }}');
       }
    },
    pageLength: 10,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
      {extend: 'copy'},
      {extend: 'csv'},
      {extend: 'excel', title: 'Gastos'},
      {extend: 'pdf', title: 'Gastos'},
      {extend: 'print',
         customize: function (win)
         {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');

            $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
        }
      }
    ],
    columns: [

        {data: 'id', name: 'id'},
        {data: 'monto', name: 'monto'},
        {data: 'descripcion', name: 'descripcion'},
        {data: 'cantidad', name: 'cantidad'},
        {data: 'proveedor', name: 'proveedor'},
        {data: 'tipo_gasto', name: 'tipo_gasto'},
        {data: 'created_at', name: 'created_at'},
        {data: 'created_by', name: 'created_by'},
        {data: 'acciones', name: 'acciones', searchable: 'false'},
    ],
  });
  </script>

  <script type="text/javascript">
  function eliminaringreso(valor){
    console.log('click');
    var url = "{{route('ingreso.eliminaringreso')}}";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    swal({
      type: 'info',
      title: "¿Esta seguro que desea eliminar el Ingreso?",
      text: "Se eliminara la información y registro del ingreso",
      icon: "warning",
      buttons: true,
      dangerMode: false,
    }).then((willDelete) => {
      if (willDelete) {
        $.ajax({
          method: 'GET',
          url: url,
          data: {
            valor : valor,
          },
          success: function( response ) {
            console.log(response);
            console.log('peligro');
            swal("Se eliminó el ingreso", {
              icon: "success",
            });
            setTimeout("location.reload()",1000);

          },
          error: function( response ) {
            console.log(response);
            console.log('peligro');
            swal("Lo sentimos, Ocurrió un error!", {
              icon: "warning",
            });

          }
        });
      } else {
        swal("No se eliminó el ingreso",{icon: "warning",});
      }
    });
  }

  function eliminartipoingreso(valor){
    console.log('click');
    var url = "{{route('tipo.ingreso.eliminar')}}";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    swal({
      type: 'info',
      title: "¿Esta seguro que desea eliminar este tipo de ingreso?",
      text: "Se eliminara la información del tipo y los ingresos registrados a este tipos",
      icon: "warning",
      buttons: true,
      dangerMode: false,
    }).then((willDelete) => {
      if (willDelete) {
        $.ajax({
          method: 'GET',
          url: url,
          data: {
            valor : valor,
          },
          success: function( response ) {
            console.log(response);
            console.log('peligro');
            swal("Se eliminó el tipo de ingreso", {
              icon: "success",
            });
            setTimeout("location.reload()",1000);

          },
          error: function( response ) {
            console.log(response);
            console.log('peligro');
            swal("Lo sentimos, Ocurrió un error!", {
              icon: "warning",
            });

          }
        });
      } else {
        swal("No se eliminó el tipo de ingreso",{icon: "warning",});
      }
    });
  }
  </script>
@endsection
