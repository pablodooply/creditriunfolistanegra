@extends('layouts.app')
@section('title', 'Gastos')
@section('link')
  <link rel="stylesheet" href="{{asset('css/datatables/datatables.min.css')}}">
@endsection
@section('content')
@section('nombre','Gastos')

@section('ruta')
<li class="active">
  <strong>Gastos</strong>
</li>
@endsection

@include('extras.gasto.partials.edit_gasto')
@include('extras.gasto.partials.modal_foto')

<div class="wrapper wrapper-content">
  @if(session()->has('message'))
      <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <strong>{{ session()->get('message') }}</strong>
      </div>
  @endif
  <div class="row">
    <div class="col-sm-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <div class="row">
            <div class="col-sm-4">
              <h5>Ingresar gasto</h5>
            </div>
          </div>
        </div>

        <div class="ibox-content">
          {!! Form::open(array('route' => 'gastos.store', 'method' => 'POST', 'id' => 'ingreso_gasto', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
          <div class="row">
            <div class="col-sm-4">
              <label for="">Tipo de gasto (*)</label>
              <select class="input form-control" name="tipo_gasto" id="tipo_gasto">
                <option value="Oficina">Oficina</option>
                <option value="Comun">Comun</option>
                <option value="Planilla">Planilla</option>
              </select>
            </div>
            <div class="col-sm-4">
              <label for="">Total (*)</label>
              <input type="text" placeholder="" class="input form-control" id="monto" name="monto" required>
            </div>
            <div class="col-sm-4" id="cant">
              <label for="">Cantidad (*)</label>
              <input type="text" placeholder="" class="input form-control" value="1" id="cantidad" name="cantidad" required>
            </div>
          </div>
          <br>
          <div class="row">

            <div class="col-sm-4">
              <label for="">Descripción (*)</label>
              <input type="text" placeholder="" class="input form-control" id="descripcion" name="descripcion" required>
            </div>
            <div class="col-sm-4">
              {{ Form::label('foto', 'Foto de recibo') }}
              {{ Form::file('recibo', ['class' => 'form-control']) }}
            </div>
            <div class="col-sm-4" id='prov'>
              <label for="">Pagado a</label>
              <input type="text" placeholder="" class="input form-control" id="proveedor" name="proveedor">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-sm-4" id="cola" style="display:none">
              <label for="">Colaborador (*)</label>
              <select class="input form-control" name="colaborador" id="colaborador">
                @foreach ($usuarios as $user)
                  <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach

              </select>
            </div>
            <div class="col-sm-4" id="meh" style="display:none">
              <label for="">Mes (*)</label>
              <select class="input form-control" name="mes" id="mes">
                <option value="Enero">Enero</option>
                <option value="Febrero">Febrero</option>
                <option value="Marzo">Marzo</option>
                <option value="Abril">Abril</option>
                <option value="Mayo">Mayo</option>
                <option value="Junio">Junio</option>
                <option value="Julio">Julio</option>
                <option value="Agosto">Agosto</option>
                <option value="Septiembre">Septiembre</option>
                <option value="Octubre">Octubre</option>
                <option value="Noviembre">Noviembre</option>
                <option value="Diciembre">Diciembre</option>
              </select>
            </div>
            <div class="col-sm-4" align="center">
              <br>
              {{-- <a class="btn btn-primary" style="text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ingresar gastos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> --}}
              <input type="submit" class="btn btn-primary" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ingresar gastos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <div class="row">
                <div class="col-sm-4">
                  <h5>Listado de gastos</h5>
                </div>
              </div>
            </div>
            <div class="ibox-content">
              <div class="row">
                <div class="table-responsive">
                  <table id="tabla_gastos" class="table table-striped table-bordered table-hover dataTables-example text-center">
                    <thead>
                      <tr>
                        <th>Código</th>
                        <th>Total</th>
                        <th>Descripcion</th>
                        <th>Cantidad</th>
                        <th>Pagado a</th>
                        <th>Tipo gasto</th>
                        <th>Fecha</th>
                        <th>Pagado por</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>Código</th>
                        <th>Total</th>
                        <th>Descripcion</th>
                        <th>Cantidad</th>
                        <th>Pagado a</th>
                        <th>Tipo gasto</th>
                        <th>Fecha</th>
                        <th>Pagado por</th>
                        <th>Acciones</th>
                      </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
  <script type="text/javascript">
  function clickeliminar(){
  $(".eliminargasto").click(function(){
    var valor = $(this).attr('value');
    eliminargasto(valor);
  });
  }

  function eliminargasto(valor){
    console.log('click');
    var url = "{{route('gastos.eliminargasto')}}";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    swal({
      type: 'info',
      title: "¿Esta seguro que desea eliminar el Gasto?",
      text: "Se eliminara la información y registro del gasto",
      icon: "warning",
      buttons: true,
      dangerMode: false,
    }).then((willDelete) => {
      if (willDelete) {
        $.ajax({
          method: 'GET',
          url: url,
          data: {
            valor : valor,
          },
          success: function( response ) {
            console.log(response);
            console.log('peligro');
            swal("Se eliminó el gasto", {
              icon: "success",
            });
            setTimeout("location.reload()",1000);

          },
          error: function( response ) {
            console.log(response);
            console.log('peligro');
            swal("Lo sentimos, Ocurrió un error!", {
              icon: "warning",
            });

          }
        });
      } else {
        swal("No se eliminó el gasto",{icon: "warning",});
      }
    });
  }


  </script>
  <script type="text/javascript">
  $("tbody").on('click', '#foto_gasto', function(){
    var valor = $(this).attr("value");
    console.log(valor);
    var url = '{{ route("gastos.foto", ":id") }}';
    url = url.replace(':id', valor);
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $("#img_foto").attr("src",response['foto']);
          $('#modal_foto').modal('show');
        }
    });
  });
  </script>
  <script type="text/javascript">
  $("tbody").on('click', '#edit_gasto', function(){
    var valor = $(this).attr("value");
    console.log(valor);
    var url = '{{ route("gastos.edit", ":id") }}';
    url = url.replace(':id', valor);
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $('#gasto_id').val(response['gasto_id']);
          $("div.selectChampet select").val(response['tipo']);
          if($("div.selectChampet select").val()=='Planilla')
          {
            $("#cola_edit").css({'display': "block" });
            $("#meh_edit").css({'display': "block" });
            $("#prov_edit").css({'display': "none" });
            $("#cant_edit").css({'display': "none" });
            $('#monto_edit').val(response['total']);
            $('#descripcion_edit').val(response['descripcion']);
            $("div.selectMes select").val(response['mes']);
            $("div.selectUsuario select").val(response['usuario']);
          }
          if($("div.selectChampet select").val()=='Comun')
          {
            $("#cola_edit").css({'display': "none" });
            $("#meh_edit").css({'display': "none" });
            $("#prov_edit").css({'display': "block" });
            $("#cant_edit").css({'display': "block" });

            $('#monto_edit').val(response['total']);
            $('#descripcion_edit').val(response['descripcion']);
            $('#proveedor_edit').val(response['proveedor']);
            $('#cantidad_edit').val(response['cantidad']);
          }
          if($("div.selectChampet select").val()=='Oficina')
          {
            $("#cola_edit").css({'display': "none" });
            $("#meh_edit").css({'display': "none" });
            $("#prov_edit").css({'display': "block" });
            $("#cant_edit").css({'display': "block" });

            $('#monto_edit').val(response['total']);
            $('#descripcion_edit').val(response['descripcion']);
            $('#proveedor_edit').val(response['proveedor']);
            $('#cantidad_edit').val(response['cantidad']);
          }

          $('#edit_modal_gasto').modal('show');
        }
    });
  });
  </script>
  <script type="text/javascript">
  $( document ).ready(function() {
    $( "#tipo_gasto_edit" ).change(function() {
      if($("#tipo_gasto_edit").val()=='Planilla')
      {
        $("#cola_edit").css({'display': "block" });
        $("#meh_edit").css({'display': "block" });
        $("#prov_edit").css({'display': "none" });
        $("#cant_edit").css({'display': "none" });
      }
      if($("#tipo_gasto_edit").val()=='Comun')
      {
        $("#cola_edit").css({'display': "none" });
        $("#meh_edit").css({'display': "none" });
        $("#prov_edit").css({'display': "block" });
        $("#cant_edit").css({'display': "block" });
      }
      if($("#tipo_gasto_edit").val()=='Oficina')
      {
        $("#cola_edit").css({'display': "none" });
        $("#meh_edit").css({'display': "none" });
        $("#prov_edit").css({'display': "block" });
        $("#cant_edit").css({'display': "block" });
      }
    });
  });
  </script>
  <script type="text/javascript">
  $( document ).ready(function() {
    $( "#tipo_gasto" ).change(function() {
      if($("#tipo_gasto").val()=='Planilla')
      {
        $("#cola").css({'display': "block" });
        $("#meh").css({'display': "block" });
        $("#prov").css({'display': "none" });
        $("#cant").css({'display': "none" });
      }
      if($("#tipo_gasto").val()=='Comun')
      {
        $("#cola").css({'display': "none" });
        $("#meh").css({'display': "none" });
        $("#prov").css({'display': "block" });
        $("#cant").css({'display': "block" });
      }
      if($("#tipo_gasto").val()=='Oficina')
      {
        $("#cola").css({'display': "none" });
        $("#meh").css({'display': "none" });
        $("#prov").css({'display': "block" });
        $("#cant").css({'display': "block" });
      }
    });
  });
  </script>
  <script type="text/javascript">
  var tGasto=$('#tabla_gastos').DataTable({
    order: [[ 0, "asc" ]],
    language: {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    paging: true,
    info: false,
    dom : 'tip',
    processing: true,
    serverSide: true,
    ajax: {
      "url": '{{route('gastos.api')}}',
      'beforeSend': function (request) {
       request.setRequestHeader("X-CSRF-TOKEN", '{{ csrf_token() }}');
       }
    },
    pageLength: 10,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
      {extend: 'copy'},
      {extend: 'csv'},
      {extend: 'excel', title: 'Gastos'},
      {extend: 'pdf', title: 'Gastos'},
      {extend: 'print',
         customize: function (win)
         {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');

            $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
        }
      }
    ],
    columns: [

        {data: 'id', name: 'id'},
        {data: 'monto', name: 'monto'},
        {data: 'descripcion', name: 'descripcion'},
        {data: 'cantidad', name: 'cantidad'},
        {data: 'proveedor', name: 'proveedor'},
        {data: 'tipo_gasto', name: 'tipo_gasto'},
        {data: 'created_at', name: 'created_at'},
        {data: 'created_by', name: 'created_by'},
        {data: 'acciones', name: 'acciones', searchable: 'false'},
    ],
  });
  </script>
@endsection
