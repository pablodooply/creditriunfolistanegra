<div class="row">
  <div id="edit_modal_gasto" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Editar gasto</h5>
                </div>
                <div class="ibox-content">
                  {!! Form::  open(['id'=> 'formulario_edit', 'method' => 'POST','route' => 'gastos.update', 'files' => true, 'enctype' => 'multipart/form-data']) !!}
                  <input type="hidden" name="gasto_id" id="gasto_id" value="">
                  <div class="row">
                    <div class="col-sm-4">
                      <label for="">Tipo de gasto (*)</label>
                      <div class="selectChampet">


                      <select class="input form-control selectChampet" name="tipo_gasto_edit" id="tipo_gasto_edit">
                        <option value="Oficina">Oficina</option>
                        <option value="Comun">Comun</option>
                        <option value="Planilla">Planilla</option>
                      </select>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <label for="">Total (*)</label>
                      <input type="text" placeholder="" class="input form-control" id="monto_edit" name="monto_edit" required>
                    </div>
                    <div class="col-sm-4" id="cant_edit">
                      <label for="">Cantidad (*)</label>
                      <input type="text" placeholder="" class="input form-control" value="1" id="cantidad_edit" name="cantidad_edit" required>
                    </div>
                  </div>
                  <br>
                  <div class="row">

                    <div class="col-sm-4">
                      <label for="">Descripción (*)</label>
                      <input type="text" placeholder="" class="input form-control" id="descripcion_edit" name="descripcion_edit" required>
                    </div>
                    <div class="col-sm-4">
                      {{ Form::label('foto', 'Foto de recibo') }}
                      {{ Form::file('recibo_edit', ['class' => 'form-control']) }}
                    </div>
                    <div class="col-sm-4" id='prov_edit'>
                      <label for="">Pagado a</label>
                      <input type="text" placeholder="" class="input form-control" id="proveedor_edit" name="proveedor_edit">
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-sm-4" id="cola_edit" style="display:none">
                      <label for="">Colaborador (*)</label>
                      <div class="selectUsuario">
                      <select class="input form-control" name="colaborador_edit" id="colaborador_edit">
                        @foreach ($usuarios as $user)
                          <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach

                      </select>
                      </div>
                    </div>
                    <div class="col-sm-4" id="meh_edit" style="display:none">
                      <label for="">Mes (*)</label>
                      <div class="selectMes">
                      <select class="input form-control" name="mes_edit" id="mes_edit">
                        <option value="Enero">Enero</option>
                        <option value="Febrero">Febrero</option>
                        <option value="Marzo">Marzo</option>
                        <option value="Abril">Abril</option>
                        <option value="Mayo">Mayo</option>
                        <option value="Junio">Junio</option>
                        <option value="Julio">Julio</option>
                        <option value="Agosto">Agosto</option>
                        <option value="Septiembre">Septiembre</option>
                        <option value="Octubre">Octubre</option>
                        <option value="Noviembre">Noviembre</option>
                        <option value="Diciembre">Diciembre</option>
                      </select>
                      </div>
                    </div>
                    <div class="col-sm-4" align="center">
                      <br>
                      {{-- <a class="btn btn-primary" style="text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ingresar gastos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> --}}
                      <input type="submit" class="btn btn-primary" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Actualizar gasto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                    </div>
                  </div>

                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
