<div class="row">
  <div id="modal_foto" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Modal foto</h5>
                </div>
                <div class="ibox-content pull-center" style="justify-content:center;">
                  <a href="" id="a_foto"><img src="{{asset('images/default/perfil.jpg')}}" id="img_foto" height="1020" width="750" class="pull-center"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
