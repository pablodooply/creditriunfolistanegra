<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Creditriunfo | Login</title>

    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
  <div>
      <h1 class="logo-name"><img class="img-responsive" src="{{asset('images/system/logo1.png')}}"></img></h1>
  </div>
</div>

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h3>Bienvenido a Creditriunfo</h3>
            <p>Sistema de creditos
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p>Entrar</p>
            <form method="POST" class="m-t" role="form" action="{{route ('login')}}">
            @csrf
                <div class="form-group">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" placeholder="Usuario" required autofocus>

                           @if ($errors->has('email'))
                               <span class="invalid-feedback">
                                   <strong>{{ $errors->first('email') }}</strong>
                               </span>
                           @endif
                </div>
                <div class="form-group">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" placeholder="Contraseña" required>

                           @if ($errors->has('password'))
                               <span class="invalid-feedback">
                                   <strong>{{ $errors->first('password') }}</strong>
                               </span>
                           @endif
                </div>

                <button type="submit" class="btn btn-primary block full-width m-b">{{ __('Login') }}</button>

            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>

</body>

</html>
