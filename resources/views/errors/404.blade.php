<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CrediStar | 404 Error</title>


        <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
        <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <h3 class="font-bold">Pagina no encontrada</h3>

        <div class="error-desc">
            Lo sentimos, pero no se ha encontrado la pagina que esta buscando, Intente comprobar la URL en busca
            de errores.
            <form class="form-inline m-t" role="form">
                <a href="{{route('home')}}" class="btn btn-primary">Regresar a inicio</a>
            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>

</body>

</html>
