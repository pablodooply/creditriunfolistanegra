<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CrediStar | Error 401</title>


        <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
        <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>401</h1>
        <h3 class="font-bold">Tiempo de Sesión Expirado</h3>

        <div class="error-desc">
            Lo sentimos, pero se cerró la sesión debido a que se sobrepasó el tiempo sin actividad, Recargue la página para loguearse de nuevo.
            <form class="form-inline m-t" role="form">
                <a href="{{route('home')}}" class="btn btn-primary">Recargar la página</a>
            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>

</body>

</html>
