@extends('layouts.app')

@section('title', 'Rutas')

@section('link')
  <link rel="stylesheet" href="{{asset('css/icheck/custom.css')}}">
@endsection
@section('content')

@section('nombre','Listado de rutas')
@section('ruta')
  <li class="active">
      <strong>Listado de rutas</strong>
  </li>
@endsection
{!! Form::open(["id"=>"asignacion" ,'class'=>'col-lg-12', 'route' => 'rutas.asignacion', 'method' => 'POST']) !!}
  <div class="modal fade bd-example-modal-lg" id="modalSupervisor" data-backdrop="static"  role="dialog" aria-labelledby="modal"
        aria-hidden="true">
       <div class="modal-dialog modal-lg">
           <div class="modal-content container-fluid">
           </div>
       </div>
   </div>
{!!Form::close()!!}
<div class="modal fade bd-example-modal-lg" id="modal-transferir" data-backdrop="static"  role="dialog" aria-labelledby="modal"
      aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content container-fluid">
         </div>
     </div>
 </div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <a class="btn btn-primary btn-outline btn-sm pull-right"  data-toggle="modal" data-target="#modalSupervisor" href="modalSupervisor">Asignar supervisor</a>
          <h5>Rutas </h5>
        </div>
        <div class="ibox-content">
          <div class="row">
            <div class="col-md-12">
                <input placeholder="Busqueda de ruta" class="form-control" id="busqueda-rutas">
            </div>
            <ul class="nav">
              <p><span class="pull-left text-muted">{{count($rutas)}} rutas</span></p>
            </ul>
            <div class="col-xs-12">
              <div class="table-responsive">
                @include('Ruta.tables.tableIndex')
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/icheck/icheck.min.js')}}"></script>


<script type="text/javascript">
var total=0;
$(document).ready(function(){
  function calc_total(){
    var sum = 0;
    $(".valor_total").each(function(){
      sum += parseFloat($(this)[0].innerHTML);
    });
    $('#suma_total').text("Q." + sum);
  }

  function calc_pres(){
    var sum = 0;
    $(".cantidad_pres").each(function(){
      sum += parseFloat($(this)[0].innerHTML);
    });
    $('#total_pres').text(sum);
  }

  var oTable = $('#tablas-rutas').DataTable({
    "language": {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    "paging":   true,
    "info":     false,
    'dom' : 'tip',
    "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i *1 :
                typeof i === 'number' ?
                    i : 0;
        };

      // Total over all pages
      total = api
          .column( 1 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );

          // Update footer
          var que = api.columns('.sum', { page: 'current'}).every( function () {
            var sum = this
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              },0 );

            this.footer().innerHTML = sum;
          } );
      }
  });

  $('#busqueda-rutas').keyup(function(){
        oTable.search($(this).val()).draw();
  });

  calc_total();
  calc_pres();
});
</script>
@endsection
