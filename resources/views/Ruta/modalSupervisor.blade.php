      <div class="modal-header">
        <h5 class="modal-title">Asignacion de supervisor</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
              <div class="form-group">
                <label for="">Supervisor</label>
                <select class="form-control" name="super">
                  @foreach ($supervisores as $supervisor)
                    <option value="{{$supervisor->id}}">{{$supervisor->persona->nombre . $supervisor->persona->apellido}}</option>
                  @endforeach
                </select>
              </div>
          </div>
          <div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th>
                    {{-- <div class="i-checks"><label> <input type="checkbox" value=""> <i></i></label></div> --}}
                  </th>
                  <th>Código</th>
                  <th>Nombre</th>
                  <th>Promotor</th>
                  <th>Supervisor</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($rutas as $ruta)
                  <tr>
                    <td><input value="{{$ruta->id}}" type="checkbox"  checked class="i-checks" name="input[]"></td>
                    <td>{{$ruta->id}}</td>
                    <td>{{$ruta->nombre}}</td>
                    <td>{{$ruta->user->persona->nombre}}</td>
                    @if($ruta->supervisor)
                      <td>{{$ruta->supervisor->persona->nombre}}</td>
                    @else
                      <td>No esta asignado</td>
                    @endif
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a id="btn-asig"  class="btn btn-primary">Asignar rutas</a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>


<script type="text/javascript">
$('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    cursor: true

});

$('#btn-asig').click(function(){
  $('#asignacion').submit();
  // console.log('asignar');
});

$('#asignacion').submit(function(){
  var url = "{{route('rutas.asignacion')}}";
  var form = $(this);
  var data = form.serialize();
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    cache: false,
    success: function (data) {
      console.log(data);
    }
  });
});
</script>
