<a class="btn btn-sm btn-white btn-bitbucket btn-show-categoria" onclick="show_categoria('{{$id}}')" data-toggle="modal" data-target="#modal-categorias">
    <i class="fa fa-eye text-success"></i>
</a>
<a class="btn btn-sm btn-white btn-bitbucket btn-edit-categoria" onclick="edit_categoria('{{$id}}')" data-toggle="modal" data-target="#modal-categorias">
    <i class="fa fa-edit text-warning"></i>
</a>
<a class="btn btn-sm btn-white btn-bitbucket btn-delete-categoria" onclick="delete_categoria('{{$id}}')">
    <i class="fa fa-trash text-danger"></i>
</a>
<script type="text/javascript">
  function show_categoria(id)
  {
    console.log(id)
    var url = '{{route("categoria.modal.show", ":slug")}}';
    url = url.replace(':slug', id);
    $('#cont-modal-categoria').html(' ');
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $('#cont-modal-categoria').html(response);
        }
    });
  }

  function edit_categoria(id)
  {
    console.log(id)
    var url = '{{route("categoria.modal.edit", ":slug")}}';
    url = url.replace(':slug', id);
    $('#cont-modal-categoria').html(' ');
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $('#cont-modal-categoria').html(response);
        }
    });
  }

  function delete_categoria(valor)
  {
    console.log(valor)
    var id = $(this).attr('value');
    var url = '{{ route("categoria.delete", ":slug") }}';
    url = url.replace(':slug', valor);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    swal({
     icon: "warning",
     title: '¿Desea eliminar?',
     text: "Si se elimina ésta categoría, ya no aparecerá en el sistema.",
     buttons: true,
     dangerMode: true,
    }).then((result) => {
     if (result) {
       $.ajax({
         method: 'GET',
         url: url,
         data: {
           valor : valor,
         },
         success: function( response ) {
           console.log(response);
           swal({
             icon: 'success',
             title: 'Eliminada!',
             text: "Se eliminó ésta CATEGORIA con éxito!",
             timer: 1000
           });
           // DatatablesExtensionButtons.t.draw('page');
           var t = $("#tabla-categorias").DataTable();
           t.ajax.reload( null, false );


         },
         error: function( response ) {
           console.log(response);
           console.log('peligro');
           swal({
             icon: 'error',
             title: 'Lo sentimos...',
             text: 'Ocurrió un error!',
             timer: 1500
           });

         }
       });
     }
     else {
       swal('Cancelado',
        'Ésta categoría no se eliminará.',
        'info');
     }
    })
  }
</script>
