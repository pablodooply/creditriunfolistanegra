@extends('layouts.app')
@section('title', 'Categorías')
@section('link')
  <link rel="stylesheet" href="{{asset('css/datatables/datatables.min.css')}}">
@endsection
@section('content')
  @php
  use Carbon\Carbon;
  @endphp
  @section('nombre','Categorías')
  @section('ruta')
    <li class="breadcrumb-item active">
        <strong>Categorias</strong>
    </li>
  @endsection

  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox ">
          <div class="ibox-title">
            <h5>Listado de categorías</h5>
            <div class="ibox-tools">
              <button type="button"  class="btn btn-primary float-right" name="button" onclick="create_categoria()" data-toggle="modal" data-target="#modal-categorias"> <span class="fa fa-plus"></span> Nuevo</button>
              <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
              </a>
              <a class="close-link">
                <i class="fa fa-times"></i>
              </a>
            </div>
          </div>
          <div class="ibox-content">

            <div class="table-responsive">
              <table id="tabla-categorias" class="table table-striped table-bordered table-hover dataTables-example text-center">
                <thead>
                  <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Acción</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                  <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Acción</th>
                  </tr>
                </tfoot>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal inmodal fade" id="modal-categorias" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div id="cont-modal-categoria">
        </div>
      </div>
    </div>
  </div>
@endsection
@include('categorias.partials.skeleton-create-content')
@section('scripts')
  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> --}}
    <!-- Include this after the sweet alert js file -->
    @include('sweet::alert')
  <script type="text/javascript">
    $( "#btn-edit-categoria" ).click(function() {
      $( "#nombrecat" ).focus();
    });

    $(document).ready(function(){
      $('#tabla-categorias').DataTable({
        order: [[ 0, "desc" ]],
        language: {
            "url": "{{asset('fonts/dataTablesEsp.json')}}",
        },
        paging: true,
        info: false,
        dom : 'tip',
        processing: true,
        serverSide: true,
        ajax: {
          "url": '{{route('categoria.api')}}'
        },
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
          {extend: 'copy'},
          {extend: 'csv'},
          {extend: 'excel', title: 'Categorías'},
          {extend: 'pdf', title: 'Categorías'},
          {extend: 'print',
             customize: function (win)
             {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');

                $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
            }
          }
        ],
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nombre', name: 'nombre'},
            {data: 'accion', name: 'accion'}
        ]
      });
    });


    function create_categoria()
    {
      var url = '{{route("categoria.modal.create")}}';
      // $('#cont-modal-categoria').html(' ');
      // $('#contenido').load('#skeleton');
       $.ajax({
           type: "GET",
           url: url,
           success: function( response ) {
             $('#cont-modal-categoria').html(response);
           }
       });
    }

  </script>


@endsection
