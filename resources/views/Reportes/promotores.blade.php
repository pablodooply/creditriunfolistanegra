@extends('layouts.app')

@section('title', 'Reportes')

@section('link')
  <link rel="stylesheet" href="{!! asset('css/dataTables/datatables.min.css') !!}" />
@endsection

@section('content')

  @section('nombre','Reportes')
  @section('ruta')
    <li class="active">
        <strong>Reportes colaboradores</strong>
    </li>
  @endsection
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Reportes </h5>
        <div class="ibox-tools">
          {{-- <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a> --}}
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
            <i style="color:black" class="fa fa-wrench fa-2x"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a id="tipo_1">Reporte tipo 1</a>
            </li>
            <li><a id="tipo_2">Reporte tipo 2</a>
            </li>
        </ul>
        </div>
      </div>
      <div class="ibox-content" id="contenido">
        {{-- <div class="row">
          <div class="col-sm-5 m-b-xs">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Tipo</label>
                <select class="form-control" name="tipo_reporte" id="tipo_reporte">
                  <option value="1">Colocado monto</option>
                  <option value="2">Colocado cantidad</option>
                </select>
              </div>
            </div>
          </div>
        <div class="col-sm-5 m-b-xs">
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Desde:</label>
              {{ Form::date('fecha_inicio', \Carbon\Carbon::now()->startOfMonth()->subMonth(),['class' => 'form-control', 'id' => 'inicioFecha'])}}
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Hasta:</label>
              {{ Form::date('fecha_fin', \Carbon\Carbon::now(),['class' => 'form-control', 'id' => 'finFecha'])}}
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label"></label>
            <button type="submit" id="busqueda" class="btn btn-primary pull-center" name="button">Buscar</button>
          </div>
        </div>
        </div>
        <div class="row">
          <div class="table-responsive col-xs-12">
            <table class="table table-striped table-bordered table-hover dataTables-example" >
              <thead>
                <tr>
                  <th>Colaborador</th>
                  <th class="sum">Diario</th>
                  <th class="sum">Semanal</th>
                  <th class="sum">Quincena</th>
                  <th class="sum">Total</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Total: </th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div> --}}
        @include('Reportes.tabla.reporte_promotor_1')
      </div>
    </div>
  </div>

</div>
</div>
@endsection

@section('scripts')

  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

  <script>

  $('#tipo_1').click(function(){
    var url = '{{route('reportes.index', '7')}}'
    $('#contenido').html('');
    $('#contenido').load(url);
  });


  $('#tipo_2').click(function(){
    var url = '{{route('reportes.index', '8')}}'
    $('#contenido').html('');
    $('#contenido').load(url);
  });




      $(document).ready(function(){
          var  oTable = $('.dataTables-example').DataTable({
            "language": {
                "url": "{{asset('fonts/dataTablesEsp.json')}}",
            },
              pageLength: 10,
              "processing": true,
              "serverSide": true,
              ajax: {
                   url: '{{route('reportes.promotores')}}',
                   data: function (d) {
                       d.inicio = $('input[name=fecha_inicio]').val();
                       d.fin = $('input[name=fecha_fin]').val();
                       d.tipo = $('#tipo_reporte option:selected').val();
                   }
               },
              'columns': [
                {data: 'promotor'},
                {data: 'diario'},
                {data: 'semanal'},
                {data: 'quincena'},
                {data: 'total'},
              ],
              dom: '<"html5buttons"B>lTfgitp',
              buttons: [
                  { extend: 'copy'},
                  {extend: 'csv'},
                  {extend: 'excel', title: 'Reportes'},
                  {extend: 'pdf', title: 'Reportes'},

                  {extend: 'print',
                   customize: function (win){
                          $(win.document.body).addClass('white-bg');
                          $(win.document.body).css('font-size', '10px');

                          $(win.document.body).find('table')
                                  .addClass('compact')
                                  .css('font-size', 'inherit');
                  }
                  }
              ],
              "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i *1 :
                    typeof i === 'number' ?
                        i : 0;
            };

          // Total over all pages
          total = api
              .column( 1 )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

              // Update footer
              api.columns('.sum', { page: 'current'}).every( function () {
                var sum = this
                  .data()
                  .reduce( function (a, b) {
                      return intVal(a) + intVal(b);
                  }, 0 );

                this.footer().innerHTML = sum;
              } );
        }
          });

          $('#busqueda').on('click', function(e) {
              oTable.draw();
              e.preventDefault();
          });
    });

  </script>

@endsection

