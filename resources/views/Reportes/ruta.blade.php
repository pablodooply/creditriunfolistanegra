@extends('layouts.app')

@section('title', 'Reportes')

@section('link')
  <link rel="stylesheet" href="{!! asset('css/dataTables/datatables.min.css') !!}" />
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Reportes </h5>
      </div>
      <div class="ibox-content">
        <div class="row">
          <div class="col-sm-5 m-b-xs">
            <div class="form-group">
              <label class='control-label'>Ruta</label>
              <select class="input-sm form-control input-s-sm inline" id="ruta">
                @foreach ($hoja_ruta as $hoja)
                  <option value="{{$hoja->id}}">{{$hoja->nombre}}</option>
                @endforeach
            </select>
            </div>
        </div>
        <div class="col-sm-5 m-b-xs">
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Desde:</label>
              {{ Form::date('fecha_inicio', \Carbon\Carbon::now()->startOfMonth(),['class' => 'form-control', 'id' => 'inicioFecha'])}}
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Hasta:</label>
              {{ Form::date('fecha_fin', \Carbon\Carbon::now(),['class' => 'form-control', 'id' => 'finFecha'])}}
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label"> </label>
            <button type="submit" id="busqueda" class="btn btn-primary pull-center" name="button">Buscar</button>
          </div>
        </div>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover dataTables-example" >
          <thead>
          <tr>
              <th>Cliente</th>
              <th>Prestamo</th>
              <th>No. Cuota</th>
              <th class="sum">Interes</th>
              <th class="sum">Mora</th>
              <th class="sum">Capital recuperado</th>
              <th class="sum">Total pagado</th>
          </tr>
          </thead>
          <tfoot>
          <tr>
            <th>Total:</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
          </tfoot>
          </table>
        </div>

      </div>
    </div>
  </div>

</div>
</div>
@endsection

@section('scripts')

  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

  <script>
      $(document).ready(function(){
          var  oTable = $('.dataTables-example').DataTable({
            "language": {
                "url": "{{asset('fonts/dataTablesEsp.json')}}",
            },
              pageLength: 10,
              "processing": true,
              "serverSide": true,
              ajax: {
                   url: '{{route('reportes.rutas')}}',
                   data: function (d) {
                       d.inicio = $('input[name=fecha_inicio]').val();
                       d.fin = $('input[name=fecha_fin]').val();
                       d.ruta = $('#ruta :selected').val();
                   }
               },
               "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
               "bPaginate": true,
               "sPaginationType": "full_numbers",
              'columns': [
                {data: 'nombre'},
                {data: 'id'},
                {data: 'no_dia'},
                {data: 'interes'},
                {data: 'mora'},
                {data: 'capital'},
                {data: 'monto'}
              ],
              dom: '<"html5buttons"B>lTfgitp',
              buttons: [
                  { extend: 'copy'},
                  {extend: 'csv'},
                  {extend: 'excel', title: 'Reportes'},
                  {extend: 'pdf', title: 'Reportes'},

                  {extend: 'print', 'footer':true,
                   customize: function (win){
                          $(win.document.body).addClass('white-bg');
                          $(win.document.body).css('font-size', '10px');

                          $(win.document.body).find('table')
                                  .addClass('compact')
                                  .css('font-size', 'inherit');
                  }
                  }
              ],
              "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i *1 :
                    typeof i === 'number' ?
                        i : 0;
            };

          // Total over all pages
          total = api
              .column( 1 )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

              // Update footer
              var que = api.columns('.sum').every( function () {
                var sum = this
                  .data()
                  .reduce( function (a, b) {
                      return intVal(a) + intVal(b);
                  }, 0 );

                this.footer().innerHTML = parseFloat(sum).toFixed(2);
              } );
        }
          });

          $('#busqueda').on('click', function(e) {
              oTable.draw();
              e.preventDefault();
          });
});

  </script>

@endsection
