@extends('layouts.app')

@section('title', 'Reportes')

@section('link')
  <link rel="stylesheet" href="{!! asset('css/dataTables/datatables.min.css') !!}" />
@endsection

@section('content')

  @section('nombre','Reporte')
  @section('ruta')
    <li class="active">
        <strong>Reporte cliente(s)</strong>
    </li>
  @endsection
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Reportes</h5>
      </div>
      <div class="ibox-content">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label">Estado de Cliente:</label>
              <select class="form-control" name="tipo_cliente" id="tipo_cliente">
                <option value="1">Activos</option>
                <option value="2">Bloqueados</option>
              </select>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label">Estado de crédito:</label>
              <select class="form-control" name="tipo_prestamo" id="tipo_prestamo">
                <option value="1">Pendiente</option>
                <option value="2">Examinado</option>
                <option value="3">Aprobado</option>
                <option value="4">Desembolsado</option>
                <option value="5">Activo</option>
                <option value="6">Activo Moroso</option>
                <option value="9">Vencido Cancelado</option>
                <option value="10">Vencido Moroso</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Desde:</label>
              {{ Form::date('fecha_inicio', \Carbon\Carbon::now()->startOfMonth()->subMonth(),['class' => 'form-control', 'id' => 'inicioFecha'])}}
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Hasta:</label>
              {{ Form::date('fecha_fin', \Carbon\Carbon::now(),['class' => 'form-control', 'id' => 'finFecha'])}}
            </div>
          </div>
          <div class="col-sm-2">
            <div class="form-group">
              <br>
              <button type="submit" id="busqueda" class="btn btn-primary pull-center" name="button">Buscar</button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover dataTables-example">
          <thead>
          <tr>
              <th>DPI</th>
              <th>Cliente</th>
              <th class="sum">Capital total</th>
              <th class="sum">Capital activo</th>
              <th class="sum">Interes</th>
              <th class="sum">Mora</th>
              <th class="sum">Recolectado</th>
              <th class="sum">Saldo</th>
              <th>Colocador</th>
              <th>Plan</th>
              <th>Estado</th>
          </tr>
          </thead>
          <tfoot>
          <tr>
              <th>Total:</th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
          </tr>
          </tfoot>
          </table>
        </div>

      </div>
    </div>
  </div>

</div>
</div>
@endsection

@section('scripts')

  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

  <script>
          var  oTable = $('.dataTables-example').DataTable({
            "language": {
                "url": "{{asset('fonts/dataTablesEsp.json')}}",
            },
              pageLength: 10,
              "processing": true,
              "serverSide": true,
              ajax: {
                   url: '{{route('reportes.busqueda')}}',
                   data: function (d) {
                       d.inicio = $('input[name=fecha_inicio]').val();
                       d.fin = $('input[name=fecha_fin]').val();
                       d.tipo = $('#tipo_cliente :selected').val();
                       d.prestamo = $('#tipo_prestamo :selected').val();
                   }
               },
              'columns': [
                {data: 'dpi'},
                {data: 'nombre'},
                {data: 'capital_total'},
                {data: 'capital_activo'},
                {data: 'interes'},
                {data: 'mora'},
                {data: 'total_recolectado'},
                {data: 'pendiente'},
                {data: 'promotor'},
                {data: 'plan'},
                {data: 'estado'},
              ],
              dom: '<"html5buttons"B>lTfgitp',
              buttons: [
                  { extend: 'copy' , footer: true},
                  {extend: 'csv', footer: true},
                  {extend: 'excel', title: 'Reportes', footer: true},
                  {extend: 'pdf', title: 'Reportes', footer: true},

                  {extend: 'print', footer: true,
                   customize: function (win){
                          $(win.document.body).addClass('white-bg');
                          $(win.document.body).css('font-size', '10px');

                          $(win.document.body).find('table')
                                  .addClass('compact')
                                  .css('font-size', 'inherit');
                  }
                  }
              ],
              "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i *1 :
                    typeof i === 'number' ?
                        i : 0;
            };

          // Total over all pages
          total = api
              .column( 1 )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

              // Update footer
              var que = api.columns('.sum', { page: 'current'}).every( function () {
                var sum = this
                  .data()
                  .reduce( function (a, b) {
                      return intVal(a) + intVal(b);
                  },0 );

                this.footer().innerHTML = sum;
              } );
        }
    });

          $('#busqueda').on('click', function(e) {
              oTable.draw();
              e.preventDefault();
          });

  </script>

@endsection
