<div class="row">
  <div id="modal-create" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h3>Crear prospecto</h3>
                </div>
                <div class="ibox-content">
                  {!! Form::  open(['id'=> 'form-credito', 'route' => 'prospecto.create','method' => 'POST']) !!}
                  <div class="form-group col-sm-4">
                    {{ Form::label('name', 'Nombre') }}
                    {{ Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombre', 'required']) }}
                  </div>
                  <div class="form-group col-sm-4">
                    {{ Form::label('name', 'Apellido') }}
                    {{ Form::text('apellido', null, ['class' => 'form-control', 'id' => 'nombre', 'required']) }}
                  </div>
                  <div class="form-group col-sm-4">
                    {{ Form::label('name', 'Teléfono') }}
                    {{ Form::text('telefono', null, ['class' => 'form-control', 'id' => 'nombre', 'required']) }}
                  </div>
                  <div class="form-group col-sm-8">
                    {{ Form::label('name', 'Domicilio') }}
                    {{ Form::text('domicilio', null, ['class' => 'form-control', 'id' => 'nombre', 'required']) }}
                  </div>
                  <div class="form-group col-sm-4">
                    {{ Form::label('name', 'Asignar ruta') }}
                    <select class="form-control" name="ruta">
                      @foreach ($rutas as $ruta)
                        <option value="{{$ruta->id}}">{{$ruta->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-sm-12">
                    {{ Form::label('name', 'Fecha proxima visita:') }}
                    <div class="form-group" id="guardar">
                      <div class="col-xs-4">
                        {{ Form::date('fecha', Carbon\Carbon::now(), ['class' => 'form-control', 'id' => 'nombre', 'required']) }}
                      </div>
                      <div class="col-xs-8">
                        {{ Form::submit('Registrar', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton', 'value' => 'pago']) }}
                        {{ Form::button('Cancelar',['class' => 'btn btn-md btn-danger', 'data-dismiss'=>"modal"])}}
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="form-row">
                  </div>
                  {{Form::close()}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
