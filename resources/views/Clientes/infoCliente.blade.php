<div class="row">
  <div class="col-md-12">
      <div class="col-md-12">
        <p><strong>Datos de cliente personales:</strong></p>
        <div class="col-md-6">
          <ul class="list-group clear-list">
            <li class="list-group-item fist-item">
              Nombre:
              <span class="pull-right">{{$cliente->persona->nombre}} </span>
            </li>
            <li class="list-group-item">
              DPI:
              <span class="pull-right">{{$cliente->persona->dpi}}</span>
            </li>
            <li class="list-group-item">
              NIT:
              <span class="pull-right">{{$cliente->persona->nit}}</span>
            </li>
            <li class="list-group-item">
              Fecha nacimiento:
              <span class="pull-right">{{$cliente->persona->fecha_nacimiento}} </span>
            </li>
            <li class="list-group-item">
              Direccion domicilio:
              <span class="pull-right">{{$cliente->persona->domicilio}}</span>
            </li>
            <li class="list-group-item">
              Tipo de casa:
              <span class="pull-right">{{$cliente->tipo_casa}}</span>
            </li>
            <li class="list-group-item">
              Nombre recibo de luz:
              <span class="pull-right">{{$cliente->nombre_recibo}}</span>
            </li>
          </ul>
        </div>
        <div class="col-md-6">
          <ul class="list-group clear-list">
            <li class="list-group-item fist-item">
              Teléfono:
              <span class="pull-right">{{$cliente->persona->telefono_r}} </span>
            </li>
            <li class="list-group-item">
              Celular 1:
              <span class="pull-right">{{$cliente->persona->celular1}}</span>
            </li>
            <li class="list-group-item">
              Celular 2:
              <span class="pull-right">{{$cliente->persona->celular2}}</span>
            </li>
            <li class="list-group-item">
              Genero:
              @if($cliente->persona->genero==1)
               <span class="pull-right">Masculino</span>
              @else
               <span class="pull-right">Femenino</span>
              @endif
            </li>
            <li class="list-group-item">
              Estado civil:
              <span class="pull-right">{{$cliente->estado_civil}}</span>
            </li>
            <li class="list-group-item">
              Actividad economica:
              <span class="pull-right">{{$cliente->actividad}}</span>
            </li>
            <li class="list-group-item">
              Direccion recibo de luz:
              <span class="pull-right">{{$cliente->direccion_recibo}} </span>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-12">
        <p><strong>Datos Trabajo:</strong></p>
        <div class="col-md-6">
          <ul class="list-group clear-list">
            <li class="list-group-item fist-item">
              Empresa:
              <span class="pull-right">{{$cliente->empresa_trabajo}}</span>
            </li>
            <li class="list-group-item">
              Tiempo trabajando:
              <span class="pull-right">{{$cliente->tiempo_trabajando}}</span>
            </li>
          </ul>
        </div>
        <div class="col-md-6">
          <ul class="list-group clear-list">
            <li class="list-group-item fist-item">
              Telefono empresa:
              <span class="pull-right">{{$cliente->empresa_trabajo}}</span>
            </li>
            <li class="list-group-item">
              Sueldo:
              <span class="pull-right">{{$cliente->salario}}</span>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-12">
        <p><strong>Referencia personales:</strong></p>
        @foreach ($cliente->referencias as $referencia)
          <div class="col-md-4">
            <ul class="list-group clear-list">
              <li class="list-group-item fist-item">
                Nombre:
                <span class="pull-right">{{$referencia->nombre}} {{$referencia->apellido}}</span>
              </li>
              <li class="list-group-item fist-item">
                Direccion:
                <span class="pull-right">{{$referencia->direccion}}</span>
              </li>
              <li class="list-group-item fist-item">
                Telefono:
                <span class="pull-right">{{$referencia->telefono}}</span>
              </li>
            </ul>
          </div>
        @endforeach
      </div>
  </div>
</div>
