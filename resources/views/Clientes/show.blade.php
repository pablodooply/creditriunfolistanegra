@extends('layouts.app')

@section('title', 'Perfil cliente')

@section('content')

@section('nombre','Perfil Cliente')
@section('ruta')
  <li class="active">
      <strong>Perfil de cliente</strong>
  </li>
@endsection
@include('Clientes.transferencia')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row border-bottom">
          <div class="col-lg-4">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Perfil de cliente</h5>
                      {!!$cliente->obtenerClasificacion()!!}
                  </div>
                  <div>
                      <div class="ibox-content profile-content">
                        <div class="col-xs-12">
                          <div class="profile-image">
                            <img src="{{$cliente->persona->foto_perfil ? $usuario->persona->foto_perfil : asset('images/default/default_perfil.jpg')}}" class="pull-center img-circle circle-border m-b-md" alt="profile">
                          </div>
                        </div>
                        <h4>{{$cliente->persona->nombre}}  {{$cliente->persona->apellido}}</h4>
                        <strong class="text-success">Datos Personales</strong>
                        {{ Form::hidden('id_cliente', $cliente->id, ["id" => "idCliente"]) }}
                        <ul class="list-group clear-list">
                          <li class="list-group-item fist-item">
                              <span class="pull-right">Cli-{{$cliente->id}} </span>
                              Código
                          </li>
                          <li class="list-group-item fist-item">
                              <span class="pull-right">{{$cliente->obtenerEdad()}}</span>
                              Edad
                          </li>
                            <li class="list-group-item">
                                <span class="pull-right"> {{$cliente->persona->dpi}} </span>
                                DPI
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right"> {{$cliente->persona->telefono ? $cliente->persona->telefono : "No hay telefono"}} </span>
                                Téfeono
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right"> {{$cliente->persona->celular1 ?  $cliente->persona->celular1 : "No hay celular"}}</span>
                                Celular 1
                            </li>
                            {{-- <li class="list-group-item">
                                <span class="pull-right text-danger">0</span>
                                Saldo acumulado
                            </li> --}}
                        </ul>
                          @if(Auth::user()->hasAnyRole(['Administrador', 'Supervisor', 'Secretaria','Promotor']))
                            <div class="user-button">
                              <div class="row">
                                  <div class="col-md-6">
                                      @if($cliente->listabn_id == 1)
                                        @if(Auth::user()->hasAnyRole(['Administrador']))
                                          <button id="enviar"  value="{{$cliente->id}}"type="button" class="btn btn-warning btn-sm" ><i class="fa fa-envelope"></i><span id="enviar_ln" value="1"> Enviar a lista negra </span></button>
                                        @endif
                                        @if(Auth::user()->hasAnyRole(['Administrador','Supervisor']))
                                          <a class="btn btn-success btn-sm" id="update_gps">Actualizar ubicacion</a>
                                        @endif
                                        @if(($cliente->prestamos->last()!==null ? $cliente->prestamos->last()->estado_p_id : 9)  == '9')
                                          <a href="{{route('creditos.renovacion', $cliente->id)}}" class="btn btn-success btn-sm" name="button">Renovacion</a>
                                        @else
                                          @if(Auth::user()->hasAnyRole(['Administrador', 'Secretaria']))
                                            <a href="{{route('creditos.renovacion', $cliente->id)}}" class="btn btn-success btn-sm" name="button">Renovacion</a>
                                          @else
                                            <a  class="btn btn-success btn-sm" name="button" disabled>Renovacion</a>
                                          @endif
                                        @endif
                                      @else
                                        @if(Auth::user()->hasRole('Administrador'))
                                          <button id="enviar" value="{{$cliente->id}}"type="button" class="btn btn-warning btn-sm" ><i class="fa fa-envelope"></i><span id="enviar_ln" value="2"> Enviar a lista blanca </span></button>
                                        @endif
                                        @if(Auth::user()->hasAnyRole(['Administrador','Supervisor']))
                                          <a class="btn btn-success btn-sm" id="update_gps">Actualizar ubicacion</a>
                                        @endif
                                          <a href="{{route('creditos.renovacion', $cliente->id)}}" class="btn btn-success btn-sm" name="button">Renovacion</a>
                                      @endif
                                      @if(Auth::user()->hasAnyRole(['Administrador', 'Supervisor']))
                                          <button id="transferir" type="button" class="btn btn-primary btn-sm "
                                          data-toggle="modal" data-target=".bd-example-modal-lg">Transferir</button>
                                      @endif
                                  </div>
                              </div>
                          </div>
                          @endif
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Direccion</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <address>
                            <strong>Domicilio:</strong><br>
                            {{$cliente->persona->domicilio}}<br>
                        </address>
                        <address>
                            <strong>Trabajo:</strong><br>
                            {{$cliente->empresa_trabajo}}<br>
                            {{$cliente->direccion_trabajo}}<br>
                        </address>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Referecias Personales</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                      @foreach ($cliente->referencias as $referencia)
                        <address>
                          <strong>{{$referencia->nombre}}</strong><br>
                          {{$referencia->direccion}}<br>
                          <abbr title="Phone"></abbr> {{$referencia->telefono}}
                        </address>
                      @endforeach
                    </div>
                </div>
            </div>
          </div>
          <div class="col-lg-8 col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Prestamos</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                  <div class="tabs-container">
                    <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#prestamos">Prestamos</a></li>
                      <li><a data-toggle="tab" href="#informacion">Informacion</a></li>
                      <li><a data-toggle="tab" href="#imagenes">Imagenes</a></li>
                    </ul>
                    <div class="tab-content">
                      <div id="prestamos" class="tab-pane active">
                        <div class="table-responsive">
                          <table class='table table-striped table-hover'>
                            <thead>
                              <tr>
                                <th>Código</th>
                                <th>Estado</th>
                                <th>Monto</th>
                                <th>Mora</th>
                                <th>Clasificación</th>
                                <th>Acciones</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($cliente->prestamos as $prestamo)
                                <tr>
                                  <td>Cre-{{$prestamo->id}}</td>
                                  {!!$prestamo->obtenerEstado()!!}
                                  <td>{{$prestamo->monto}}</td>
                                  <td>{{$prestamo->mora}}</td>
                                  {!!$prestamo->obtenerClasificacion()!!}
                                  <td>
                                    @if ($prestamo->estado_p_id == 1 || $prestamo->estado_p_id == 2 || $prestamo->estado_p_id == 15)
                                      <a href="#" class="btn btn-outline btn-success btn-sm" disabled><i class="fa fa-eye"></i></a>
                                    @else
                                      <a href="{{route('creditos.show',$prestamo->id)}}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                    @endif
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div id="informacion" class="tab-pane">
                          @include('Clientes.infoCliente')
                      </div>

                      <div id="imagenes" class="tab-pane">
                        <div class="row">
                          {!! Form::open(['id'=> 'form-images', 'route' => ['clientes.add_img',$cliente->id],'method' => 'POST', 'files' => true]) !!}
                            <div class="col-sm-12">
                              <h4>Imagen DPI</h4>
                              @if($cliente->foto_dpi == null)
                                <h5>No tiene imagen de DPI, seleccione la imagen de DPI</h5>
                                {!! Form::file('img_dpi', null) !!}
                              @else
                                <img class="img-responsive" alt="image" src="{{asset('images/clientes/documentos')."/".$cliente->foto_dpi}}">
                              @endif
                              <br>
                              <button type="submit" value="1" class="btn btn-primary btn-sm add_img">Agregar imagenes</button>
                            </div>


                            {{Form::close()}}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
      </div>

    </div>
@endsection

@section('scripts')
  <script src="{!! asset('js/slimscroll/jquery.slimscroll.min.js') !!}"></script>
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>


<script type="text/javascript">
  $("#enviar").click(function(){
    var valor = $(this).attr("value");
    var url = '{{ route("clientes.ln", ":id") }}';
    var tipo_cliente = $('#enviar_ln').attr('value');
    if(tipo_cliente == 1){
      var title_swal = "Desea bloquear a este cliente?";
      var content = "Esta seguro de bloquear a este cliente?";
    } else{
      var title_swal = "Desea desbloquear a este cliente?";
      var content = "Esta seguro de desbloquear a este cliente?";
    }
    swal({
      title: title_swal ,
      text: content,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        url = url.replace(':id', valor);
        $.ajax({
            type: "GET",
            url: url,
            success: function( response ) {
              if(response == 1){
                swal("Cliente desbloqueado", {
                  icon: "success",
                });
                 $('#enviar_ln').text(' Enviar a lista negra');
                 $('#enviar_ln').attr('value', 1);
              }else{
                swal("Cliente bloqueado", {
                  icon: "success",
                });
                $('#enviar_ln').text(' Enviar a lista blanca');
                $('#enviar_ln').attr('value', 2);
              }
            }
        });
      } else {
        swal("No se realizo ninguna accion");
      }
    });

  });

  $("#hoja_ruta").change(function(){
    var valor = $("#hoja_ruta :selected").val(); // The text content of the selected option
    var url = '{{ route("ruta.promotor", ":id") }}';
    url = url.replace(':id', valor);
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $("#nom_promotor").val(response);
        }
    });
  });


  $('[data-toggle="tab"]').click(function(e){
    // var now_tab = e.target // activated tab
    var valor = $(this);
    console.log(valor);
  });

  $('#update_gps').click(function(){
    swal({
      title: "Esta seguro de actualizar la ubicacion del cliente??",
      text: "Se cambiara la ubicacion del cliente",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        activeGps();
      } else {
        swal("No se realizaron cambios");
      }
    });
  })

  function activeGps(){
      var id = $("#idCliente").val();
      if (navigator.geolocation)
      {
        /* la geolocalizacion esta disponible */
        navigator.geolocation.getCurrentPosition(function(position){
          var latitud = position.coords.latitude;
          var longitud = position.coords.longitude;
          sendLocation(latitud, longitud, id);
        });

      }
      else
      {
        swal("No se puedo cambiar la actualizacion, verifique si tiene activo el gps.", {
          icon: "error",
        });
      }
  }

  function sendLocation(lat, lon, id){
    // var id = $(this).attr("value");
    console.log(lat + " " + lon + " " + id);
    url = '{{url('coordenadas/cliente/')}}';
    $.ajax({
        type: "GET",
        url: url,
        data: {
          'latitude' : lat,
          'longitude': lon,
          'secret'  : id,
        },success: function(html){
          swal("Ubicacion cambiada!", {
            icon: "success",
          });
        }
    });
  }

</script>
@endsection
