@extends('layouts.app')



@section('title', 'Cumpleañeros')





@section("link")

<link href="{{asset('css/rangedatepicker/daterangepicker.css')}}" rel="stylesheet"  media="screen">

@endsection

@section('content')





@section('nombre','Cumpleañeros')

@section('ruta')

  <li class="active">

      <strong>Cumpleañeros</strong>

  </li>

@endsection





@php

  use Carbon\Carbon;

  setlocale(LC_TIME, 'Spanish');

  Carbon::setUtf8(true);



@endphp

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row">

                    <div class="col-sm-12">

                      <div class="ibox float-e-margins">

                        <div class="ibox-title">

                          <h5>Clientes cumpleañeros</h5>

                        </div>

                        <div class="ibox-content">

                          <div class="row">

                            <div class="col-md-4">

                              <label>Tipo:</label>

                                <select class="form-control" name="tipo" id="tipo">

                                  <option value="1">Activos</option>

                                  <option value="0">No activos</option>

                                </select>

                            </div>

                            <div class="col-md-4">

                              <label>Mes:</label>

                              <select class="form-control" name="tipo" id="daterange">
                              </select>

                            </div>
                            <div class="col-md-4">

                              <label>Día:</label>

                              <select class="form-control" name="tipo" id="dayrange">
                              </select>

                            </div>

                            <div class="col-xs-12 col-md-12">

                              <div class="table-responsive">

                                <table id="tabla-cumple" class='table table-striped table-hover'>

                                  <thead>

                                    <tr>

                                      <th>Código</th>

                                      <th>Cliente</th>

                                      <th>Clas.</th>

                                      <th>Cumpleaños</th>

                                      <th>Prestamo</th>

                                      <th>Monto</th>

                                      <th>Ruta</th>

                                      <th>Promotor</th>

                                      <th>Acciones</th>

                                    </tr>

                                  </thead>

                                  <tbody>

                                    @foreach ($personas->sortBy('fecha_nacimiento') as $persona)

                                      @if($persona->cliente->first() == null)

                                      @else

                                        <tr>

                                          <td><a class="client-link" href="{{route('clientes.show', $persona->cliente->first()->id) }}">Cli-{{$persona->cliente->first()->id}}</a></td>

                                          <td>{{$persona->nombre}} {{$persona->apellido}}</td>

                                          @php

                                            $ClienteClas = \App\Cliente::find($persona->cliente->first()->id);

                                            $ClasCliente = \App\Prestamo::where('cliente_id',$ClienteClas->id)->orderBy('id','DESC')->first();

                                          @endphp

                                          @switch(isset($ClasCliente->clasificacion_id) ? $ClasCliente->clasificacion_id : 0)

                                            @case(0)

                                            <td><span class="badge badge-primary">A</span></td>

                                            @break

                                            @case(1)

                                            <td><span class="badge badge-primary">A</span></td>

                                            @break

                                            @case(2)

                                            <td><span class="badge badge-warning">B</span></td>

                                            @break

                                            @case(3)

                                            <td><span class="badge badge-danger">C</span></td>

                                            @break

                                          @endswitch

                                          <td>{{Carbon::parse($persona->fecha_nacimiento)->format('d-m-Y')}}</td>

                                          <td>{{isset($persona->cliente->first()->prestamos->last()->id) ? "Cre-".$persona->cliente->first()->prestamos->last()->id : "No hay"}}</td>

                                          <td>{{isset($persona->cliente->first()->prestamos->last()->monto) ? $persona->cliente->first()->prestamos->last()->monto : "No hay"}}</td>

                                          <td>{{isset($persona->cliente->first()->prestamos->last()->ruta) ? $persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->nombre : "No hay"}}</td>

                                          <td>{{isset($persona->cliente->first()->prestamos->last()->ruta) ? $persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->user->persona->nombre . " " . $persona->cliente->first()->prestamos->first()->ruta->first()->hoja_ruta->user->persona->apellido : "No hay"}}</td>

                                          {{-- <td>{{$persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->nombre</td> --}}

                                          {{-- <td>{{isset($persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->user->persona->nombre) ? $persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->user->persona->nombre . " " . $persona->cliente->first()->prestamos->first()->ruta->first()->hoja_ruta->user->persona->apellido : "No hay"}}</td> --}}

                                          <td><a class="btn btn-sm btn-outline btn-primary" href="{{route('clientes.show',$persona->cliente->first()->id)}}"><span class="fa fa-eye"></span></a>

                                            @if(Auth::user()->hasRole('Administrador'))

                                              <a value="{{$persona->cliente->first()->id}}" class="btn btn-outline btn-sm btn-danger quitar_cumple"><span class="fa fa-times"></span></a></td>

                                            @endif

                                        </tr>

                                      @endif

                                    @endforeach

                                  </tbody>

                                </table>

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                </div>

            </div>

@endsection



@section('scripts')



  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

  <script type="text/javascript" src="{{asset('js/moment/moment.js')}}"></script>

  <script type="text/javascript" src="{{asset('js/rangedatepicker/daterangepicker.js')}}"></script>

  {{-- <script scr="{{asset('js/sweetalert/sweetalert.min.js')}}"></script> --}}

  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>



<script type="text/javascript">

var oTable;

$(document).ready(function(){

  oTable = $('#tabla-cumple').DataTable({

    "language": {

        "url": "{{asset('fonts/dataTablesEsp.json')}}",

    },

    "paging":   true,

    "info":     false,

    'dom' : 'tip',

    "processing": true,

    // "serverSide": true,

    dom: '<"html5buttons" B>tip',

    buttons: [

        {extend: 'excel',

        title: 'Creditos',

        },

        {extend: 'pdf',

        title: 'Creditos activos',

        },

        {extend: 'print',

         customize: function (win){

                $(win.document.body).addClass('white-bg');

                $(win.document.body).css('font-size', '10px');



                $(win.document.body).find('table')

                        .addClass('compact')

                        .css('font-size', 'inherit');

        }

      },

    ]

  });



$('#busqueda-cumple').keyup(function(){

        oTable.search($(this).val()).draw();

  });

  // var select = $("#rangedate")
  var select = document.getElementById("daterange");
  var meses = [{
        value: "1",
        text: "Enero"
      },
      {
        value: "2",
        text: "Febrero"
      }, {
        value: "3",
        text: "Marzo"
      }, {
        value: "4",
        text: "Abril"
      }, {
        value: "5",
        text: "Mayo"
      }, {
        value: "6",
        text: "Junio"
      }, {
        value: "7",
        text: "Julio"
      }, {
        value: "8",
        text: "Agosto"
      }, {
        value: "9",
        text: "Septiembre"
      }, {
        value: "10",
        text: "Octubre"
      }, {
        value: "11",
        text: "Noviembre"
      }, {
        value: "12",
        text: "Diciembre"
      }
    ],
    option,
    i = 0,
    il = meses.length;

  for (; i < il; i++) {
    option = document.createElement('option');
    option.setAttribute('value', meses[i].value);
    option.appendChild(document.createTextNode(meses[i].text));
    select.appendChild(option);
  }











  var selectday = document.getElementById("dayrange");
  var dias = [{
        value: "1",
        text: "1"
      },
      {
        value: "2",
        text: "2"
      }, {
        value: "3",
        text: "3"
      }, {
        value: "4",
        text: "4"
      }, {
        value: "5",
        text: "5"
      }, {
        value: "6",
        text: "6"
      }, {
        value: "7",
        text: "7"
      }, {
        value: "8",
        text: "8"
      }, {
        value: "9",
        text: "9"
      }, {
        value: "10",
        text: "10"
      }, {
        value: "11",
        text: "11"
      }, {
        value: "12",
        text: "12"
      }, {
        value: "13",
        text: "13"
      }, {
        value: "14",
        text: "14"
      }, {
        value: "15",
        text: "15"
      }, {
        value: "16",
        text: "16"
      }, {
        value: "17",
        text: "17"
      }, {
        value: "18",
        text: "18"
      }, {
        value: "19",
        text: "19"
      }, {
        value: "20",
        text: "20"
      }, {
        value: "21",
        text: "21"
      }, {
        value: "22",
        text: "22"
      }, {
        value: "23",
        text: "23"
      }, {
        value: "24",
        text: "24"
      }, {
        value: "25",
        text: "25"
      }, {
        value: "26",
        text: "26"
      }, {
        value: "27",
        text: "27"
      }, {
        value: "28",
        text: "28"
      }, {
        value: "29",
        text: "29"
      }, {
        value: "30",
        text: "30"
      }, {
        value: "31",
        text: "31"
      }
    ],
    option,
    idia = 0,
    ildia = dias.length;



  for (; idia < ildia; idia++) {
    option = document.createElement('option');
    option.setAttribute('value', dias[idia].value);
    option.appendChild(document.createTextNode(dias[idia].text));
    selectday.appendChild(option);
  }






  $("#daterange").change(function(e){

    obtenerClientesCumple();

  });

  $("#dayrange").change(function(e){

    obtenerClientesCumple();

  });



  function obtenerClientesCumple(){

    var tabla = $("#tabla-cumple tbody")

    // var rango_fechas = $("#daterange").val();
    var mes = $("#daterange").val();
var dia = $("#dayrange").val();
    // var valores = rango_fechas.split(' - ');

    // var inicio = valores[0];

    // var fin = valores[1];

    // console.log(inicio);

    var url = "{{route('info.cumpleanios')}}";

    var tipo = $('#tipo :selected').val();

    $.ajax({

        type: "GET",

        url: url,

        data: {

          'mes' : mes,

          // 'fin'    : fin,
          'dia' : dia,

          'tipo'  : tipo,

      },

        success: function( response ) {

          var table = $('#tabla-cumple').DataTable();

          datos = response;

          table.clear().draw();

          console.log(response);

          if(response==null){



          } else{

            var url_cliente = "{{route('clientes.show', ':id')}}";

            for (var i = 0; i < datos.length; i++) {

              var datos_ac = datos[i];

              var url2 = url_cliente;

              var temp = "";

              url2 = url2.replace(':id', datos_ac.codigo);

              if(datos_ac.tipo == 1){

                temp = "<a class='btn btn-outline btn-sm btn-success' href=" + url2 + "><span class='fa fa-eye'></span></a><a value=" + datos_ac.codigo +" class='btn btn-outline btn-sm btn-danger quitar_cumple'><span class='fa fa-times'></span></a>";

              } else{

                temp = "<a class='btn btn-outline btn-sm btn-success' href=" + url2 + "><span class='fa fa-eye'></span></a>";

              }

              table.row.add([

                'Cre-' + datos_ac.codigo,

                datos_ac.cliente,

                datos_ac.clasificacion,

                datos_ac.fecha,

                datos_ac.prestamo,

                datos_ac.monto,

                datos_ac.hoja_ruta,

                datos_ac.promotor,

                temp,

              ]).draw();

            }

          }

        }

    });

  }



  $('#tabla-cumple').on('click','.quitar_cumple',function(){

    var id = $(this).attr('value');

    swal({

      title: "Esta seguro de quitar al cliente de la lista de cumpleañeros?",

      text: "el cliente sera quitado de la lista",

      icon: "warning",

      buttons: true,

      dangerMode: true,

    })

    .then((willDelete) => {

      if (willDelete) {

        quitarCumple(id);

        // $("#formulario-del").submit();

      } else {

        swal("Cliente sigue en la lista", {

          icon: "warning"

        });

      }

    });

  });

  function quitarCumple(id){

    var url = "{{route('clientes.quitarCumple',':id')}}";

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    url = url.replace(':id',id);

    $.ajax({

      type: "POST",

      url: url,

      data: {_token: CSRF_TOKEN},

      // dataType: 'JSON',

      success: function () {

        swal("Cliente quitado de la lista", {

          icon: "success",

        });

        obtenerClientesCumple();

      }

    });

  }

});





</script>



@endsection
