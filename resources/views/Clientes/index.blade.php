@extends('layouts.app')



@section('title', 'Clientes')



@section('content')



@section('nombre','Clientes')

@section('ruta')

  <li class="active">

      <strong>Listado de clientes</strong>

  </li>

@endsection
@php
    $fecha = \Carbon\Carbon::parse('2019-01-01')->format('Y-m-d');
    $fecha2 = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
@endphp
<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-sm-8">
                  <div class="ibox float-e-margins">
                    <div class="ibox-title">
                      <h5>Listado de clientes</h5>
                      <div class="ibox-tools">
                                <a href="/cliente/create/unique" class="btn btn-success btn-xs text-white"><i class="fa fa-plus"></i> Agregar Cliente</a>
                            </div>
                    </div>
                    <div class="ibox-content">
                      <div class="tabs-container">

                        <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#clientes">Clientes</a></li>
                          <li><a data-toggle="tab" href="#listaNegra">Lista de bloqueados</a></li>
                        </ul>
                        <div class="tab-content">
                          <div id="clientes" class="tab-pane active">
                            <div class="row">

                            <div class="input-group">
                              <input type="text" placeholder="Código: Cli-123, Cliente: Nombre, Prestamo: Cre-123" class="input form-control" id="busqueda-clientes">
                              <span class="input-group-btn">
                                <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                              </span>
                            </div>
                              <div class="row">
                                <form method="POST" id="search-form-total">
                                  <div class="input-daterange form-row" data-plugin="datepicker" data-option="{}" id="datepicker">
                                    <div class="form-group col-md-12" id="data_1">
                                        <div class="form-group col-md-6">
                                          {{ Form::label('name', 'Desde' ) }} <input type="date" id="fecha1" class="form-control" name="fecha1" value="{{$fecha}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                          {{ Form::label('name', 'Hasta ' ) }}<input type="date" id="fecha2" class="form-control" name="fecha2" value="{{$fecha2}}">
                                        </div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                            <ul class="nav">
                              <span class="pull-left text-muted"> {{count($clientes)}} Clientes</span>
                            </ul>
                            <div class="table-responsive">
                              <table class='table table-striped table-hover' id="tabla-clientes">
                                <thead>
                                  <tr>
                                    <th>Código</th>
                                    <th>Clasificación</th>
                                    <th>Nombre</th>
                                    <th>Fecha de ingreso</th>
                                    <th>Tipo de cliente</th>
                                    <th>DPI</th>
                                    <th>Prestamo</th>
                                    <th>Acciones</th>
                                  </tr>
                                </thead>
                                  </table>
                                </div>
                          </div>
                          <div id="listaNegra" class="tab-pane">
                            <div class="input-group">
                              <input type="text" placeholder="Codigo: Cre-123, Cliente: Nombre del cliente" class="input form-control" id="busqueda-clientes-listaNegra">
                              <span class="input-group-btn">
                                <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                              </span>
                            </div>
                            <ul class="nav">
                              <span class="pull-left text-muted"> {{count($clientes_listaNegra)}} Clientes</span>
                            </ul>
                            <div class="table-responsive">
                              <table class='table table-striped table-hover' id="tabla-cliente-listaNegra">
                                <thead>
                                  <tr>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>DPI</th>
                                    <th>Prestamo</th>
                                    <th>Acciones</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @if($clientes_listaNegra->isEmpty())
                                    <h2> No hay clientes </h2>
                                  @else
                                    @foreach ($clientes_listaNegra as $cliente)
                                      <tr>
                                        <td><a class="client-link" value='{{$cliente->id}}'>Cli-{{$cliente->id}}</a></td>
                                        <td>{{$cliente->persona->nombre}} {{$cliente->persona->apellido}}</td>
                                        <td>{{$cliente->persona->dpi}}</td>
                                          <td>{{(isset($cliente->prestamos->last()->id)) ? "Cred-" .$cliente->prestamos->last()->id : "No hay"}}
                                            {{-- {{ dd(isset($cliente->prestamos))}} --}}
                                            {!!($cliente->prestamos->last()!==null) ? $cliente->prestamos->last()->obtenerEstado2() : "No hay status"!!}</td>
                                            <td class="project-actions">
                                              <a href="{{route('clientes.show',$cliente->id)}}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                              @if(Auth::user()->hasRole('Administrador'))
                                                <a href="{{route('clientes.edit',$cliente->id)}}" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>
                                              @endif
                                            </td>
                                          </tr>
                                        @endforeach
                                      @endif
                                    </tbody>
                                  </table>
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="ibox ">
                      <div id='resumen_perfil' class="ibox-content">
                      <div class="tab-content">
                        @if($clientes->isEmpty())
                          <h3>No hay clientes</h3>
                        @else
                        <div id="contact-1" class="tab-pane active">
                            <div class="row m-b-lg">
                                <div class="col-lg-12 text-center">
                                    <h2>{{$clientes->first()->persona->nombre}} {{$clientes->first()->persona->apellido}}</h2>
                                </div>
                            </div>
                            <div class="client-detail">
                              <div class="full-height-scroll">
                                  <strong>Datos Personales</strong>
                                  <ul class="list-group clear-list">
                                      <li class="list-group-item fist-item">
                                          <span class="pull-right"> 24 </span>
                                          Edad
                                      </li>
                                      <li class="list-group-item fist-item">
                                          <span class="pull-right">{{$clientes->first()->persona->dpi}}</span>
                                          Dpi
                                      </li>
                                      <li class="list-group-item">
                                          <span class="pull-right">{{$clientes->first()->persona->nit}}</span>
                                          Nit
                                      </li>
                                      <li class="list-group-item">
                                          <span class="pull-right">{{$clientes->first()->persona->telefono}}</span>
                                          Telefono
                                      </li>
                                      <li class="list-group-item">
                                          <span class="pull-right">{{$clientes->first()->persona->celular1}}</span>
                                          Celular 1
                                      </li>
                                      <li class="list-group-item">
                                          <span class="pull-right">{{$clientes->first()->persona->celular2}}</span>
                                          Celular 2
                                      </li>
                                  </ul>
                                  <strong>Prestamos</strong>
                                  <table class='table table-striped table-hover'>
                                    <thead>
                                      <tr>
                                        <th>Código</th>
                                        <th>Estado</th>
                                        <th>Clasificación</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach ($clientes->first()->prestamos as $prestamo)
                                          <tr>
                                            <td>{{$prestamo->id}}</td>
                                            {!!$prestamo->obtenerEstado()!!}
                                            {!!$prestamo->obtenerClasificacion()!!}
                                          </tr>
                                      @endforeach
                                    </tbody>
                                  </table>
                              </div>
                            </div>
                        </div>
                      @endif
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $.fn.dataTable.ext.errMode = 'none';

    var oTable=$('#tabla-clientes').DataTable({
      order: [[ 0, "desc" ]],
      language: {
          "url": "{{asset('fonts/dataTablesEsp.json')}}",
      },
      paging: true,
      info: false,
      dom : 'tip',
      processing: true,
      serverSide: true,
      ajax: {
        "url": '{{route('clientes.tableCliente')}}',
        'beforeSend': function (request) {
         request.setRequestHeader("X-CSRF-TOKEN", '{{ csrf_token() }}');
         },
         data: function(d) {


         d.fecha1 = $('input[name=fecha1]').val();
         d.fecha2 = $('input[name=fecha2]').val();
         }
      },
      pageLength: 10,
      responsive: true,
      dom: '<"html5buttons"B>lTfgitp',
      buttons: [
        {extend: 'copy'},
        {extend: 'csv'},
        {extend: 'excel', title: 'Asesores'},
        {extend: 'pdf', title: 'Asesores'},
        {extend: 'print',
           customize: function (win)
           {
              $(win.document.body).addClass('white-bg');
              $(win.document.body).css('font-size', '10px');

              $(win.document.body).find('table')
                      .addClass('compact')
                      .css('font-size', 'inherit');
          }
        }
      ],
      columns: [
        {data: 'Codigo', name: 'Codigo'},
        {data: 'clasificacion', name: 'clasificacion', className: 'text-center'},
        {data: 'Nombre', name: 'Nombre'},
        {data: 'Fecha', name: 'Fecha'},
        {data: 'Tipo', name: 'Tipo'},
        {data: 'DPI', name: 'DPI'},
        {data: 'Prestamo', name: 'Prestamo'},
        {data: 'Acciones', name: 'Acciones'},
      ],

    });


      $('#search-form-total').on('change', function(e) {
        var a = $('input[name=fecha1]').val();
        var b = $('input[name=fecha2]').val();
        oTable.draw();

      });

      $('#busqueda-clientes').keyup(function(){

            oTable.search($(this).val()).draw() ;

      });



      var oTable2 = $('#tabla-cliente-listaNegra').DataTable({

        "language": {

          "url": '{{asset('fonts/dataTablesEsp.json')}}',

          },

        "paging":   true,

        "info":     false,

        'dom' : 'tip',

      });



      $('#busqueda-clientes-listaNegra').keyup(function(){

            oTable2.search($(this).val()).draw() ;

      });





    	$("#tabla-clientes tbody").on('click', '.client-link',function(){

          var valor = $(this).attr("value");

          var url = '{{ route("clientes.show", ":id") }}';

          url = url.replace(':id', valor);

          $.ajax({

              type: "GET",

              url: url,

              success: function( response ) {

                $('#resumen_perfil').html(response);

              }

          });



    	});

});



</script>

@endsection
