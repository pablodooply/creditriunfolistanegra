
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style type="text/css">
    @media print {
        body {
        /* transform: scale(.5); */
        margin-top: 1cm;
        /* : 2.5cm; */
        margin-left: 3cm;
        margin-right: 25cm;
        zoom: 50%;
    }

    .estilo{
      height: 8mm;
    }

    .col1{
      width: 7mm;
    }

    .col2{
      width: 16mm;
    }

    .texto{
      font-size: 17px;
    }
}
        /* table {page-break-inside: avoid;
        } */
    </style>
  </head>
  <body>
    <div style="border: black 1px solid; width:600px">
      <b class="texto"><h4 style="text-align: right;">PAGO A COLABORADOR &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h4></b>
      <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
      <b class="texto"><p style="text-align: left;">&nbsp; &nbsp;&nbsp; &nbsp; Nombre:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{$colaborador->persona->nombre}} {{$colaborador->persona->apellido}} </p></b>
      <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
      <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
      <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
      <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>

            <div class="table-responsive">
        <table border="1" style="border-collapse: collapse; width: 100%;">
          <tbody>
            <tr>
              <td class="estilo col1">No.</td>
              <td class="estilo col2`">Fecha</td>
              <td class="estilo col2">Sueldo</td>
              <td class="estilo col1">Comisión 1</td>
              <td class="estilo col1">Comisión 2</td>
              <td class="estilo col2">Descuento</td>
              <td class="estilo col2">Combustible</td>
              <td class="estilo col1">Total</td>
            </tr>
            <tr>
              <td class="estilo col1">1</td>
              <td class="estilo col2">{{ \Carbon\Carbon::parse($boleta->fecha)->format('d/m/Y')}}</td>
              <td class="estilo col2">{{$boleta->sueldo_base}}</td>
              <td class="estilo col1">{{$boleta->comision_clientes}}</td>
              <td class="estilo col1">{{$boleta->comision_capital}}</td>
              <td class="estilo col1">{{$boleta->descuento_mora}}</td>
              <td class="estilo col1">{{$boleta->combustible}}</td>
              <td class="estilo col1">{{$boleta->total}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <p style="text-align: left;"></p>
  </body>
</html>
<script>
       window.print();
       window.close();
</script>
