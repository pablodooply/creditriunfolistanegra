<div class="row">

    <div class="col-md-12">


      <div class="col-lg-6 col-md-6 col-sm-6">

          <div class="widget style1 navy-bg">

            <div class="row">

              <a class="client-link" target="_blank" href="{{route('printComFirma', $credito->id)}}">

                <div class="col-md-12">

                  <div class="col-lg-4 col-md-2 text-center text-uppercase	">

                    <i class="fa fa-pencil  fa-5x"></i>

                  </div>

                  <div class="col-md-8  text-center">

                    <h3 class="">Contrato con Firma </h3>
                  </div>

                </div>

              </a>

            </div>

          </div>

      </div>

      <div class="col-lg-6 col-md-6 col-sm-6">

          <div class="widget style1 lazur-bg">

            <div class="row">

              <a class="client-link" target="_blank" href="{{route('printComHuella', $credito->id)}}">

                <div class="col-md-12">

                <div class="col-md-4  text-center">

                  <i class="fa fa-plus-square fa-5x"></i>

                </div>

                <div class="col-md-8 text-center">

                  <h3>Contrato con Huella</h3>
                </div>

              </div>

              </a>

            </div>

          </div>

      </div>

    </div>

</div>
