<head>
<style>
p {
line-height: 250%   /*esta es la propiedad para el interlineado*/
}
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536859905 -1073732485 9 0 511 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:106%;
	mso-pagination:widow-orphan;
	font-size:13.0pt;
	font-family:"Calibri",sans-serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
	{mso-style-priority:1;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
  font-size:13.0pt;
	font-family:"Calibri",sans-serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
p.msonormal0, li.msonormal0, div.msonormal0
	{mso-style-name:msonormal;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:13.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
p.msochpdefault, li.msochpdefault, div.msochpdefault
	{mso-style-name:msochpdefault;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:14.0pt;
	font-family:"Calibri",sans-serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
p.msopapdefault, li.msopapdefault, div.msopapdefault
	{mso-style-name:msopapdefault;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:106%;
	mso-pagination:widow-orphan;
	font-size:13.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:13.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Calibri",sans-serif;
	mso-ascii-font-family:Calibri;
	mso-hansi-font-family:Calibri;
	mso-bidi-font-family:Calibri;}
.MsoPapDefault
	{mso-style-type:export-only;
	margin-bottom:8.0pt;
	line-height:106%;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:70.85pt 3.0cm 70.85pt 3.0cm;
	mso-header-margin:35.4pt;
	mso-footer-margin:35.4pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Tabla normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin-top:0cm;
	mso-para-margin-right:0cm;
	mso-para-margin-bottom:8.0pt;
	mso-para-margin-left:0cm;
	line-height:106%;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Calibri",sans-serif;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>
<div class=WordSection1>
<br><br>
<p class=MsoNormal align=center style='text-align:center'><b></b><b><span
lang=EN-US style='mso-ansi-language:EN-US'>  </span></b></p>
@php
//use NumerosEnLetras;
$fecha_mes = Carbon\Carbon::now()->format('m');
$mes = '';
if ($fecha_mes  == 1) {
  $mes = 'Enero';
}
if ($fecha_mes  == 2) {
  $mes = 'Febrero';
}
if ($fecha_mes  == 3) {
  $mes = 'Marzo';
}
if ($fecha_mes  == 4) {
  $mes = 'Abril';
}
if ($fecha_mes  == 5) {
  $mes = 'Mayo';
}
if ($fecha_mes  == 6) {
  $mes = 'Junio';
}
if ($fecha_mes  == 7) {
  $mes = 'Julio';
}
if ($fecha_mes  == 8) {
  $mes = 'Agosto';
}
if ($fecha_mes  == 9) {
  $mes = 'Septiembre';
}
if ($fecha_mes  == 10) {
  $mes = 'Octubre';
}
if ($fecha_mes  == 11) {
  $mes = 'Noviembre';
}
if ($fecha_mes  == 12) {
  $mes = 'Dciembre';
}
@endphp
<p class=MsoNoSpacing align=justify style=' line-height:24pt;'> En el municipio de Retalhuleu, del departamento de Retalhuleu, el {{\NumerosEnLetras::convertir(Carbon\Carbon::now()->format('d'))}}
de {{$mes}} de {{\NumerosEnLetras::convertir(Carbon\Carbon::now()->format('Y'))}}, <strong>NOSOTROS: AMANDA BEATRIZ OVANDO PALACIOS,</strong> de treinta y cuatro años de edad, casada, guatemalteca,
comerciante, de este domicilio, me identifico con el Documento Personal de Identificación que corresponde al Código Único de Identificación número dos mil quinientos noventa y nueve espacio setenta
y siete mil doscientos diecisiete espacio cero novecientos uno (2599 77217 0901) Extendido por el Registro Nacional de las Personas de la República de Guatemala, comparezco en mi calidad de Representante
Legal de la empresa <strong>CREDITRIUNFO SOCIEDAD ANÓNIMA,</strong> la cual acredito con la fotocopia autenticada del acta de nombramiento faccionada en la ciudad de Quetzaltenango por el Notario
<strong> JUAN RAMON MORALES RIVERA</strong> de fecha cuatro de octubre de dos mil diecinueve, inscrita en el Registro Mercantil bajo el número ciento treinta y seis mil doscientos diecinueve. (136219),
Folio novecientos treinta y seis (936) del libro doscientos veintinueve (229) de Auxiliares de Comercio y <strong>{{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</strong>,
de {{\NumerosEnLetras::convertir($credito->cliente->obtenerEdad())}} años de edad,
 @php
 if($credito->cliente->persona->genero==0)
 {
   if($credito->cliente->estado_civil=='Soltero')
   {
     $estadoCivil='Soltera';
   }
   if($credito->cliente->estado_civil=='Casado')
   {
     $estadoCivil='Casada';
   }
   if($credito->cliente->estado_civil=='Divorciado')
   {
     $estadoCivil='Divorciada';
   }
   if($credito->cliente->estado_civil=='Viudo')
   {
     $estadoCivil='Viuda';
   }
   if($credito->cliente->estado_civil=='Unión de hecho')
   {
     $estadoCivil='Unida de hecho';
   }
 }
 if($credito->cliente->persona->genero==1)
 {
   if($credito->cliente->estado_civil=='Soltero')
   {
     $estadoCivil='Soltero';
   }
   if($credito->cliente->estado_civil=='Casado')
   {
     $estadoCivil='Casado';
   }
   if($credito->cliente->estado_civil=='Divorciado')
   {
     $estadoCivil='Divorciado';
   }
   if($credito->cliente->estado_civil=='Viudo')
   {
     $estadoCivil='Viudo';
   }
   if($credito->cliente->estado_civil=='Unión de hecho')
   {
     $estadoCivil='Unido de hecho';
   }
 }

 $interes=100*$credito->plan->interes;
 $totalPorcentaje=round($interes/$credito->plan->total,3);
 $dpi1=\NumerosEnLetras::convertir(substr($credito->cliente->persona->dpi, 0, 4));
 $ex_dpi2 = substr($credito->cliente->persona->dpi, 5, 5);
 // dd($ex_dpi2);
 // $multi = 1;
 $dpi2=\NumerosEnLetras::convertir($ex_dpi2);
 $dpi3=\NumerosEnLetras::convertir(substr($credito->cliente->persona->dpi, 11, 15));
 @endphp
{{$estadoCivil}}, {{$credito->cliente->nacionalidad}}, {{$credito->cliente->actividad}},
con domicilio en {{$credito->cliente->direccion_recibo}}, me identifico con el Documento Personal de Identificación que corresponde al Código Único de Identificación número
{{$dpi1}} espacio {{$dpi2}} espacio {{$dpi3}} ({{$credito->cliente->persona->dpi}}) extendido por el Registro Nacional de las Personas de la República de Guatemala,
Centroamérica. Por este medio del presente documento celebramos <strong>CONTRATO DE RECONOCIMIENTO DE DEUDA</strong> en documento privado, de conformidad con las
cláusulas siguientes: <strong>PRIMERA:</strong> yo <strong>{{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}} </strong>, manifiesto que recibí de la señora
<strong>AMANDA BEATRIZ OVANDO PALACIOS</strong> la cantidad de {{\NumerosEnLetras::convertir($credito->plan->capital)}} QUETZALES (Q. {{$credito->plan->capital}}), el cual deberan ser cancelados en
{{\NumerosEnLetras::convertir($credito->plan->periodo->tiempo)}} pagos diarios de {{\NumerosEnLetras::convertir($credito->plan->cuota)}} QUETZALES (Q. {{$credito->plan->cuota}}) más intereses,
el cual comenzará a pagarse el {{\NumerosEnLetras::convertir(\Carbon\Carbon::parse($credito->fecha_inicio)->format('d'))}} de {{$mes}} de {{\NumerosEnLetras::convertir(\Carbon\Carbon::parse($credito->fecha_inicio)->format('Y'))}}
y vence el {{\NumerosEnLetras::convertir(\Carbon\Carbon::parse($credito->fecha_fin)->format('d'))}} de {{$mes}} de {{\NumerosEnLetras::convertir(\Carbon\Carbon::parse($credito->fecha_inicio)->format('Y'))}}.
Todo pago, tanto de capital como de interés se harán sin necesidad de cobro o requerimiento alguno, cualquier gasto que se hiciera si se hace el cobro por la vía Judicial será por mi cuenta;

<strong>SEGUNDA:</strong> Que en los términos relacionados, Yo, <strong>AMANDA BEATRIZ OVANDO PALACIOS, ACEPTO </strong> recibir los  {{\NumerosEnLetras::convertir($credito->plan->periodo->tiempo)}} pagos diarios de {{\NumerosEnLetras::convertir($credito->plan->cuota)}} QUETZALES (Q. {{$credito->plan->cuota}}), de
parte de <strong>{{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</strong>;

<strong>TERCERA:</strong> yo <strong>{{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</strong> me reconozco <strong>{{$credito->cliente->persona->genero==0 ? 'DEUDORA' : 'DEUDOR'}}</strong>
de la señora <strong>AMANDA BEATRIZ OVANDO PALACIOS</strong> por la cantidad de {{\NumerosEnLetras::convertir($credito->plan->total)}} ({{$credito->plan->total}}) y
en caso de mora reconoceré el interés moratorio mensual, en el porcentaje que corresponda al interés pactado por el crédito, que serán {{\NumerosEnLetras::convertir($credito->plan->mora)}} QUETZALES
(Q. {{\NumerosEnLetras::convertir($credito->plan->mora)}}.00) diarios, sobre el saldo del capital en mora desde el día en que debió ser pagado hasta la fecha en que se haga efectiva
la cancelación del saldo moroso.

<strong>CUARTA:</strong> yo, <strong> {{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</strong> dejo en calidad de <strong>GARANTIA:</strong>
@php
 $garantias = \App\Garantia::where('prestamo_id', $credito->id)->get();
@endphp
@foreach ($garantias as $garantia)
 {{\NumerosEnLetras::convertir($garantia->cantidad)}} {{$garantia->nombre}}, {{$garantia->descripcion}},
@endforeach
bajo apercibimiento de que si se establece que no es de mi propiedad estaría incurriendo en el delito de <strong>ESTAFA</strong>, tipificando en los artículos
263 y 264 del Código Penal;

<strong>QUINTA:</strong> yo <strong>{{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</strong> por este medio faculto a el asesor,
gerente o supervisor de créditos de la empresa <strong>CREDITRIUNFO S. A.</strong>, para que me visiten las veces que consideren necesario para recoger los pagos, sin necesidad de que yo alegue por la
vía Penal ningún tipo de extorsión, coacción o amenaza de parte de los mismos, siempre y cuando vayan debidamente identificados como empleados de la misma, así como también les permito el ingreso a mi
domicilio al momento que sea girada la orden de embargo por parte del Juzgado para retirar el bien que dejó en garantía en la cláusula cuarta.

<strong>SEXTA: </strong> Yo <strong>{{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</strong> renuncio a renuncio a la posibilidad de proponer realizar mis pagos conforme
a mis posibiidades, sino que los mismos serán en base a lo establecido en las cláusulas primera, segunda y tercera del presente contrato, además establezco que la falta de pago de una sola de las cuotas
en concepto de capital da derecho a la señora <strong>AMANDA BEATRIZ OVANDO PALACIOS </strong> para dar por vencido el plazo y exigir el cumplimiento de la obligación.

<strong>SEPTIMA: EFECTOS PROCESALES:</strong> en caso de incumplimiento: a) Me comprometo a ENTREGAR DE MANERA VOLUNTARIA, la garantía descrita en la cláusula cuarta del presente
contrato, caso contrario permitiré que me sea retirada por la vía legal; b) Renuncio al fuero de mi domicilio y a cualquier otra competencia que pudiera corresponderme, sometiéndose
a la jurisdicción de los tribunales que la señora <strong>AMANDA BEATRIZ OVANDO PALACIOS </strong> elija; c) Señalo como lugar para recibir notificaciones mi    domicilio  señalado
en el presente contrato; d) Acepto desde ya como buenas y exactas las cuentas que la señora <strong>AMANDA BEATRIZ OVANDO PALACIOS </strong> me formule en relación al presente documento;
y como liquida y exigible la cantidad que me reclame; e) Asímismo manifiesto que le doy la calidad de Título Ejecutivo al presente Documento. Y ambos leemos el contenido del presente contrato,
quienes bien enterados de su contenido, objeto, validez y demás efectos legales, lo ratificamos, aceptamos y firmamos. En el municipio de Retalhuleu del departamento de Retalhuleu,
el {{\NumerosEnLetras::convertir(Carbon\Carbon::now()->format('d'))}} de {{$mes}} de {{\NumerosEnLetras::convertir(Carbon\Carbon::now()->format('Y'))}}, Yo, <strong>JUAN RAMÓN MORALES RIVERA </strong>,
Notario, <strong> DOY FE</strong>, que las firmas que anteceden son <strong>AUTÉNTICAS</strong>, por haber sido puestas el día de hoy en mi presencia por <strong>AMANDA BEATRIZ OVANDO PALACIOS</strong>
quien se identifica con el Documento Personal de Identificación que corresponde al Código Único de Identificación número dos mil quinientos noventa y nueve espacio setenta y siete mil doscientos diecisiete
espacio cero novecientos uno (2599 77217 0901) extendido por el Registro Nacional de las Personas de la República de Guatemala, Centroamérica y {{$credito->cliente->persona->genero==0 ? 'la señora' : 'el señor'}} <strong>{{$credito->cliente->persona->nombre}}
{{$credito->cliente->persona->apellido}}</strong>, quien se identifica con el Documento Personal de Identificación que corresponde Código Único de Identificación número {{$dpi1}} {{$dpi2}} {{$dpi3}} ({{$credito->cliente->persona->dpi}})
extendido por el Registro Nacional de las Personas de la República de Guatemala. Ambas firmas calzan un documento privado de <strong>CONTRATO DE RECONOCIMIENTO DE DEUDA</strong>. Los signatarios vuelven a firmar la presente acta de legalizacion de firma.
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<strong>ANTE MÍ:</stron3.g>
</p>
</div>
<script>
       window.print();
       //window.close(); Se comento est[a ] linea que Google Chrome abría y cerrraba la pestaña al querer imprimir.
</script>
