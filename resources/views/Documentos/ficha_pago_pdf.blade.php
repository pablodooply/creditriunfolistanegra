
<style>
  /* empieza fuente calibri */
  table.documento, th.documento, td.documento { font-family: Calibri; font-size: 10pt; font-style: normal; font-variant: normal;  }
  table.documento, th.documento, td.documento {
    border: 1px solid black;
  }

  table.documento {
    border-collapse: collapse;
  }

  td.primera {
    width: 8.2cm;
  }

  td.segunda {
    width: 1.4cm;
  }

  td.tercera {
    width: 1.7cm;
  }

  td.td-primera {
    width: 0.7cm;
  }

  td.td-segunda {
    width: 0.7cm;
  }

  td.td-tercera {
    width: 2.4cm;
  }

  td.td-cuarta {
    width: 1.1cm;
  }

  td.td-quinta {
    width: 1cm;
  }

  td.td-sexta {
    width: 2cm;
  }

  td.td-septima {
    width: 1.4cm;
  }

  td.td-octava {
    width: 1.5cm;
  }
</style>


@php
$f_ruta = \App\Ruta::where('prestamo_id', $credito->id)->first();
$cant1 = $credito->fichas_reales()->count();
$tabla = $credito->fichas_reales();
@endphp
<div class="table-responsive">
<table class="documento">
  <tr>
    <td  colspan="3" class="documento text-center"><strong>CREDITRIUNFO</strong></td>
    <td class="documento" colspan="5">Teléfono Oficina: 7736 4204</td>
  </tr>
  <tr>
    <td colspan="6" class="documento primera">NOMBRE: {{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</td>
    <td class="documento segunda">MONTO</td>
    <td class="documento tercera">Q. {{number_format($credito->plan->capital, 2, '.', '')}}</td>
  </tr>
  <tr>
    <td class="documento" colspan="6">TELS: Acesor:{{isset($f_ruta->hoja_ruta->user->persona->celular1) ? $f_ruta->hoja_ruta->user->persona->celular1 : 'No Tiene '}} Supervisor:{{isset($f_ruta->hoja_ruta->supervisor->persona->celular1) ? $f_ruta->hoja_ruta->supervisor->persona->celular1 : 'No Tiene '}}</td>
    <td class="documento">Gestor:</td>
    <td class="documento">Jorge</td>
  </tr>
  <tr>
    <td class="documento">No.</td>
    <td class="documento">Día</td>
    <td class="documento">Fecha</td>
    <td class="documento">Mora</td>
    <td class="documento">Pago</td>
    <td class="documento">Saldo</td>
    <td class="documento">Firma</td>
    <td class="documento">F de pag</td>
  </tr>
  <tr>
    <td class="documento"></td>
    <td class="documento"></td>
    <td class="documento"></td>
    <td class="documento"></td>
    <td class="documento"></td>
    <td class="documento">Q. {{number_format($credito->plan->total, 2, '.', '')}}</td>
    <td class="documento"></td>
    <td class="documento"></td>
  </tr>
  @if ($cant1 < 26)
    @for ($i=0; $i < $cant1; $i++)
      <tr>
        <td class="documento">{{$tabla[$i]->no_dia}}</td>
        <td class="documento"></td>
        <td class="documento">{{\Carbon\Carbon::parse($tabla[$i]->fecha)->format('d/m/Y')}}</td>
        <td class="documento"></td>
        <td class="documento"></td>
        <td class="documento">Q. {{number_format(($credito->plan->total - ($tabla[$i]->no_dia * $credito->plan->cuota)), 2, '.', '')}}</td>
        <td class="documento"></td>
        <td class="documento"></td>
      </tr>
    @endfor
  @else
    @for ($i=0; $i < 25; $i++)
      <tr>
        <td class="documento">{{$tabla[$i]->no_dia}}</td>
        <td class="documento"></td>
        <td class="documento">{{\Carbon\Carbon::parse($tabla[$i]->fecha)->format('d/m/Y')}}</td>
        <td class="documento"></td>
        <td class="documento"></td>
        <td class="documento">Q. {{number_format(($credito->plan->total - ($tabla[$i]->no_dia * $credito->plan->cuota)), 2, '.', '')}}</td>
        <td class="documento"></td>
        <td class="documento"></td>
      </tr>
    @endfor
  </table>
    <div class="saltopagina">

      <table class="documento">
    @for ($i=25; $i < $cant1; $i++)
      <tr>
        <td class="documento td-primera">{{$tabla[$i]->no_dia}}</td>
        <td class="documento td-segunda"></td>
        <td class="documento td-tercera">{{\Carbon\Carbon::parse($tabla[$i]->fecha)->format('d/m/Y')}}</td>
        <td class="documento td-cuarta"></td>
        <td class="documento td-quinta"></td>
        <td class="documento td-sexta">Q. {{number_format(($credito->plan->total - ($tabla[$i]->no_dia * $credito->plan->cuota)), 2, '.', '')}}</td>
        <td class="documento td-septima"></td>
        <td class="documento td-octava"></td>
      </tr>
    @endfor

  </table>
    </div>
  @endif

</div>
