<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
    /* empieza fuente calibri */
    table, th, td { font-family: Calibri; font-size: 10pt; font-style: normal; font-variant: normal;  }
    table, th, td {
      border: 1px solid black;
    }

    table {
      border-collapse: collapse;
    }

    td.primera {
      width: 8.2cm;
    }

    td.segunda {
      width: 1.4cm;
    }

    td.tercera {
      width: 1.7cm;
    }

    td.td-primera {
      width: 0.7cm;
    }

    td.td-segunda {
      width: 0.7cm;
    }

    td.td-tercera {
      width: 2.4cm;
    }

    td.td-cuarta {
      width: 1.1cm;
    }

    td.td-quinta {
      width: 1cm;
    }

    td.td-sexta {
      width: 2cm;
    }

    td.td-septima {
      width: 1.4cm;
    }

    td.td-octava {
      width: 1.5cm;
    }
    @media all {
       div.saltopagina{
          display: none;
       }
    }
    @media print{
      @page { margin: 0;
       size: auto; }
       div.saltopagina{
          display:block;
          page-break-before:always;
       }

       /*No imprimir*/
       .oculto {display:none}
    }
  </style>
  <body>
    @php
    $f_ruta = \App\Ruta::where('prestamo_id', $credito->id)->first();
  @endphp
  <div class="table-responsive">
    <table>
      <tr>
        <td colspan="3" class="text-center"><strong>CREDITRIUNFO</strong></td>
        <td colspan="5">Teléfono Oficina: 7736 4204</td>
      </tr>
      <tr>
        <td colspan="6" class="primera">NOMBRE: {{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</td>
        <td class="segunda">MONTO</td>
        <td class="tercera">Q. {{number_format($credito->plan->capital, 2, '.', '')}}</td>
      </tr>
      <tr>
        <td class="documento" colspan="6">TELS: Acesor:{{isset($f_ruta->hoja_ruta->user->persona->celular1) ? $f_ruta->hoja_ruta->user->persona->celular1 : 'No Tiene '}} Supervisor:{{isset($f_ruta->hoja_ruta->supervisor->persona->celular1) ? $f_ruta->hoja_ruta->supervisor->persona->celular1 : 'No Tiene '}}</td>
        <td>Gestor:</td>
        <td>Jorge</td>
      </tr>
      <tr>
        <td>No.</td>
        <td>Día</td>
        <td>Fecha</td>
        <td>Mora</td>
        <td>Pago</td>
        <td>Saldo</td>
        <td>Firma</td>
        <td>F de pag</td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Q. {{number_format($credito->plan->total, 2, '.', '')}}</td>
        <td></td>
        <td></td>
      </tr>
      @if ($cant1 < 26)
        @for ($i=0; $i < $cant1; $i++)
          <tr>
            <td>{{$tabla[$i]->no_dia}}</td>
            <td></td>
            <td>{{\Carbon\Carbon::parse($tabla[$i]->fecha)->format('d/m/Y')}}</td>
            <td></td>
            <td></td>
            <td>Q. {{number_format(($credito->plan->total - ($tabla[$i]->no_dia * $credito->plan->cuota)), 2, '.', '')}}</td>
            <td></td>
            <td></td>
          </tr>
        @endfor
      @else
        @for ($i=0; $i < 25; $i++)
          <tr>
            <td>{{$tabla[$i]->no_dia}}</td>
            <td></td>
            <td>{{\Carbon\Carbon::parse($tabla[$i]->fecha)->format('d/m/Y')}}</td>
            <td></td>
            <td></td>
            <td>Q. {{number_format(($credito->plan->total - ($tabla[$i]->no_dia * $credito->plan->cuota)), 2, '.', '')}}</td>
            <td></td>
            <td></td>
          </tr>
        @endfor
      </table>
        <div class="saltopagina">

          <table>
        @for ($i=25; $i < $cant1; $i++)
          <tr>
            <td class="td-primera">{{$tabla[$i]->no_dia}}</td>
            <td class="td-segunda"></td>
            <td class="td-tercera">{{\Carbon\Carbon::parse($tabla[$i]->fecha)->format('d/m/Y')}}</td>
            <td class="td-cuarta"></td>
            <td class="td-quinta"></td>
            <td class="td-sexta">Q. {{number_format(($credito->plan->total - ($tabla[$i]->no_dia * $credito->plan->cuota)), 2, '.', '')}}</td>
            <td class="td-septima"></td>
            <td class="td-octava"></td>
          </tr>
        @endfor

      </table>
        </div>
      @endif

  </div>
  </body>
  <script>
  window.setInterval('imprimir()',1000);
  function imprimir(){
    window.print();
    window.close();
  }
  </script>
</html>
