<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
    .contenedor{
        position: relative;
        display: inline-block;
        text-align: center;
      }

      .cuenta-banco-1{
        position: absolute;
        top: 28px;
        left: 110px;
        font-size: 3mm;
        color:#000000;
      }
      .cuenta-banco-2{
        position: absolute;
        top: 38px;
        left: 110px;
        font-size: 3mm;
        color:#000000;
      }
      .lugar-fecha{
        position: absolute;
        top: 65px;
        left: 63px;
        font-size: 3mm;
        color:#000000;
      }
      .monto{
        position: absolute;
        top: 65px;
        left: 500px;
        font-size: 3mm;
        color:#000000;
      }
      .nombre{
        position: absolute;
        top: 83px;
        left: 78px;
        font-size: 3mm;
        color:#000000;
      }
      .suma{
        position: absolute;
        top: 103px;
        left: 50px;
        font-size: 3mm;
        color:#000000;
      }
      .centrado{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        color:#000000;
      }
    </style>
  </head>
  @php
  //use NumerosEnLetras;
  $fecha_mes = Carbon\Carbon::now()->format('m');
  $mes = '';
  if ($fecha_mes  == 1)  {    $mes = 'Enero';       }
  if ($fecha_mes  == 2)  {    $mes = 'Febrero';     }
  if ($fecha_mes  == 3)  {    $mes = 'Marzo';       }
  if ($fecha_mes  == 4)  {    $mes = 'Abril';       }
  if ($fecha_mes  == 5)  {    $mes = 'Mayo';        }
  if ($fecha_mes  == 6)  {    $mes = 'Junio';       }
  if ($fecha_mes  == 7)  {    $mes = 'Julio';       }
  if ($fecha_mes  == 8)  {    $mes = 'Agosto';      }
  if ($fecha_mes  == 9)  {    $mes = 'Septiembre';  }
  if ($fecha_mes  == 10) {    $mes = 'Octubre';     }
  if ($fecha_mes  == 11) {    $mes = 'Noviembre';   }
  if ($fecha_mes  == 12) {    $mes = 'Dciembre';    }
  @endphp
  <body>
    <div class="contenedor">
      <img src="{{asset('images/cheque.jpg')}}" height="200px" width="600px" alt="">
      <div class="cuenta-banco-1"><strong>OSCAR BATRES MACHIC</strong></div>
      <div class="cuenta-banco-2"><strong>3537019480</strong></div>
      <div class="lugar-fecha"><strong>Quetzaltenango, {{$mes}} de {{Carbon\Carbon::now()->format('Y')}}</strong></div>
      <div class="monto"><strong>{{number_format($credito->plan->capital, 2)}}</strong></div>
      <div class="nombre"><strong>{{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</strong></div>
      <div class="suma"><strong>{{\NumerosEnLetras::convertir($credito->plan->capital)}} quetzales exactos</strong></div>
    </div>
    <br>
  </body>
</html>
