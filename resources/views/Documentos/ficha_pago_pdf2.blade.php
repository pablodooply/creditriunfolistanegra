<style>
.pagina{
    width: 14cm;
    height: 21.6cm;
    font-family:'calibri'; 
    font-style: normal; 
    font-variant: normal;
}

.encabezado{
    display: flex;
    justify-content: space-between;
    
}
.encabezado-left{
    margin-left: 15px;
    margin-top: 20px;
    
    
}
.encabezado-left img{
    width: 135px;
    height: 140px;
}

.encabezado-left p{
    font-weight: bold;
    font-size: 16px;
    margin: 0px;
    text-align: center;
    
}

.encabezado-right{
    margin-right: 15px;
    margin-top: 20px;
  
}

.encabezado-right span{
    margin-left: 8px;
    font-weight: normal;
}

.encabezado-right h2 {

    text-align: center;
    font-weight: bold;
    font-size: 16px;
    margin: 0px;
}

.encabezado-right h3 {
    font-weight: bold;
    font-size: 14px;
}
.encabezado-right p {
    padding: 6px; 
    border: 1px solid black;
    font-weight: bold;
    font-size: 12px;
    margin: 0px;
}
 
.contenido{
    display: flex;
    justify-content: center;
    margin-top: 5px; 
    font-size: 10pt; 
    margin-left: 15px;
    margin-right: 15px;
}

.contenido-tabla{
    border:  1px solid black;
    border-collapse: collapse;
}

.contenido-tabla  td{
    border:  1px solid black;
    padding: 3px;
    text-align: center;
}

.contenido-tabla__encabezado{
    border:  1px solid black;
    padding: 3px;
    text-align: center;
}
.ancho-column{
    width: 1.9cm;
    
}
 


</style>

@php
$f_ruta = \App\Ruta::where('prestamo_id', $credito->id)->first();
$telefonoagencia =\App\Agencia::first();
//$nombresupervisor = \app\user::where('id',$f_ruta->hoja_ruta->supervisor_id)->value("name");
//$telefonosupervi = \App\persona::where('id',$f_ruta->hoja_ruta->supervisor->persona_id)->first();
//$nombresupervisor = \app\user::where('id',$f_ruta->hoja_ruta->supervisor_id)->get();
//$nombresupervisor = \app\user::where('id',$f_ruta->hoja_ruta->supervisor_id)->pluck("name")->first();
//dd($telefonosupervi);
$cant1 = $credito->fichas_reales()->count();
$tabla = $credito->fichas_reales();
@endphp

<div class="pagina">
    <div class="encabezado">
        <div class="encabezado-left">
            <img src="/images/empleados/perfil/logo.png" alt="logo">
        <p>Tel: {{$telefonoagencia->telefono}}</p>
        </div>
    
        <div class="encabezado-right">
            <h2>Distribuidora Creditriunfo</h2>
            <p>Nombre: <span>{{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</span></p>
            <p>Telefono: <span>{{$credito->cliente->persona->telefono}}</span></p>
            <p>Monto: <span>Q. {{number_format($credito->plan->capital, 2, '.', '')}}</span>  &nbsp;  &nbsp; No. Credito:<span>Cre-{{$credito->id}}</span></p>
            @if($f_ruta->hoja_ruta->user->telefono!='')
                    <p>Asesor: <span>{{$f_ruta->hoja_ruta->user->persona->nombre}}</span>  &nbsp;  &nbsp; Telefono:<span> {{$$f_ruta->hoja_ruta->user->telefono }}  </span></p>
                @elseif($f_ruta->hoja_ruta->user->celular1!='')
                    <p>Asesor: <span>{{$f_ruta->hoja_ruta->user->persona->nombre}}</span>  &nbsp;  &nbsp; Telefono:<span> {{$f_ruta->hoja_ruta->user->celular1 }}  </span></p>
                @elseif($f_ruta->hoja_ruta->user->celular2!='')
                    <p>Asesor: <span>{{$f_ruta->hoja_ruta->user->persona->nombre}}</span>  &nbsp;  &nbsp; Telefono:<span> {{$f_ruta->hoja_ruta->user->celular2 }}  </span></p>
                @else
                    <p>Asesor: <span>{{$f_ruta->hoja_ruta->user->persona->nombre}}</span>  &nbsp;  &nbsp; Telefono:<span> No Tiene  </span></p>
                @endif
            <p>No. de Cuenta: Banrural 3265044308 / Bac Credomatic 90-340221-2</p>
        </div>
    </div>

     <div class="contenido">
            <table class="contenido-tabla" >
                <thead>
                    <tr>
                        <th class="contenido-tabla__encabezado">No.</th>
                        <th class="contenido-tabla__encabezado">Dia</th>
                        <th class="ancho-column contenido-tabla__encabezado">Fecha</th> 
                        <th class="ancho-column contenido-tabla__encabezado">Mora</th>
                        <th class="ancho-column contenido-tabla__encabezado">Pago</th>
                        <th class="ancho-column contenido-tabla__encabezado">Saldo</th>
                        <th class="ancho-column contenido-tabla__encabezado">Firma</th>
                        <th class="ancho-column contenido-tabla__encabezado">F de Pago</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Q. {{number_format($credito->plan->total, 2, '.', '')}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    @if ($cant1 < 22)
                        @for ($i=0; $i < $cant1; $i++)
                            <tr>
                                <td>{{$tabla[$i]->no_dia}}</td>
                                <td>{{\Carbon\Carbon::parse($tabla[$i]->fecha)->formatLocalized('%A')}}</td>
                                <td>{{\Carbon\Carbon::parse($tabla[$i]->fecha)->format('d/m/Y')}}</td>
                                <td></td>
                                <td></td>
                                <td>Q. {{number_format(($credito->plan->total - ($tabla[$i]->no_dia * $credito->plan->cuota)), 2, '.', '')}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endfor
                    @else
                        @for ($i=0; $i < 21; $i++)
                            <tr>
                                <td>{{$tabla[$i]->no_dia}}</td>
                                <td>{{\Carbon\Carbon::parse($tabla[$i]->fecha)->formatLocalized('%A')}}</td>
                                <td>{{\Carbon\Carbon::parse($tabla[$i]->fecha)->format('d/m/Y')}}</td>
                                <td></td>
                                <td></td>
                                <td>Q. {{number_format(($credito->plan->total - ($tabla[$i]->no_dia * $credito->plan->cuota)), 2, '.', '')}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endfor
                    @endif
                </tbody>
             </table>
    </div>
</div>
