<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
    .contenedor{
        position: relative;
        display: inline-block;
        text-align: center;
      }

      body {
        font-size: 3mm;
      }
      @media screen {
        /* Contenido del fichero home.css */
        body {
          font-size: 1mm;
        }
      }

      @media print {
        /* Contenido del fichero print.css */
        body {
          font-size: 1mm;
        }
      }
    </style>
  </head>
  @php
  //use NumerosEnLetras;
  $fecha_mes = Carbon\Carbon::now()->format('m');
  $mes = '';
  if ($fecha_mes  == 1)  {    $mes = 'Enero';       }
  if ($fecha_mes  == 2)  {    $mes = 'Febrero';     }
  if ($fecha_mes  == 3)  {    $mes = 'Marzo';       }
  if ($fecha_mes  == 4)  {    $mes = 'Abril';       }
  if ($fecha_mes  == 5)  {    $mes = 'Mayo';        }
  if ($fecha_mes  == 6)  {    $mes = 'Junio';       }
  if ($fecha_mes  == 7)  {    $mes = 'Julio';       }
  if ($fecha_mes  == 8)  {    $mes = 'Agosto';      }
  if ($fecha_mes  == 9)  {    $mes = 'Septiembre';  }
  if ($fecha_mes  == 10) {    $mes = 'Octubre';     }
  if ($fecha_mes  == 11) {    $mes = 'Noviembre';   }
  if ($fecha_mes  == 12) {    $mes = 'Dciembre';    }
  @endphp
  <body>
    <br>
    <table class="tabla" border="1" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
      <p>&nbsp;</p>
      <p>&nbsp;</p>
        <p>	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;<strong>Quetzaltenango, {{$mes}} de {{Carbon\Carbon::now()->format('Y')}}	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp; {{number_format($credito->plan->capital, 2)}}</strong></p>
        <p> &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;<strong>{{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</strong></p>
        <p>&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;<strong>{{\NumerosEnLetras::convertir($credito->plan->capital)}} quetzales exactos</strong></p>
    </table>
  </body>
  <script>
         window.print();
         //window.close(); Se comento est[a ] linea que Google Chrome abría y cerrraba la pestaña al querer imprimir.
  </script>
</html>
