@extends('Documentos.layout.base')

@section('base-content')
  <div class="row">
    <div class="col-xs-12">
      <center><h3>Reporte de creditos activos</h3></center>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Codigo</th>
            <th>Monto</th>
            <th>Estado</th>
            <th>Clasificacion</th>
            <th>Cliente</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($creditos as $credito)
            <tr>
              <td>Cre-{{$credito->id}}</td>
              <td>{{$credito->monto}}</td>
              {!!$credito->obtenerEstado()!!}
              {!!$credito->obtenerClasificacion()!!}
              <td>{{$credito->cliente->persona->nombre}}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection

@if($tipo == 1)
  @section('base-scripts')
    <script type="text/javascript">
      window.print();
      window.close();      
    </script>
  @endsection
@endif
