<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">

        <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
        {{-- <link rel="stylesheet" href="{!! asset('css/app.css') !!}" /> --}}

    <title></title>
  </head>
  <body>
    @yield('base-content')
    <script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
    @yield('base-scripts')
  </body>
</html>
