<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title">Información de Categoría <span class="text-success">CAT-{{str_pad($categoria->id,6,"0",STR_PAD_LEFT)}}</span></h4>
    {{-- <small class="font-bold"><a href="/empleado/perfil/{{$usuario->id}}">(EMP-{{str_pad($usuario->id,6,"0",STR_PAD_LEFT)}})</a> {{$usuario->persona->nombre}} {{$usuario->persona->apellido}}</small> --}}
</div>
  <div class="widget lazur-bg p-xl">
    <h2>  {{$categoria->nombre}}  </h2>
    <ul class="list-unstyled m-t-md">
      <li>
          <span class="fa fa-user m-r-xs"></span>
          <label>Creado por:</label>
          @php
            $usuario = App\User::find($categoria->created_by);
          @endphp
          {{$usuario->name}}
      </li>
      <li>
          <span class="fa fa-calendar m-r-xs"></span>
          <label>Fecha creación:</label>
          {{$categoria->created_at}}
      </li>
    </ul>

  </div>
<div class="modal-footer">
    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
</div>
