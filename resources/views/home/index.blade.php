@extends('layouts.app')

@section('title', 'Inicio')
@section('link')
  <link rel="stylesheet" href="{{asset('css/datatables/datatables.min.css')}}">
@endsection
@section('content')
  @section('nombre','Inicio')
    <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row">
          <div class="col-md-12">

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget style1 navy-bg">
                  <div class="row">
                    <a class="client-link" href="{{route('reportes.index',1)}}">
                      <div class="col-md-12">
                        <div class="col-lg-4 col-md-2 text-center text-uppercase	">
                          <i class="fa  fa-5x"><strong>Q</strong></i>
                        </div>
                        <div class="col-md-8  text-center">
                          <h3 class="">Capital total </h3>
                          <h4>Q. {{number_format(\App\Pago::sum('monto'), 2)}}</h4>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget style1 lazur-bg">
                  <div class="row">
                    <a class="client-link" href="{{route('reportes.index',1)}}">
                      <div class="col-md-12">
                      <div class="col-md-4  text-center">
                        <i class="fa fa-plus-square fa-5x"></i>
                      </div>
                      <div class="col-md-8 text-center">
                        <h3>Capital recuperado</h3>
                        <h4>Q. {{number_format(\App\Pago::sum('capital'),2)}}</h4>
                      </div>
                    </div>
                    </a>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget style1 yellow-bg">
                  <div class="row">
                    <a class="client-link" href="{{route('reportes.index',1)}}">
                      <div class="col-md-12">
                      <div class="col-md-4 text-center">
                        <i class="fa fa-bell fa-5x"></i>
                      </div>
                      <div class="col-md-8 text-center">
                        <h3>Intereses cobrados</h3>
                        <h4>Q. {{number_format(\App\Pago::sum('interes'),2)}}</h4>
                      </div>
                    </div>
                    </a>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget style1 red-bg">
                  <div class="row">
                    <a class="client-link" href="{{route('reportes.index',1)}}">
                      <div class="col-md-12">
                      <div class="col-md-4 text-center">
                        <i class="fa fa-user fa-5x"></i>
                      </div>
                      <div class="col-md-8 text-center">
                        <h3>Mora recaudada</h3>
                        <h4>Q. {{number_format(\App\Pago::sum('mora'),2)}}</h4>
                      </div>
                    </div>
                    </a>
                  </div>
                </div>
            </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-6">
            <div class="widget-head-color-box navy-bg p-lg text-center">
                <div class="m-b-md">
                  <h2 class="font-bold no-margins">
                    Capital Activo
                  </h2>
                  <a id="empCap"class="client-link"><h4 id="empleado_capital"></h4></a>
                  <input type="hidden" id="aEmpleadoCapital">
                </div>
                <img id="img_capital" src="{{asset('images/default/perfil.jpg')}}" class="img-circle circle-border m-b-md" alt="profile">
                <div>
                </div>
            </div>
          </div>
          <div class="col-md-6">
                <div class="widget-head-color-box red-bg p-lg text-center">
                    <div class="m-b-md">
                    <h2 class="font-bold no-margins">
                        Mora recuperada
                    </h2>
                        <a id="empMora" class="client-link"><h4 id="empleado_mora"></h4></a>
                        <input type="hidden" id="aEmpleadoMora">
                    </div>
                    <img id="img_mora" src="{{asset('images/default/perfil.jpg')}}" class="img-circle circle-border m-b-md" alt="profile">
                    <div>
                    </div>
                </div>
          </div>
        </div>
      </div>


        <div class="row">
          <div class="col-lg-12">

            <br>
          </div>
        </div>



        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a class="nav-link active" id="clic-todos" data-toggle="tab" href="#tab-1"> Todos los créditos</a></li>
                        <li><a class="nav-link" data-toggle="tab" id="clic-asesor" href="#tab-2"> Cuadre por asesor</a></li>
                        <li><a class="nav-link" data-toggle="tab" id="clic-general" href="#tab-3"> Cuadre general</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                          <div id="tab-creditos">

                          </div>
                        </div>
                        <div role="tabpanel" id="tab-2" class="tab-pane">
                          <div id="tab-asesor">

                          </div>
                        </div>
                        <div role="tabpanel" id="tab-3" class="tab-pane">
                          <div id="tab-general">

                          </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="modal inmodal fade" id="modal-detalles" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div id="cont-modal-detalles">
          </div>
        </div>
      </div>
    </div>

    <div class="modal inmodal fade" id="modal-asesor" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div id="cont-modal-asesor">
          </div>
        </div>
      </div>
    </div>


@endsection


@section('scripts')
  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> --}}
    <!-- Include this after the sweet alert js file -->
    @include('sweet::alert')
<script type="text/javascript">
$(document).ready(function(){
  $('#clic-todos').trigger('click');
});
$("#clic-todos").on('click', function(){
  var url = '{{ route("tabla.creditos") }}';
  console.log(url);
  $('#tab-creditos').html(' ');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#tab-creditos').html(response);
      }
  });
});

$("#clic-asesor").on('click', function(){
  var url = '{{ route("tabla.asesor") }}';
  $('#tab-asesor').html(' ');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#tab-asesor').html(response);
      }
  });
});

$("#clic-general").on('click', function(){
  console.log('hola');
  var url = '{{ route("tabla.general") }}';
  $('#tab-general').html(' ');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#tab-general').html(response);
      }
  });
});

  $(document).ready(function(){

    function obtenerEmpleados(){
      obtenerEmpleadoCapital();
      obtenerEmpleadoMora();
    }

    function obtenerEmpleadoCapital(){
      var base_imagen = "{{asset('images/empleados/perfil/')}}"
      url = "{{route('usuarios.posicionesCapital')}}";
      var urlHref = "{{route('usuarios.show',':id')}}";
      $.ajax({
          type: "GET",
          url: url,
          success: function( response ) {
            $('#empleado_capital').text(response.nombre);
            urlHref = urlHref.replace(':id', response.codigo);
            $("#empCap").attr("href", urlHref);
            // if(response.imagen !=null){
            //   $('#img_capital').attr('src',"/" + base_imagen + response.imagen);
            // }
          }
      });
    }

    function obtenerEmpleadoMora(){
      url = "{{route('usuarios.posicionesMora')}}";
      var urlHref = "{{route('usuarios.show',':id')}}";
      $.ajax({
          type: "GET",
          url: url,
          success: function( response ) {
            $('#empleado_mora').text(response.nombre);
            urlHref = urlHref.replace(':id', response.codigo);
            $("#empMora").attr("href", urlHref);
          }
      });
    }
    obtenerEmpleados();

    $('#empCap').click(function(){

    });
  });


  function detallesRapidos(id)
  {
    console.log(id)
    var url = '{{route("home.detallesRapidos", ":slug")}}';
    url = url.replace(':slug', id);
    $('#cont-modal-detalles').html(' ');
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $('#cont-modal-detalles').html(response);
        }
    });
  }

  function detallesAsesor(id)
  {

    var url = '{{route("home.detallesAsesor", ":slug")}}';
    url = url.replace(':slug', id);
    $('#cont-modal-asesor').html(' ');
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $('#cont-modal-asesor').html(response);
        }
    });
  }
</script>

@endsection
