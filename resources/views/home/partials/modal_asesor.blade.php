
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title"> <strong>Emp-{{$credito->id}} </strong> </h4>
    </div>
    <div class="modal-body">
      <div class="table-responsive">
        <table id="tabla-credito-asesor" class="table table-striped table-bordered table-hover dataTables-example text-center">
          <thead>
            <tr>
              <th>Codigo</th>
              <th>Monto</th>
              <th>Número de cuotas</th>
              <th>Cuota diaria</th>
              <th>Estado</th>
              <th>Clasificacion</th>
              <th>Tipo crédito</th>
              <th>Cliente</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
          <tfoot>
            <tr>
              <th>Codigo</th>
              <th>Monto</th>
              <th>Número de cuotas</th>
              <th>Cuota diaria</th>
              <th>Estado</th>
              <th>Clasificacion</th>
              <th>Tipo crédito</th>
              <th>Cliente</th>
              <th>Acciones</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
    </div>
</div>

<script>
$(document).ready(function(){
  $('#tabla-credito-asesor').DataTable({
    order: [[ 0, "desc" ]],
    language: {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    paging: true,
    info: false,
    dom : 'tip',
    processing: true,
    serverSide: true,
    ajax: {

      'beforeSend': function (request) {
       request.setRequestHeader("X-CSRF-TOKEN", '{{ csrf_token() }}');
       },
       data: function(d) {


       d.fecha1 = $('input[name=fecha1]').val();
       d.fecha2 = $('input[name=fecha2]').val();
     },
      "url": '{{route('home.apiAsesorCreditos', $credito->id)}}'
    },
    pageLength: 10,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
      {extend: 'copy'},
      {extend: 'csv'},
      {extend: 'excel', title: 'Creditos'},
      {extend: 'pdf', title: 'Creditos'},
      {extend: 'print',
         customize: function (win)
         {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');

            $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
        }
      }
    ],
    columns: [
        {data: 'Codigo', name: 'Codigo'},
        {data: 'Monto', name: 'Monto'},
        {data: 'Suma', name: 'Suma'},
        {data: 'Cuota', name: 'Cuota'},
        {data: 'Estado', name: 'Estado'},
        {data: 'Clasificacion', name: 'Clasificacion'},
        {data: 'Tipo', name: 'Tipo'},
        {data: 'Cliente', name: 'Cliente'},
        {data: 'Acciones', name: 'Acciones'},
    ]
  });
});
</script>
