<div class="row wrapper border-bottom white-bg page-heading">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title"> <strong>Cre-{{$credito->id}} </strong> </h4>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-lg-6">
        <h4>Detalle de prestamo</h4>
        <hr>
        @php
          $pagosSaldo = \App\Pago::where('prestamo_id',$credito->id)->sum('monto');
          $pagosMora = \App\Pago::where('prestamo_id',$credito->id)->sum('mora');
          $totalPagosFicha=\App\Ficha_pago::where('prestamo_id',$credito->id)->whereNotNull('pago_id')->get();
          $sumaTotalPagos=0;
          foreach ($totalPagosFicha as $pagosList) {
            $listadoPagos[]=\App\Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
            // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
          }
          if(isset($listadoPagos))
          {
            foreach ($listadoPagos as $sumaPagos) {
              $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
            }
          }
          else {
            $sumaTotalPagos=0;
          }
          // $totalRealPago=\App\Ficha_pago::where('')
           $saldoTotal=$sumaTotalPagos-$pagosMora;
           // dd($sumaTotalPagos);
        @endphp
        {{-- Informacion de datos del prestamo --}}
        <div class="col-sm-12 table-responsive panel">
          <table class="table table-striped">
            <tbody>
              <tr>
                <td><label>Monto</label></td>
                <td>Q.{{$credito->monto}}</td>
              </tr>
              {{-- <tr>
                <td><label>Capital</label></td>
                <td>Q.{{$credito->capital_activo}}</td>
              </tr>
              <tr>
                <td><label>Capital cobrado</label></td>
                <td>Q.{{$credito->capital_recuperado}}</td>
              </tr> --}}
              @php
              $abonadoFinal=$saldoTotal;
              // dd($saldoFinal);
                if($abonadoFinal>$credito->monto)
                {
                  $esteAbonado=$credito->monto;
                }
                else {
                  $esteAbonado=$saldoTotal;
                }
              @endphp
              <tr>
                <td><label>Abonado</label></td>
                {{-- <td>Q.{{$credito->interes + $credito->capital_recuperado}}</td> --}}
                <td class="text-danger">Q.{{$esteAbonado}}</td>
              </tr>
              <tr>

                <td><label>Saldo</label></td>
                @php
                $saldoFinal=$credito->monto - $saldoTotal;
                // dd($saldoFinal);
                  if($saldoFinal<0)
                  {
                    $esteSaldo=0;
                  }
                  else {
                    $esteSaldo=$saldoFinal;
                  }
                @endphp
                <td class="text-danger">Q.{{$esteSaldo}}</td>
                {{-- <td class="text-danger">Q.{{round($credito->monto - ($credito->interes + $credito->capital_recuperado), 2)}}</td> --}}
              </tr>
              <tr>
                <td><label>Total de Mora</label></td>
                <td>Q.{{$credito->mora}}</td>
              </tr>
              <tr>
                <td><label>Mora recuperada</label></td>
                <td>Q.{{$credito->mora_recuperada}}</td>
              </tr>
              <tr>
                <td><label>Mora pendiente</label></td>
                <td class="text-danger">Q.{{$credito->mora - $credito->mora_recuperada}}</td>
              </tr>

        {{-- Informacion general del prestamo--}}

              <tr>
                <td><label>Fecha de creacion:</label></td>
                <td>{{\Carbon\Carbon::parse($credito->created_at)->format('Y-m-d')}}</td>
              </tr>
              <tr>
                <td><label>Fecha de inicio:</label></td>
                <td>{{$credito->fecha_inicio ? $credito->fecha_inicio : "Pendiente"}}</td>
              </tr>
              <tr>
                <td><label>Fecha fin</label></td>
                <td>{{$credito->fecha_fin ? $credito->fecha_fin : "Pendiente"}}</td>
              </tr>
              <tr>
                <td><label>Fecha 1er. Pago:</label></td>
                <td>{{$credito->ficha_pago->first() ? $credito->ficha_pago->first()->fecha : "Pendiente" }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="col-lg-6">
      <h4>Detalle de cliente</h4>
      <hr>

      {{-- Informacion de datos del prestamo --}}
      <div class="col-sm-12 table-responsive panel">
        <table class="table table-striped">
          <tbody>
            <tr>
              <td><label>Nombre</label></td>
              <td>{{$credito->cliente->persona->nombre .' '. $credito->cliente->persona->apellido}}</td>
            </tr>
            <tr>
              <td><label>Codigo</label></td>
              <td>Cli-{{$credito->cliente->id}}</td>
            </tr>
            <tr>
              @if(isset($credito->cliente->celular1))
              <td><label>Celular</label></td>
              <td>{{$credito->cliente->persona->celular1}}</td>
            @else
              <td><label>Telefono</label></td>
              <td>{{$credito->cliente->persona->telefono}}</td>
              @endif
            </tr>
            <tr>
              <td><label>DPI</label></td>
              <td>{{$credito->cliente->persona->dpi}}</td>
            </tr>
            <tr>
              <td><label>Direccion</label></td>
              <td>{{$credito->cliente->persona->domicilio}}</td>
              {{-- <td>Q.{{$credito->mora_recuperada}}</td> --}}
            </tr>
            <tr>
              <td><label>Cliente desde:</label></td>
              <td>{{ \Carbon\Carbon::parse($credito->cliente->fecha_ingreso)->formatLocalized('%A') ." " . \Carbon\Carbon::parse($credito->cliente->fecha_ingreso)->format('d-m-Y')}}</td>

            </tr>
            <tr>
              <td><label>Fecha de Nacimiento:</label></td>
              <td>{{\Carbon\Carbon::parse($credito->cliente->persona->fecha_nacimiento)->format('d-m-Y')}}</td>
            </tr>
            <tr>
              <td><label>Detalles cliente</label></td>
              <td style="text-align:center">
              <a href="{{route('clientes.show', $credito->cliente->id)}}" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-eye"> Ver</i></a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
      </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
    </div>
</div>
