<div class="panel-body">
  <div class="ibox ">
    <div class="ibox-title">
      <h5>Créditos</h5>

    </div>
    <div class="ibox-content">

      <div class="table-responsive">
        <table id="tabla-creditos" class="table table-striped table-bordered table-hover dataTables-example text-center">
          <thead>
            <tr>
              <th>Código</th>
              <th>Monto</th>
              <th>Estado</th>
              <th>Plan</th>
              <th>Clasificación</th>
              <th>Tipo crédito</th>
              <th>Cliente</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
          <tfoot>
            <tr>
              <th>Código</th>
              <th>Monto</th>
              <th>Estado</th>
              <th>Plan</th>
              <th>Clasificación</th>
              <th>Tipo crédito</th>
              <th>Cliente</th>
              <th>Acciones</th>
            </tr>
          </tfoot>
        </table>
      </div>

    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#tabla-creditos').DataTable({
    order: [[ 0, "desc" ]],
    language: {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    paging: true,
    info: false,
    dom : 'tip',
    processing: true,
    serverSide: true,
    ajax: {
      "url": '{{route('home.apiCreditos')}}'
    },
    pageLength: 10,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
      {extend: 'copy'},
      {extend: 'csv'},
      {extend: 'excel', title: 'Creditos'},
      {extend: 'pdf', title: 'Creditos'},
      {extend: 'print',
         customize: function (win)
         {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');

            $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
        }
      }
    ],
    columns: [
        {data: 'Codigo', name: 'Codigo'},
        {data: 'Monto', name: 'Monto'},
        {data: 'Estado', name: 'Estado'},
        {data: 'Plan', name: 'Plan'},
        {data: 'Clasificacion', name: 'Clasificacion'},
        {data: 'Tipo', name: 'Tipo'},
        {data: 'Cliente', name: 'Cliente'},
        {data: 'Acciones', name: 'Acciones'},
    ]
  });
});
</script>
