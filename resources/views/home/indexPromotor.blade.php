@extends('layouts.app')

@section('title', 'Inicio')

@section('content')

  <div id="espacio-modal">
    @include('Creditos.pago')
    @include('Pagos.pagoVencido')
  </div>
  @include('Creditos.modalTemp')
    <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>{{Auth::user()->persona->nombre}} {{Auth::user()->persona->apellido}}</h5>
                </div>
                <div class="ibox-content">
                  <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#creditos_hoy">Pagos de hoy</a></li>
                    <li><a data-toggle="tab" href="#pagos_pendientes">Pagos pendientes</a></li>
                    <li><a data-toggle="tab" href="#all_creditos">Prestamos vencidos mora</a></li>
                  </ul>
                  <div class="tab-content">
                    <div id="creditos_hoy" class="tab-pane fade in active">
                      <div class="input-group col-md-12">
                        <input type="text" placeholder="Busqueda Ejemplo: DPI: 22086815### Nombre: Nombre1 Prestamo: Cre-1" class="input form-control" id="busqueda-rutas">
                      </div>
                      <div class="table-responsive">
                        <table class='table table-striped table-hover' id="tabla-rutas">
                          <thead>
                            <tr>
                              <th>DPI</th>
                              <th>Cliente</th>
                              <th>Ruta</th>
                              <th>Hora</th>
                              <th>Cod. prestamo</th>
                              <th>No. cuota</th>
                              <th class="sum">Cuota</th>
                              <th>Estado</th>
                              <th>Acciones</th>
                            </tr>
                          </thead>
                          <tbody>
                            @if($fichas_pago_hoy->isEmpty())
                              <h3> No hay pagos el dia de hoy<h3>
                            @else
                              @foreach ($fichas_pago_hoy as $ficha_pago)
                                <tr>
                                  <td>{{$ficha_pago->prestamo->cliente->persona->dpi}}</td>
                                  <td><a class="client-link" href="{{route('clientes.show',$ficha_pago->prestamo->cliente->id)}}">{{$ficha_pago->prestamo->cliente->persona->nombre ." " .
                                      $ficha_pago->prestamo->cliente->persona->apellido}}</a></td>
                                  <td><a class="client-link" href="{{route('rutas.show',$ficha_pago->prestamo->ruta->first()->hoja_ruta->id)}}">
                                    {{$ficha_pago->prestamo->ruta->first()->hoja_ruta->nombre}}</a></td>
                                  <td>{{$ficha_pago->prestamo->ruta->first()->hora}}</td>
                                  <td><a class="client-link" href="{{route('creditos.show', $ficha_pago->prestamo->id)}}">Cre-{{$ficha_pago->prestamo->id}}</a></td>
                                  <td>{{$ficha_pago->no_dia}}</td>
                                  <td>{{$ficha_pago->cuota}}</td>
                                  @switch($ficha_pago->estado_p)
                                    @case (0)
                                      <td><a><span class='badge badge-warning pull-center'>Pendiente</span></a></td>
                                      @break
                                    @case (1)
                                      <td><a><span class="badge badge-primary pull-center">Pagado</span></a></td>
                                      @break
                                    @case (2)
                                      <td><a><span class="badge badge-danger pull-center">No Pagado</span></a></td>
                                      @break
                                    @case (3)
                                      <td><a><span class="badge badge-info pull-center">Pago parcial</span></a></td>
                                      @break
                                    @case (4)
                                      <td><a><span class="badge badge-success pull-center">Pago adelantado</span></a></td>
                                     @break
                                    @case (5)
                                       <td><a><span class="badge badge-success pull-center">Parcial adelantado</span></a></td>
                                      @break;
                                  @endswitch
                                  <td>
                                    @if($ficha_pago->estado_p==0 || $ficha_pago->estado_p==5)
                                      <a id="pagar-prestamo" value="{{$ficha_pago->prestamo->id}}" class="btn btn-outline btn-primary btn-sm"><i class="fa"><strong>Q</strong></i></a>
                                    @else
                                      <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa">Q</i></a>
                                    @endif
                                    @php
                                      $ficha_coment = App\Ficha_pago::where('prestamo_id', $ficha_pago->prestamo->id)->where('no_dia',$ficha_pago->no_dia)->first();
                                      $advertencia = App\Ficha_pago::where('prestamo_id', $ficha_pago->prestamo->id)->where('tipo','=',1)->where('fecha','<',\Carbon\Carbon::now())->where('estado_p', '=',0)->count();

                                    @endphp
                                    @if($ficha_coment->comentarios->isNotEmpty())
                                      <a onclick="comentario('{{$ficha_coment->comentarios->first()->descripcion}}')" value="{{$ficha_pago->prestamo->id}}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-comment-o"></i></a>
                                    @endif
                                    @if($advertencia > 0)
                                      <a onclick="swal('','Existen pagos pendientes fuera de fecha','warning')"  class="btn btn-danger btn-sm"><i class="fa fa-exclamation"></i></a>
                                    @endif
                                  </td>
                                </tr>
                              @endforeach
                            @endif
                          </tbody>
                          <tfoot>
                            <tr>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th>Total:</th>
                              <th></th>
                              <th></th>
                              <th></th>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                    <div id="pagos_pendientes" class="tab-pane fade">
                      <div class="input-group col-md-12">
                        <input type="text" placeholder="Busqueda Ejemplo: DPI: 22086815### Nombre: Nombre1 Prestamo: Cre-1" class="input form-control" id="busqueda-pendientes">
                      </div>
                      <div class="table-responsive">
                        <table class='table table-striped table-hover' id="tabla-pendientes">
                          <thead>
                            <tr>
                              <th>DPI</th>
                              <th>Cliente</th>
                              <th>Ruta</th>
                              <th>Hora</th>
                              <th>Cod. prestamo</th>
                              <th>No. cuota</th>
                              <th class="sum">Cuota</th>
                              <th>Estado</th>
                              <th>Acciones</th>
                            </tr>
                          </thead>
                          <tbody>
                            @if($ficha_pagos_pendientes->isEmpty())
                              <h3> No hay pagos pendientes<h3>
                              @else
                                @foreach ($ficha_pagos_pendientes as $ficha_pago)
                                  <tr>
                                    <td>{{$ficha_pago->prestamo->cliente->persona->dpi}}</td>
                                    <td><a class="client-link" href="{{route('clientes.show',$ficha_pago->prestamo->cliente->id)}}">{{$ficha_pago->prestamo->cliente->persona->nombre ." " .
                                      $ficha_pago->prestamo->cliente->persona->apellido}}</a></td>
                                      <td><a class="client-link" href="{{route('rutas.show',$ficha_pago->prestamo->ruta->first()->hoja_ruta->id)}}">
                                        {{$ficha_pago->prestamo->ruta->first()->hoja_ruta->nombre}}</a></td>
                                        <td>{{$ficha_pago->prestamo->ruta->first()->hora}}</td>
                                        <td><a class="client-link" href="{{route('creditos.show', $ficha_pago->prestamo->id)}}">Cre-{{$ficha_pago->prestamo->id}}</a></td>
                                        <td>{{$ficha_pago->no_dia}}</td>
                                        <td>{{$ficha_pago->cuota}}</td>
                                        @switch($ficha_pago->estado_p)
                                          @case (0)
                                          <td><a><span class='badge badge-warning pull-center'>Pendiente</span></a></td>
                                          @break
                                          @case (1)
                                          <td><a><span class="badge badge-primary pull-center">Pagado</span></a></td>
                                          @break
                                          @case (2)
                                          @if($ficha_pago->cont == 1)
                                            <td><a><span class="badge badge-danger pull-center">No Pagado</span></a></td>
                                          @else
                                            <td><a><span class="badge badge pull-center">Perdon</span></a></td>
                                          @endif
                                          @break
                                          @case (3)
                                          <td><a><span class="badge badge-info pull-center">Pago parcial</span></a></td>
                                          @break
                                          @case (4)
                                          <td><a><span class="badge badge-success pull-center">Pago adelantado</span></a></td>
                                          @break
                                          @case (5)
                                          <td><a><span class="badge badge-success pull-center">Parcial adelantado</span></a></td>
                                          @break;
                                        @endswitch
                                        <td>
                                          @if($ficha_pago->estado_p==0 || $ficha_pago->estado_p==5)
                                            <a id="pagar-prestamo" value="{{$ficha_pago->prestamo->id}}" class="btn btn-outline btn-primary btn-sm"><i class="fa"><strong>Q</strong></i></a>
                                          @else
                                            <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa">Q</i></a>
                                          @endif
                                          @php
                                            $ficha_coment = App\Ficha_pago::where('prestamo_id', $ficha_pago->prestamo->id)->where('no_dia',$ficha_pago->no_dia)->first();
                                          @endphp
                                          @if($ficha_coment->comentarios->isNotEmpty())
                                            <a onclick="comentario('{{$ficha_coment->comentarios->first()->descripcion}}')" value="{{$ficha_pago->prestamo->id}}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-comment-o"></i></a>
                                          @endif
                                        </td>
                                      </tr>
                                    @endforeach
                                  @endif
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Total:</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                  </tr>
                                </tfoot>
                              </table>
                            </div>
                          </div>
                    <div id="all_creditos" class="tab-pane fade">
                      <div class="input-group col-md-12">
                        <input type="text" placeholder="Buscar credito " class="input form-control" id="busqueda-vencidos">
                      </div>
                      <div class="table-responsive">
                        <table class='table table-striped table-hover' id="tabla-vencidos">
                          <thead>
                            <tr>
                              <th>DPI</th>
                              <th>Cliente</th>
                              <th>Prestamo</th>
                              <th>Estado</th>
                              <th>Saldo</th>
                              <th>Accion</th>
                            </tr>
                          </thead>
                          <tbody>
                            @if($prestamos_vencidos->isNotEmpty())
                              @foreach ($prestamos_vencidos as $prestamo )
                              @php
                                $prestamo_real = App\Prestamo::find($prestamo->prestamo_id);
                              @endphp
                                <tr>
                                  <td>{{$prestamo_real->cliente->persona->dpi}}</td>
                                  <td>{{$prestamo_real->cliente->persona->nombre . " " . $prestamo_real->cliente->persona->apellido}}</td>
                                  <td>Cre-{{$prestamo_real->id}}</td>
                                  {!!$prestamo_real->obtenerEstado()!!}

                                  @php
                                  $pagosSaldo=0;
                                  $pagosMora=0;
                                  $totalPagosFicha=[];
                                  $listadoPagos=[];
                                  $sumaTotalPagos=0;
                                  $saldoTotal=0;
                                  $pagosSaldo = \App\Pago::where('prestamo_id',$prestamo_real->id)->sum('monto');
                                  $pagosMora = \App\Pago::where('prestamo_id',$prestamo_real->id)->sum('mora');
                                  $totalPagosFicha=\App\Ficha_pago::where('prestamo_id',$prestamo_real->id)->whereNotNull('pago_id')->get();
                                  $sumaTotalPagos=0;
                                  foreach ($totalPagosFicha as $pagosList) {
                                    $listadoPagos[]=\App\Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
                                    // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
                                  }
                                  if(isset($listadoPagos))
                                  {
                                    foreach ($listadoPagos as $sumaPagos) {
                                      $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
                                    }
                                  }
                                  else {
                                    $sumaTotalPagos=0;
                                  }
                                  // $totalRealPago=\App\Ficha_pago::where('')
                                   $saldoTotal=$sumaTotalPagos-$pagosMora;

                                   $capitalVencido=round(($prestamo_real->monto - $saldoTotal)+($prestamo_real->mora - $prestamo_real->mora_recuperada), 2);
                                   if($capitalVencido<0)
                                   {
                                     $capitalFinal=0;
                                   }
                                   else {
                                     $capitalFinal=$capitalVencido;
                                   }
                                  @endphp
                                  {{-- dd($prestamo_real) --}}
                                  <td>{{$capitalFinal}}</td>
                                  <td><a id="pago-vencido" class="btn btn-primary btn-sm pagar-btn" type="button" value="{{$prestamo_real->id}}"
                                        data-toggle="modal" data-target=".pago-vencido"><i class=""></i>Pagar</a></td>
                              @endforeach
                            @else
                              <h3>No hay prestamos vencidos</h3>
                            @endif
                          </tbody>
                          <tfoot>
                            <tr>
                              <th></th>
                              <th> </th>
                              <th> </th>
                              <th> </th>
                              <th></th>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection


@section('scripts')

  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>


<script type="text/javascript">
$(document).ready(function(){
  var oTable = $('#tabla-rutas').DataTable({
    "language": {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    "paging":   true,
    "info":     false,
    "ordering": false,
    'dom' : 'tip',
  });

  $('#busqueda-rutas').keyup(function(){
        oTable.search($(this).val()).draw();
  });
  var oTable2 = $('#tabla-pendientes').DataTable({
    "language": {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    "paging":   true,
    "info":     false,
    "ordering": false,
    'dom' : 'tip',
  });

  $('#busqueda-pendientes').keyup(function(){
        oTable2.search($(this).val()).draw();
  });
  var oTable3 = $('#tabla-vencidos').DataTable({
    "language": {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    "paging":   true,
    "info":     false,
    "ordering": false,
    'dom' : 'tip',
  });

  $('#busqueda-vencidos').keyup(function(){
        oTable3.search($(this).val()).draw();
  });
});

$(".pagar-btn").click(function(){
  var valor = $(this).attr('value');
  obtenerDatos(valor);
});

$("tbody").on('click', '#pagar-prestamo', function(){
  var valor = $(this).attr("value");
  var url = '{{ route("pagos.info", ":id") }}';
  url = url.replace(':id', valor);
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#nom_cliente').val(response['cliente']);
        $('#cliente').val(response['prestamo']);
        $('#no_cuota').val(response['no_cuota']);
        $('#total').val(response['cuota']);
        $('#prestamo').val(response['prestamo']);
        $('#modal-pago').modal('show');
        if(response['dia_perdon'] == '1'){
          $('#btn-perdon').hide();
        } else{
          $('#btn-perdon').show();
        }

      }
  });
});

$("tbody").on('click', '#pago-vencido', function(){
var valor = $(this).attr("value");
var url = '{{ route("info.credito", ":id") }}';
console.log(valor);
url = url.replace(':id', valor);
$.ajax({
type: "GET",
url: url,
success: function( response ) {
  console.log(response);
  $('#nom_cliente_v').val(response['cliente']);
  $('#prestamo_v').val(response['prestamo']);
  $('#no_prestamo_v').val(response['no_prestamo']);
  $('#capital_v').val(response['capital_vencido']);
  $('#mora_v').val(response['mora']);
  $('#interes_v').val(response['interes']);
  $('#total_v').val(response['cuota']);

}
});
});
    function comentario(texto){
      swal ("Comentario", texto);
    }


</script>


@endsection
